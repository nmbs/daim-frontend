// vue.config.js
module.exports = {
  publicPath: './',
  productionSourceMap: process.env.NODE_ENV !== 'production',
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import '@/styles/variables/vars.scss';
          @import '@/styles/variables/fonts.scss';
          @import '@/styles/variables/colors.scss';
          @import '@/styles/variables/breakpoints.scss';
          @import "@/styles/grid.scss";
          @import '@/styles/mixins.scss';
        `
      }
    }
  },
  // Chain with webpack custom config to remove data-cy attributes in production env
  chainWebpack: (config) => {
    config.module
      .rule('vue') // Apply following config on vue file
      .use('vue-loader') // Use vue-loader to process those files
      .tap((options) => {
        options.compilerOptions.modules = [
          {
            preTransformNode(astEl) {
              if (process.env.NODE_ENV === 'production') {
                const { attrsMap, attrsList } = astEl;
                if (attrsMap['data-cy']) {
                  // Remove data-cy attribute from attrsMap and attrsList props of AST element
                  delete attrsMap['data-cy'];
                  const index = attrsList.findIndex((x) => x.name === 'data-cy');
                  attrsList.splice(index, 1);
                }
              }
              return astEl;
            }
          }
        ];
        return options;
      });
  }
};
