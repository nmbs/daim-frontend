import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';
import { StepState } from '@/models/StepState';
import { Action, Getter, Mutation, State } from 'vuex-class';
import { VisibleMutationPayload } from '@/models/subscription/VisibleMutationPayload';
import { RootState, SimulatorState } from '@/store/types';
import { Colors } from '@/models/Colors';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';
import { RouteStates } from '@/models/subscription/RouteState';
import { SubscriptionPayload, T1Payload } from '@/models/subscription/SubscriptionPayload';
import { BusinessRule } from '@/services/BusinessRule';
import { RoutePathId } from '@/models/RoutesPath';
import { easeInOutQuad } from '@/utils/animations';
import Button from '@/components/graphic/Button.vue';
import LayoutTwoBloc from '@/components/template/LayoutTwoBloc.vue';
import BorrowerContainer from '@/components/subscription/container-element/BorrowerContainer.vue';
import Accordion from '@/components/subscription/Accordion.vue';
import Popin from '@/components/graphic/Popin.vue';
import RefuseInsuranceWarning from '@/components/insurance/RefuseInsuranceWarning.vue';

@Component({
  components: {
    'app-button': Button,
    'app-layout-two-bloc': LayoutTwoBloc,
    'app-borrower-container': BorrowerContainer,
    'app-accordion': Accordion,
    'app-popin': Popin,
    'app-refuse-insurance-warning': RefuseInsuranceWarning
  }
})
export class AbstractInsuranceStep extends Vue {
  @Prop() protected stepState!: StepState;

  @Mutation protected updateVisibleRoute!: (payload: VisibleMutationPayload) => void;

  @State((state: RootState) => state.apiConfig.colors) protected colors!: Colors;
  @State((state: RootState) => state.simulator.selectedProposal)
  protected selectedProposal!: SimulationFinancialProposal;
  @State((state: RootState) => state.subscription.routeStates) protected routeStates!: RouteStates;
  @State((state: RootState) => state.client.isMobile) protected isMobile!: boolean;
  @State((state: RootState) => state.simulator) protected simulatorState!: SimulatorState;

  @Getter protected isCreditForTwo!: boolean;
  @Getter protected getT1IsActive!: (pathId: string) => boolean;
  @Getter protected getValidationStatus!: (formName: string, isPrincipal?: boolean) => boolean;
  @Getter protected screenOption: (key: string) => any;

  @Action('setT1IsActive') protected setT1IsActive!: (payload: T1Payload) => void;
  @Action('setSubscriptionData') protected setSubscriptionData!: (payload: SubscriptionPayload) => void;
  @Action('updateProposals') protected updateProposals!: (proposals: SimulationFinancialProposal[]) => void;

  protected opened: boolean = false;

  protected animation: boolean = false;
  protected slideLeft: boolean = false;
  protected slideRight: boolean = false;

  protected get partnerName(): string {
    return (
      this.stepState &&
      this.stepState.screenOptions &&
      this.stepState.screenOptions.partnerName &&
      this.stepState.screenOptions.partnerName.value
    );
  }

  protected get documentUrl(): string {
    return (
      this.stepState &&
      this.stepState.screenOptions &&
      this.stepState.screenOptions.documentUrl &&
      this.stepState.screenOptions.documentUrl.value
    );
  }

  /**
   * If the go back to summary step button must be displayed to the user.
   */
  protected get showGoBackToSummaryStepBtn(): boolean {
    return BusinessRule.showGoToSummaryStepBtn(
      this.isMobile,
      this.stepState,
      this.stepState.getNext(),
      this.routeStates
    );
  }

  /**
   * Force the navigation to the summary step with a slide left animation.
   */
  protected navigateToSummaryStep(): void {
    this.navigateToWithSlideAnimation(RoutePathId.SUMMARY, true);
  }

  /**
   * Navigate to another step with a slide right or left animation between the current step and the next/previous step.
   *
   * @param nextRouteName the next/previous route name
   * @param slideLeftDirection the slide animation direction: if true, slide on left else slide on right
   * @param slideDelay the slide delay (by default it is 300ms)
   */
  protected navigateToWithSlideAnimation(
    routeName: RoutePathId | string,
    slideLeftDirection = false,
    slideDelay: number = 300
  ): void {
    this.animation = true;
    this.goTo(0, () => {
      if (slideLeftDirection) {
        this.slideLeft = true;
      } else {
        this.slideRight = true;
      }
      setTimeout(() => {
        this.$router.push({ name: routeName });
      }, slideDelay);
    });
  }

  protected goTo(to: any, callback: any = null) {
    const form = document.querySelector('.subscriptionPageContainer .wrapper .content') as HTMLElement;
    const start = window.scrollY > form.scrollTop ? window.scrollY : form.scrollTop;

    const change = to - start;
    const increment = 20;
    let currentTime = 0;
    const animateScroll = () => {
      const duration = 300;
      const val = easeInOutQuad(currentTime, start, change, duration);
      currentTime += increment;

      if (window.scrollTo) {
        window.scrollTo(0, val);
      }

      if (form && form.scrollTop) {
        form.scrollTop = val;
      }

      if (currentTime < duration) {
        setTimeout(animateScroll, increment);
      } else if (callback) {
        callback();
      }
    };
    animateScroll();
  }

  protected goPrev() {
    const previousState = this.stepState.getPrevious();
    if (previousState !== null) {
      this.navigateToWithSlideAnimation(previousState.pathId, false);
    }
  }
}
