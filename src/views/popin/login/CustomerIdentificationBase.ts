import { Component, Inject, Vue } from 'vue-property-decorator';
import { Mutation, State } from 'vuex-class';
import { IdentificationService } from '@/services/IdentificationService';
import { LoginFormType } from '@/models/subscription/authentication/Authentication';
import { RootState } from '@/store/types';
import { Colors } from '@/models/Colors';
import { ScrollOptions } from 'vue-scrollto';

@Component({})
export default class CustomerIdentificationBase extends Vue {
  @State((state: RootState) => state.apiConfig.colors) public colors!: Colors;

  @Inject() public identificationService: IdentificationService;

  @Mutation public clearAuthenIdentification!: () => void;

  public LoginFormType = LoginFormType;
  public scrollContainer: HTMLDivElement;

  public mounted() {
    // Scroll on the top (customerIdentification = HTML div element that contains the scroll bar)
    this.scrollContainer = this.$parent.$slots.default[0].context.$refs.customerIdentificationWrapper as HTMLDivElement;
    this.scrollContainer.scrollTop = 0;
  }

  public navigateTo(newLoginForm: LoginFormType): void {
    if (newLoginForm) {
      if (newLoginForm === LoginFormType.SigninForm) {
        this.clearAuthenIdentification();
      }
      this.$emit('navigateTo', newLoginForm);
    }
  }

  public scrollToAlertError(): void {
    this.$nextTick(() => {
      const element: HTMLDivElement = this.$refs.customerIdentificationError as HTMLDivElement;
      const options: ScrollOptions = {
        container: this.scrollContainer,
        offset: -10
      };
      this.$scrollTo(element, 300, options);
    });
  }
}
