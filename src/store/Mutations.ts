export const SAVE_NOMENCLATURE = 'saveNomenclatureData';
export const SAVE_APP_CONFIGURATION = 'saveAppConfiguration';
export const SAVE_ROOT_STEP_STATES = 'saveRootStepsStates';
export const SAVE_CONFIGURATION_ROUTES = 'saveConfigurationRoutes';
export const SAVE_ROUTE_LIST = 'saveRoutesList';
export const MARK_FORM_AS_VALID = 'markFormAsValid';
export const MARK_FORM_AS_OPEN = 'markFormAsOpen';
export const MARK_FORM_AS_INVALID = 'markFormAsInvalid';
export const MARK_ONE_FORM_AS_INVALID = 'markOneFormAsInvalid';
export const ACTIVATE_ROUTE = 'activateRoute';
export const UPDATE_IS_MOBILE = 'updateIsMobile';
export const SET_BLOC_ROUTE_TO_FORM_ROUTES = 'setBlocRouteToFormRoutes';

// Subscription module mutations
export const SAVE_PRINCIPAL = 'savePrincipal';
export const SAVE_SECONDARY = 'saveSecondary';
export const SET_CREDIT_FOR_TWO = 'setCreditForTwo';
export const SET_ROUTE_STATES = 'setRouteStates';
export const SET_INSURANCE_FORM_VERSION = 'setInsuranceFormVersion';
