import { SubscriptionData } from '@/store/types';
import { ICustomerContext } from '@/models/BusinessDataTransfer';
import { FormStates } from '@/models/subscription/FormState';
import { RouteStates } from '@/models/subscription/RouteState';
import { GetCities, GetCountries } from '@/services/AutocompleteAPI';
import { SelectObject } from '@/models/selectObject';
import { AuthenRequest, AuthenResponse } from '@/models/subscription/authentication/Authentication';
import {
  ICustomerIdentification,
  ICustomerIdentificationChoice,
  ICustomerIdentificationForm
} from '@/models/subscription/authentication/ICustomerIdentification';

export async function mapContextToState(context: ICustomerContext): Promise<SubscriptionData> {
  const result = getInitialState();
  if (!context) {
    return result;
  }

  let birthCountry = context.birthCountryCode ? (await GetCountries(context.birthCountryCode)).data : null;
  let addressCity = context.zipCode ? (await GetCities(String(context.zipCode))).data : null;
  let nationality = context.citizenshipCode ? (await GetCountries(String(context.citizenshipCode))).data : null;
  result.name.gender = context.civilityCode == 1 ? 1 : context.civilityCode == 2 ? 0 : null; // tslint:disable-line:triple-equals

  if (birthCountry !== null) {
    birthCountry = birthCountry.find((e: SelectObject) => e.code === context.birthCountryCode);
  }

  if (addressCity !== null) {
    addressCity = addressCity.find((e: SelectObject) => e.code === context.zipCode);
  }

  if (nationality !== null) {
    nationality = nationality.find((e: SelectObject) => e.code === context.citizenshipCode);
  }

  result.name.firstName = context.firstName || null;
  result.name.lastName = context.lastName || null;
  result.name.birthName = context.birthName || null;
  result.civility.birthDate = context.birthDate ? convertDate(context.birthDate) : null;
  result.civility.birthCountry = birthCountry;
  result.civility.nationality = nationality;
  result.adresse.adresseLabel = context.street || null;
  result.adresse.complementAdresse = context.additionalStreet || null;
  result.adresse.adresseCity = addressCity;
  result.contact.mobilePhone = context.mobileNumber || null;
  result.contact.homePhone = context.phoneNumber || null;
  result.contact.email = context.emailAddress || null;
  return result;
}

export function getInitialFormStates(): FormStates {
  return {
    loan: {
      t1IsActive: true
    },
    name: {
      t1IsActive: true
    },
    family: {
      t1IsActive: true
    },
    contact: {
      t1IsActive: true
    },
    civility: {
      t1IsActive: true
    },
    incomes: {
      t1IsActive: true
    },
    charges: {
      t1IsActive: true
    },
    employment: {
      t1IsActive: true
    },
    housing: {
      t1IsActive: true
    },
    insurance: {
      version: 'v1',
      t1IsActive: true,
      t1IsEligible: true,
      t1IsEligibleRecommended: false,
      t1IsEligibleAlternative: false,
      t2IsEligible: true,
      t2IsEligibleRecommended: false,
      t2IsEligibleAlternative: false
    }
  };
}

export function getInitialRouteStates(): RouteStates {
  return {
    informationBloc: {
      valid: false,
      disabled: false,
      active: true,
      visible: true,
      isBlock: true,
      hasChildren: true
    },
    loan: {
      valid: false,
      disabled: false,
      active: false,
      visible: true,
      hasChildren: false
    },
    name: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      hasChildren: false
    },

    contact: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      hasChildren: false
    },
    situationBloc: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      isBlock: true,
      hasChildren: true
    },
    civility: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      hasChildren: false
    },
    family: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      hasChildren: false
    },
    housing: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      hasChildren: false
    },
    incomesBloc: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      isBlock: true,
      hasChildren: true
    },
    employment: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      hasChildren: false
    },
    incomes: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      hasChildren: false
    },
    charges: {
      valid: false,
      disabled: true,
      active: false,
      visible: true,
      hasChildren: false
    },
    insurance: {
      valid: false,
      disabled: true,
      active: false,
      visible: false,
      isBlock: true,
      hasChildren: false
    },
    card: {
      disabled: true,
      active: false,
      visible: true,
      isBlock: true,
      hasChildren: false
    },
    summary: {
      disabled: true,
      active: false,
      visible: true,
      isBlock: true,
      hasChildren: false
    },
    loading: {
      disabled: true,
      active: false,
      visible: false,
      isBlock: true,
      hasChildren: false
    },
    result: {
      disabled: true,
      active: false,
      visible: true,
      isBlock: true,
      hasChildren: false
    },
    resultSofinco: {
      disabled: true,
      active: false,
      visible: true,
      isBlock: true,
      hasChildren: false
    }
  };
}

export function getInitialState(): SubscriptionData {
  return {
    name: {
      gender: null,
      firstName: null,
      lastName: null,
      birthName: null,
      maritalStatus: null,
      hasSpouse: false
    },
    family: {
      childrenNbr: 0
    },
    contact: {
      email: null,
      mobilePhone: null,
      homePhone: null,
      lenderOffer: null,
      partnerOffer: null,
      contactOffer: false
    },
    civility: {
      birthDate: null,
      birthCity: null,
      birthCountry: null,
      nationality: null
    },
    incomes: {
      netIncome: null,
      salaryMonths: 12,
      moreIncomes: false,
      housingBenefits: 0,
      familyBenefits: 0,
      otherIncomes: 0
    },
    charges: {
      rental: null,
      refundHouseCredit: null,
      hasAlimony: false,
      alimony: null,
      moreCredits: false,
      carCredit: 0,
      revolving: 0,
      otherCredit: 0
    },
    employment: {
      activitySector: null,
      contract: null,
      profession: null,
      professionalStatus: null,
      sinceWhen: null
    },
    adresse: {
      adresseLabel: null,
      complementAdresse: null,
      adresseCity: null,
      housingType: null,
      sinceWhen: null
    },
    insurance: {
      acceptInsuranceOffer: null,
      confirmRefusal: false
    },
    insurancev2: {
      selectedInsurance: null,
      refuseInsurance: false
    },
    wantCard: null,
    cardCode: null
  };
}

export function getAuthenRequestInitialState(): AuthenRequest {
  return {
    id: null,
    birthDate: null,
    keypadId: null,
    passwordMask: null
  };
}

export function getAuthenResponseInitialState(): AuthenResponse {
  return {
    token: null,
    customerId: null,
    role: null
  };
}

export function getAuthIdentificationInitialState(): ICustomerIdentification {
  const identificationForm: ICustomerIdentificationForm = {
    lastName: null,
    firstName: null,
    birthDate: null,
    birthPostCode: null
  };
  const identificationFormChoice: ICustomerIdentificationChoice = {
    id: null,
    token: null,
    encodedMobileNumber: null,
    encodedEmailAddress: null,
    postCode: null,
    sendBy: null
  };
  return {
    form: identificationForm,
    choice: identificationFormChoice
  };
}

/**
 * Converts yyyy-mm-dd string to date
 * @param input
 */
export function convertDate(input: string): string | null {
  if (!input) {
    return null;
  }
  const split = input.split('-');
  return split[2] + '/' + split[1] + '/' + split[0];
}
