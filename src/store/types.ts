import { Name } from '@/models/subscription/Name';
import { AppConfiguration } from '@/models/Configuration';
import { Family } from '@/models/subscription/Family';
import { Civility } from '@/models/subscription/Civility';
import { Contact } from '@/models/subscription/Contact';
import { Colors } from '@/models/Colors';
import { Incomes } from '@/models/subscription/Incomes';
import { Charges } from '@/models/subscription/Charges';
import { Employment } from '@/models/subscription/Employment';
import { Adresse } from '@/models/subscription/Adresse';
import { Insurance } from '@/models/subscription/Insurance';
import { FormStates } from '@/models/subscription/FormState';
import { RouteStates } from '@/models/subscription/RouteState';
import { FolderResponse } from '@/models/subscription/FolderResponse';
import { BusinessDataTransfer } from '@/models/BusinessDataTransfer';
import { FilterCriterion } from '@/models/simulator/tools/filterCriterion';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';
import { SimulationContext } from '@/models/simulator/dto/simulationContext';
import { SeConfig } from '@/models/subscription/api/SeConfig';
import { AuthenticationState } from '@/models/subscription/authentication/Authentication';
import { Basket } from '@/models/basket';
import { InsuranceV2 } from '@/models/subscription/InsuranceV2';

export interface RootState {
  apiConfig: ApiConfigState;
  simulator: SimulatorState;
  subscription: SubscriptionState;
  routing: RoutingData;
  client: ClientState;
}

export interface ApiConfigState {
  partnerCode: string;
  offerCode?: string;
  businessProviderId?: string;
  equipmentCode?: string;
  scaleCode?: string;
  applicationId: string;
  appConfiguration: AppConfiguration;
  colors?: Colors;
  colorTheme?: any;
  simulateurLight: boolean;
  numFidelity: string;
}

export interface SimulatorState {
  requiredAmount: number | null;
  dueNumber: number | null;
  homeReturnUrl: string | null;
  proposals: SimulationFinancialProposal[] | null;
  selectedProposal: SimulationFinancialProposal | null;
  minInsuranceProposal: SimulationFinancialProposal | null;
  maxRateProposal: SimulationFinancialProposal | null;
  proposalFilter: FilterCriterion | null;
  timesUserSelectedOffer: number;
  uiActiveStates: {
    forceUserAmountInput: boolean;
    overlay: boolean;
    parametersZoom: boolean;
    preContractPopup: boolean;
    showLoanParamsPopup: boolean;
    showOfferSelectorPopup: boolean;
    showOfferSubmitPopup: boolean;
  };
  loading: boolean;
  simulationContext: SimulationContext;
  simulationStandardConditions?: SimulationFinancialProposal;
  offerPreSelection: SimulatorOfferPreSelectionState;
  recommendedProposal: SimulationFinancialProposal;
  alternativeProposal: SimulationFinancialProposal;
}

export interface SimulatorOfferPreSelectionState {
  suggestedDueNumber?: number;
  suggestedScaleCode?: string;
  // Indicate whether pre-selection has been performed
  // Needed in order to avoid pre-selecting an offer after user change simulation amount
  landingDone: boolean;
}

export interface SubscriptionData {
  name: Name;
  family: Family;
  contact: Contact;
  civility: Civility;
  incomes: Incomes;
  charges: Charges;
  employment: Employment;
  adresse: Adresse;
  insurance: Insurance;
  insurancev2: InsuranceV2;
  wantCard: boolean | null;
  cardCode: string | null;
}

export interface PreContractMail {
  hasBeenSent: boolean;
  email: string;
  offer: SimulationFinancialProposal | null;
}

export interface SubscriptionState {
  preContractMail: PreContractMail;
  principal: SubscriptionData;
  secondary: SubscriptionData;
  formStates: FormStates;
  routeStates: RouteStates;
  routesList: string[];
  response: FolderResponse | null;
  blocRouteToFormRoutes: Map<any, any> | null;
  context: BusinessDataTransfer | null;
  experianData: ExperianData;
  creditForTwo: boolean | null;
  seConfig: SeConfig | null;
  tosAccepted?: boolean | null;
  creditCardChosen?: boolean | null;
  creditCard?: string | null;
  cardChoiceAlert?: string | null;
  authentication: AuthenticationState;
  cardId?: string;
  keyring?: string;
  restoringCard?: boolean;
  basket?: Basket;
  funnelId?: string;
}

export interface RoutingData {
  [key: string]: any;
}

export interface ClientState {
  isMobile: boolean;
}

export interface ExperianData {
  cfuserPrefs: string | null;
  cfuserPrefs2: string | null;
}

export interface GlobalUiActiveState {
  contactModule: ContactModuleGlobalUiActiveState;
}

export interface ContactModuleGlobalUiActiveState {
  showMobileHelpButton: boolean; // Show/hide button to open mobile help menu
  showMobileHelpMenu: boolean; // State of mobile help menu (opened or closed)
  showMobileCallHelpButton: boolean; // Show/hide button to open call feature of contact module
}
