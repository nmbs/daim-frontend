import { isNullOrEmpty } from '@/utils/utils';
import { Gender, Name, MaritalStatusCode } from '@/models/subscription/Name';
import { Family } from '@/models/subscription/Family';
import { Contact } from '@/models/subscription/Contact';
import { Civility } from '@/models/subscription/Civility';
import { Incomes } from '@/models/subscription/Incomes';
import { Charges } from '@/models/subscription/Charges';
import { Employment } from '@/models/subscription/Employment';
import { Adresse } from '@/models/subscription/Adresse';
import { BusinessRule } from '@/services/BusinessRule';
import { Insurance } from '@/models/subscription/Insurance';
import { AuthenRequest } from '@/models/subscription/authentication/Authentication';
import DateUtils from '@/utils/dateUtils';
import { ICustomerIdentificationForm } from '@/models/subscription/authentication/ICustomerIdentification';
import { Card } from '@/models/card';
import { InsuranceV2 } from '@/models/subscription/InsuranceV2';

export function isNameFormValid(principalNameState: Name, secondaryNameState: Name, isPrincipal: boolean): boolean {
  const nameState = isPrincipal ? principalNameState : secondaryNameState;
  const isMarried =
    principalNameState.maritalStatus && principalNameState.maritalStatus.code === MaritalStatusCode.MARRIED;
  const isWidow = principalNameState.maritalStatus && principalNameState.maritalStatus.code === MaritalStatusCode.WIDOW;
  const birthNameValid =
    nameState.gender === Gender.MAN || !(isMarried || isWidow) || !isNullOrEmpty(nameState.birthName);

  return (
    !isNullOrEmpty(nameState.gender) &&
    !isNullOrEmpty(nameState.firstName) &&
    !isNullOrEmpty(nameState.lastName) &&
    birthNameValid &&
    ((isPrincipal && !isNullOrEmpty(nameState.maritalStatus) && !isNullOrEmpty(nameState.hasSpouse)) || !isPrincipal)
  );
}

export function isFamilyFormValid(familyState: Family): boolean {
  return !isNullOrEmpty(familyState.childrenNbr);
}

export function isContactFormValid(contact: Contact, isPrincipal: boolean): boolean {
  return (
    !isNullOrEmpty(contact.email) &&
    (!isNullOrEmpty(contact.mobilePhone) || !isNullOrEmpty(contact.homePhone)) &&
    (!isPrincipal || contact.lenderOffer !== null) &&
    !isNullOrEmpty(contact.contactOffer) &&
    (!isPrincipal || !isNullOrEmpty(contact.partnerOffer) || !BusinessRule.hasPartnerOptin())
  );
}

export function isCivilityFormValid(civilityState: Civility): boolean {
  return (
    !isNullOrEmpty(civilityState.birthDate) &&
    !isNullOrEmpty(civilityState.birthCountry) &&
    !isNullOrEmpty(civilityState.birthCity) &&
    !isNullOrEmpty(civilityState.nationality)
  );
}

export function isIncomesFormValid(incomes: Incomes): boolean {
  return (
    !isNullOrEmpty(incomes.netIncome) &&
    !isNullOrEmpty(incomes.salaryMonths) &&
    (!incomes.moreIncomes ||
      (!isNullOrEmpty(incomes.housingBenefits) &&
        !isNullOrEmpty(incomes.familyBenefits) &&
        !isNullOrEmpty(incomes.otherIncomes)))
  );
}

export function isChargesFormValid(charges: Charges, adresse: Adresse, isPrincipal = true): boolean {
  if (!isPrincipal) {
    return true;
  }
  const businessRule = new BusinessRule();
  const mandatory =
    (!charges.hasAlimony || !isNullOrEmpty(charges.alimony)) &&
    (charges.refundHouseCredit !== null || !businessRule.showRefundHouseCredit(adresse)) &&
    (charges.rental !== null || !businessRule.showRentCharges(adresse));

  const optional =
    !charges.moreCredits ||
    (!isNullOrEmpty(charges.carCredit) && !isNullOrEmpty(charges.revolving) && !isNullOrEmpty(charges.otherCredit));

  return mandatory && optional;
}

export function isEmploymentFormValid(employment: Employment): boolean {
  if (isNullOrEmpty(employment.professionalStatus)) {
    return false;
  }

  let workFieldsValidation;
  switch (employment.professionalStatus.code) {
    case '1_1':
    case '2_2':
      workFieldsValidation =
        employment.profession !== null && employment.contract !== null && employment.activitySector !== null;
      break;
    case '4_4':
      workFieldsValidation = employment.profession !== null && employment.activitySector !== null;
      break;
    case '7_7':
      workFieldsValidation = employment.profession !== null;
      break;
    case '6_6':
    default:
      workFieldsValidation = true;
  }
  return !isNullOrEmpty(employment.sinceWhen) && workFieldsValidation;
}

export function isHousingFormValid(adresse: Adresse, isPrincipal: boolean = true): boolean {
  if (!isPrincipal) {
    // secondary is always valid as secondary does not need to fill housing form.
    return true;
  }
  return (
    !isNullOrEmpty(adresse.adresseLabel) &&
    !isNullOrEmpty(adresse.housingType) &&
    !isNullOrEmpty(adresse.sinceWhen) &&
    !isNullOrEmpty(adresse.adresseCity)
  );
}

export function isInsuranceFormValid(
  insurance: Insurance,
  isPrincipal: boolean,
  t1Eligibility: boolean,
  t2Eligibility: boolean
): boolean {
  // If the T1 or T2 are not eligible to insurance step, the form is always considered as valid
  return (isPrincipal && t1Eligibility) || (!isPrincipal && t2Eligibility)
    ? insurance.acceptInsuranceOffer === true || insurance.confirmRefusal
    : true;
}

export function isInsuranceV2FormValid(
  insurance: InsuranceV2,
  isPrincipal: boolean,
  t1Eligibility: boolean,
  t2Eligibility: boolean
) {
  return (isPrincipal && t1Eligibility) || (!isPrincipal && t2Eligibility)
    ? insurance.selectedInsurance !== null || insurance.refuseInsurance
    : true;
}

export function isLoanFormValid(principalNameState: Name, creditForTwo: boolean | null): boolean {
  return principalNameState.maritalStatus !== null && creditForTwo !== null;
}

export function isCardFormValid(card: boolean): boolean {
  return card !== null && card !== undefined;
}

export function isAuthFormValid(authRequest: AuthenRequest): boolean {
  const { id, passwordMask, keypadId } = authRequest;
  return id !== null && passwordMask !== null && keypadId !== null;
}

export function isIdentificationFormValid(identificationForm: ICustomerIdentificationForm): boolean {
  const { lastName, firstName, birthPostCode, birthDate } = identificationForm;

  const lastNameIsValid = !isNullOrEmpty(lastName);
  const firstNameIsValid = !isNullOrEmpty(firstName);
  const birthPostCodeIsValid = !isNullOrEmpty(birthPostCode);
  const birthDateIsValid = DateUtils.isDateNotNull(birthDate);

  return lastNameIsValid && firstNameIsValid && birthPostCodeIsValid && birthDateIsValid;
}
