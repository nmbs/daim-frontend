import Vue from 'vue';
import Vuex from 'vuex';
import { RootState } from '@/store/types';
import { apiConfig } from '@/store/modules/apiConfig';
import { simulator } from '@/store/modules/simulator';
import { subscription } from '@/store/modules/subscription';
import { routing } from '@/store/modules/routing';
import { nomenclatureApi } from '@/store/modules/nomenclatureApi';
import { uiActiveState } from '@/store/modules/uiActiveState';
import { client } from '@/store/modules/client';

Vue.use(Vuex);

export default new Vuex.Store<RootState>({
  modules: {
    apiConfig,
    subscription,
    routing,
    nomenclatureApi,
    client,
    simulator,
    uiActiveState
  }
});
