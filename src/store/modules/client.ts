import { ClientState, RootState } from '@/store/types';
import { Module, MutationTree } from 'vuex';

const initialState: ClientState = {
  isMobile: false
};

const mutations: MutationTree<ClientState> = {
  updateIsMobile(state, isMobile: boolean) {
    state.isMobile = isMobile;
  }
};

const namespaced = false;

export const client: Module<ClientState, RootState> = {
  namespaced,
  state: initialState,
  mutations
};
