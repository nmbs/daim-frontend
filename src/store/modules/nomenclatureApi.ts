import { RootState } from '@/store/types';
import { ActionTree, Module, MutationTree, GetterTree } from 'vuex';
import { Nomenclature } from '@/models/Nomenclature';
import { SelectObject } from '@/models/selectObject';

const initialState: Nomenclature = {
  statusPro: null,
  areaList1: null,
  areaList2: null,
  areaList4: null,
  areaList6: null,
  areaList7: null,
  professionList1: null,
  professionList2: null,
  professionList4: null,
  professionList6: null,
  professionList7: null,
  contractType: null,
  maritalStatus1: null,
  maritalStatus2: null,
  housingStateType: null
};

const actions: ActionTree<Nomenclature, RootState> = {
  saveNomenclatureData({ commit }, nomenclatureData) {
    commit('saveNomenclatureData', nomenclatureData);
  }
};

const mutations: MutationTree<Nomenclature> = {
  saveNomenclatureData(state, nomenclatureData: Nomenclature) {
    state = Object.assign(state, nomenclatureData);
  }
};

const getters: GetterTree<Nomenclature, RootState> = {
  getFamilyStateChildren: () => {
    return [
      {
        label: '0',
        code: 0
      },
      {
        label: '1',
        code: 1
      },
      {
        label: '2',
        code: 2
      },
      {
        label: '3',
        code: 3
      },
      {
        label: '4',
        code: 4
      },
      {
        label: '5',
        code: 5
      },
      {
        label: '6+',
        code: 6
      }
    ];
  },
  getIncomesMonth: () => {
    return [
      {
        label: '12',
        code: 12
      },
      {
        label: '13',
        code: 13
      },
      {
        label: '14',
        code: 14
      },
      {
        label: '15',
        code: 15
      },
      {
        label: '16',
        code: 16
      }
    ];
  },
  getHousingStateType: (state) => {
    return state.housingStateType;
  },
  getProfessionalStatus: (state) => {
    return state.statusPro;
  },
  getProfessionalStatusArea: (state) => (professionalStatus: SelectObject): SelectObject[] | null => {
    const code = professionalStatus ? professionalStatus.code : '1_1';
    switch (code) {
      case '1_1': {
        return state.areaList1;
      }
      case '2_2': {
        return state.areaList2;
      }
      case '4_4': {
        return state.areaList4;
      }
      case '6_6': {
        return state.areaList6;
      }
      case '7_7': {
        return state.areaList7;
      }
      default: {
        return [];
      }
    }
  },
  getProfessionalStatusCategory: (state) => (professionalStatus: SelectObject): SelectObject[] | null => {
    const code = professionalStatus ? professionalStatus.code : '1_1';

    switch (code) {
      case '1_1': {
        return state.professionList1;
      }
      case '2_2': {
        return state.professionList2;
      }
      case '4_4': {
        return state.professionList4;
      }
      case '6_6': {
        return state.professionList6;
      }
      case '7_7': {
        return state.professionList7;
      }
      default: {
        return [];
      }
    }
  },
  getProfessionalStatusContract: (state) => (professionalStatus: SelectObject): SelectObject[] | null => {
    const code = professionalStatus ? professionalStatus.code : '1_1';

    switch (code) {
      case '1_1':
      case '2_2': {
        return state.contractType;
      }
      default: {
        return [{ code: -1, label: 'Non Applicable' }];
      }
    }
  },
  getMaritalStatus: (state) => (creditForTwo: boolean | null = false) => {
    if (creditForTwo) {
      return state.maritalStatus2;
    } else {
      return state.maritalStatus1;
    }
  }
};

const namespaced = false;

export const nomenclatureApi: Module<Nomenclature, RootState> = {
  namespaced,
  state: initialState,
  actions,
  mutations,
  getters
};
