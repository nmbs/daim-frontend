import { ApiConfigState, RootState } from '@/store/types';
import { Module, MutationTree, GetterTree } from 'vuex';
import { Colors } from '@/models/Colors';
import { AppConfiguration } from '@/models/Configuration';
import { RoutePathId } from '@/models/RoutesPath';

const initialState: ApiConfigState = {
  partnerCode: null, // Alias to Q6
  offerCode: null, // Alias to X1,
  businessProviderId: null, // Alias to A9
  equipmentCode: null, // Alias to N2
  applicationId: 'creditPartner',
  scaleCode: null, // Alias to C5
  appConfiguration: null,
  colors: null,
  colorTheme: null,
  simulateurLight: null,
  numFidelity: null
};

const mutations: MutationTree<ApiConfigState> = {
  savePartnerCode(state, partnerCode) {
    state.partnerCode = partnerCode;
  },
  saveOfferCode(state, offerCode) {
    state.offerCode = offerCode;
  },
  saveBusinessProviderId(state, businessProviderId: string) {
    state.businessProviderId = businessProviderId;
  },
  saveEquipmentCode(state, equipmentCode: string) {
    state.equipmentCode = equipmentCode;
  },
  saveScaleCode(state, scaleCode: string) {
    state.scaleCode = scaleCode;
  },
  saveAppConfiguration(state, configuration: AppConfiguration) {
    state.appConfiguration = configuration;
  },
  saveColors(state, colors: Colors) {
    state.colors = colors;
  },
  saveColorTheme(state, colorTheme: any) {
    state.colorTheme = colorTheme;
  },
  saveSimulateurLight(state, simulateurLight: string) {
    state.simulateurLight = simulateurLight.toLowerCase() === 'true' ? true : false;
  },
  saveFidelityNumber(state, numFidelity: string) {
    state.numFidelity = numFidelity;
  },
  updateCreditType(state, creditType: string) {
    state.appConfiguration.screenOptions['credit-type'].value = creditType;
  }
};

/**
 * Recursively go through steps to find a step based on its pathId
 */
function findPathId(pathId: string, steps: any[]): any {
  let stepFound: any = null;
  steps.forEach((step) => {
    if (!stepFound) {
      if (step.pathId === pathId) {
        stepFound = step;
      } else if (step.steps) {
        stepFound = findPathId(pathId, step.steps);
      }
    }
  });
  return stepFound;
}

export const getters: GetterTree<ApiConfigState, RootState> = {
  getStep: (state) => (stepId: string) => {
    return state.appConfiguration && state.appConfiguration.steps
      ? findPathId(stepId, state.appConfiguration.steps)
      : null;
  },
  coBorrowerDisplayed: (state) => {
    const defaultValue = true; // Default value to return when no specified in config.
    const subscriptionStep =
      state.appConfiguration && state.appConfiguration.steps
        ? state.appConfiguration.steps.find((step: any) => step.pathId === RoutePathId.SUBSCRIPTION)
        : null;
    const screenOptions = subscriptionStep && subscriptionStep.screenOptions;
    const coBorrowerDisplayedOption = screenOptions ? screenOptions.coBorrowerDisplayed : null;
    return coBorrowerDisplayedOption ? coBorrowerDisplayedOption.value : defaultValue;
  },
  dataUsageLink: (state, gettersList) => {
    return gettersList.screenOption('data-usage-link') || null;
  },
  legalMentionLink: (state, gettersList) => {
    return gettersList.screenOption('legal-mention-link') || null;
  },
  cguLink: (state, gettersList) => {
    return gettersList.screenOption('cgu-link') || null;
  },
  isVersionSimulationApiIn: (state, gettersList) => (version: number) => {
    return Number(gettersList.screenOption('simulation-api-version')) === version;
  },
  getCreditType: (state, gettersList) => {
    return gettersList.screenOption('credit-type');
  },
  isSofincoPartnerCode: (state) => {
    return state.partnerCode !== null && state.partnerCode === 'web_sofinco';
  },
  screenOption(state: ApiConfigState): (key: string) => any {
    return (key: string) => {
      const sOptions: any = state.appConfiguration && state.appConfiguration.screenOptions;
      return sOptions && sOptions[key] && sOptions[key].value;
    };
  }
};

const namespaced = false;

export const apiConfig: Module<ApiConfigState, RootState> = {
  namespaced,
  state: initialState,
  mutations,
  getters
};
