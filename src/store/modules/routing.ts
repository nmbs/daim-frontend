import { RoutingData, RootState } from '@/store/types';
import { ActionTree, GetterTree, Module, MutationTree } from 'vuex';
import { Route } from 'vue-router';
import { StepState } from '@/models/StepState';
import { RoutePathId } from '@/models/RoutesPath';

const initialState: RoutingData = {
  configurationRoutes: null,
  rootStepsStates: null
};

const actions: ActionTree<RoutingData, RootState> = {};

const mutations: MutationTree<RoutingData> = {
  saveConfigurationRoutes(state, routes: Route[]) {
    state.configurationRoutes = routes;
  },
  saveRootStepsStates(state, routes: Route[]) {
    state.rootStepsStates = routes;
  }
};

export const getters: GetterTree<RoutingData, RootState> = {
  getSubscriptionSteps: (state): any[] => {
    return state.rootStepsStates.steps.find((step: StepState) => step.pathId === RoutePathId.SUBSCRIPTION).steps;
  },
  getFirstSubscriptionStep: (state) => {
    const subscriptionSteps = state.rootStepsStates.steps.find(
      (step: StepState) => step.pathId === RoutePathId.SUBSCRIPTION
    ).steps;
    return subscriptionSteps[0] && subscriptionSteps[0].steps[0];
  },
  initialStep: (state) => {
    return state.rootStepsStates.steps[0];
  },
  getNumberSubscriptionSteps: (state): number => {
    const steps = state.rootStepsStates.steps.find((step: StepState) => step.pathId === RoutePathId.SUBSCRIPTION).steps;
    let nbSteps = 0;

    steps.forEach((step: StepState) => {
      nbSteps += step.steps.length > 0 ? step.steps.length : 1;
    });

    return nbSteps;
  },
  getCurrentStepIndex: (state) => (pathId: string): number => {
    // TODO: improve by concatening all steps in a more generic way. Difficulty is that
    // some category (ex: such as subscription) are routed and should not be included
    // in leafs index

    let leafSteps: string[] = [];

    const indexStepSubscription = state.rootStepsStates.steps.findIndex(
      (step: StepState) => step.pathId === RoutePathId.SUBSCRIPTION
    );
    const stepsSubscription = state.rootStepsStates.steps[indexStepSubscription].steps;

    if (indexStepSubscription > 0) {
      leafSteps.push(state.rootStepsStates.steps[indexStepSubscription - 1].pathId);
    }

    stepsSubscription.forEach((step: StepState) => {
      const hasRoutedChildSteps = step.steps.filter((e) => e.routed).length > 0;
      if (hasRoutedChildSteps) {
        leafSteps = leafSteps.concat(...step.steps.map((e) => e.pathId));
      } else {
        leafSteps.push(step.pathId);
      }
    });

    return leafSteps.indexOf(pathId);
  },
  getNextStep: (state) => (pathId: string): StepState | null => {
    if (state.rootStepsStates) {
      const currentStep = state.rootStepsStates.find(pathId);

      if (currentStep) {
        const nextStep = currentStep.getNext();
        return nextStep ? nextStep : null;
      }
    }

    return null;
  },
  getPreviousStep: (state) => (pathId: string, getLastChild?: boolean): StepState | null => {
    if (state.rootStepsStates) {
      const currentStep = state.rootStepsStates.find(pathId);

      if (currentStep) {
        const nextStep = currentStep.getPrevious(getLastChild);
        return nextStep ? nextStep : null;
      }
    }

    return null;
  },
  getPreviousSteps: (state, gettersParam) => (currentStepPathId: string): StepState[] => {
    const result: StepState[] = [];

    let step: StepState | null = gettersParam.getFirstSubscriptionStep;

    // For testing purposes as getters are raw functions during tests
    if (typeof step === 'function') {
      // @ts-ignore
      step = step(state, gettersParam);
    }
    while (step !== null && step.pathId !== currentStepPathId) {
      result.push(step);
      step = step.getNext();
    }

    return result;
  },
  getCurrentStepStates: (state) => (pathId: string): StepState | null => {
    return state.rootStepsStates ? state.rootStepsStates.find(pathId) : null;
  }
};

const namespaced = false;

export const routing: Module<RoutingData, RootState> = {
  namespaced,
  state: initialState,
  actions,
  mutations,
  getters
};
