import { MutationTree, ActionTree, Module } from 'vuex';
import { RootState, GlobalUiActiveState } from '@/store/types';

const initialState: GlobalUiActiveState = {
  contactModule: {
    showMobileHelpButton: false,
    showMobileHelpMenu: false,
    showMobileCallHelpButton: false
  }
};

const actions: ActionTree<GlobalUiActiveState, RootState> = {};

const mutations: MutationTree<GlobalUiActiveState> = {
  updateShowMobileHelpButton(state: GlobalUiActiveState, value: boolean) {
    state.contactModule.showMobileHelpButton = value;
  },
  updateShowMobileHelpMenu(state: GlobalUiActiveState, value: boolean) {
    state.contactModule.showMobileHelpMenu = value;
  },
  updateShowMobileHelpCallButton(state: GlobalUiActiveState, value: boolean) {
    state.contactModule.showMobileCallHelpButton = value;
  }
};

const namespaced = false;

export const uiActiveState: Module<GlobalUiActiveState, RootState> = {
  namespaced,
  state: initialState,
  actions,
  mutations
};
