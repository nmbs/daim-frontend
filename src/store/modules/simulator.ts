import { RootState, SimulatorState } from '@/store/types';
import { ActionTree, Module, MutationTree, GetterTree } from 'vuex';
import { FilterCriterion } from '@/models/simulator/tools/filterCriterion';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';

const initialState: SimulatorState = {
  requiredAmount: 0,
  dueNumber: null,
  homeReturnUrl: null,
  proposals: null,
  selectedProposal: null,
  minInsuranceProposal: null,
  maxRateProposal: null,
  proposalFilter: null,
  timesUserSelectedOffer: 0, // How many times a user selected an offer (needed for tag's plan)
  uiActiveStates: {
    forceUserAmountInput: true,
    overlay: false,
    parametersZoom: false,
    preContractPopup: false,
    showLoanParamsPopup: false,
    showOfferSelectorPopup: false,
    showOfferSubmitPopup: false
  },
  loading: false,
  simulationContext: {
    offerId: 'vac',
    partnerId: 'web_ikea',
    businessDataTransferToken: '',
    tokenOwner: '',
    sourceId: 'NEOURL41', // TODO: The value should be provided by the simulation API
    projectId: '5', // TODO: The value should be provided by the simulation API
    productId: 'PBCAMPIN', // TODO: The value should be provided by the simulation API
    simulationId: null,
    simulationOrigin: null,
    offer: null,
    vehicleAge: null,
    vehicleAmount: null,
    cardType: null,
    mfactoryid: null
  },
  simulationStandardConditions: null,
  offerPreSelection: {
    suggestedDueNumber: null,
    suggestedScaleCode: null,
    landingDone: false
  },
  recommendedProposal: null,
  alternativeProposal: null
};

const actions: ActionTree<SimulatorState, RootState> = {
  triggerParametersZoom: (context: any): void => {
    context.commit('updateOverlay', !context.state.uiActiveStates.parametersZoom);
    context.commit('updateParametersZoom', !context.state.uiActiveStates.parametersZoom);
  },
  triggerShowLoanParamsPopup: (context: any): void => {
    context.commit('updateShowLoanParamsPopup', !context.state.uiActiveStates.showLoanParamsPopup);
  },
  triggerPreContractPopup: (context: any): void => {
    context.commit('updateOverlay', !context.state.uiActiveStates.preContractPopup);
    context.commit('updatePreContractPopup', !context.state.uiActiveStates.preContractPopup);
  },
  applyProposalFilter: (context: any, filter: FilterCriterion): void => {
    context.commit('updateSelectedProposal', null);
    context.commit('updateProposalFilter', filter);
  },
  clearProposalFilter: (context: any): void => {
    context.commit('updateSelectedProposal', null);
    context.commit('updateProposalFilter', null);
  },
  clearProposals: (context: any): void => {
    context.commit('updateSelectedProposal', null);
    context.commit('updateSelectedProposal', null);
    context.commit('updateProposalFilter', null);
    context.commit('updateProposals', Array());
  },
  updateProposals({ commit }, proposals: SimulationFinancialProposal[]) {
    commit('updateProposals', proposals);
  },
  setSelectedProposal: (context: any, proposal: SimulationFinancialProposal): void => {
    context.commit('updateSelectedProposal', proposal);
    if (proposal.scaleCode) {
      context.commit('saveScaleCode', proposal.scaleCode);
    }
  }
};

const mutations: MutationTree<SimulatorState> = {
  updateBusinessDataTransferToken(state, businessDataTransferToken: string) {
    state.simulationContext.businessDataTransferToken = businessDataTransferToken;
  },
  updateTokenOwner(state: SimulatorState, tokenOwner: string) {
    state.simulationContext.tokenOwner = tokenOwner;
  },
  updateOfferId(state: SimulatorState, offerId: string) {
    state.simulationContext.offerId = offerId;
  },
  updateMfactoryid(state: SimulatorState, mfactoryid: string[] | string) {
    if (Array.isArray(mfactoryid)) {
      state.simulationContext.mfactoryid = mfactoryid;
    } else {
      state.simulationContext.mfactoryid = [mfactoryid];
    }
  },
  updateProjectId(state: SimulatorState, projectId: string) {
    state.simulationContext.projectId = projectId;
  },
  updateSimulationId(state: SimulatorState, simulationId: string) {
    state.simulationContext.simulationId = simulationId;
  },
  updateSimulationOrigin(state: SimulatorState, simulationOrigin: string) {
    state.simulationContext.simulationOrigin = simulationOrigin;
  },
  updateOffer(state: SimulatorState, offer: string) {
    state.simulationContext.offer = offer;
  },
  updateVehicleAge(state: SimulatorState, vehicleAge: string) {
    state.simulationContext.vehicleAge = vehicleAge;
  },
  updateVehicleAmount(state: SimulatorState, vehicleAmount: string) {
    state.simulationContext.vehicleAmount = vehicleAmount;
  },
  updateCardType(state: SimulatorState, cardType: string) {
    state.simulationContext.cardType = cardType;
  },
  updateSourceId(state: SimulatorState, sourceId: string) {
    state.simulationContext.sourceId = sourceId;
  },
  updateProductId(state: SimulatorState, productId: string) {
    state.simulationContext.productId = productId;
  },
  updatePartnerId(state: SimulatorState, partnerId: string) {
    state.simulationContext.partnerId = partnerId;
  },
  updateRequiredAmount(state: SimulatorState, value) {
    state.requiredAmount = value;
  },
  updateDueNumber(state: SimulatorState, value) {
    state.dueNumber = value;
  },
  updateHomeReturnUrl(state: SimulatorState, value) {
    state.homeReturnUrl = value;
  },
  updateProposals(state: SimulatorState, value) {
    state.proposals = value;
  },
  updateSelectedProposal(state: SimulatorState, value: SimulationFinancialProposal) {
    state.selectedProposal = value;
    if (!state.minInsuranceProposal) {
      state.minInsuranceProposal = value;
    }
  },
  updateMaxRateProposal(state: SimulatorState, value: SimulationFinancialProposal) {
    state.maxRateProposal = value;
  },
  updateMinInsuranceProposal(state: SimulatorState, value: SimulationFinancialProposal) {
    state.minInsuranceProposal = value;
  },
  updateProposalFilter(state: SimulatorState, value) {
    state.proposalFilter = value;
  },
  updateForceUserAmountInput(state: SimulatorState, value) {
    state.uiActiveStates.forceUserAmountInput = value;
  },
  updateOverlay(state: SimulatorState, value) {
    state.uiActiveStates.overlay = value;
  },
  updateParametersZoom(state: SimulatorState, value) {
    state.uiActiveStates.parametersZoom = value;
  },
  updateShowLoanParamsPopup(state: SimulatorState, value) {
    state.uiActiveStates.showLoanParamsPopup = value;
  },
  updatePreContractPopup(state: SimulatorState, value) {
    state.uiActiveStates.preContractPopup = value;
  },
  updateLoading(state: SimulatorState, value: boolean) {
    state.loading = value;
  },
  increaseTimesUserSelectedOffer(state: SimulatorState) {
    state.timesUserSelectedOffer++;
  },
  resetTimesUserSelectedOffer(state: SimulatorState) {
    state.timesUserSelectedOffer = 0;
  },
  updateSimulationStandardConditions(state: SimulatorState, value: SimulationFinancialProposal) {
    state.simulationStandardConditions = value;
  },
  updatePreSelectionSuggestedDueNumber(state: SimulatorState, value: string) {
    if (value === '') {
      return;
    } else {
      const valueNumber = +value;
      if (isNaN(valueNumber)) {
        return;
      } else {
        state.offerPreSelection.suggestedDueNumber = valueNumber;
      }
    }
  },
  updatePreSelectionSuggestedScaleCode(state: SimulatorState, value: string) {
    state.offerPreSelection.suggestedScaleCode = value;
  },
  updateShowOfferSelectorPopup(state: SimulatorState, value: boolean) {
    state.uiActiveStates.showOfferSelectorPopup = value;
  },
  updateShowOfferSubmitPopup(state: SimulatorState, value: boolean) {
    state.uiActiveStates.showOfferSubmitPopup = value;
  },
  updateOfferPreSelectionLandingDone(state: SimulatorState) {
    state.offerPreSelection.landingDone = true;
  },
  updateRecommendedProposal(state: SimulatorState, recommendedProposal: SimulationFinancialProposal) {
    state.recommendedProposal = recommendedProposal;
  },
  updateAlternativeProposal(state: SimulatorState, alternativeProposal: SimulationFinancialProposal) {
    state.alternativeProposal = alternativeProposal;
  }
};

const getters: GetterTree<SimulatorState, RootState> = {
  emptyProposals: (state) => {
    return state.proposals && state.proposals.length === 0;
  },
  hasEnoughProposalsForFiltering: (state: SimulatorState) => {
    return state.proposals && state.proposals.length > 5;
  },
  hasOfferPreSelected: (state) => {
    if (state.offerPreSelection.suggestedDueNumber === 0) {
      return true;
    } else {
      return state.offerPreSelection.suggestedDueNumber;
    }
  }
};

const namespaced = false;

export const simulator: Module<SimulatorState, RootState> = {
  namespaced,
  state: initialState,
  actions,
  mutations,
  getters
};
