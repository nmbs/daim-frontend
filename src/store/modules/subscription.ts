import { PreContractMail, RootState, SubscriptionData, SubscriptionState } from '@/store/types';
import { ActionTree, GetterTree, Module, MutationTree } from 'vuex';
import {
  isAuthFormValid,
  isCardFormValid,
  isChargesFormValid,
  isCivilityFormValid,
  isContactFormValid,
  isEmploymentFormValid,
  isFamilyFormValid,
  isHousingFormValid,
  isIdentificationFormValid,
  isIncomesFormValid,
  isInsuranceFormValid,
  isInsuranceV2FormValid,
  isLoanFormValid,
  isNameFormValid
} from '@/store/validators/informationValidators';
import { EligibilityPayload, SubscriptionPayload, T1Payload } from '@/models/subscription/SubscriptionPayload';
import { VisibleMutationPayload } from '@/models/subscription/VisibleMutationPayload';
import { FolderResponse } from '@/models/subscription/FolderResponse';
import { BusinessDataTransfer } from '@/models/BusinessDataTransfer';
import {
  getAuthenRequestInitialState,
  getAuthenResponseInitialState,
  getAuthIdentificationInitialState,
  getInitialFormStates,
  getInitialRouteStates,
  getInitialState,
  mapContextToState
} from '@/store/utils';
import { BusinessRule } from '@/services/BusinessRule';
import { SeConfig } from '@/models/subscription/api/SeConfig';
import { AuthenRequest, AuthenResponse } from '@/models/subscription/authentication/Authentication';
import {
  ICustomerIdentificationChoice,
  ICustomerIdentificationForm
} from '@/models/subscription/authentication/ICustomerIdentification';
import { RoutePathId } from '@/models/RoutesPath';
import { Basket } from '@/models/basket';
import { BeforeLeaveStep } from '@/models/subscription/BeforeLeaveStep';
import * as Mutations from '@/store/Mutations';
import { InsuranceFormState } from '@/models/subscription/FormState';

const initialState: SubscriptionState = {
  preContractMail: {
    hasBeenSent: false,
    email: '',
    offer: null
  },
  principal: getInitialState(),
  secondary: getInitialState(),
  formStates: getInitialFormStates(),
  routeStates: getInitialRouteStates(),
  routesList: [],
  blocRouteToFormRoutes: new Map(),
  response: null,
  context: null,
  creditForTwo: null,
  seConfig: null,
  tosAccepted: false,
  creditCardChosen: false,
  creditCard: null,
  cardChoiceAlert: null,
  experianData: {
    cfuserPrefs: null,
    cfuserPrefs2: null
  },
  authentication: {
    // Store values retrieved from authentication form
    request: getAuthenRequestInitialState(),
    // Store values retrieved after submitted authentication form
    response: getAuthenResponseInitialState(),
    // Store values from customer identification form (Forgotten id)
    identification: getAuthIdentificationInitialState(),
    clientIsLogging: false
  },
  cardId: null,
  keyring: null,
  restoringCard: false,
  funnelId: null
};

const actions: ActionTree<SubscriptionState, RootState> = {
  setSubscriptionData({ commit, getters, state }, payload: SubscriptionPayload) {
    const { isPrincipal, subStateName, subStateValue } = payload;
    const activeRouteIndex = getters.getActiveRouteIndex;
    let currentRouteName = '';
    if (activeRouteIndex >= 0) {
      state.routesList.forEach((route: string, index: number) => {
        if (index > activeRouteIndex) {
          commit(Mutations.MARK_FORM_AS_INVALID, route);
        } else if (index === activeRouteIndex) {
          currentRouteName = route;
        }
      });
    }

    commit('setSubscriptionData', payload);
    if (!getters.getCompleteValidationStatus(currentRouteName)) {
      commit(Mutations.MARK_ONE_FORM_AS_INVALID, currentRouteName);
    } else {
      commit(Mutations.MARK_FORM_AS_VALID, currentRouteName);
    }
  },

  setT1IsActive({ commit }, t1Payload: T1Payload) {
    commit('setT1IsActive', t1Payload);
  },
  setFolderResponse({ commit }, folderResponse: FolderResponse) {
    commit('setFolderResponse', folderResponse);
  },
  setBlocRouteToFormRoutes({ commit }, blocRouteToFormRoutes: Map<any, any>) {
    commit('setBlocRouteToFormRoutes', blocRouteToFormRoutes);
  },
  setCreditForTwo({ commit }, creditForTwo: boolean) {
    commit('setCreditForTwo', creditForTwo);
  },
  setInsuranceEligibility({ commit, state }, eligibilityPayload: EligibilityPayload) {
    const businessRule = new BusinessRule();

    let t1IsEligible = false;
    let t2IsEligible = true; // By default t2 is eligible even if there is no co-borrower

    if (state.formStates.insurance.version === 'v1') {
      // Test the T1 insurance eligibility
      t1IsEligible = businessRule.canTakeInsurance(
        eligibilityPayload.creditType,
        eligibilityPayload.firstMonthlyInsuranceAmount,
        eligibilityPayload.totalInsuranceAmount
      );
      if (state.creditForTwo) {
        // Test the T2 insurance eligibility
        t2IsEligible = businessRule.canTakeInsurance(
          eligibilityPayload.creditType,
          eligibilityPayload.firstMonthlyInsuranceAmountT2,
          eligibilityPayload.totalInsuranceAmountT2
        );
      }
    } else if (state.formStates.insurance.version === 'v2') {
      const {
        t1IsEligibleToRecommendedInsurance,
        t1IsEligibleToAlternativeInsurance,
        t2IsEligibleToRecommendedInsurance,
        t2IsEligibleToAlternativeInsurance
      } = eligibilityPayload;
      t1IsEligible = t1IsEligibleToRecommendedInsurance || t1IsEligibleToAlternativeInsurance;
      if (state.creditForTwo) {
        t2IsEligible = t2IsEligibleToRecommendedInsurance || t2IsEligibleToAlternativeInsurance;
      }
      commit('setInsuranceV2Eligibility', {
        t1IsEligibleToRecommendedInsurance,
        t1IsEligibleToAlternativeInsurance,
        t2IsEligibleToRecommendedInsurance,
        t2IsEligibleToAlternativeInsurance
      });
    } else {
      // This insurance version does not yet implemented
    }
    // Update the store with eligibility results
    commit('setInsuranceEligibility', {
      t1IsEligible,
      t2IsEligible
    });
  },
  async loadBusinessDataContext({ commit }, payload: BusinessDataTransfer) {
    // Save context
    commit('saveContext', payload);

    // Initialize borrower
    const principal: SubscriptionData = await mapContextToState(payload.customerContext);
    commit('savePrincipal', principal);

    // Initialize coborrower
    const secondary: SubscriptionData = await mapContextToState(payload.coBorrowerContext);
    commit('saveSecondary', secondary);
  },
  setSeConfig({ commit }, seConfig: SeConfig) {
    commit('setSeConfig', seConfig);
  }
};

export const mutationsSubscription: MutationTree<SubscriptionState> = {
  updateFunnelId(state: SubscriptionState, funnelId: string) {
    state.funnelId = funnelId;
  },
  updateBasket(state: SubscriptionState, basket: Basket) {
    state.basket = basket;
  },
  updateRestoringCard(state: SubscriptionState, restoringCard: boolean) {
    state.restoringCard = restoringCard;
  },
  updateCardChoiceAlert(state: SubscriptionState, cardChoiceAlert: string) {
    state.cardChoiceAlert = cardChoiceAlert;
  },
  updateCardId(state: SubscriptionState, cardId: string) {
    state.cardId = cardId;
  },

  updateKeyring(state: SubscriptionState, keyring: string) {
    state.keyring = keyring;
  },
  updateCreditCard(state: SubscriptionState, creditCard: string) {
    state.creditCard = creditCard;
  },
  updateCreditCardChosen(state: SubscriptionState, creditCardChosen: boolean) {
    state.creditCardChosen = true;
  },
  updateTosAccepted(state: SubscriptionState, tosAccepted: boolean) {
    state.tosAccepted = true;
  },
  saveContext(state: SubscriptionState, context: BusinessDataTransfer) {
    state.context = context;
  },
  savePrincipal(state: SubscriptionState, principal: SubscriptionData) {
    state.principal = principal;
  },
  saveSecondary(state: SubscriptionState, secondary: SubscriptionData) {
    state.secondary = secondary;
  },
  setBlocRouteToFormRoutes(state, blocRouteToFormRoutes: Map<any, any>) {
    state.blocRouteToFormRoutes = blocRouteToFormRoutes;
  },
  setSubscriptionData(state, payload: SubscriptionPayload) {
    const { isPrincipal, subStateName, subStateValue } = payload;
    if (isPrincipal) {
      // @ts-ignore
      state.principal[subStateName] = subStateValue;
    } else {
      // @ts-ignore
      state.secondary[subStateName] = subStateValue;
    }
  },
  setT1IsActive(state, t1Payload: T1Payload) {
    const { pathId, t1IsActive } = t1Payload;
    // @ts-ignore
    state.formStates[pathId].t1IsActive = t1IsActive;
  },
  setInsuranceEligibility(state, eligibilityPayload: EligibilityPayload) {
    const { t1IsEligible, t2IsEligible } = eligibilityPayload;
    const insuranceFormStates: InsuranceFormState = state.formStates['insurance'];
    if (t1IsEligible !== null && t1IsEligible !== undefined) {
      insuranceFormStates.t1IsEligible = t1IsEligible;
    }
    if (t2IsEligible !== null && t2IsEligible !== undefined) {
      insuranceFormStates.t2IsEligible = t2IsEligible;
    }
  },
  setInsuranceV2Eligibility(state, eligibilityPayload: EligibilityPayload) {
    const {
      t1IsEligibleToRecommendedInsurance,
      t2IsEligibleToRecommendedInsurance,
      t1IsEligibleToAlternativeInsurance,
      t2IsEligibleToAlternativeInsurance
    } = eligibilityPayload;
    const insuranceFormStates: InsuranceFormState = state.formStates['insurance'];
    if (t1IsEligibleToRecommendedInsurance !== null && t1IsEligibleToRecommendedInsurance !== undefined) {
      insuranceFormStates.t1IsEligibleRecommended = t1IsEligibleToRecommendedInsurance;
    }
    if (t2IsEligibleToRecommendedInsurance !== null && t2IsEligibleToRecommendedInsurance !== undefined) {
      insuranceFormStates.t2IsEligibleRecommended = t2IsEligibleToRecommendedInsurance;
    }
    if (t1IsEligibleToAlternativeInsurance !== null && t1IsEligibleToAlternativeInsurance !== undefined) {
      insuranceFormStates.t1IsEligibleAlternative = t1IsEligibleToAlternativeInsurance;
    }
    if (t2IsEligibleToAlternativeInsurance !== null && t2IsEligibleToAlternativeInsurance !== undefined) {
      insuranceFormStates.t2IsEligibleAlternative = t2IsEligibleToAlternativeInsurance;
    }
  },
  updatePreContractMail(state, payload: PreContractMail) {
    state.preContractMail = payload;
  },
  saveRoutesList(state, routesList) {
    state.routesList = routesList;
  },
  setRouteStates(state: any, routeStates: any) {
    state.routeStates = routeStates;
  },
  activateRoute(state: any, routeName: string) {
    for (const route in state.routeStates) {
      if (state.routeStates.hasOwnProperty(route)) {
        state.routeStates[route].active = false;
        state.routeStates[route].opened = false;
      }
    }

    if (state.routeStates[routeName]) {
      state.routeStates[routeName].disabled = false;
      state.routeStates[routeName].active = true;
    }

    // bloc -> disabled: false, visible: true
    const blocName = [...state.blocRouteToFormRoutes.keys()].find((b: string) => {
      const routes = state.blocRouteToFormRoutes.get(b);
      return routes.find((r: string) => r === routeName);
    });

    const currentBloc = state.routeStates[blocName];
    if (currentBloc) {
      currentBloc.disabled = false;
      currentBloc.active = true;
    }
  },
  markFormAsValid(state: any, routeName: string) {
    if (state.routeStates[routeName]) {
      state.routeStates[routeName].disabled = false;
      state.routeStates[routeName].valid = true;
    }

    // Check synchronize bloc validation status with route status
    [...state.blocRouteToFormRoutes.keys()].forEach((blocName: string) => {
      const routes = state.blocRouteToFormRoutes.get(blocName);
      if (routes) {
        if (routes.every((r: string) => state.routeStates[r].valid)) {
          state.routeStates[blocName].valid = true;
        }
      }
    });
  },
  markFormAsOpen(state: any, routeName: string) {
    if (state.routeStates[routeName]) {
      state.routeStates[routeName].opened = true;
    }
  },
  markFormAsClose(state: any, routeName: string) {
    if (state.routeStates[routeName]) {
      state.routeStates[routeName].opened = false;
    }
  },
  markFormAsInvalid(state: any, routeName: string) {
    if (state.routeStates[routeName]) {
      state.routeStates[routeName].disabled = true;
      state.routeStates[routeName].valid = false;
      state.routeStates[routeName].active = false;
    }

    // Check synchronize bloc validation status with route status
    [...state.blocRouteToFormRoutes.keys()].forEach((blocName: string) => {
      const routes = state.blocRouteToFormRoutes.get(blocName);
      if (blocName === routeName && routes) {
        if (routes.every((r: string) => !state.routeStates[r].valid)) {
          state.routeStates[blocName].active = false;
          state.routeStates[blocName].valid = false;
          state.routeStates[blocName].disabled = true;
        }
      }
    });
  },
  markOneFormAsInvalid(state: any, routeName: string) {
    if (state.routeStates[routeName]) {
      state.routeStates[routeName].valid = false;
    }
  },
  updateVisibleRoute(state: any, payload: VisibleMutationPayload) {
    const { route, visible } = payload;
    state.routeStates[route].visible = visible;
  },
  setFolderResponse(state: any, folderResponse: FolderResponse) {
    state.response = folderResponse;
  },
  setCreditForTwo(state: any, creditForTwo: boolean) {
    state.creditForTwo = creditForTwo;
  },
  setSeConfig(state: any, seConfig: SeConfig) {
    state.seConfig = seConfig;
  },
  setExperianData1(state: any, value) {
    state.experianData.cfuserPrefs = value;
  },
  setExperianData2(state: any, value) {
    state.experianData.cfuserPrefs2 = value;
  },
  setAuthenRequest(state: SubscriptionState, value: AuthenRequest) {
    state.authentication.request = value;
  },
  clearAuthenRequest(state: SubscriptionState) {
    state.authentication.request = getAuthenRequestInitialState();
  },
  setAuthenResponse(state: SubscriptionState, value: AuthenResponse) {
    state.authentication.response = value;
  },
  clearAuthenResponse(state: SubscriptionState) {
    state.authentication.response = getAuthenResponseInitialState();
  },
  setAuthIdentificationForm(state: SubscriptionState, payload: ICustomerIdentificationForm) {
    state.authentication.identification.form = payload;
  },
  setAuthIdentificationChoice(state: SubscriptionState, paypload: ICustomerIdentificationChoice) {
    state.authentication.identification.choice = paypload;
  },
  clearAuthenIdentification(state: SubscriptionState) {
    state.authentication.identification = getAuthIdentificationInitialState();
  },
  setAuthenticationClientIsLogging(state: SubscriptionState, value: boolean) {
    state.authentication.clientIsLogging = value;
  },
  setInsuranceFormVersion(state: SubscriptionState, version: 'v1' | 'v2') {
    state.formStates.insurance.version = version;
  }
};

export const gettersSubcription: GetterTree<SubscriptionState, RootState> = {
  isResultOK: (state) => {
    return state.response && (state.response.acceptedCode === '0' || state.response.acceptedCode === '2');
  },

  isResultKO: (state) => {
    return state.response && !(state.response.acceptedCode === '0' || state.response.acceptedCode === '2');
  },

  interruptibilityPossible: (state) => {
    return !state.restoringCard && state.principal && state.principal.contact && state.principal.contact.mobilePhone;
  },
  hasBorrower: (state) => {
    return state.principal.name.hasSpouse;
  },
  getT1IsActive: (state, getters) => (pathId: string) => {
    // @ts-ignore
    return getters.isCreditForTwo ? state.formStates[pathId].t1IsActive : true;
  },
  getCompleteValidationStatus: (state, getters) => (pathId: string) => {
    return (
      getters.getValidationStatus(pathId, true) && (!getters.hasBorrower || getters.getValidationStatus(pathId, false))
    );
  },
  getValidationStatus: (state) => (pathId: string, isPrincipal: boolean = true) => {
    switch (pathId) {
      case RoutePathId.LOAN:
        return isLoanFormValid(state.principal.name, state.creditForTwo);
      case RoutePathId.CARD:
        return isCardFormValid(state.principal.wantCard);
      case RoutePathId.NAME:
        return isNameFormValid(state.principal.name, state.secondary.name, isPrincipal);
      case RoutePathId.FAMILY_SITUATION:
        return isFamilyFormValid(state[isPrincipal ? 'principal' : 'secondary'].family);
      case RoutePathId.CONTACT:
        return isContactFormValid(state[isPrincipal ? 'principal' : 'secondary'].contact, isPrincipal);
      case RoutePathId.CIVILITY:
        return isCivilityFormValid(state[isPrincipal ? 'principal' : 'secondary'].civility);
      case RoutePathId.INCOMES:
        return isIncomesFormValid(state[isPrincipal ? 'principal' : 'secondary'].incomes);
      case RoutePathId.CHARGES:
        return isChargesFormValid(
          state[isPrincipal ? 'principal' : 'secondary'].charges,
          state[isPrincipal ? 'principal' : 'secondary'].adresse,
          isPrincipal
        );
      case RoutePathId.EMPLOYMENT:
        return isEmploymentFormValid(state[isPrincipal ? 'principal' : 'secondary'].employment);
      case RoutePathId.HOUSING:
        return isHousingFormValid(state[isPrincipal ? 'principal' : 'secondary'].adresse, isPrincipal);
      case RoutePathId.INSURANCE:
        const t1Eligibility: boolean = state.formStates.insurance.t1IsEligible;
        const t2Eligibility: boolean = state.formStates.insurance.t2IsEligible;
        if (state.formStates.insurance.version === 'v1') {
          return isInsuranceFormValid(
            state[isPrincipal ? 'principal' : 'secondary'].insurance,
            isPrincipal,
            t1Eligibility,
            t2Eligibility
          );
        } else {
          return isInsuranceV2FormValid(
            state[isPrincipal ? 'principal' : 'secondary'].insurancev2,
            isPrincipal,
            t1Eligibility,
            t2Eligibility
          );
        }
      case RoutePathId.AUTH:
        return isAuthFormValid(state.authentication.request);
      case RoutePathId.IDENTIFICATION:
        return isIdentificationFormValid(state.authentication.identification.form);
      default:
        return true;
    }
  },
  getPreviousActiveRoute: (state) => {
    const activeRouteIndex = Object.keys(state.routeStates).findIndex((e: string) => {
      // @ts-ignore
      return state.routeStates[e].active;
    });
    return Object.keys(state.routeStates)[activeRouteIndex - 1];
  },

  getActiveRouteIndex: (state) => {
    const routeNameActive = Object.keys(state.routeStates).find((e: string) => {
      return state.routeStates[e].active && !state.routeStates[e].isBlock;
    });
    return routeNameActive ? state.routesList.findIndex((r) => r === routeNameActive) : -1;
  },
  isCreditForTwo: (state) => {
    return state.creditForTwo;
  },
  isInsuranceEligibleForT2AndNotT1: (state) => {
    const { t1IsEligible, t2IsEligible } = state.formStates.insurance;
    return !t1IsEligible && t2IsEligible;
  },
  isInsuranceEligibleForT1AndNotT2: (state) => {
    const { t1IsEligible, t2IsEligible } = state.formStates.insurance;
    return t1IsEligible && !t2IsEligible;
  },
  isClientLogged: (state) => {
    return state.authentication && state.authentication.response && state.authentication.response.customerId;
  },
  getBeforeLeaveStep: (state, gettersList) => (hasInterruptibility: boolean) => {
    let popup = BeforeLeaveStep.NOTACTIVATE;
    // TODO: The first step may be different depending on the partner configuration
    // TODO: Remove the hardcoded route by the first route
    if (!state.routeStates.loan.active && gettersList.getValidationStatus(RoutePathId.LOAN)) {
      if (!hasInterruptibility) {
        popup = BeforeLeaveStep.NOTSAVEABLE;
      } else if (gettersList.isResultKO) {
        popup = BeforeLeaveStep.FINISHED_KO;
      } else if (state.response && state.response.customerId) {
        popup = BeforeLeaveStep.FINISHED;
      } else if (state.routeStates.charges.valid) {
        popup = BeforeLeaveStep.ALMOSTFINISHED;
      } else if (state.principal.employment.professionalStatus !== null) {
        popup = BeforeLeaveStep.HALFWAY;
      } else if (state.principal.contact.mobilePhone !== null) {
        popup = BeforeLeaveStep.JUSTSTARTED;
      } else {
        popup = BeforeLeaveStep.NOTSAVEABLE;
      }
    }
    return popup;
  }
};

const namespaced = false;

export const subscription: Module<SubscriptionState, RootState> = {
  namespaced,
  state: initialState,
  actions,
  mutations: mutationsSubscription,
  getters: gettersSubcription
};
