import { createLocalVue, shallowMount } from '@vue/test-utils';
import Keypad from './Keypad.vue';

import VueRouter from 'vue-router';
import VueI18n from 'vue-i18n';
import Vue from 'vue';
import * as messagesJson from '@/assets/i18n/messages.json';

const localVue = createLocalVue();
localVue.use(VueRouter);

const router = new VueRouter();
Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages: messagesJson.default
});

describe('Keypad', () => {
  const props = {
    column: 3,
    row: 4,
    digitWidth: 100,
    digitHeight: 75,
    maxLength: 12
  };

  it('should render the component', () => {
    const wrapper = shallowMount(Keypad, {
      router,
      localVue,
      i18n
    });
    expect(wrapper.find('.keypad-component').exists);
  });

  it('should render display 4 rows and 3 columns each', () => {
    const wrapper = shallowMount(Keypad, {
      router,
      localVue,
      i18n,
      propsData: {
        ...props
      }
    });

    const keypadOverContainer = wrapper.find('.keypad-component .keypad-container > .keypad-over');
    const keypadOverRows = keypadOverContainer.findAll('.keypad-over__row');

    expect(keypadOverRows.length).toBe(4);

    const r1 = keypadOverRows.at(0);
    const r2 = keypadOverRows.at(1);
    const r3 = keypadOverRows.at(2);
    const r4 = keypadOverRows.at(3);

    expect(r1.findAll('.keypad-over__row__item').length).toBe(3);
    expect(r2.findAll('.keypad-over__row__item').length).toBe(3);
    expect(r3.findAll('.keypad-over__row__item').length).toBe(3);
    expect(r4.findAll('.keypad-over__row__item').length).toBe(3);
  });

  it('should update value with position format at click', () => {
    let valueUpdated: string | null = null;
    const wrapper = shallowMount(Keypad, {
      router,
      localVue,
      i18n,
      propsData: {
        ...props,
        setValue: (value: any) => {
          valueUpdated = value;
        }
      }
    });

    const keypadOverContainer = wrapper.find('.keypad-component .keypad-container > .keypad-over');
    const keypadOverRows = keypadOverContainer.findAll('.keypad-over__row');

    expect(keypadOverRows.length).toBe(4);

    const r1 = keypadOverRows.at(0);

    const firstButton = r1.findAll('.keypad-over__row__item').at(0);
    const secondButton = r1.findAll('.keypad-over__row__item').at(1);

    firstButton.trigger('click');
    secondButton.trigger('click');

    expect(valueUpdated).toEqual('00-01');
  });

  it('should send null at click on clear button', () => {
    let valueUpdated: string | null = null;
    const wrapper = shallowMount(Keypad, {
      router,
      localVue,
      i18n,
      propsData: {
        ...props,
        setValue: (value: string | null) => {
          valueUpdated = value;
        }
      }
    });

    const keypadOverContainer = wrapper.find('.keypad-component .keypad-container > .keypad-over');
    const keypadOverRows = keypadOverContainer.findAll('.keypad-over__row');

    expect(keypadOverRows.length).toBe(4);

    const r1 = keypadOverRows.at(0);

    const firstButton = r1.findAll('.keypad-over__row__item').at(0);
    const clearButton = wrapper.find('.input-clear');

    firstButton.trigger('click');

    expect(valueUpdated).toEqual('00');

    clearButton.trigger('click');

    expect(valueUpdated).toBeNull();
  });

  it('should not update value when mask is disabled', () => {
    let valueUpdated: string | null = null;
    const wrapper = shallowMount(Keypad, {
      router,
      localVue,
      i18n,
      propsData: {
        ...props,
        digitMasks: [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        setValue: (value: string | null) => {
          valueUpdated = value;
        }
      }
    });

    const keypadOverContainer = wrapper.find('.keypad-component .keypad-container > .keypad-over');
    const keypadOverRows = keypadOverContainer.findAll('.keypad-over__row');

    expect(keypadOverRows.length).toBe(4);

    const r1 = keypadOverRows.at(0);
    const firstButton = r1.findAll('.keypad-over__row__item').at(0);

    firstButton.trigger('click');

    expect(valueUpdated).toBeNull();
  });

  it('should update value when mask is clickable', () => {
    let valueUpdated: string | null = null;
    const wrapper = shallowMount(Keypad, {
      router,
      localVue,
      i18n,
      propsData: {
        ...props,
        digitMasks: [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        setValue: (value: string | null) => {
          valueUpdated = value;
        }
      }
    });

    const keypadOverContainer = wrapper.find('.keypad-component .keypad-container > .keypad-over');
    const keypadOverRows = keypadOverContainer.findAll('.keypad-over__row');

    expect(keypadOverRows.length).toBe(4);

    const r1 = keypadOverRows.at(0);
    const secondButton = r1.findAll('.keypad-over__row__item').at(1);

    secondButton.trigger('click');

    expect(valueUpdated).toEqual('01');
  });
});
