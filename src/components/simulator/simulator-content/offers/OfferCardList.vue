<template>
  <div class="offer-card__list">
    <offer-card
      v-for="(proposal, index) in filteredProposalsByDueNumber"
      :key="index"
      :id="index"
      :setValue="updateOfferSelected"
      :offerSelected="offerSelected"
      :amount="proposal.monthlyAmountWithoutInsurance"
      :period="
        proposal.lastMonthlyAmountWithoutInsurance
          ? proposal.nbMonthlyPaymentWithoutInsurance + 1
          : proposal.nbMonthlyPaymentWithoutInsurance
      "
      :tagTollFree="proposal.taeg === 0 ? true : false"
      :tagRefund="proposal.deferralMonthNumber > 0 ? true : false"
      :refundPeriod="proposal.deferralMonthNumber"
      ref="proposalCard"
    />
  </div>
</template>

<script lang="ts">
import { Component, Vue, Watch } from 'vue-property-decorator';
import { Action, Getter, Mutation, State } from 'vuex-class';
import OfferCard from '@/components/simulator/simulator-content/offers/OfferCard.vue';
import { FilterCriterion, EFilterCriterionAttribute } from '@/models/simulator/tools/filterCriterion';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';
import Config from '@/config/configSimulator';
import { getTagManagerInstance, TagManagerService } from '@/services/tagManagerFactory';
import { findIndexPreSelectedOffer } from '@/services/simulator/offerPreSelection';
import { SimulatorOfferPreSelectionState } from '@/store/types';
import { proposalSelected, sortSimulationProposal } from '@/services/simulator/sortSimulateur';

/**
 * OfferCardList
 *
 * At app initialization, the 5 best offers are displayed. When
 * the user interacts with the LoanParameters panel, the FilterCriterion
 * is then set and the displayed proposals are changed to fit users's
 * search parameters.
 *
 * If the user clicks on a proposal, the selected one is then pushed
 * into app store so it can be displayed in panel OfferSubmit and OfferDetail.
 *
 * If the user clicks on previous button, the FilterCriterion is then
 * set to null and the 5 best offers are then displayed again. The selected
 * proposal is also set to null which will clear panels OfferSubmit and OfferDetail.
 */
@Component({
  components: {
    'offer-card': OfferCard
  }
})
export default class OfferCardList extends Vue {
  @Action
  public applyProposalFilter!: (value: FilterCriterion) => void;
  @State((state) => state.simulator.proposals)
  private proposals!: SimulationFinancialProposal[] | null;
  @State((state) => state.simulator.selectedProposal)
  private selectedProposal!: SimulationFinancialProposal | null;
  @State((state) => state.simulator.proposalFilter)
  private proposalFilter!: FilterCriterion;
  @State((state) => state.simulator.offerPreSelection)
  private offerPreSelection: SimulatorOfferPreSelectionState;
  @State((state) => state.simulator.dueNumber)
  private dueNumber: number;

  @Mutation
  private increaseTimesUserSelectedOffer!: () => void;
  @Mutation
  private updateShowOfferSubmitPopup!: (value: boolean) => void;
  @Mutation
  private updateOfferPreSelectionLandingDone!: () => void;
  @Mutation
  private updateDueNumber!: (value) => void;
  @Action
  private setSelectedProposal!: (value: SimulationFinancialProposal) => void;
  @Getter
  private hasOfferPreSelected!: boolean;

  // ID of the OfferCard currently selected by user, set this to null to clear selection
  private offerSelected: number = null;

  private tagManagerService: TagManagerService;

  @Watch('proposalFilter')
  private onProposalFilterChange() {
    if (!this.proposalFilter) {
      this.updateDueNumber(null);
    }
  }

  private created() {
    this.tagManagerService = getTagManagerInstance();
    if (this.dueNumber) {
      this.applyProposalFilter({
        type: EFilterCriterionAttribute.TIMELINE,
        value: this.dueNumber
      });
    }
  }

  private mounted() {
    if (this.hasOfferPreSelected) {
      // Vue.nextTick fix issues with concurency between component's watchers/computed properties and mounted hook.
      // It delays research of pre-selected offer after filteredProposals getter is computed (so that it doesn't overrides
      // value defined in below lines)
      // TODO: refacto component in order to remove that delay
      Vue.nextTick(() => {
        this.initalizePreSelectedOffer();
      });
    }
  }

  private get filteredProposals(): Array<any> {
    // OfferCard collection has changed: reset inputs
    this.offerSelected = null;
    if (!this.proposals) {
      return [];
    } else if (!this.proposalFilter) {
      // Comment mweydert 09/06/20: offers will be sorted in a dedicated back-office (no work in front for this)
      return this.proposals.slice(0, 5);
    } else {
      // Sort proposals regarding filter (monthlyAmount or duration) and tagFree in case of equality
      const proposalsSortResult = sortSimulationProposal(this.proposals, this.proposalFilter);

      // Find index of proposal that is the closest to filter
      const nearestProposalIndex = this.findNearestProposal(this.proposalFilter, proposalsSortResult);

      return this.extractProposalsSubset(nearestProposalIndex, proposalsSortResult);
    }
  }

  private get filteredProposalsByDueNumber(): Array<any> {
    const proposal = this.proposalByDueNumber;
    if (this.dueNumber && proposal && proposal.length > 0) {
      return proposal;
    } else {
      return this.filteredProposals;
    }
  }

  private get proposalByDueNumber(): Array<number> {
    return this.filteredProposals.filter((proposal) => {
      const month = proposal.lastMonthlyAmountWithoutInsurance
        ? proposal.nbMonthlyPaymentWithoutInsurance + 1
        : proposal.nbMonthlyPaymentWithoutInsurance;
      return this.dueNumber && month ? month.toString() === this.dueNumber.toString() : false;
    });
  }

  /**
   * Extract a subset around a specified index.
   *
   * Extracts at two elements before and after element at specified index.
   * If not enough elements are avaible (e.g. close to boundaries) then
   * the maximum available elements are returned.
   */
  private extractProposalsSubset(nearestProposalIndex: number, proposals: SimulationFinancialProposal[]) {
    const lowestProposalIndex = nearestProposalIndex - 2;
    const highestProposalIndex = nearestProposalIndex + 3;

    return proposals.slice(
      lowestProposalIndex < 0 ? 0 : lowestProposalIndex,
      highestProposalIndex > proposals.length ? proposals.length : highestProposalIndex
    );
  }

  /**
   * Iterate over an array of SimulationFinancialProposal and compute distance to FilterCriterion.
   * The distance is measuring monthlyAmount or dueNumber according to FilterCriterion.type.
   *
   * This method returns the index of the element that has the lowest distance to filter.
   */
  private findNearestProposal(filter: FilterCriterion, proposals: SimulationFinancialProposal[]): number {
    return proposalSelected(filter, proposals, this.offerPreSelection.suggestedScaleCode);
  }

  /**
   * OfferCard.setValue() callback
   */
  private updateOfferSelected(value: number) {
    this.offerSelected = value;
    this.updateShowOfferSubmitPopup(true);
  }

  private initalizePreSelectedOffer(): void {
    if (this.filteredProposals && this.filteredProposals.length && !this.offerPreSelection.landingDone) {
      this.offerSelected = findIndexPreSelectedOffer(
        this.filteredProposals,
        this.offerPreSelection.suggestedDueNumber,
        this.offerPreSelection.suggestedScaleCode
      );
      this.updateOfferPreSelectionLandingDone();
    }
  }

  @Watch('selectedProposal')
  private onSelectedProposal() {
    if (!this.selectedProposal) {
      this.offerSelected = null;
    }
  }

  @Watch('offerSelected')
  private onOfferSelect() {
    if (this.offerSelected !== null && this.offerSelected !== null) {
      this.increaseTimesUserSelectedOffer();
      const selectedProposal = this.filteredProposals[this.offerSelected];
      // If independant simulator trigger a tag at each offer selection
      if (!Config.chainWithSubscription) {
        this.tagManagerService.tagIndependantSimulatorOffersSelection(this.$route, selectedProposal);
      }

      this.setSelectedProposal(selectedProposal);
    }
  }
}
</script>

<style scoped lang="scss">
.offer-card {
  &:not(:last-child) {
    margin-bottom: 7px;
  }

  &__list {
    max-width: $max-width-tablet;
    margin: 0 auto;
  }
}
</style>
