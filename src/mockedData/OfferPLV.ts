export const mockOfferPLV: any = {
  data: {
    id: 'crs_plv',
    sysType: 'crs_plv',
    screenOptions: {
      'legal-notices': {
        value:
          'https://recette.esigate.sofinco.fr/sites/partners/home.labelsAction.do?path=ln&partner=web-ikea&route=w2s',
        description: 'legal notices link'
      }
    },
    steps: [
      {
        pathId: 'homePlv',
        screenOptions: {},
        noReturn: false,
        routed: true,
        steps: [
          {
            pathId: 'pedagogicSteps',
            screenOptions: {
              'pedagogic-steps-key': { value: 'web2store.pedagogie.items', description: 'key of pedagogic steps' }
            },
            noReturn: false,
            routed: false,
            steps: []
          }
        ]
      },
      {
        pathId: 'subscription',
        screenOptions: {
          coBorrowerDisplayed: { value: false, description: 'whether the user can see the co-borrower' }
        },
        noReturn: false,
        routed: true,
        steps: [
          {
            pathId: 'informationBloc',
            screenOptions: {
              label: { value: 'souscription.menuNavigation.blocInformation', description: 'label key in Jahia' },
              type: { value: 'category', description: '' }
            },
            noReturn: false,
            routed: true,
            steps: [
              {
                pathId: 'name',
                screenOptions: {
                  label: { value: 'souscription.menuNavigation.votreNom', description: 'label key in Jahia' },
                  type: { value: 'form', description: '' }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'contact',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.vosCoordonnes',
                    description: 'label key in Jahia'
                  },
                  type: { value: 'form', description: '' }
                },
                noReturn: false,
                routed: true,
                steps: []
              }
            ]
          },
          {
            pathId: 'situationBloc',
            screenOptions: {
              label: { value: 'souscription.menuNavigation.blocSituation', description: 'label key in Jahia' },
              type: { value: 'category', description: '' }
            },
            noReturn: false,
            routed: true,
            steps: [
              {
                pathId: 'civility',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.votreCivilite',
                    description: 'label key in Jahia'
                  },
                  type: { value: 'form', description: '' }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'family',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.situationFamiliale',
                    description: 'label key in Jahia'
                  },
                  type: { value: 'form', description: '' }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'housing',
                screenOptions: {
                  label: { value: 'souscription.menuNavigation.votreAdresse', description: 'label key in Jahia' },
                  type: { value: 'form', description: '' }
                },
                noReturn: false,
                routed: true,
                steps: []
              }
            ]
          },
          {
            pathId: 'incomesBloc',
            screenOptions: {
              label: { value: 'souscription.menuNavigation.votreBudget', description: 'label key in Jahia' },
              type: { value: 'category', description: '' }
            },
            noReturn: false,
            routed: true,
            steps: [
              {
                pathId: 'employment',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.votreTravail',
                    description: 'label key in Jahia'
                  },
                  type: { value: 'form', description: '' }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'incomes',
                screenOptions: {
                  label: { value: 'souscription.menuNavigation.vosRevenus', description: 'label key in Jahia' },
                  type: { value: 'form', description: '' }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'charges',
                screenOptions: {
                  label: { value: 'souscription.menuNavigation.vosCharges', description: 'label key in Jahia' },
                  type: { value: 'form', description: '' }
                },
                noReturn: false,
                routed: true,
                steps: []
              }
            ]
          },
          {
            pathId: 'card',
            screenOptions: {
              preselected: { value: true, description: 'preselected card' },
              'card-code': { value: 'E', description: 'card code' },
              documentUrl: {
                value:
                  'https://recette.esigate.sofinco.fr/sites/partners/home.documentsAction.do?q6=web_ikea&p=/cartes/informations-carte.pdf',
                description: 'card informations document'
              },
              label: { value: 'souscription.menuNavigation.votreCarte', description: 'label key in Jahia' }
            },
            noReturn: false,
            routed: true,
            steps: [
              {
                pathId: 'advantages',
                screenOptions: {
                  'advantages-key': {
                    value: 'souscription.carte.avantages',
                    description: 'key of card advantages'
                  },
                  'advantages-length': { value: 6, description: 'length of card advantages' }
                },
                noReturn: false,
                routed: false,
                steps: []
              }
            ]
          },
          {
            pathId: 'insurance',
            screenOptions: {
              documentUrl: {
                value:
                  'https://recette.esigate.sofinco.fr/sites/partners/home.documentsAction.do?q6=web_ikea&p=/assurances/informations-assurance-rev.pdf',
                description: 'insurance informations document'
              },
              label: { value: 'souscription.menuNavigation.votreAssurance', description: 'label key in Jahia' }
            },
            noReturn: false,
            routed: true,
            steps: []
          },
          {
            pathId: 'summary',
            screenOptions: {
              label: { value: 'souscription.menuNavigation.ficheDialogue', description: 'label key in Jahia' }
            },
            noReturn: false,
            routed: true,
            steps: []
          },
          { pathId: 'loading', screenOptions: {}, noReturn: false, routed: true, steps: [] },
          {
            pathId: 'result',
            screenOptions: {
              label: { value: 'souscription.menuNavigation.reponsePrincipe', description: 'label key in Jahia' }
            },
            noReturn: false,
            routed: true,
            steps: []
          }
        ]
      }
    ],
    _links: {
      self:
        'https://int-gw-intranet-api.ca-cf.gca/applicationsPartnersConfiguration/v1/applications/creditPartner/partners/web_ikea/routes/w2s',
      parameters:
        'https://int-gw-intranet-api.ca-cf.gca/applicationsPartnersConfiguration/v1/applications/creditPartner/partners/web_ikea/routes/w2s/parameters/'
    }
  }
};
