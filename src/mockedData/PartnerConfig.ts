export const MockPartnerConfig = {
  data: {
    screenOptions: {
      'primary-color': {
        value: 'personnalisation.couleurs.primaire',
        description: 'primary color'
      },
      'messages-partner': {
        value: 'https://recette.esigate.sofinco.fr/sites/partners/home.labelsAction.do?partner=web-ikea',
        description: 'partner messages link'
      },
      'quaternary-color': {
        value: 'personnalisation.couleurs.quaternaire',
        description: 'quaternary color'
      },
      'secondary-color': {
        value: 'personnalisation.couleurs.secondaire',
        description: 'secondary color'
      },
      'resources-path': {
        value: 'https://recette.esigate.sofinco.fr/files/live/sites/partners/files/web_ikea/resources',
        description: 'resources path to use for icons, logos,...'
      },
      'tertiary-color': {
        value: 'personnalisation.couleurs.tertiaire',
        description: 'tertiary color'
      },
      'messages-default': {
        value: 'https://recette.esigate.sofinco.fr/sites/partners/home.labelsAction.do?partner=default',
        description: 'default messages link'
      }
    },
    _links: {
      self: 'https://rct-api.sofinco.fr/configuration/v1/applications/creditPartner/partners/web_ikea',
      parameters:
        'https://rct-api.sofinco.fr/configuration/v1/applications/creditPartner/partners/web_ikea/techOptions/',
      offers: 'https://rct-api.sofinco.fr/configuration/v1/applications/creditPartner/partners/web_ikea/offers/'
    }
  }
};
