export const MockNomenclature: any = {
  data: {
    areaList1: [
      {
        label: "Activités financières et d'assurance",
        alternativeLabel: null,
        code: '9_9'
      },
      {
        label: 'Activités spécialisées, scientifiques et techniques',
        alternativeLabel: null,
        code: '10_10'
      },
      {
        label: 'Activité de services',
        alternativeLabel: null,
        code: '14_14'
      },
      {
        label: 'Administration publique',
        alternativeLabel: null,
        code: '11_11'
      },
      {
        label: 'Agriculture, sylviculture et pêche',
        alternativeLabel: null,
        code: '1_1'
      },
      {
        label: 'Commerce',
        alternativeLabel: null,
        code: '5_5'
      },
      {
        label: 'Construction / Bâtiment',
        alternativeLabel: null,
        code: '4_4'
      },
      {
        label: 'Enseignement',
        alternativeLabel: null,
        code: '12_12'
      },
      {
        label: 'Hébergement / Tourisme et restauration',
        alternativeLabel: null,
        code: '7_7'
      },
      {
        label: 'Industrie',
        alternativeLabel: null,
        code: '2_2'
      },
      {
        label: 'Information et communication',
        alternativeLabel: null,
        code: '8_8'
      },
      {
        label: 'Production et distribution (électricité, gaz, eau...)',
        alternativeLabel: null,
        code: '3_3'
      },
      {
        label: 'Santé humaine et action sociale',
        alternativeLabel: null,
        code: '13_13'
      },
      {
        label: 'Transports et entreposage',
        alternativeLabel: null,
        code: '6_6'
      },
      {
        label: 'Autres activités',
        alternativeLabel: null,
        code: '15_15'
      }
    ],
    areaList2: [
      {
        label: "Activités financières et d'assurance",
        alternativeLabel: null,
        code: '9_9'
      },
      {
        label: 'Activités spécialisées, scientifiques et techniques',
        alternativeLabel: null,
        code: '10_10'
      },
      {
        label: 'Activité de services',
        alternativeLabel: null,
        code: '14_14'
      },
      {
        label: 'Administration publique',
        alternativeLabel: null,
        code: '11_11'
      },
      {
        label: 'Agriculture, sylviculture et pêche',
        alternativeLabel: null,
        code: '1_1'
      },
      {
        label: 'Commerce',
        alternativeLabel: null,
        code: '5_5'
      },
      {
        label: 'Construction / Bâtiment',
        alternativeLabel: null,
        code: '4_4'
      },
      {
        label: 'Enseignement',
        alternativeLabel: null,
        code: '12_12'
      },
      {
        label: 'Hébergement / Tourisme et restauration',
        alternativeLabel: null,
        code: '7_7'
      },
      {
        label: 'Industrie',
        alternativeLabel: null,
        code: '2_2'
      },
      {
        label: 'Information et communication',
        alternativeLabel: null,
        code: '8_8'
      },
      {
        label: 'Production et distribution (électricité, gaz, eau...)',
        alternativeLabel: null,
        code: '3_3'
      },
      {
        label: 'Santé humaine et action sociale',
        alternativeLabel: null,
        code: '13_13'
      },
      {
        label: 'Transports et entreposage',
        alternativeLabel: null,
        code: '6_6'
      },
      {
        label: 'Autres activités',
        alternativeLabel: null,
        code: '15_15'
      }
    ],
    areaList4: [
      {
        label: "Activités financières et d'assurance",
        alternativeLabel: null,
        code: '9_9'
      },
      {
        label: 'Activités spécialisées, scientifiques et techniques',
        alternativeLabel: null,
        code: '10_10'
      },
      {
        label: 'Activité de services',
        alternativeLabel: null,
        code: '14_14'
      },
      {
        label: 'Administration publique',
        alternativeLabel: null,
        code: '11_11'
      },
      {
        label: 'Agriculture, sylviculture et pêche',
        alternativeLabel: null,
        code: '1_1'
      },
      {
        label: 'Commerce',
        alternativeLabel: null,
        code: '5_5'
      },
      {
        label: 'Construction / Bâtiment',
        alternativeLabel: null,
        code: '4_4'
      },
      {
        label: 'Enseignement',
        alternativeLabel: null,
        code: '12_12'
      },
      {
        label: 'Hébergement / Tourisme et restauration',
        alternativeLabel: null,
        code: '7_7'
      },
      {
        label: 'Industrie',
        alternativeLabel: null,
        code: '2_2'
      },
      {
        label: 'Information et communication',
        alternativeLabel: null,
        code: '8_8'
      },
      {
        label: 'Production et distribution (électricité, gaz, eau...)',
        alternativeLabel: null,
        code: '3_3'
      },
      {
        label: 'Santé humaine et action sociale',
        alternativeLabel: null,
        code: '13_13'
      },
      {
        label: 'Transports et entreposage',
        alternativeLabel: null,
        code: '6_6'
      },
      {
        label: 'Autres activités',
        alternativeLabel: null,
        code: '15_15'
      }
    ],
    areaList6: [
      {
        label: "Activités financières et d'assurance",
        alternativeLabel: null,
        code: '9_9'
      },
      {
        label: 'Activités spécialisées, scientifiques et techniques',
        alternativeLabel: null,
        code: '10_10'
      },
      {
        label: 'Activité de services',
        alternativeLabel: null,
        code: '14_14'
      },
      {
        label: 'Administration publique',
        alternativeLabel: null,
        code: '11_11'
      },
      {
        label: 'Agriculture, sylviculture et pêche',
        alternativeLabel: null,
        code: '1_1'
      },
      {
        label: 'Commerce',
        alternativeLabel: null,
        code: '5_5'
      },
      {
        label: 'Construction / Bâtiment',
        alternativeLabel: null,
        code: '4_4'
      },
      {
        label: 'Enseignement',
        alternativeLabel: null,
        code: '12_12'
      },
      {
        label: 'Hébergement / Tourisme et restauration',
        alternativeLabel: null,
        code: '7_7'
      },
      {
        label: 'Industrie',
        alternativeLabel: null,
        code: '2_2'
      },
      {
        label: 'Information et communication',
        alternativeLabel: null,
        code: '8_8'
      },
      {
        label: 'Production et distribution (électricité, gaz, eau...)',
        alternativeLabel: null,
        code: '3_3'
      },
      {
        label: 'Santé humaine et action sociale',
        alternativeLabel: null,
        code: '13_13'
      },
      {
        label: 'Transports et entreposage',
        alternativeLabel: null,
        code: '6_6'
      },
      {
        label: 'Autres activités',
        alternativeLabel: null,
        code: '15_15'
      }
    ],
    areaList7: [
      {
        label: "Activités financières et d'assurance",
        alternativeLabel: null,
        code: '9_9'
      },
      {
        label: 'Activités spécialisées, scientifiques et techniques',
        alternativeLabel: null,
        code: '10_10'
      },
      {
        label: 'Activité de services',
        alternativeLabel: null,
        code: '14_14'
      },
      {
        label: 'Administration publique',
        alternativeLabel: null,
        code: '11_11'
      },
      {
        label: 'Agriculture, sylviculture et pêche',
        alternativeLabel: null,
        code: '1_1'
      },
      {
        label: 'Commerce',
        alternativeLabel: null,
        code: '5_5'
      },
      {
        label: 'Construction / Bâtiment',
        alternativeLabel: null,
        code: '4_4'
      },
      {
        label: 'Enseignement',
        alternativeLabel: null,
        code: '12_12'
      },
      {
        label: 'Hébergement / Tourisme et restauration',
        alternativeLabel: null,
        code: '7_7'
      },
      {
        label: 'Industrie',
        alternativeLabel: null,
        code: '2_2'
      },
      {
        label: 'Information et communication',
        alternativeLabel: null,
        code: '8_8'
      },
      {
        label: 'Production et distribution (électricité, gaz, eau...)',
        alternativeLabel: null,
        code: '3_3'
      },
      {
        label: 'Santé humaine et action sociale',
        alternativeLabel: null,
        code: '13_13'
      },
      {
        label: 'Transports et entreposage',
        alternativeLabel: null,
        code: '6_6'
      },
      {
        label: 'Autres activités',
        alternativeLabel: null,
        code: '15_15'
      }
    ],
    professionList1: [
      {
        label: "Cadres d'entreprise",
        alternativeLabel: null,
        code: '37_36'
      },
      {
        label: 'Contremaîtres, agents de maîtrise',
        alternativeLabel: null,
        code: '48_48'
      },
      {
        label: "Employés administratifs d'entreprise",
        alternativeLabel: null,
        code: '54_54'
      },
      {
        label: 'Employés de commerce',
        alternativeLabel: null,
        code: '55_55'
      },
      {
        label: 'Ouvriers',
        alternativeLabel: null,
        code: '60_60'
      },
      {
        label: 'Ouvriers agricoles',
        alternativeLabel: null,
        code: '69_69'
      },
      {
        label: 'Personnels des services directs aux particuliers',
        alternativeLabel: null,
        code: '56_56'
      },
      {
        label: 'Professions intermédiaires administratives et commerciales des entreprises',
        alternativeLabel: null,
        code: '46_46'
      },
      {
        label: "Professions intermédiaires de l'enseignement, de la santé et assimilés",
        alternativeLabel: null,
        code: '43_43'
      },
      {
        label: 'Professions libérales ou assimilés',
        alternativeLabel: null,
        code: '34_34'
      },
      {
        label: 'Techniciens',
        alternativeLabel: null,
        code: '47_47'
      }
    ],
    professionList2: [
      {
        label: 'Cadres de la fonction publique',
        alternativeLabel: null,
        code: '33_32'
      },
      {
        label: 'Employés de la fonction publique',
        alternativeLabel: null,
        code: '52_52'
      },
      {
        label: 'Ouvriers',
        alternativeLabel: null,
        code: '60_60'
      },
      {
        label: 'Policiers et militaires',
        alternativeLabel: null,
        code: '53_53'
      },
      {
        label: "Professions intermédiaires de l'enseignement, de la santé et assimilés",
        alternativeLabel: null,
        code: '43_43'
      },
      {
        label: 'Professions intermédiaires de la fonction publique',
        alternativeLabel: null,
        code: '45_45'
      },
      {
        label: 'Techniciens',
        alternativeLabel: null,
        code: '47_47'
      }
    ],
    professionList4: [
      {
        label: 'Agriculteurs exploitants',
        alternativeLabel: null,
        code: '10_10'
      },
      {
        label: 'Artisans',
        alternativeLabel: null,
        code: '21_21'
      },
      {
        label: 'Auto-entrepreneur',
        alternativeLabel: null,
        code: '29_21'
      },
      {
        label: "Chefs d'entreprise de 10 salariés ou plus",
        alternativeLabel: null,
        code: '23_23'
      },
      {
        label: 'Commerçants et assimilés',
        alternativeLabel: null,
        code: '22_22'
      },
      {
        label: 'Professions intellectuelles et artistique',
        alternativeLabel: null,
        code: '35_35'
      },
      {
        label: 'Professions intermédiaires commerciales',
        alternativeLabel: null,
        code: '46_46'
      },
      {
        label: 'Professions libérales',
        alternativeLabel: null,
        code: '31_31'
      }
    ],
    professionList6: [
      {
        label: 'Anciennes professions intermédiaires',
        alternativeLabel: null,
        code: '75_70'
      },
      {
        label: 'Anciens agriculteurs exploitants',
        alternativeLabel: null,
        code: '71_70'
      },
      {
        label: "Anciens artisans, commerçants, chefs d'entreprise",
        alternativeLabel: null,
        code: '72_70'
      },
      {
        label: 'Anciens cadres',
        alternativeLabel: null,
        code: '74_70'
      },
      {
        label: 'Anciens employés',
        alternativeLabel: null,
        code: '77_70'
      },
      {
        label: 'Anciens ouvriers',
        alternativeLabel: null,
        code: '78_70'
      }
    ],
    professionList7: [
      {
        label: 'Etudiant',
        alternativeLabel: null,
        code: '84_84'
      },
      {
        label: 'Rentier',
        alternativeLabel: null,
        code: '82_82'
      },
      {
        label: 'Sans emploi',
        alternativeLabel: null,
        code: '81_81'
      },
      {
        label: 'Sans profession',
        alternativeLabel: null,
        code: '85_85'
      }
    ],
    contractType: [
      {
        label: 'CDI',
        alternativeLabel: null,
        code: '0'
      },
      {
        label: 'CDD',
        alternativeLabel: null,
        code: '1'
      },
      {
        label: 'Intérim',
        alternativeLabel: null,
        code: '2'
      }
    ],
    statusPro: [
      {
        label: 'Salarié du privé',
        alternativeLabel: null,
        code: '1_1'
      },
      {
        label: 'Salarié du public',
        alternativeLabel: null,
        code: '2_2'
      },
      {
        label: 'Indépendant',
        alternativeLabel: null,
        code: '4_4'
      },
      {
        label: 'Retraité',
        alternativeLabel: null,
        code: '6_6'
      },
      {
        label: 'Autres',
        alternativeLabel: null,
        code: '7_7'
      }
    ],
    maritalStatus1: [
      {
        label: 'Célibataire',
        alternativeLabel: null,
        code: 'C_C'
      },
      {
        label: 'Divorcé(e)',
        alternativeLabel: null,
        code: 'D_D'
      },
      {
        label: 'Marié(e)',
        alternativeLabel: null,
        code: 'M_M'
      },
      {
        label: 'Pacsé(e)',
        alternativeLabel: null,
        code: 'P_M'
      },
      {
        label: 'Séparé(e)',
        alternativeLabel: null,
        code: 'S_S'
      },
      {
        label: 'En couple',
        alternativeLabel: null,
        code: 'U_U'
      },
      {
        label: 'Veuf(Veuve)',
        alternativeLabel: null,
        code: 'V_V'
      }
    ],
    maritalStatus2: [
      {
        label: 'Marié(e)',
        alternativeLabel: null,
        code: 'M_M'
      },
      {
        label: 'Pacsé(e)',
        alternativeLabel: null,
        code: 'P_M'
      },
      {
        label: 'En couple',
        alternativeLabel: null,
        code: 'U_U'
      }
    ],
    housingStateType: [
      {
        label: 'Locataire',
        alternativeLabel: null,
        code: 'L_L'
      },
      {
        label: 'Propriétaire avec prêt(s) en cours',
        alternativeLabel: null,
        code: 'C_C'
      },
      {
        label: 'Propriétaire sans remboursement',
        alternativeLabel: null,
        code: 'P_P'
      },
      {
        label: 'Logé par administration',
        alternativeLabel: null,
        code: 'A_A'
      },
      {
        label: 'Logé par employeur',
        alternativeLabel: null,
        code: 'E_E'
      },
      {
        label: 'Logé par conjoint propriétaire',
        alternativeLabel: null,
        code: 'J_L'
      },
      {
        label: 'Logé par parents ou enfants',
        alternativeLabel: null,
        code: 'F_F'
      },
      {
        label: 'Logé par un autre membre de la famille',
        alternativeLabel: null,
        code: 'T_S'
      },
      {
        label: 'Logé par un ami ou sous location',
        alternativeLabel: null,
        code: 'S_S'
      },
      {
        label: 'Autres',
        alternativeLabel: null,
        code: 'X_X'
      }
    ]
  }
};
