export const MockLegalMentions = {
  items: [
    {
      titre: 'Notice légale',
      items: [
        {
          titre: 'Exploitant du site Internet',
          contenu:
            'Le site Internet <a target="_blank" href="https://www.sofinco.fr">www.sofinco.fr</a> est exploité par CA Consumer Finance dont Sofinco est une marque. <br/><br/>\
      CA Consumer Finance est une Société anonyme (SA) au capital de 554 482 422 €, enregistré au RCS Evry sous le numéro SIREN 542 097 522. <br/><br/>\
      La SA CA Consumer Finance est un établissement de crédit autorisé à exercer ses activités par l\'Autorité de Contrôle Prudentiel et de résolution. <br/><br/>\
      Egalément intermédiaire d\'assurances, inscrite à l\'ORIAS (Organisme pour le Registre des Intermédiaires d\'Assurance) sous le n° 07 008 079 (consultable sur <a target="_blank" href="https://www.orias.fr">www.orias.fr</a>). <br/><br/>\
      La SA CA Consumer Finance est assujettie à la taxe sur la valeur ajoutée sous le numéro d\'identification FR 84 542 097 522. <br/><br/>\
      Le siège social de CA Consumer Finance est situé : <br/>\
      1 rue Victor Basch - CS 70001 <br/>\
      91068 Massy Cedex <br/><br/>\
      Téléphone : 01 60 76 36 36 - numéro non surtaxé - coût d\'un appel local en France métropolitaine <br/><br/>\
      Service commerciaux :  <br/><br/>\
      Adresse mail : <a href="mailto:contact.sofinco@sofinco.fr">contact.sofinco@sofinco.fr</a> <br/>\
      Formulaire de contact : <a target="_blank" href="https://www.sofinco.fr/contact/non-securise/contact-email.htm">écrire au webmaster du site www.sofinco.fr</a>'
        },
        {
          titre: 'Direction du site',
          contenu:
            'Directeur de la publication : Philippe Dumont - Directeur Général <br/>\
      Responsable de la communication : Didier Bourdonnais – Directeur Marketing et Communication<br/>\
      Coordonnées du Délégué à la Protection des Données : vous pouvez contacter le délégué à la protection des données personnelles par email : <a href="mailto:dpdcacf@ca-cf.fr">dpdcacf@ca-cf.fr</a> ou par courrier à l’adresse suivante :<br/>\
      CACF – Délégué à la protection des données personnelles<br/>\
      BP 50075<br/>\
      77213 AVON Cedex<br/>\
      Webmaster : Nicolas Debaets'
        },
        {
          titre: 'Hébergement et moyens techniques',
          contenu: `Le présent site Internet est hébergé par :<br/>\
      CA Consumer Finance<br/>\
      Direction des Systèmes d'Informations<br/>\
      1 rue Victor Basch – CS 70001<br/>\
      91068 MASSY Cedex<br/>\
      Téléphone : 01 60 76 36 36 - numéro non surtaxé`
        },
        {
          titre: `Droits d'auteur`,
          contenu: `Les éléments appartenant à CA Consumer Finance, tels que le site web, les bases de données, les marques, les dessins et modèles, les images, les textes, ainsi que l’identité graphique, sont la propriété exclusive du site ou lui ont été cédés.<br/><br/>\
      Le site doit être considéré comme un tout indissociable. Les informations y figurant sont réservées à un usage exclusivement personnel et ne peuvent être en tout ou partie ni reproduites, ni communiquées. L'ensemble des données (textes, sons ou images) figurant sur les pages de ce site est la propriété exclusive de CA Consumer Finance ou de ses partenaires.<br/>\
      Toute reproduction, représentation, extraction ou diffusion, à des fins autres que personnelles, en tout ou partie du contenu de ce site sur quelque support ou par tout procédé que ce soit est interdite. Le non-respect de cette interdiction constitue une contrefaçon au sens des articles L. 335-2 et suivants du Code de la propriété intellectuelle susceptible d'engager la responsabilité civile et pénale du contrefacteur. Il est strictement interdit au sens de l'article L.713-2 du Code de la propriété intellectuelle d'utiliser ou de reproduire notre marque et / ou notre logo, seuls ou associés, à quelque titre que ce soit, et notamment à des fins publicitaires, sans l'accord préalable écrit de CA Consumer Finance.<br/><br/>\
      Les demandes d’autorisation de reproduction d’un contenu doivent être adressées à <a href=\"mailto:presse@ca-cf.fr\">presse@ca-cf.fr</a>. La demande devra préciser le contenu visé ainsi que le site sur lequel ce dernier figurera.<br/><br/>\
      Une fois cette autorisation obtenue, la reproduction d’un contenu doit être assortie de :<br/>\
      •	l’insertion d’une icône représentant le site internet de CA Consumer Finance.<br/>\
      •	l’ajout de la mention : \"paru sur le site de CA Consumer Finance - www.ca-consumerfinance.com\". Cette mention pointera grâce à un lien hypertexte directement sur le contenu.<br/>\
      •	l’ajout en bas de chaque contenu de la mention \"Droits réservés\".<br/><br/>\
      En outre, les informations utilisées et autorisées ne doivent l’être qu’à des fins personnelles, associatives ou professionnelles, toute diffusion ou utilisation à des fins commerciales ou publicitaires étant exclues. D’une manière générale, la mise en place de \"liens profonds\" pointant directement sur le document en question devra être privilégiée, la reproduction de contenus demeurant le plus limité possible.<br/><br/>\
      L'utilisation du Site par l'Internaute ne lui confère aucun droit de propriété intellectuelle sur le Site et/ou son contenu.<br/><br/>\
      Aucune disposition des présentes « Informations Légales » ne pourra être interprétée comme concédant à l'Internaute un droit de quelque nature que ce soit sur les éléments protégés par la propriété intellectuelle, dont CA Consumer finance pourrait avoir la propriété ou le droit exclusif d'exploitation.`
        },
        {
          titre: 'Hypertexte',
          contenu: `L'éditeur du site web ne saurait être responsable de votre accès via les liens hypertextes mis en place dans le cadre du site web en direction d'autres ressources présentes sur le réseau internet. <br/> \
      Tout lien hypertexte avec ce site doit faire l'objet d'un courrier électronique à l'adresse : <a href=\"mailto:contact.sofinco@sofinco.fr\">contact.sofinco@sofinco.fr</a><br/> \
      L'éditeur du site se réserve la possibilité de s'opposer à ces liens hypertextes ou à proposer une alternative. En tout état de cause, l'éditeur du site se réserve le droit de remettre en cause toutes les autorisations expresses ou tacites. Dans ce cas, la société réalisatrice des liens hypertextes s'engage à supprimer ces hyperliens dès notification par courrier électronique de l'éditeur du site web. En aucun cas, une autorisation expresse ou tacite ne peut créer des droits acquis au profit de la société ayant mis un hyperlien vers le présent site Internet.`
        },
        {
          titre: "Responsabilité de l'éditeur",
          contenu: `L'éditeur du site web met tout en œuvre pour vous offrir des informations et/ou outils disponibles et vérifiés mais ne saurait être tenu pour responsable, d'une absence de disponibilité des informations et/ou de la présence d'un virus sur son site.\
      <br/>L’utilisateur reconnaît utiliser les informations fournies par le présent site sous sa responsabilité exclusive.\
      <br/>En cas d'interruption momentanée de service pour cause de maintenance, une fenêtre indique l'indisponibilité temporaire du site et mentionne la nécessité pour l'internaute de se connecter à un autre moment afin d'effectuer ses opérations.\
      <br/>En particulier, les résultats obtenus en utilisant les services de simulations et calculés sur la base des informations que vous donnez sont fournis à titre indicatif et revêtent une valeur non contractuelle.`
        },
        {
          titre: 'Conception et réalisation',
          contenu: 'Le présent site est réalisé avec le concours des agences : BETC, Biig, Gingerminds et SOPRA.'
        },
        {
          titre: 'Crédits photos',
          contenu: 'Les photos utilisées sur le site sont fournies par iStockphoto.'
        },
        {
          titre: 'Autres informations',
          contenu: `Afin de vous informer clairement sur les engagements pris en matière de protection des données personnelles et de sécurité, CA Consumer Finance tient à votre disposition une <a>Charte des Données personnelles et de Sécurité</a>. \
      <br/>Pour connaître les conditions d'utilisation du présent site, consultez les <a>Conditions générales d'utilisation du site.</a>`
        },
        {
          titre: 'Contactez-nous',
          contenu:
            'Afin de permettre à CA Consumer Finance, d\'apporter une réponse appropriée répondant au plus près à vos attentes, veuillez utiliser la procédure correspondante ci-après à la situation que vous rencontrez.\
      <br/>\
      <br/><b>- Si votre question porte sur le fonctionnement du site ou présente un caractère technique :</b> <a>Contactez le webmaster du site www.sofinco.fr</a>\
      <br/>\
      <br/><b>- Si votre question porte sur l\'exercice de vos droits liés à la réglementation sur les données personnelles :</b>\
      <br/>\
      <br/>Nous attirons votre attention sur le fait que les demandes présentant un caractère nominatif nécessitent de vérifier votre identité, afin de protéger votre sécurité et votre vie privée. Pour une telle demande, nous vous remercions de bien vouloir nous adresser votre question accompagnée d\'un titre d\'identité à l\'adresse de CA Consumer Finance mentionnée ci-après :\
      <br/>\
      <br/>Par voie postale :\
      <br/>CACF – Délégué à la protection des données personnelles\
      <br/>BP 50075\
      <br/>77213 AVON Cedex\
      <br/>\
      <br/>Par courrier électronique :\
      <br/><a href="mailto:dpdcacf@ca-cf.fr">dpdcacf@ca-cf.fr</a>\
      <br/>\
      <br/>> Le cas échéant, vous pouvez introduire une réclamation auprès de la CNIL :\
      <br/><a target="_blank" href="https://www.cnil.fr/fr/plaintes">https://www.cnil.fr/fr/plaintes</a>\
      <br/>\
      <br/><b>- Si vous souhaitez effectuer une réclamation, vous pouvez consulter notre charte des réclamations via le lien ci-dessous :</b>\
      <br/><a target="_blank" href="https://www.sofinco.fr/charte-reclamation.htm">https://www.sofinco.fr/charte-reclamation.htm</a>\
      <br/>\
      <br/><b>- Si vous souhaitez recourir à un dispositif de médiation :</b>\
      <br/>\
      <br/>Par voie postale à :\
      <br/>Monsieur le Médiateur de l\'ASF – 75854 Paris Cedex 17\
      <br/>\
      <br/>Pour plus d’informations sur l’activité du médiateur de l’ASF, rendez-vous sur :\
      <br/><a target="_blank" href="http://lemediateur.asf-france.com">http://lemediateur.asf-france.com/</a>\
      <br/>\
      <br/>Par voie postale à :\
      <br/>Monsieur le médiateur auprès de la FBF -CS 151 – 75422 Paris Cedex 09\
      <br/>\
      <br/>Pour plus d’informations sur l’activité du médiateur de la FBF, rendez-vous sur :\
      <br/>lemediateur.fbf.fr\
      <br/>\
      <br/><b>- Si vous souhaitez obtenir des informations supplémentaires sur nos offres, produits ou services :</b> <a>Posez votre question en cliquant ici</a>\
      <br/>\
      <br/>Vous pouvez également nous contacter au   \
      <br/>\
      <br/><b>- Si vous êtes client et que votre question porte sur un dossier en cours de remboursement :</b> <a>Posez votre question en cliquant ici</a>\
      <br/>\
      <br/>Ou bien connectez-vous à votre <a>espace client</a>\
      <br/>\
      <br/><b>- Si vous êtes client et que votre question porte sur une demande de crédit, l\'avancement de votre dossier :</b> <a>Posez votre question en cliquant ici</a>\
      <br/>\
      <br/>Ou bien connectez-vous à votre <a>espace client</a>\
      <br/>\
      <br/>Vous pouvez également envoyer un courrier à l\'adresse postale ci-dessous, en joignant à votre demande une copie de votre carte d\'identité :\
      <br/>\
      <br/>Agence Service Client\
      <br/>BP 50075\
      <br/>77213 AVON Cedex\
      <br/>\
      <br/><b>- Si votre question porte sur une demande de code d\'accès égaré ou perdu :</b>\
      <br/>\
      <br/>Les demandes présentant un caractère nominatif nécessitent de vérifier votre identité, afin de protéger votre sécurité et votre vie privée. Pour une telle demande, nous vous remercions de bien vouloir utiliser la procédure mise à votre disposition dans la page de connexion à l\'espace client, accessible depuis le lien ci-dessous.\
      <br/><a>Accédez au formulaire de demande de code d’accès</a>'
        },
        {
          titre: 'Loi applicable',
          contenu: 'Ce site est soumis à la loi française<br/> \
      Dernière mise à jour le 25/05/2018'
        }
      ]
    },
    {
      titre: 'Données personnelles et sécurité',
      items: [
        {
          titre: 'CHARTE DE DONNÉES PERSONNELLES ET SÉCURITÉ',
          contenu:
            'La présente Charte de protection des données à caractère personnel et de sécurité a pour objectif de vous informer des engagements et mesures pratiques pris par CA Consumer Finance afin de veiller au respect de vos données personnelles lorsque vous utilisez le présent site internet.<br/><br/>\
      Les informations recueillies sur ce site sont enregistrées dans un fichier informatisé par CA Consumer Finance, Responsable de Traitement, situé 1 rue Victor Basch à Massy 91300, dont vous pouvez contacter le Délégué à la protection des données à l’adresse :<br/><br/>\
      CACF – Délégué à la protection des données personnelles<br/>\
      BP 50075<br/>\
      77213 AVON Cedex<br/>\
      dpdcacf@ca-cf.fr'
        },
        {
          titre: 'DONNÉES COLLECTÉES ET FINALITÉ DE LA COLLECTE',
          contenu: `La consultation du présent site peut être effectuée de façon anonyme. Dans ce contexte, l'utilisateur peut sans identification préalable le consulter et obtenir toutes les informations sur les offres commercialisées, le cas échéant via un simulateur.<br/><br/>\
      Lorsque des données à caractère personnel sont collectées directement auprès de l’internaute, cette collecte est toujours effectuée de manière loyale et informative grâce à la présence de mentions informatives.<br/><br/>\
      Etant précisé qu’il faut entendre par données personnelles, toute information identifiant directement ou indirectement une personne physique (ex. nom, no d’immatriculation, no de téléphone, photographie, date de naissance, commune de résidence, empreinte digitale...).<br/><br/>\
      CA Consumer Finance veille à ce que les données à caractère personnel demandées à l'utilisateur soient proportionnées et nécessaires à la finalité poursuivie par la collecte.<br/><br/>\
      Des données à caractère personnel sont notamment collectées directement auprès de l'internaute, avec son accord, lorsque ce dernier souhaite aller plus loin qu'une simulation afin de bénéficier d'un service personnalisé, ou réaliser une demande particulière.<br/><br/>\
      Tel est le cas, par exemple, lorsque ce dernier sollicite une demande immédiate et personnalisée, souhaite accéder à l'espace client qui est mis à sa disposition ou demander la communication d'un mot de passe égaré.<br/><br/>\
      Les données à caractère personnel collectées permettent aux services de CA Consumer Finance de prendre en compte chaque situation individuelle.<br/><br/>\
      <b>Aussi, lorsque vos données personnelles sont collectées dans le cadre d’un formulaire de souscription à une offre de contrat de crédit.</b> Le formulaire vise à collecter des données personnelles vous concernant à l’occasion de l’étude d’une demande de financement, de son octroi ainsi qu’ultérieurement, dans le cadre de l’exécution de votre contrat de crédit.<br/><br/>\
      Les champs suivis d’un astérisque sont obligatoires. A défaut, votre demande de financement ne pourra pas être traitée.<br/><br/>\
      a) Catégories des Données Personnelles<br/><br/>\
      Ces données sont relatives à votre identité et à votre identification, à votre vie personnelle, à votre situation économique et financière, aux personnes qui vous sont économiquement liées, à vos données de connexion, ainsi que, le cas échéant, à votre caution.<br/><br/>\
      b) Traitements et Finalités<br/><br/>\
      Elles sont recueillies dans le cadre de mesures précontractuelles et contractuelles, afin d’assurer l’étude et l’octroi de votre demande de crédit, ainsi que dans le cadre de l’exécution de votre contrat de crédit, afin d’assurer la gestion, le recouvrement du crédit, votre relation commerciale avec le Responsable de Traitement et, plus généralement, pour prendre en compte vos demandes relatives au crédit.<br/><br/>\
      Dans l’intérêt légitime du Responsable de Traitement, vos données personnelles seront analysées par un service centralisé, sans qu’il ne soit constitué aucun fichier spécifique, pour détecter toute incohérence, au moment de l’étude de votre demande de crédit.<br/><br/>\
      Vos données feront l’objet d’une prise de décision automatisée, y compris le profilage, nécessaire à la conclusion du contrat afin d’évaluer le risque de défaut de remboursement attaché à votre demande de crédit en vue d’apporter une aide à l’instruction de votre demande. Ce dernier permet le cas échéant de donner un avis favorable à votre demande sous la seule réserve de productions des pièces justificatives. Dans le cas contraire, vous pouvez présenter vos observations sur votre situation financière personnelle à un agent habilité à procéder à un réexamen.<br/><br/>\
      Dans le cadre de ses obligations légales, le Responsable de Traitement sera amené à consulter le FICP (Fichier national des Incidents de remboursement des Crédits aux Particuliers) au moment de l’octroi, ainsi que le cas échéant de toute augmentation de capital, lors de la reconduction annuelle du contrat, ainsi que lors de la vérification triennale de votre solvabilité. Conformément à ses obligations légales, CA Consumer Finance sera tenu de procéder à une inscription sur le FICP en cas d’incident de paiement caractérisé.<br/><br/>\
      Pour satisfaire aux obligations légales et réglementaires qui incombent au Responsable de Traitement, des Données vous concernant pourront être transmises sur demande aux autorités de contrôle, de tutelle et judiciaires légalement habilitées, notamment dans le cadre de la lutte contre le blanchiment des capitaux et le financement du terrorisme. Elles pourront également être utilisées au soutien de toute poursuite, plainte, ou autre procédure judiciaire ou extra-judiciaire concernant un litige entre vous et le Responsable de Traitement, et notamment pour la défense des droits du Prêteur en justice dictée par son intérêt légitime.<br/><br/>\
      Par ailleurs, en l’absence d’opposition de votre part, le Responsable de Traitement ou ses enseignes partenaires pourront communiquer des offres commerciales, par voie postale ou téléphonique. Des offres commerciales portant sur des services analogues à ceux fournis au titre du présent contrat pourront être proposées par CA Consumer Finance via des communications électroniques.<br/><br/>\
      Avec votre accord exprès, vous pourrez également recevoir d'autres offres commerciales de CA Consumer Finance ou de ses enseignes partenaires par voie électronique.<br/><br/>\
      Si vous ne souhaitez pas faire l'objet de prospection, ou si vous souhaitez exercer votre droit d'opposition à quelque titre que ce soit, soit immédiatement, soit, à tout moment, vous pouvez le faire en contactant le Délégué à la protection des données personnelles.<br/><br/>\
      Nous attirons votre attention sur le fait que les demandes présentant un caractère nominatif nécessitent de vérifier votre identité, afin de protéger votre sécurité et votre vie privée. Pour une telle demande, nous vous remercions de bien vouloir nous adresser votre question accompagnée d'un titre d'identité à l'adresse de CA Consumer Finance mentionnée ci-après :<br/><br/>\
      Par voie postale :<br/><br/>\
      CACF – Délégué à la protection des données personnelles<br/><br/>\
      BP 50075<br/><br/>\
      77213 AVON Cedex<br/><br/>\
      Par courrier électronique :<br/><br/>\
      <a href=\"mailto:dpdcacf@ca-cf.fr\">dpdcacf@ca-cf.fr</a><br/><br/>\
      Vous pouvez aussi vous inscrire à la liste d’opposition au démarchage téléphonique Bloctel, <a target=\"_blank\" href=\"https://www.bloctel.gouv.fr\">www.bloctel.gouv.fr</a>, si vous ne souhaitez plus être démarché téléphoniquement par des professionnels avec lesquels vous n’avez pas de relation contractuelle en cours.`
        },
        {
          titre: 'DONNÉES COLLECTÉES ET DURÉE DE CONSERVATION',
          contenu:
            'CA Consumer Finance conservera vos données personnelles pour les durées suivantes :<br/><br/>\
      •	l’ensemble des données personnelles collectées auprès de l’utilisateur, client, seront conservées par le Responsable de Traitement pendant une durée correspondant à celle de la relation précontractuelle et/ou contractuelle, augmentée des délais légaux de conservation, de prescription et d’épuisement des voies de recours auxquels il est tenu;<br/>\
      •	l’ensemble des données personnelles collectées auprès de l’utilisateur, prospect, seront conservées pendant 3 années à compter soit de leur collecte soit du dernier contact émanant de l’internaute (par exemple demande d’information sur un produit, un clic sur un lien hypertexte contenu dans un courriel) ;<br/>\
      •	pour les cookies, leurs durées de conservation est de 13 mois à compter de leur installation sur votre navigateur pour les cookies publicitaires et le temps de votre session sur notre site pour les cookies techniques.<br/><br/>\
      À l’issue de ces délais, les données personnelles seront soit supprimées soit anonymisées et utilisées à des fins historiques, d’études et de statistiques.'
        },
        {
          titre: 'DESTINATAIRES DES DONNÉES COLLECTÉES',
          contenu: `En qualité d'établissement de crédit, CA Consumer Finance s'engage à respecter son obligation de confidentialité conformément aux lois et règlements en vigueur relatifs au secret professionnel.<br/><br/>\
      Les informations concernant l'utilisateur sont destinées aux services de CA Consumer Finance, notamment à l'agence locale de CA Consumer Finance en charge du suivi et du traitement de votre dossier. Cette dernière se situe dans la zone géographique de votre lieu de résidence.<br/><br/>\
      Dans son intérêt légitime, le Responsable de Traitement pourra, sauf opposition de votre part, partager avec les établissements de crédit et les sociétés de financement, membres de son groupe de Partenaires, qui lui sont associés en participation ou en capital (liste sur simple demande), les données couvertes par le secret bancaire auquel ils sont tous tenus, en ce compris les Données recueillies soit antérieurement, soit à l'occasion de la constitution du présent dossier ainsi que de son traitement et son exécution, et ce aux fins d'étude et de gestion du dossier, prévention des fraudes et des impayés, recouvrement des créances.<br/><br/>\
      Vos Données pourront également être communiquées par CA Consumer Finance, : <br/><br/>\
      - aux sous-traitants qui lui fournissent des services dans le cadre notamment des encaissements, du recouvrement, de l'hébergement, de la vente et de l'après-vente, et/ou des vérifications relatives à la lutte anti-blanchiment et contre le financement du terrorisme (notamment le dispositif LAB-FT) conformément à l’intérêt légitime du Responsable de Traitement.<br/><br/>\
      - à un établissement de crédit, une société de financement, un organisme de titrisation tiers, un auditeur ou une agence de notation dans le cadre d’une cession de votre contrat de crédit ou de l’ensemble des créances nées de votre contrat de crédit, et ce dans l’intérêt légitime du Responsable de Traitement, tels que pour la défense de ses droits et intérêts, le recouvrement de ses créances, le refinancement ou la gestion de ses risques via la titrisation, sous réserve des dispositions applicables.<br/><br/>\
      Vos données personnelles sont stockées en France mais peuvent également être transférées à des prestataires ou sous-traitants dans un pays non membre de l’Union Européenne, notamment à des sociétés affiliées à CA Consumer Finance, situées au Maroc et en Inde. Dans ce cas, vous serez préalablement informé du transfert de vos données, des destinataires et des finalités. En cas d’absence de décision d’adéquation le transfert s’effectue par des garanties appropriées ou adaptées, telles que la mise en place de contrats de transferts de données reprenant les clauses contractuelles types publiées par la Commission européenne, seront mises en place.`
        },
        {
          titre: 'INFORMATIONS SUR LES COOKIES ET ASSIMILÉS',
          contenu: `<b>QU'EST-CE QU'UN COOKIE ?</b><br/><br/>\
      Lors de la consultation de notre site des informations relatives à la navigation de votre terminal (ordinateur, tablette, smartphone, etc.) sur notre site, sont susceptibles d'être enregistrées dans des fichiers \"Cookies\" installés sur votre terminal, sous réserve des choix que vous auriez exprimés concernant les Cookies et que vous pouvez modifier à tout moment. Seul l'émetteur d'un cookie est susceptible de lire ou de modifier des informations qui y sont contenues.<br/><br/>\
      Le terme de \"Cookie\" recouvre notamment :<br/><br/>\
      •	les cookies http,<br/><br/>\
      •	les cookies \"flash\",<br/><br/>\
      •	le résultat du calcul d'empreinte dans le cas du \" fingerprinting \" (calcul d'un identifiant unique de la machine basée sur des éléments de sa configuration à des fins de traçage),<br/><br/>\
      •	les pixels invisibles ou \" web bugs \",<br/><br/>\
      •	tout autre identifiant généré par un logiciel ou un système d'exploitation, par exemple.<br/><br/>\
      L'enregistrement d'un cookie dans un terminal est subordonné à la volonté de l'utilisateur du Terminal, que celui-ci peut exprimer et modifier à tout moment et gratuitement à travers les choix qui lui sont offerts par son logiciel de navigation.<br/><br/>\
      Si vous avez accepté dans votre logiciel de navigation l'enregistrement de cookies dans votre Terminal, les cookies intégrés dans les pages et contenus que vous avez consultés pourront être stockés temporairement dans un espace dédié de votre Terminal. Ils y seront lisibles uniquement par leur émetteur.<br/><br/>\
      Si vous refusez l'enregistrement de cookies dans votre terminal, ou si vous supprimez ceux qui y sont enregistrés, vous ne pourrez plus bénéficier d'un certain nombre de fonctionnalités qui sont néanmoins nécessaires pour naviguer dans certains espaces de notre site. Tel serait le cas si vous tentiez d'accéder à nos contenus ou services qui nécessitent de vous identifier. Tel serait également le cas lorsque nous -ou nos prestataires- ne pourrions pas reconnaître, à des fins de compatibilité technique, le type de navigateur utilisé par votre terminal.<br/><br/>\
      Le cas échéant, nous déclinons toute responsabilité pour les conséquences liées au fonctionnement dégradé de nos services résultant de l'impossibilité pour nous d'enregistrer ou de consulter les cookies nécessaires à leur fonctionnement et que vous auriez refusés ou supprimés.<br/><br/>\
      <b>A QUOI SERVENT LES COOKIES QUE NOUS EMETTONS SUR NOTRE SITE ?</b><br/><br/>\
      Lorsque vous vous connectez à notre site nous pouvons être amenés, sous réserve de vos choix, à installer divers cookies dans votre terminal nous permettant de reconnaître le navigateur de votre terminal pendant la durée de validité du cookie concerné. Les Cookies que nous émettons sont utilisés aux fins décrites ci-dessous, sous réserve de vos choix.<br/><br/>\
      Les cookies que nous émettons nous permettent :<br/><br/>\
      •	d'établir des statistiques et volumes de fréquentation et d'utilisation des diverses éléments composant notre site (rubriques et contenus visités, parcours), nous permettant d'améliorer l'intérêt et l'ergonomie de nos services ;<br/><br/>\
      •	d'adapter la présentation de notre site aux préférences d'affichage de votre terminal (langue utilisée, résolution d'affichage, système d'exploitation utilisé, etc...) lors de vos visites sur notre site, selon les matériels et les logiciels de visualisation ou de lecture que votre terminal comporte ;<br/><br/>\
      •	de mémoriser des informations relatives à un formulaire que vous avez rempli sur notre site (inscription ou accès à votre compte) ou à des produits, services ou informations que vous avez choisis sur notre site ;<br/><br/>\
      •	de vous donner accès à des espaces réservés et personnels de notre site tels que votre Espace Client, grâce à des identifiants ou des données que vous nous avez éventuellement antérieurement confiés ;<br/><br/>\
      •	de mettre en œuvre des mesures de sécurité, par exemple lorsqu'il vous est demandé de vous connecter à nouveau à un contenu ou à un service après un certain laps de temps ;<br/><br/>\
      •	de comptabiliser le nombre total de publicités affichées par nos soins sur nos espaces publicitaires, d'identifier ces publicités, leur nombre d'affichages respectifs, le nombre d'utilisateurs ayant cliqué sur chaque publicité et, le cas échéant, les actions ultérieures effectuées par ces utilisateurs sur les pages auxquelles mènent ces publicités, afin de calculer les sommes dues aux acteurs de la chaîne de diffusion publicitaire (agence de communication, régie publicitaire, site/support de diffusion) et d'établir des statistiques;<br/><br/>\
      •	d'adapter les contenus publicitaires affichés sur notre site par nos espaces publicitaires ou les contenus publicitaires que vous êtes susceptible de recevoir selon votre navigation sur notre site et/ou en fonction des données personnelles que vous nous avez fournies;<br/><br/>\
      <b>À QUOI SERVENT LES COOKIES ÉMIS SUR NOTRE SITE PAR DES TIERS ?</b><br/><br/>\
      L'émission et l'utilisation de cookies par des tiers, sont soumises aux politiques de protection de la vie privée de ces tiers. Nous vous informons de l'objet des cookies dont nous avons connaissance et des moyens dont vous disposez pour effectuer des choix à l'égard de ces cookies.<br/><br/>\
      Il existe deux dispositifs :<br/><br/>\
      1 - Via des contenus de tiers diffusés dans nos espaces publicitaires<br/><br/>\
      Les contenus publicitaires (graphismes, animations, vidéos, etc..) diffusés dans nos espaces publicitaires sont susceptibles de contenir des Cookies émis par des tiers : soit l'annonceur à l'origine du contenu publicitaire concerné, soit une société tierce à l'annonceur (agence conseil en communication, société de mesure d'audience, prestataire de publicité ciblée, etc.…), qui a associé un cookie au contenu publicitaire d'un annonceur.<br/><br/>\
      Le cas échéant, les cookies émis par ces tiers peuvent leur permettre, pendant la durée de validité de ces cookies :<br/><br/>\
      •	de comptabiliser le nombre d'affichages des contenus publicitaires diffusés via nos espaces publicitaires, d'identifier les publicités ainsi affichées, le nombre d'utilisateurs ayant cliqué sur chaque publicité, leur permettant de calculer les sommes dues de ce fait et d'établir des statistiques ;<br/><br/>\
      •	de reconnaître votre terminal lors de sa navigation ultérieure sur tout autre site ou service sur lequel ces annonceurs ou ces tiers émettent également des cookies et, le cas échéant, d'adapter ces sites et services tiers ou les publicités qu'ils diffusent, à la navigation de votre terminal dont ils peuvent avoir connaissance.<br/><br/>\
      2- Du fait d'applications tierces intégrées à notre site<br/><br/>\
      Nous sommes susceptibles d'inclure sur notre site, des applications informatiques émanant de tiers, qui vous permettent notamment de partager des contenus de notre Site avec d'autres personnes ou de faire connaître à ces autres personnes votre consultation ou votre opinion concernant un contenu de notre site/application.<br/><br/>\
      Tel est notamment le cas des boutons \"Partager\", \"J'aime\", issus de réseaux sociaux tels que \"Facebook\", \"Twitter\", etc.<br/><br/>\
      Le réseau social fournissant un tel bouton applicatif est susceptible de vous identifier grâce à ce bouton, même si vous n'avez pas utilisé ce bouton lors de votre consultation de notre site. En effet, ce type de bouton applicatif peut permettre au réseau social concerné de suivre votre navigation sur notre site, du seul fait que votre compte au réseau social concerné était activé sur votre terminal (session ouverte) durant votre navigation sur notre site.<br/><br/>\
      Nous n'avons aucun contrôle sur le processus employé par les réseaux sociaux pour collecter des informations relatives à votre navigation sur notre site et associées aux données personnelles dont ils disposent. Nous vous invitons à consulter les politiques de protection de la vie privée de ces réseaux sociaux afin de prendre connaissance des finalités d'utilisation, notamment publicitaires, des informations de navigation qu'ils peuvent recueillir grâce à ces boutons applicatifs. Ces politiques de protection doivent notamment vous permettre d'exercer vos choix auprès de ces réseaux sociaux, notamment en paramétrant vos comptes d'utilisation de chacun de ces réseaux.<br/><br/>\
      Tel est le cas également de iAdvize, un service de conversation instantanée intégré à notre site. iAdvize utilise des cookies et des scripts JavaScript, qui sont des fichiers placés et exécutés sur l'ordinateur de l'internaute pour mesurer la navigation en temps réel, identifier l'utilisateur de la messagerie instantanée Clic to Tchat et historiser ses passages de connexion sur le site Sofinco.fr. Les données générées par les cookies concernant l'utilisation du site Sofinco.fr (y compris votre adresse IP) seront transmises et stockées par iAdvize sur ses serveurs. iAdvize utilisera ces informations dans le but d'évaluer l'utilisation de la messagerie instantanée Clic to Tchat sur le site sofinco.fr, de compiler des rapports sur l'activité de ce service sur sofinco.fr. iAdvize est susceptible de communiquer ces données à des tiers en cas d'obligation légale ou lorsque ces tiers traitent des données pour le compte d'iAdvize.<br/><br/>\
      <b>A QUOI SERVENT LES COOKIES ACCOMPAGNANT NOS CONTENUS PUBLICITAIRES DIFFUSES PAR DES TIERS ?</b><br/><br/>\
      Lorsque vous accédez à un site/application contenant des espaces publicitaires diffusant une de nos annonces publicitaires, cette annonce est susceptible de contenir un cookie. Ce cookie est susceptible, sous réserve de vos choix, d'être enregistré dans votre terminal et de nous permettre de reconnaître le navigateur de votre terminal pendant la durée de validité du cookie concerné.<br/><br/>\
      Les Cookies intégrés à nos annonces publicitaires diffusées par des tiers sont utilisés aux fins décrites ci-dessous, sous réserve de vos choix, qui résultent des paramètres de votre logiciel de navigation utilisé lors de votre visite de notre site, que vous pouvez exprimer à tout moment.<br/><br/>\
      Les Cookies que nous émettons nous permettent, si votre terminal a permis leur enregistrement, selon vos choix :<br/><br/>\
      •	de comptabiliser le nombre d'affichages et d'activations de nos contenus publicitaires diffusés sur des sites/applications de tiers, d'identifier ces contenus et ces sites/applications, de déterminer le nombre d'utilisateurs ayant cliqué sur chaque contenu ;<br/><br/>\
      •	de calculer les sommes dues aux acteurs de la chaîne de diffusion publicitaire (agence de communication, régie publicitaire, site/support de diffusion) et d'établir des statistiques ;<br/><br/>\
      •	d'adapter la présentation du site auquel mène un de nos contenus publicitaires, selon les préférences d'affichage de votre terminal (langue utilisée, résolution d'affichage, système d'exploitation utilisé, etc) lors de vos visites sur notre Site et selon les matériels et les logiciels de visualisation ou de lecture que votre terminal comporte ;<br/><br/>\
      •	de suivre la navigation ultérieure effectuée par votre terminal sur des sites/applications ou sur d'autres contenus publicitaires.<br/><br/>\
      <b>QUEL EST L'INTERET DE VOIR S'AFFICHER DES PUBLICITES ADAPTEES A VOTRE NAVIGATION ?</b><br/><br/>\
      Notre objectif est de vous présenter les publicités les plus pertinentes possibles. A cette fin, la technologie des cookies permet de déterminer en temps réel quelle publicité afficher sur un terminal, en fonction de sa navigation récente sur un(e) ou plusieurs sites ou applications.<br/><br/>\
      Votre intérêt pour les contenus publicitaires qui sont affichés sur votre terminal lorsque vous consultez un site, détermine souvent les ressources publicitaires de celui-ci lui permettant d'exploiter ses services, souvent fournis aux utilisateurs à titre gratuit. Vous préférez sans doute voir s'afficher des publicités qui correspondent à ce qui vous intéresse plutôt que les publicités qui n'ont aucun intérêt pour vous. De même, les annonceurs qui souhaitent voir leurs publicités diffusées, sont intéressés par l'affichage de leurs offres auprès d'utilisateurs susceptibles d'être les plus intéressés par celles-ci.<br/><br/>\
      <b>QUEL EST L'INTERET DE RECEVOIR DES PUBLICITES ADAPTEES SUITE A VOTRE NAVIGATION ?</b><br/><br/>\
      Nous sommes susceptibles d'adapter les offres et publicités qui vous sont destinées à des informations relatives à la navigation de votre terminal sur notre site ou au sein de sites ou de services édités par des tiers et sur lesquels nous émettons des cookies.<br/><br/>\
      Dans la mesure où vous nous avez fourni des données personnelles vous concernant, notamment vos coordonnées électroniques, lors de votre inscription ou de votre accès à l'un de nos services, nous sommes susceptibles, sous réserve de votre accord, d'associer des informations de navigation relatives à votre terminal, traitées par les cookies que nous émettons, avec vos données personnelles afin de vous adresser, par exemple, des prospections électroniques ou d'afficher sur votre terminal, au sein d'espaces publicitaires contenant des cookies que nous émettons, des publicités personnalisées qui vous sont spécifiquement destinées, susceptibles de vous intéresser personnellement.<br/><br/>\
      Vous pourrez à tout moment nous demander de ne plus recevoir de publicités ou de prospections adaptées aux informations de navigation de votre terminal, en nous contactant directement et gratuitement, ou au moyen du lien de désinscription inclus dans toute prospection que nous serions susceptible de vous adresser par courrier électronique. Le cas échéant les publicités que vous pourriez continuer à recevoir, sauf opposition de votre part exercée auprès de nous, ne seront plus adaptées à la navigation de votre terminal.<br/><br/>\
      <b>QUE SE PASSE-T-IL SI VOUS PARTAGEZ L'UTILISATION DE VOTRE TERMINAL AVEC D'AUTRES PERSONNES ?</b><br/><br/>\
      Si votre terminal est utilisé par plusieurs personnes et lorsqu'un même terminal dispose de plusieurs logiciels de navigation, nous ne pouvons pas nous assurer de manière certaine que les services et publicités destinés à votre terminal correspondent bien à votre propre utilisation de ce terminal et non à celle d'un autre utilisateur de ce terminal.<br/><br/>\
      Le cas échéant, le partage avec d'autres personnes de l'utilisation de votre terminal et la configuration des paramètres de votre navigateur à l'égard des cookies, relèvent de votre libre choix et de votre responsabilité.<br/><br/>\
      <b>COMMENT EFFECTUER VOS CHOIX CONCERNANT LES COOKIES ?</b><br/><br/>\
      Plusieurs possibilités vous sont offertes pour gérer les cookies. Tout paramétrage que vous pouvez entreprendre sera susceptible de modifier votre navigation sur Internet et vos conditions d'accès à certains services nécessitant l'utilisation de Cookies.<br/><br/>\
      Vous pouvez faire le choix à tout moment d'exprimer et de modifier vos souhaits en matière de cookies, par les moyens décrits ci-dessous.<br/><br/>\
      Les choix qui vous sont offerts par votre logiciel de navigation<br/><br/>\
      Vous pouvez configurer votre logiciel de navigation de manière à ce que des cookies soient enregistrés dans votre terminal ou, au contraire, qu'ils soient rejetés, soit systématiquement, soit selon leur émetteur. Vous pouvez également configurer votre logiciel de navigation de manière à ce que l'acceptation ou le refus des cookies vous soient proposés ponctuellement, avant qu'un cookie soit susceptible d'être enregistré dans votre terminal. Pour plus d'informations, consultez la rubrique ci-dessous \"Comment exercer vos choix, selon le navigateur que vous utilisez ?\"<br/><br/>\
      <b>COMMENT EXERCER VOS CHOIX, SELON LE NAVIGATEUR QUE VOUS UTILISEZ ?</b><br/><br/>\
      Pour la gestion des cookies et de vos choix, la configuration de chaque navigateur est différente. Elle est décrite dans le menu d'aide de votre navigateur, qui vous permettra de savoir de quelle manière modifier vos souhaits en matière de cookies.<br/><br/>\
      Pour Internet Explorer™ : <a>Cliquez ici</a><br/><br/>\
      Pour Chrome™ : <a>Cliquez ici</a><br/><br/>\
      Pour Firefox™ : <a>Cliquez ici</a><br/><br/>\
      Pour Opera™ : <a>Cliquez ici</a><br/><br/>\
      etc.<br/><br/>\
      <b>COMMENT EXERCER VOS CHOIX AUPRES DE PLATEFORMES INTERPROFESSIONNELLES ?</b><br/><br/>\
      Vous pouvez vous connecter au site Youronlinechoices, proposé par les professionnels de la publicité digitale regroupés au sein de l'association européenne EDAA (European Digital Advertising Alliance) et géré en France par l'Interactive Advertising Bureau France.<br/><br/>\
      Vous pourrez ainsi connaître les entreprises inscrites à cette plate-forme et qui vous offrent la possibilité de refuser ou d'accepter les cookies utilisés par ces entreprises pour adapter à vos informations de navigation les publicités susceptibles d'être affichées sur votre terminal : http://www.youronlinechoices.com/fr/controler-ses-cookies/ .<br/><br/>\
      Cette plate-forme européenne est partagée par des centaines de professionnels de la publicité sur Internet et constitue une interface centralisée vous permettant d'exprimer votre refus ou votre acceptation des cookies susceptibles d'être utilisés afin d'adapter à la navigation de votre terminal les publicités susceptibles d'y être affichées. Notez que cette procédure n'empêchera pas l'affichage de publicités sur les sites Internet que vous visitez. Elle ne bloquera que les technologies qui permettent d'adapter des publicités à vos centres d'intérêts.<br/><br/>\
      <b>Les cookies \"Flash\"© de \"Adobe Flash Player\"™</b><br/><br/>\
      \"Adobe Flash Player\"™ est une application informatique qui permet le développement rapide des contenus dynamiques utilisant le langage informatique \"Flash\". Flash (et les applications de même type) mémorise les paramètres, les préférences et l'utilisation de ces contenus grâce à une technologie similaire aux cookies. Toutefois, \"Adobe Flash Player\"™ gère ces informations et vos choix via une interface différente de celle fournie par votre logiciel de navigation.<br/><br/>\
      Dans la mesure où votre terminal serait susceptible de visualiser des contenus développés avec le langage Flash, nous vous invitons à accéder à vos outils de gestion des cookies Flash, directement depuis le site http://www.adobe.com/fr/.`
        },
        {
          titre: 'VOS DROITS',
          contenu: `<b>Une information licite, loyale et transparente</b><br/><br/>\
      <b>Lorsqu'il est procédé à une collecte de données à caractère personnel par voie de questionnaire</b>, l'utilisateur du site est informé clairement de :<br/><br/>\
      •	l’identité du responsable de traitement et des coordonnées du Délégué à la Protection des Données,<br/>\
      •	de la finalité pour laquelle des informations le concernant sont demandées ainsi que de la base juridique du traitement (consentement, exécution du contrat, obligation légale ou intérêt légitime) <a>Cf. Données collectées et finalité de la collecte</a><br/>\
      •	et de la durée de conservation des données ou, lorsque ce n’est pas possible, des critères utilisés pour déterminer cette durée, notamment par référence aux délais de prescription légale applicables. <a>Cf. Données collectées et durée de conservation</a><br/>\
      <b>L’utilisateur est également informé :</b><br/><br/>\
      •	du caractère facultatif ou obligatoire des réponses à apporter ainsi que sur les conséquences éventuelles de la non-fourniture de ces données,<br/>\
      •	des destinataires des données (partenaires, sous-traitants), <a>Cf. Destinataires des données collectées</a><br/>\
      •	ainsi que des droits dont il dispose en application de la réglementation sur la protection des données personnelles.<br/><br/>\
      <b>En cas de transfert des données vers un pays tiers</b>, l’utilisateur est informé du fait que le responsable de traitement a l’intention de transférer les données vers un pays tiers (pays ne présentant pas un niveau de protection équivalent à celui de l’Union Européenne) ou à une organisation internationale, ainsi que l’existence ou l’absence de décision d’adéquation rendue par la Commission européenne. Lorsqu’un transfert de données vers un pays tiers s’effectue moyennant des garanties appropriées, ou est fondé sur des règles d’entreprise contraignantes ou des dérogations liés à des situations particulières, la référence à ces garanties appropriées ou adaptées et les moyens d’en obtenir une copie ou l’endroit où elles sont mises à disposition.<br/>\
      Le cas échéant, l’utilisateur est informé de l’existence d’une prise de décision automatisé, y compris le profilage, des informations utiles lui sont alors communiquées concernant la logique sous-jacente ainsi que l’importance et les conséquences de ce traitement.<br/>\
      <b>Concernant les droits dont l’utilisateur dispose en application de la réglementation sur les données personnelles.</b><br/><br/>\
      <b>Ces derniers sont listés ci-dessous et détaillés ci-après :</b><br/>\
      •	Le droit de demander au responsable de traitement l’accès, la rectification ou l’effacement des données personnelles, ou une limitation du traitement ainsi que d’exercer ses droits d’opposition et de portabilité des données.<br/>\
      •	Le droit de ne pas faire l’objet d’une décision fondée exclusivement sur un traitement automatisé, y compris le profilage ainsi que, pour tout traitement fondé sur le consentement, le droit de retirer son consentement à tout moment, sans porter atteinte à la licéité du traitement fondé sur le consentement effectué avant retrait de celui-ci.<br/>\
      •	Le droit d’exercer ses droits en ligne et d’effectuer une réclamation auprès de la CNIL.<br/><br/>\
      <b>1/ Vos droits d’accès, de rectification ou d’effacement, de limitation, d’opposition et de portabilité des données vous concernant</b><br/><br/>\
      Les informations communiquées par l'utilisateur peuvent donner lieu par ce dernier à l'exercice de ses droits d'accès, de rectification, de limitation et d’oppositions prévus par la réglementation sur les données personnelles.<br/><br/>\
      Vous disposez du <b>droit d’accéder</b> aux données personnelles qui ont été collectées à votre sujet et d’exercer ce droit facilement et à des intervalles raisonnables. Ce dernier vous permet de prendre connaissance du ou des traitements dont vous faites l’objet et d’en vérifier la licéité.<br/><br/>\
      Vous pouvez exercer votre droit d’accès de façon expresse en précisant l’information à laquelle vous souhaitez avoir accès ou standard. Dans ce dernier cas, vous serez notamment informé :<br/><br/>\
      •	des finalités du ou des traitement(s),<br/>\
      •	des catégories de données concernées,<br/>\
      •	de leur durée de conservation ou des critères utilisés pour déterminer cette durée,<br/>\
      •	de l’existence d’une décision automatisée (y compris profilage) et au moins dans ces cas de la logique que sous-tend le traitement ainsi que l’importance et les conséquences que ce traitement pourrait avoir pour vous,<br/>\
      •	de l’origine et des destinataires des données,<br/>\
      •	des éventuels transferts de ces informations vers des pays n’appartenant pas à l’Union Européenne ainsi que des garanties appropriées que nous avons prises.<br/><br/>\
      Vous pouvez demander et obtenir dans les meilleurs délais la <b>rectification</b> des données vous concernant qui sont inexactes ou incomplètes.<br/><br/>\
      Vous avez le droit d’obtenir <b>l’effacement</b>, dans les meilleurs délais, de données à caractère personnel vous concernant lorsque :<br/><br/>\
      •	ces données ne sont plus nécessaires au regard des finalités pour lesquelles elles ont été collectées ou traitées d’une autre manière.<br/>\
      •	vous souhaitez retirer votre consentement sur lequel est fondé le traitement et qu’il n’existe pas d’autre fondement juridique au traitement.<br/>\
      •	vous souhaitez vous opposer au traitement et il n’existe pas de motif légitime impérieux pour le traitement.<br/>\
      •	vos données ont fait l’objet d’un traitement illicite.<br/>\
      •	vos données doivent être effacées pour respecter une obligation légale prévue par le droit de l’Union ou par le droit français.<br/><br/>\
      Vous pouvez obtenir la <b>limitation</b> du traitement vous concernant lorsque l’un des éléments suivants s’applique :<br/><br/>\
      •	lorsque vous estimez que les données traitées sont inexactes.<br/>\
      •	le traitement dont vous faites l’objet est illicite et vous vous opposez à l’effacement des données.<br/>\
      •	les données personnelles vous concernant ne sont plus nécessaires au traitement mais uniquement pour la contestation, l’exercice ou la défense des droits en justice.<br/>\
      •	vous vous êtes opposé au traitement de vos données et il faut vérifier si les motifs légitimes poursuivis par le traitement prévalent sur les vôtres, le traitement de vos données est donc limité pendant cette période de vérification.<br/><br/>\
      Votre droit <b>d’opposition</b>, vous permet de vous opposer à tout moment au traitement de vos données :<br/><br/>\
      •	pour des raisons tenant à une situation particulière vous concernant, si le traitement est fondé sur un intérêt légitime que nous poursuivons, votre demande sera prise en compte sauf s’il est avéré que le traitement effectué par nos soins répond à des motifs légitimes et impérieux prévalant sur les intérêts et les droits et libertés motivant votre demande, ou s’il est nécessaire pour la contestation, l’exercice ou la défense de nos droits en justice.<br/>\
      •	sans motif lorsque le traitement est effectué à des fins de prospection commerciale.<br/><br/>\
      Vous avez le droit à la <b>portabilité</b> de données vous concernant, c’est-à-dire la possibilité de vous les faire communiquer ainsi que de demander à ce que vos données soient transmises à un autre acteur du marché. Etant précisé que le droit de portabilité ne s’applique qu’aux traitements fondés sur consentement ou sur un contrat et effectués à l’aide de procédés automatisés.<br/><br/>\
      2/ <b>Vous avez le droit de ne pas faire l’objet d’une décision fondée exclusivement sur un traitement automatisé, y compris le profilage</b>, produisant des effets juridiques vous concernant ou vous affectant de manière significative sauf si cette décision individuelle automatisée est nécessaire à la conclusion ou à l’exécution d’un contrat ou si elle est autorisée par la loi. En dehors de ces deux cas votre consentement est un prérequis nécessaire au traitement susvisé.<br/><br/>\
      De façon générale, lorsque le traitement de vos données personnelles est fondé sur le consentement, vous pouvez à tout moment, sans porter atteinte à la licéité du traitement <b>retirer votre consentement.</b><br/><br/>\
      3/ <b>Vous avez la possibilité d’adresser une réclamation à la CNIL et d’exercer vos droits en ligne.</b><br/><br/>\
      Vous pouvez introduire une <b>réclamation auprès de la CNIL</b> et ce sans préjudice de tout autre recours administratif ou juridictionnel, si vous considérez que le traitement de données à caractère personnel dont vous faites l’objet ne respecte pas la réglementation sur les données personnelles.<br/><br/>\
      <a target=\"_blank\" href=\"https://www.cnil.fr/fr/plaintes\">www.cnil.fr/fr/plaintes</a><br/><br/>\
      Pour toute demande ayant trait à l'exercice des droits précités :<br/><br/>\
      Nous attirons votre attention sur le fait que les demandes présentant un caractère nominatif nécessitent de vérifier votre identité, afin de protéger votre sécurité et votre vie privée. Pour une telle demande, nous vous remercions de bien vouloir nous adresser votre question accompagnée d'un titre d'identité à l'adresse de CA Consumer Finance mentionnée ci-après :<br/><br/>\
      Par voie postale :<br/><br/>\
      CACF – Délégué à la protection des données personnelles<br/><br/>\
      BP 50075<br/><br/>\
      77213 AVON Cedex<br/><br/>\
      Par courrier électronique :<br/><br/>\
      <a href=\"mailto:dpdcacf@ca-cf.fr\">dpdcacf@ca-cf.fr</a>`
        },
        {
          titre: 'SÉCURITÉ',
          contenu: `<b>Mesures de sécurisation, techniques et organisationnelles, adoptées par CA Consumer Finance</b><br/><br/>\
      CA Consumer Finance met en œuvre des mesures d'ordre technique et organisationnel de sécurisation destinées à garantir la sécurité et la confidentialité des sessions et des transactions que vous réalisez sur ce site. A titre indicatif, et de façon non exhaustive, plusieurs précisions vous sont apportées ci-après.<br/><br/>\
      - Le protocole de sécurité SSL : Tous les échanges entre les serveurs CA Consumer Finance et votre navigateur sont cryptés grâce au protocole SSL (Security Socket Layer). Cette technologie permet d'assurer l'authentification des utilisateurs, la confidentialité et l'intégrité des données échangées.<br/>\
      - La sécurité physique et logique : La sécurisation de nos serveurs eux-mêmes est assurée de manière physique par un accès restreint (locaux sécurisés, personnel habilité), et grâce à la présence de firewalls.<br/>\
      - Une double clé de sécurisation : Pour consulter et gérer vos contrats, vous devez vous identifier à l'aide de votre identifiant et d'un code d’accès. Ces données sont cryptées, un lien sécurisé est alors établi entre votre navigateur et le serveur web du site.<br/>\
      - Signalétique : votre navigateur affiche une signalétique dans la barre d'état.<br/><br/>\
      <b>La sécurisation des paiements en ligne :</b><br/><br/>\
      Afin de vous protéger contre d'éventuelles utilisations frauduleuses de votre carte bancaire lorsque vous effectuez un achat en ligne, CA Consumer Finance met en place un système d'authentification renforcée sur les sites marchands affichant le logo « Verified by VISA ».<br/><br/>\
      Dorénavant, lorsque vous effectuez un achat en ligne :<br/><br/>\
      1. Saisissez les informations habituelles de votre carte (n° carte bancaire, date d'expiration, cryptogramme visuel)<br/>\
      2. Vous recevez alors un code sécurité à usage unique par SMS. Un nouvel écran Sofinco apparaît sur lequel vous pouvez vous identifier en saisissant le code sécurité reçu par SMS.<br/>\
      3. Votre paiement est validé.<br/><br/>\
      Lors de chaque paiement de ce type, un nouveau code sécurité vous sera envoyé.<br>\
      Pour bénéficier de ce nouveau service, vous devez nous avoir communiqué votre numéro de téléphone portable au préalable et vous assurer de sa mise à jour en cas de modification. Si vous n'avez pas encore communiqué cette information, nous vous invitons à vous rapprocher d'un conseiller.<br/><br/>\
      Si vous êtes dans l'impossibilité de recevoir le code sécurité par SMS, une procédure alternative vous sera proposée. Laissez-vous guider par les indications qui vous seront données.<br/><br/>\
      Ce nouveau dispositif de sécurisation des paiements en ligne vous permet d'établir avec le site marchand, un climat de confiance mutuel propice au dénouement de la transaction.<br/><br/>\
      Pour toute information complémentaire, veuillez suivre la procédure proposée au sein de la rubrique <a>Contactez-nous</a> de la présente Charte.`
        },
        {
          titre: 'CHARTE DES DONNÉES PERSONNELLES DU GROUPE CRÉDIT AGRICOLE',
          contenu: 'Consultez et téléchargez la <a>charte des données personnelles du groupe Crédit Agricole</a>.'
        },
        {
          titre: 'POLITIQUE DE TRAITEMENT DE VOS DONNEES PERSONNELLES DE L’ASSURANCE EMPRUNTEUR',
          contenu: `Consultez et téléchargez <a>le feuillet d'information GDPR V0 - Assurance</a><br/> \
      <br/><br/>\
      Dernière mise à jour le 25/05/2018`
        }
      ]
    },
    {
      titre: 'Conditions générales d’utilisation',
      items: [
        {
          titre: 'AVERTISSEMENT',
          contenu: `Vous reconnaissez avoir pris connaissance des présentes conditions générales d'utilisation que vous vous engagez à respecter.<br/><br/>\
      Vous reconnaissez disposer de la compétence et des moyens nécessaires pour accéder et utiliser ce site.<br/><br/>\
      Vous reconnaissez avoir vérifié que la configuration informatique utilisée ne contient aucun virus et qu'elle est en parfait état de fonctionnement.`
        },
        {
          titre: 'L’OBJET DU SITE',
          contenu:
            'Réservé aux particuliers, le site <a target="_blank" href="https://www.sofinco.fr">www.sofinco.fr</a> est un site d\'information et de demande de prêt sur les produits de crédit à la consommation distribués par CA Consumer Finance sous la marque SOFINCO.<br/><br/>\
      Vous avez notamment la possibilité de souscrire aux offres de financement éligibles à la signature électronique directement sur le site.'
        },
        {
          titre: `SÉCURITÉ ET CONDITIONS D'ACCESSIBILITÉ DE L'ESPACE CLIENT`,
          contenu: `L'Espace client du site <a target=\"_blank\" href=\"https://www.sofinco.fr\">www.sofinco.fr</a> est réservé aux clients ayant souscrit un produit SOFINCO ou aux prospects en cours de souscription ; il permet à l’internaute de visualiser et gérer les contrats en ligne (prêt bancaire et crédit renouvelable), ou l’avancement de l’instruction de leur dossier. Son accès est limité et sécurisé via des codes d'accès personnalisés pour chaque internaute.<br/><br/>\
      <a>Accédez à votre espace client</a><br/><br/>\
      Vous êtes seul responsable de la préservation et de la confidentialité de l’identifiant et du code d’accès qui vous sont attribués.<br/><br/>\
      Pour des raisons de sécurité et de confidentialité, nous vous recommandons de garder confidentiel votre numéro d’identifiant et votre code d'accès.<br/><br/>\
      En cas d'oubli ou de perte de votre code d'accès, nous vous remercions de bien vouloir utiliser la procédure mise à votre disposition en cliquant sur le lien \"code d'accès oublié\" présent sous le pavé de saisie du code confidentiel de votre page de connexion à l'espace client.<br/><br/>\
      Si vous accédez au site à partir d'un ordinateur partagé (exemples : dans un cybercafé, à l'hôtel, chez des amis, etc.) ou public, nous vous suggérons pour des raisons de confidentialité, de supprimer toute trace technique sur l'ordinateur utilisé (supprimer les cookies par exemple).<br/><br/>\
      En aucun cas, les demandes de prêts effectuées sur le site ne peuvent être faites pour le compte d'un tiers.<br/><br/>\
      L’accès à cet espace est, comme nos offres, réservé aux personnes majeures.<br/><br/>\
      Les fonctionnalités de votre espace sont les suivantes<br/><br/>\
      •	<b>Fourniture de l’offre contractuelle</b> (crédit) au sens de l’article L. 312-8 al. 1 du Code de la consommation, de tout autre document contractuel ou précontractuel vous concernant mais aussi de tout courrier échangé entre SOFINCO et vous ;<br/>\
      •	Mise à disposition de <b>vos exemplaires électroniques originaux ou de vos copies numérisées ;</b><br/>\
      •	<b>Conservation des documents et informations</b> dans des conditions de nature à garantir leur intégrité dans le temps. Vous convenez expressément que SOFINCO se réserve la possibilité de clôturer cet espace en vous informant par tous moyens à sa convenance deux mois avant la clôture effective.<br/>\
      Votre espace vous permet également de<br/>\
      •	<b>Contractualiser avec CA Consumer Finance en intégrant un module de signature électronique</b> (pour des avenants, nouveaux contrats…) ;<br/>\
      •	<b>souscrire des produits ou services commerciaux analogues proposés par SOFINCO</b>.<br/>\
      CA Consumer Finance vous offre la possibilité de recevoir vos relevés de compte sous forme électronique (E-relevé), et non plus sous format papier.<br/><br/>\
      L’effectivité de ce service est subordonnée à la collecte de votre adresse e-mail, et à votre engagement de nous faire part de toute modification de cette dernière sans délai.<br/><br/>\
      Vous pouvez à tout moment demander la dématérialisation de votre relevé de compte en vous connectant à votre espace client, rubrique « Mes e-services », depuis votre profil. Cliquez sur le bouton « Souscrire », vérifiez votre adresse mail et prenez connaissance des modalités d’adhésion, puis confirmez.<br/><br/>\
      Vous pouvez néanmoins revenir au format papier à tout moment, depuis votre espace client en cliquant sur le bouton « Me désabonner » de la rubrique « Mes e-services ».`
        },
        {
          titre: `SÉCURITÉ ET CONDITIONS D'ACCESSIBILITÉ AUX SERVICES DIGITAUX DONT LA SIGNATURE ÉLECTRONIQUE`,
          contenu:
            'Les Conditions générales des Services Digitaux ont pour but de définir les termes et les conditions dans lesquelles vous pouvez :<br/><br/>\
      1. entrer en relation à distance de manière fiable via le Processus d’entrée en relation à distance ;<br/><br/>\
      2. souscrire une offre de crédit via le Processus de souscription en ligne ; et/ou<br/><br/>\
      3. adhérer à tout autre Service Digital.<br/><br/>\
      L’effectivité de ce service est subordonnée à la collecte de votre adresse e-mail, de votre numéro de portable, et à votre engagement de nous faire part de toute modification de ces données sans délai.<br/><br/>\
      Pour en savoir plus, consultez les <a>Conditions générales des Services Digitaux de Sofinco.fr</a>'
        },
        {
          titre: `SÉCURITÉ ET CONDITIONS D'ACCESSIBILITÉ AUX DIFFÉRENTES FONCTIONNALITÉS DU SITE`,
          contenu: `<b>Clic to tchat</b><br/><br/>\
      1. Objet du service<br/><br/>\
      Sofinco.fr, site Internet offre l'accès à un service de messagerie instantanée (désigné « Clic to Tchat »), permettant l'échange instantané de messages textuels destiné à des particuliers majeurs souhaitant dialoguer avec des conseillers Sofinco afin d'obtenir des informations relatives aux produits SOFINCO, d'être assisté dans leur choix et dans leur saisie en ligne de demande de dossier d'ouverture de crédit.<br/><br/>\
      Dans le cadre de l'utilisation du Clic to Tchat, les conseillers pourront suivre et visualiser vos actions.<br/><br/>\
      En utilisant le Clic to Tchat, vous vous engagez à respecter toutes les conditions décrites ci-dessous. Nous nous réservons le droit de modifier les Conditions d'Utilisation, à tout moment, afin de les adapter à l'évolution du Clic to Tchat.<br/><br/>\
      Pour des raisons de sécurité ou en cas de comportement contraire aux présentes conditions d'utilisation du Clic to Tchat, CA Consumer Finance se réserve le droit, de refuser toute tentative de connexion au Clic to Tchat ou de déconnecter temporairement ou définitivement un utilisateur au cours du Clic to Tchat sans avis préalable.<br/><br/>\
      2. Accès au service<br/><br/>\
      Pour accéder au service, vous devez disposer d'un ordinateur ou d'un terminal mobile compatible, d'un modem ou de tout autre outil de connexion, afin d'assurer sa connexion au réseau Internet. Votre navigateur internet doit avoir les caractéristiques suivantes : Internet Explorer versions > 7 (version 6 fortement dégradé), Firefox versions > 3, Chrome toutes versions, Safari toutes versions, Opéra toutes versions.<br/><br/>\
      3. Utilisation<br/><br/>\
      Au cours de votre navigation sur le site Sofinco.fr vous pouvez accéder au Clic to Tchat dans la rubrique « Besoin d'aide ? / Dialoguer en direct avec un conseiller » en cliquant sur le bouton « Tchater en ligne ». Ensuite une fenêtre s'affichera pour pouvoir dialoguer en direct. Vous devez alors saisir votre question et cliquer sur le bouton « Envoyez».<br/><br/>\
      4. Engagements<br/><br/>\
      Le service est destiné notamment à échanger sur les produits SOFINCO.<br/><br/>\
      Dans ce contexte, les propos tenus par l'usager du service et également par le conseiller ne doivent en aucun cas contenir de :<br/><br/>\
      Message haineux, agressif ou incitant à la discrimination, y compris les insinuations, à caractère raciste, xénophobe, portant sur la religion, le handicap, le sexe, l'âge ou l'orientation/identité sexuelle ainsi que propos obscène ou pornographique;<br/><br/>\
      Propos diffamatoire ou injurieux à l'égard de toute personne morale ou physique;<br/><br/>\
      Lien hypertexte renvoyant vers des sites extérieurs dont le contenu serait susceptible d'être contraire aux lois et règlements en vigueur en France ou tout lien hypertexte intégré sans autorisation;<br/><br/>\
      Message qui relèverait du harcèlement à l'égard de toute personne physique;<br/><br/>\
      Elément qui serait protégé par le droit d'auteur (type photo, base de données) ou/et le droit des marques (type logo, marque) sans en détenir les droits d'utilisation;<br/><br/>\
      Message comprenant des données à caractère personnel (par exemple nom, numéro de téléphone, adresse) s'il ne s'agit pas de ses propres coordonnées personnelles;<br/><br/>\
      Message contrevenant au droit à la vie privée et au droit à l'image;<br/><br/>\
      Message qui usurperait l'identité d'une autre personne physique;<br/><br/>\
      Message de dénigrement pouvant mener au discrédit des produits et services proposés par CA Consumer Finance constituant un trouble manifestement illicite.<br/><br/>\
      <b>Les notifications Push via votre navigateur web</b><br/><br/>\
      1. Objet du service<br/><br/>\
      Une notification push permet de vous notifier grâce à une alerte venant s’afficher directement dans votre navigateur (web ou mobile). Avec nos notifications, vous serez informé du suivi de vos demandes, de l’actualité de Sofinco, ou encore de nos nouvelles offres ou promotions.<br/><br/>\
      2. Accès au service<br/><br/>\
      La mise en place de notification push vous est régulièrement proposée. Vous devez avoir préalablement donné votre consentement express pour recevoir nos messages sous forme de notification push en visitant notre site. Votre consentement est conservé.<br/><br/>\
      Il est expressément convenu que les informations relatives à votre consentement ne peuvent être concédées à des tiers.<br/><br/>\
      3. Désactivation du service<br/><br/>\
      À tout moment, vous pouvez choisir de ne plus les recevoir. Pour cela, vous avez 2 options :<br/><br/>\
      •	Cliquez sur la roue dentée ( ) lorsque vous recevez une notification, et recherchez le site web dans la liste, puis sélectionnez « Refuser »,<br/>\
      •	Ou selon votre navigateur, suivez le chemin d’accès correspondant ci-dessous :<br/>\
      o	Sur Mozilla Firefox, dans les paramètres de votre navigateur, cliquez sur « option », puis sur l’onglet « Contenu », puis sélectionnez « Choisir » dans la rubrique « Notifications » (<a>about:preferences#content</a>). Recherchez le site web dans la liste, puis sélectionnez « Refuser ».<br/>\
      o	Sur Google Chrome, dans les paramètres de votre navigateur, onglet « confidentialité », allez dans la rubrique « Notifications » pour gérer les exceptions (<a>chrome://settings/contentExceptions#notifications</a>). Recherchez le site web dans la liste, puis sélectionnez « Refuser ».<br/>\
      o	Sur Safari de Apple, dans « Préférences », cliquez sur « Sites web », puis sur « Notifications ». Recherchez le site web dans la liste, puis sélectionnez « Refuser ».<br/><br/>\
      Dernière mise à jour le 25/05/2018`
        }
      ]
    }
  ]
};
