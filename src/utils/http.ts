const NOT_FOUND_CODE = 404;

export function isNotFound(statusCode: number | undefined) {
  return statusCode === NOT_FOUND_CODE;
}
