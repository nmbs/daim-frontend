import { DateTime } from 'luxon';

/**
 * Utility class to manage dates in JS.
 */
export default class DateUtils {
  public static readonly MIN_YEARS_DATE = 100;

  public static readonly FORMAT_DD_MM_YYYY_WITH_SLASH = 'dd/MM/yyyy';

  public static readonly FORMAT_YYYY_MM_DD_WITH_DASH = 'yyyy-MM-dd';

  /**
   * Gets the current local date in a JS date format.
   */
  public static now(): Date {
    return DateTime.local().toJSDate();
  }

  /**
   * Gets the minimum birth of date required in a JS date format.
   */
  public static minBirthDate(): Date {
    const now = DateTime.local();
    const minDate = now.minus({ years: DateUtils.MIN_YEARS_DATE });
    return minDate.toJSDate();
  }

  /**
   * Check that a date in a JS date format is null.
   *
   * @param date the date to check
   */
  public static isDateNotNull(date: Date | string): boolean {
    return date !== undefined && date !== null;
  }

  /**
   * Convert a string date into an other format.
   *
   * @param date the date in string
   * @param from input date format
   * @param to output date format
   */
  public static formatDateTo(date: string | Date, to: string, from?: string): string {
    if (date) {
      if (date instanceof Date) {
        return DateTime.fromJSDate(date).toFormat(to);
      } else if (typeof date === 'string') {
        return DateTime.fromFormat(date, from).toFormat(to);
      } else {
        throw new Error('Invalid date type');
      }
    } else {
      throw new Error('Date cannot be undefined or null');
    }
  }

  /**
   * Private constructor.
   */
  private constructor() {
    // Empty constructor
  }
}
