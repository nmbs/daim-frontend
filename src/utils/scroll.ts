import { clearAllBodyScrollLocks, disableBodyScroll } from 'body-scroll-lock';

export function removeScroll(element: HTMLDivElement | HTMLElement | Element) {
  if (element) {
    disableBodyScroll(element);
  }
}

export function restoreScroll() {
  clearAllBodyScrollLocks();
}
