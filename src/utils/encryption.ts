import uuidv4 from 'uuid/v4';
import sha256 from 'crypto-js/sha256';

const SALT = '3c68f553-5264-4a87-b8bf-91786ca39101';

export function generatePrivateKey(): string {
  return uuidv4();
}

export function hash(message: string) {
  return sha256(SALT + message).toString();
}
