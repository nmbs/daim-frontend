import getConfig from '@/config/config';

export function isNullOrEmpty(value: any) {
  return value === null || value === undefined || value.toString().trim() === '';
}

/**
 * Return an object corresponding to current GET params
 */
export function getCurrentQueryStringParams(): any {
  let query;
  // Option that enable retro-compatibility with old way of parsing query params.
  // Support <host>/?<params> and <host>/#/?<params> URLs.
  if (getConfig().OLD_URL_RETROCOMPATIBILITY) {
    query = window.location.href.split('?').pop();

    // Fix issues when user access to app using <host>/?<params> with OLD_URL_RETROCOMPATIBILITY enabled
    // Indeed if URL contains fragement (i.e. "#/" - such as when user acess to app with <host>/?<params>
    // and perform a refresh) but query params are processed using window.location.href; the fragment will
    // be included in the last query param
    if (query.includes('#')) {
      query = query.split('#')[0];
    }
  } else {
    query = window.location.search.substring(1);
  }

  const vars = query.split('&');
  const params = {};
  vars.forEach((curr: string) => {
    const pair = curr.split('=');
    if (pair.length === 2) {
      // manage multiple times same parameter => array of value
      if (params[pair[0].toUpperCase()] !== undefined) {
        if (Array.isArray(params[pair[0].toUpperCase()])) {
          params[pair[0].toUpperCase()].push(pair[1]);
        } else {
          params[pair[0].toUpperCase()] = [params[pair[0].toUpperCase()], pair[1]];
        }
      } else {
        params[pair[0].toUpperCase()] = pair[1];
      }
    }
  });
  return Object.keys(params).length ? params : null;
}

export function formatDate(date: any) {
  let month = '' + (date.getMonth() + 1);
  let day = '' + date.getDate();
  const year = date.getFullYear();

  if (month.length < 2) {
    month = '0' + month;
  }
  if (day.length < 2) {
    day = '0' + day;
  }

  return [day, month, year].join('/');
}

/**
 * Check if the value is numeric. For example:
 * - '12' or 12 are numerical values
 * - null or '0A' are not numerical values
 *
 * @param value the value to check
 */
export function isNumericalValue(value: any): boolean {
  return !(value instanceof Array) && value - parseInt(value, 10) >= 0;
}
