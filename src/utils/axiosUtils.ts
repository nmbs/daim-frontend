import axios from 'axios';
import { ID_TOKEN } from '@/config/config';
import { updateRefreshToken } from '@/services/token';

const states = {};

/**
 * Initializes and returns the retry state for the given request/config
 * @param  {AxiosRequestConfig} config
 * @return {Object}
 */
function getCurrentState(config) {
  const currentState = states[config.url] || {};
  currentState.retryCount = currentState.retryCount || 0;
  states[config.url] = currentState;
  return currentState;
}

/**
 * @param  {Error}  error
 * @return {boolean}
 */
export function isNetworkError(error) {
  return (
    !error.response && !Boolean(error.code) // Prevents retrying cancelled requests
  ); // Prevents retrying unsafe errors
}

export function enableAxiosRetry() {
  axios.interceptors.response.use(
    (success) => {
      if (success.config) {
        const currentState = getCurrentState(success.config);
        currentState.retryCount = 0;
      }
      return success;
    },
    (error) => {
      if (!error.config) {
        return Promise.reject(error);
      }
      const currentState = getCurrentState(error.config);
      if (error.response && error.response.status === 401 && currentState.retryCount < 1) {
        currentState.retryCount++;
        return updateRefreshToken('expiredToken').then((data) => {
          if (error.config.headers['Authorization']) {
            error.config.headers['Authorization'] = `Bearer ${ID_TOKEN}`;
          }

          if (error.config.headers['authorization']) {
            error.config.headers['authorization'] = `Bearer ${ID_TOKEN}`;
          }

          return axios(error.config);
        });
      }
      return Promise.reject(error);
    }
  );
}
