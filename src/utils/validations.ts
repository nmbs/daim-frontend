import { CardType } from '@/models/card';

export interface ICardInfo {
  label?: string;
  img?: string;
  infos?: string;
}

export const vCards = new Map<string, ICardInfo>();
vCards.set(CardType.P, {
  label: 'CardChoice.CARDSPremierLabel',
  img: 'cards-premier',
  infos: 'CardChoice.CARDSPremierInfos'
});
vCards.set(CardType.I, {
  label: 'CardChoice.CARDSAgileLabel',
  img: 'cards-agile',
  infos: 'CardChoice.CARDSAgileInfos'
});
vCards.set(CardType.S, {
  label: 'CardChoice.CARDSNoneLabel',
  img: 'cards-none',
  infos: null
});
