/**
 * Add or update an URL param.
 *
 * @param url the url where the param need to be added or updated
 * @param name the name of the param
 * @param value the value of the param
 */
export function addOrUpdateUrlParam(url, name, value): string {
  if (name === null || name === undefined || value === null || value === undefined) {
    return url;
  }
  let newUrl = url;
  let regex = new RegExp('[&\\?]' + name + '=');
  if (regex.test(newUrl)) {
    regex = new RegExp('([&\\?])' + name + '=\\d+');
    newUrl = url.replace(regex, '$1' + name + '=' + value);
  } else {
    if (newUrl.indexOf('?') > -1) {
      newUrl = `${url}&${name}=${value}`;
    } else {
      newUrl = `${url}?${name}=${value}`;
    }
  }
  return newUrl;
}
