export function convertDate(date: string) {
  if (!date) {
    return '';
  }
  return date.replace(/(\d+)\/(\d+)\/(\d+)/, '$3-$2-$1');
}

export function convertDateMMyyyy(date: string) {
  if (!date) {
    return '';
  }
  return date.replace(/(\d+)\/(\d+)/, '$2-$1-01');
}

export function convertToNumber(s: any): number {
  try {
    return Number(s) ? Number(s) : 0;
  } catch (ex) {
    return 0;
  }
}
