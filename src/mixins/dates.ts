export let convertDate = {
  methods: {
    convertDate(date: string) {
      if (!date) {
        return '';
      }
      return date.replace(/(\d+)\/(\d+)\/(\d+)/, '$3-$2-$1');
    }
  }
};
