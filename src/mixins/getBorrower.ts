export const PRINCIPAL = 'principal';
export const SECONDARY = 'secondary';

export let getBorrower = {
  methods: {
    getBorrower: (isPrincipal: boolean = true): string => {
      return isPrincipal ? PRINCIPAL : SECONDARY;
    }
  }
};
