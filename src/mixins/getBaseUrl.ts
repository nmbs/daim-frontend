import store from '@/store';
import ConfigSimulator from '@/config/configSimulator';
import getConfig from '@/config/config';
import { ColorThemeEnum } from '@/models/simulator/colorTheme';
import { RoutePathId } from '@/models/RoutesPath';

export let getBaseUrl = {
  computed: {
    baseUrl: () => {
      return store.getters.screenOption('resources-path') || '/images';
    },
    localThemePath: () => {
      return ConfigSimulator.style.theme === ColorThemeEnum.SOFINCO ? 'sofinco' : '';
    }
  },
  methods: {
    /**
     * Return URL of a local image defined dynamically
     * @param folder - (optional) Subfolder of @/assets/images
     * @param name - Name of img
     */
    localImage(folder: string, name: string) {
      const path = folder ? `${folder}/${name}` : name;
      // Use require.context as the path "@/assets/images/" is only processable at compilation.
      // As we compute image at runtime, we need to establish a link between folder @/assets/images
      // and folder containing assets after compilation.
      const images = require.context('@/assets/images/', true);
      return images('./' + path);
    },
    goToLegalMentions(stepId: string) {
      const getters = store.getters;
      const state = store.state;
      if (stepId === '1' && getters.dataUsageLink) {
        window.open(getters.dataUsageLink, '_blank');
      } else if (stepId === '0' && getters.legalMentionLink) {
        window.open(getters.legalMentionLink, '_blank');
      } else {
        const Q6 = state.apiConfig.partnerCode;
        const X1 = state.apiConfig.offerCode;
        const qs = `?Q6=${encodeURIComponent(Q6)}&X1=${encodeURIComponent(X1)}&stepId=${stepId || '0'}`;
        const hash = `#/${RoutePathId.LEGAL_MENTION}`;
        if (getConfig().OLD_URL_RETROCOMPATIBILITY) {
          const url = location.origin.concat(location.pathname, hash, qs);
          window.open(url, '_blank');
        } else {
          const url = location.href.replace(location.hash, hash).replace(location.search, qs);
          window.open(url, '_blank');
        }
      }
    }
  }
};
