import { ColorThemeEnum } from '@/models/simulator/colorTheme';
import Config from '@/config/configSimulator';

export let getIsTheme = {
  computed: {
    isTheme: () => {
      return Config.style.theme === ColorThemeEnum.SOFINCO ? false : true;
    }
  }
};
