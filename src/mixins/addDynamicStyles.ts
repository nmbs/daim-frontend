export let addDynamicStyles = {
  methods: {
    addDynamicStyles: (input: string, dynamicColor: string) => {
      return input
        .replace(/<b>/g, `<b style="color: ${dynamicColor}">`)
        .replace(/<a/g, `<a style="color: ${dynamicColor}"`);
    }
  }
};
