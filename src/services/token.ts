import axios from 'axios';
import { ACCESS_TOKEN, ID_TOKEN, REFRESH_TOKEN, setAccessToken, setIdToken, setRefreshToken } from '@/config/config';
import getConfig from '@/config/config';

export function updateRefreshToken(step) {
  const requestConfig = {
    headers: {
      Correlationid: getConfig().CORRELATION_ID,
      'Content-Type': 'application/json'
    }
  };
  let refreshUrl = getConfig().REFRESH_TOKEN_URL;
  if (step) {
    refreshUrl = refreshUrl + '?step=' + step;
  }
  const requestData = {
    idToken: ID_TOKEN,
    refreshToken: REFRESH_TOKEN
  };
  return axios.post(refreshUrl, requestData, requestConfig).then((res) => {
    const { accessToken, idToken, refreshToken } = res.data;
    setIdToken(idToken ? idToken : ID_TOKEN);
    setAccessToken(accessToken ? accessToken : ACCESS_TOKEN);
    setRefreshToken(refreshToken ? refreshToken : REFRESH_TOKEN);
    return res.data;
  });
}

export async function validateAndUpdateToken(token: string, step: string): Promise<void> {
  const requestConfig = {
    headers: {
      Correlationid: getConfig().CORRELATION_ID,
      'Content-Type': 'application/json'
    }
  };
  let requestUrl = getConfig().VALIDATE_TOKEN_URL;
  if (step) {
    requestUrl = requestUrl + '?step=' + step;
  }
  const requestData = { token };
  try {
    const response = await axios.post(requestUrl, requestData, requestConfig);
    const { accessToken, idToken, refreshToken } = response.data;
    setIdToken(idToken ? idToken : ID_TOKEN);
    setAccessToken(accessToken ? accessToken : ACCESS_TOKEN);
    setRefreshToken(refreshToken ? refreshToken : REFRESH_TOKEN);
  } catch (error) {
    throw new Error(error);
  }
}
