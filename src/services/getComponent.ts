import { Component } from 'vue-router/types/router';

import { Step } from '@/models/Step';

import Subscription from '@/views/Subscription.vue';
import CreditFromScratch from '@/views/CreditFromScratch.vue';
import HomePlv from '@/views/HomePlv.vue';

import InformationBloc from '@/views/SubscriptionSteps/InformationSteps/InformationBloc.vue';
import NameStep from '@/views/SubscriptionSteps/InformationSteps/NameStep.vue';
import ContactStep from '@/views/SubscriptionSteps/InformationSteps/ContactStep.vue';

import SituationBloc from '@/views/SubscriptionSteps/SituationSteps/SituationBloc.vue';
import CivilityStep from '@/views/SubscriptionSteps/SituationSteps/CivilityStep.vue';
import FamilySituationStep from '@/views/SubscriptionSteps/InformationSteps/FamilySituationStep.vue';
import HousingStep from '@/views/SubscriptionSteps/SituationSteps/HousingStep.vue';

import IncomesFormBloc from '@/views/SubscriptionSteps/IncomeSteps/IncomesFormBloc.vue';
import EmploymentStep from '@/views/SubscriptionSteps/IncomeSteps/EmploymentStep.vue';
import IncomesStep from '@/views/SubscriptionSteps/IncomeSteps/IncomesStep.vue';
import ChargesStep from '@/views/SubscriptionSteps/IncomeSteps/ChargesStep.vue';

import InsuranceStep from '@/views/SubscriptionSteps/InsuranceSteps/InsuranceStep.vue';
import SummaryStep from '@/views/SubscriptionSteps/SummaryStep.vue';
import CardStep from '@/views/SubscriptionSteps/CardStep.vue';
import LoadingStep from '@/views/SubscriptionSteps/LoadingStep.vue';
import ResultStep from '@/views/SubscriptionSteps/ResultStep.vue';
import SimulatorView from '@/views/SimulatorView.vue';
import LoanStep from '@/views/SubscriptionSteps/InformationSteps/LoanStep.vue';
import ResultStepSofinco from '@/views/SubscriptionSteps/ResultStepSofinco.vue';

import { RoutePathId } from '@/models/RoutesPath';

export interface ComponentInfos {
  component: Component;
}

export function getComponentOfStep(step: Step, parentStep: Step | null) {
  if (parentStep === null) {
    return getRootComponentOfStep(step);
  } else {
    return getSubscriptionComponentOfStep(step);
  }
}

function getRootComponentOfStep(step: Step): Component | undefined {
  const componentInfo = rootComponents.get(step.pathId);
  return componentInfo ? componentInfo.component : undefined;
}

function getSubscriptionComponentOfStep(step: Step) {
  const componentInfo = subscriptionComponents.get(step.pathId);
  return componentInfo ? componentInfo.component : undefined;
}

const rootComponents = new Map<string, ComponentInfos>([
  [RoutePathId.SIMULATION, { component: SimulatorView }],
  [RoutePathId.SUBSCRIPTION, { component: Subscription }],
  [RoutePathId.CREDIT_FROM_SCRATCH, { component: CreditFromScratch }],
  [RoutePathId.PLV, { component: HomePlv }]
]);

export const subscriptionComponents = new Map<string, ComponentInfos>([
  [RoutePathId.INFORMATION_BLOC, { component: InformationBloc }],
  [RoutePathId.LOAN, { component: LoanStep }],
  [RoutePathId.NAME, { component: NameStep }],
  [RoutePathId.CONTACT, { component: ContactStep }],
  [RoutePathId.SITUATION_BLOC, { component: SituationBloc }],
  [RoutePathId.CIVILITY, { component: CivilityStep }],
  [RoutePathId.FAMILY_SITUATION, { component: FamilySituationStep }],
  [RoutePathId.HOUSING, { component: HousingStep }],
  [RoutePathId.INCOMES_BLOC, { component: IncomesFormBloc }],
  [RoutePathId.EMPLOYMENT, { component: EmploymentStep }],
  [RoutePathId.INCOMES, { component: IncomesStep }],
  [RoutePathId.CHARGES, { component: ChargesStep }],
  [RoutePathId.INSURANCE, { component: InsuranceStep }],
  [RoutePathId.CARD, { component: CardStep }],
  [RoutePathId.SUMMARY, { component: SummaryStep }],
  [RoutePathId.LOADING, { component: LoadingStep }],
  [RoutePathId.RESULT, { component: ResultStep }],
  [RoutePathId.RESULT_SOFINCO, { component: ResultStepSofinco }]
]);
