import axios, { AxiosPromise } from 'axios';
import { AuthenRequest, AuthenResponse } from '@/models/subscription/authentication/Authentication';
import getConfig, { ID_TOKEN } from '@/config/config';
import { ClientData } from '@/models/subscription/authentication/Client';
import DateUtils from '@/utils/dateUtils';
import { SubscriptionData } from '@/store/types';
import { SubscriptionPayload } from '@/models/subscription/SubscriptionPayload';
import { GetCities, GetCountries } from '@/services/AutocompleteAPI';
import { convertDate } from '@/store/utils';
import { SelectObject } from '@/models/selectObject';
import { BusinessRule } from '@/services/BusinessRule';

import store from '@/store';

export class AuthService {
  public constructor() {
    // Do nothing
  }

  public login(parameters: AuthenRequest): AxiosPromise<AuthenResponse> {
    return axios.post(`${getConfig().API_AUTHEN_CLIENT}/authenticate`, this.getFormattedAuthenRequest(parameters), {
      headers: {
        Authorization: `Bearer ${ID_TOKEN}`,
        'Content-Type': 'application/json',
        Correlationid: getConfig().CORRELATION_ID,
        'context-applicationid': 'uploadSE'
      }
    });
  }

  public getClientData(customerId: string, token: string): AxiosPromise<ClientData> {
    return axios.get(getConfig().API_AUTHEN_CLIENT + `/${customerId}`, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    });
  }

  /**
   * Populate the store to auto-filled some fields with the client information.
   *
   * @param data the client information returned by the authentication API
   * @param principal the principal object that will be populate with client information
   * @param setSubscriptionData setter to update the store (to not import directly the store to avoid cyclic import)
   * @param housingTypes the list of house types available
   * @param maritalStatusTypes the list of of marital status type available
   */
  public async fillLoggedClient(
    data: ClientData,
    principal: SubscriptionData,
    housingTypes: SelectObject[] | null,
    maritalStatusTypes: SelectObject[] | null
  ): Promise<void> {
    // Name
    const maritalStatus = maritalStatusTypes
      ? maritalStatusTypes.find((e: SelectObject) => e.code === data.maritalStatusCode)
      : null;
    principal.name.firstName = data.firstName;
    principal.name.lastName = data.lastName;
    principal.name.birthName = data.birthName;
    principal.name.gender = new BusinessRule().getGender(data.titleCode);
    principal.name.maritalStatus = maritalStatus;

    const payloadName: SubscriptionPayload = {
      isPrincipal: true,
      subStateName: 'name',
      subStateValue: principal.name
    };
    store.commit('setSubscriptionData', payloadName);

    // Civility
    let birthCountry = null;
    try {
      birthCountry = data.birthCountryCode ? (await GetCountries(data.birthCountryCode)).data : null;
    } catch (error) {
      // Do nothing, the field birth country code will not be filled
    }
    if (birthCountry !== null) {
      birthCountry = birthCountry.find((e: SelectObject) => e.code === data.birthCountryCode);
    }

    let nationality = null;
    try {
      nationality = data.citizenshipCode ? (await GetCountries(data.citizenshipCode)).data : null;
    } catch (error) {
      // Do nothing, the field nationality will not be filled
    }
    if (nationality !== null) {
      nationality = nationality.find((e: SelectObject) => e.code === data.citizenshipCode);
    }
    let birthCity;
    if (data.birthPostalCode === '99000') {
      birthCity = {
        code: data.birthPostalCode,
        label: data.birthCityCode
      };
    } else {
      try {
        birthCity = data.birthPostalCode ? (await GetCities(data.birthPostalCode)).data : null;
      } catch (error) {
        // Do nothing, the field birth postal code will not be filled
      }
      if (birthCity) {
        // A postcode can be associated with multiple city
        // To fix that we use the postcode from the birth city list and the city name from the data
        birthCity = birthCity.find((e: SelectObject) => {
          if (e.code === data.birthPostalCode) {
            return {
              code: e.code,
              label: data.birthPostalCode
            };
          }
        });
      }
    }
    principal.civility.birthDate = data.birthDate ? convertDate(data.birthDate) : null;
    principal.civility.birthCountry = birthCountry;
    principal.civility.nationality = nationality;
    principal.civility.birthCity = birthCity;

    const payloadCivility: SubscriptionPayload = {
      isPrincipal: true,
      subStateName: 'civility',
      subStateValue: principal.civility
    };
    store.commit('setSubscriptionData', payloadCivility);

    // Contact
    principal.contact.email = data.personalContact ? data.personalContact.emailAddress : null;
    principal.contact.mobilePhone = data.personalContact ? data.personalContact.mobileNumber : null;
    principal.contact.homePhone = data.personalContact ? data.personalContact.phoneNumber : null;

    const payloadContact: SubscriptionPayload = {
      isPrincipal: true,
      subStateName: 'contact',
      subStateValue: principal.contact
    };
    store.commit('setSubscriptionData', payloadContact);

    // Address
    let addressCity = null;
    try {
      addressCity =
        data.personalContact && data.personalContact.postCode
          ? (await GetCities(data.personalContact.postCode)).data
          : null;
    } catch (error) {
      // Do nothing, the field address city will not be filled
    }
    if (addressCity) {
      // A postcode can be associated with multiple city name
      // To fix that we use the postcode from the address city list and the city name from the data
      addressCity = addressCity.find((e: SelectObject) => {
        if (e.code === data.personalContact.postCode) {
          return {
            code: e.code,
            label: data.personalContact.townName
          };
        }
      });
    }
    principal.adresse.adresseLabel = data.personalContact ? data.personalContact.streetName : null;
    principal.adresse.complementAdresse = data.personalContact ? data.personalContact.additionalStreetInfo : null;
    principal.adresse.adresseCity = addressCity;

    const payloadAddress: SubscriptionPayload = {
      isPrincipal: true,
      subStateName: 'adresse',
      subStateValue: principal.adresse
    };
    store.commit('setSubscriptionData', payloadAddress);
  }

  private getFormattedAuthenRequest(request: AuthenRequest) {
    return {
      id: request.id,
      passwordMask: request.passwordMask,
      birthDate: DateUtils.formatDateTo(
        request.birthDate,
        DateUtils.FORMAT_YYYY_MM_DD_WITH_DASH,
        DateUtils.FORMAT_DD_MM_YYYY_WITH_SLASH
      ),
      keypadId: request.keypadId
    };
  }
}
