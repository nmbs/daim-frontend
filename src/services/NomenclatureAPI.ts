import axios from 'axios';
import getConfig from '@/config/config';
import { MockNomenclature } from '@/mockedData/NomenclatureData';

export default class NomenclatureAPI {
  public static getNomenclatureData(token: string): any {
    return axios
      .get(getConfig().API_NOMENCLATURE, {
        headers: {
          Authorization: `Bearer ${token}`,
          Correlationid: getConfig().CORRELATION_ID,
          'Content-Type': 'application/json'
        }
      })
      .catch(() => {
        return MockNomenclature;
      });
  }

  public static validateFirstName(search: string): Promise<boolean> {
    return axios
      .get(`${getConfig().API_FIRSTNAME}`, {
        params: {
          q: search
        }
      })
      .then(() => {
        return true;
      })
      .catch(() => {
        return false;
      });
  }
}
