import axios, { AxiosRequestConfig } from 'axios';
import getConfig, { ID_TOKEN } from '@/config/config';
import { Contract } from '@/models/subscription/api/Contract';
import { SubscriptionData } from '@/store/types';
import { Gender } from '@/models/subscription/Name';
import { convertDate, convertDateMMyyyy } from '@/utils/strings';
import { SimulationParameter } from '@/models/Simulation';
import { BusinessDataTransfer } from '@/models/BusinessDataTransfer';
import { SeConfig, SeConfigParams } from '@/models/subscription/api/SeConfig';
import { FolderResponse } from '@/models/subscription/FolderResponse';
import { CreditTypeEnum } from '@/models/simulator/business/creditType';
import store from '@/store';
import { AuthenticationState } from '@/models/subscription/authentication/Authentication';
import { BusinessRule } from '@/services/BusinessRule';
import { addOrUpdateUrlParam } from '@/utils/url';
import { CardType } from '@/models/card';
import { CRBPResponse } from '@/models/subscription/api/CRBP';

export default class SubscriptionApi {
  /**
   * Subscribe to "SouscriptionAPI"
   * @param contract Object contract with client information && offer information
   * @param simulationParameter Generic parameters
   */
  public subscribe(contract: Contract, creditType: string, simulationParameter: SimulationParameter): any {
    let url: string;
    let requestHeaders: any = {
      Authorization: `Bearer ${ID_TOKEN}`,
      Correlationid: getConfig().CORRELATION_ID,
      'Content-Type': 'application/json'
    };
    // 10/08/2020: The subscription partners API cannot be used by Sofinco
    // Therefore we have two different configurations
    if (simulationParameter.applicationId !== 'SOFINCO') {
      requestHeaders = {
        ...requestHeaders,
        'Context-Applicationid': simulationParameter.applicationId,
        'Context-Partnerid': simulationParameter.partnerId,
        'Context-Sourceid': simulationParameter.sourceId
      };
      url = `${getConfig().API_SOUSCRIPTION}subscribe?creditType=${creditType}`;
    } else {
      url = `${getConfig().API_SOUSCRIPTION}`;
    }
    return axios.post(url, contract, {
      headers: requestHeaders
    });
  }

  /**
   * Get contract
   * @param contractId id of the contract
   * @param token token
   */
  public getContract(contractId: any, token: any): any {
    return axios.get(getConfig().API_SOUSCRIPTION + contractId + '/doc-contract', {
      headers: {
        Authorization: `Bearer ${token}`,
        Correlationid: getConfig().CORRELATION_ID
      },
      responseType: 'arraybuffer'
    });
  }

  /**
   * Updade Card choice and alert for CR Product
   * @param eSignKey
   * @param contractId
   * @param cardCode
   * @param cardAlertCode
   */
  public updateCreditCard(token: string, contractId: string, cardCode: string, cardAlertCode: string) {
    axios.patch(
      `${getConfig().API_SOUSCRIPTION}${contractId}/cardChoice`,
      {
        cardCode,
        cardAlertCode: cardCode === CardType.S ? 'N' : cardAlertCode
      },
      {
        headers: {
          Authorization: 'Bearer ' + token,
          Correlationid: getConfig().CORRELATION_ID
        }
      }
    );
  }

  /**
   * getElectronicSignature
   * @param contractId id of the contract
   * @param simulationParameter parameters
   * @param token token
   * @param partnerRedirectionUrl redirection URL to partner
   * @param exchangeUrl the exchange url
   * @param forceDisableInitSe force to disable the initialization of the SE
   */
  public getElectronicSignature(
    contractId: any,
    token: any,
    simulationParameter: SimulationParameter,
    partnerRedirectionUrl?: string,
    exchangeUrl?: string,
    forceDisableInitSe?: boolean
  ): any {
    if (getConfig().NEW_UPLOAD) {
      const url = `${getConfig().API_UPLOAD_SE}/${contractId}/upload-documents`;
      return axios
        .post(
          url,
          {
            callbackUrl: partnerRedirectionUrl ? partnerRedirectionUrl : '',
            exchangeUrl,
            forceDisableInitSe
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
              Correlationid: getConfig().CORRELATION_ID,
              'Content-Type': 'application/json',
              'Context-Applicationid': simulationParameter.applicationId,
              'Context-Partnerid': simulationParameter.partnerId,
              'Context-Sourceid': simulationParameter.sourceId
            }
          }
        )
        .then((res) => {
          res.data.newSe = true;
          return res;
        });
    } else {
      return axios.get(getConfig().API_SOUSCRIPTION + contractId + '/electronic-signature', {
        headers: {
          Authorization: `Bearer ${token}`,
          Correlationid: getConfig().CORRELATION_ID,
          'Content-Type': 'application/json',
          'Context-Applicationid': simulationParameter.applicationId,
          'Context-Partnerid': simulationParameter.partnerId,
          'Context-Sourceid': simulationParameter.sourceId
        }
      });
    }
  }

  public getRedirectUrl(contractId: any, token: any, forceDisableInitSe?: boolean) {
    const parameters: SimulationParameter = {
      partnerId: store.state.apiConfig.partnerCode,
      applicationId: store.state.apiConfig.applicationId,
      sourceId: store.state.apiConfig.offerCode
    };
    return this.getElectronicSignature(contractId, token, parameters, null, null, forceDisableInitSe).then(
      (res: any) => {
        let redirectUrl;
        if (res.data.newSe) {
          const splitedUrl = res.data.urlRedirect.split('?');
          const params = splitedUrl[1];
          const baseUrl = getConfig().UPLOAD_URL ? getConfig().UPLOAD_URL : splitedUrl[0];

          const redirectionUrl = `${baseUrl}?${params}&q6=${store.state.apiConfig.partnerCode}&x1=${store.state.apiConfig.offerCode}`;
          redirectUrl = redirectionUrl;
        } else {
          redirectUrl = res.data.urlRedirect;
        }
        // Add the origin canal before redirect to upload (with o = origin and w = fullweb)
        // This param is used on upload module to display or not the result and summary pages
        return addOrUpdateUrlParam(redirectUrl, 'o', 'w');
      }
    );
  }

  public formatData(
    context: BusinessDataTransfer | null,
    principal: SubscriptionData,
    secondary: SubscriptionData,
    authentication: AuthenticationState,
    simulationParameter: SimulationParameter
  ): Contract {
    return {
      hasSendingSummaryMail: !new BusinessRule().isSofincoPartnerId(store.state.apiConfig.partnerCode),
      borrower: {
        loyaltyCardId: store.state.apiConfig.numFidelity || null,
        customerId:
          authentication && authentication.response && authentication.response.customerId
            ? authentication.response.customerId
            : null,
        civilityCode: principal.name.gender === Gender.WOMAN ? 2 : 1,
        lastName: principal.name.lastName,
        maidenName:
          principal.name.gender === Gender.WOMAN ? (principal.name.birthName ? principal.name.birthName : null) : null,
        firstName: principal.name.firstName,
        birthDate: convertDate(principal.civility.birthDate + ''),
        citizenshipCode: principal.civility.nationality ? principal.civility.nationality.code : null,
        birthCountryCode: principal.civility.birthCountry ? principal.civility.birthCountry.code : null,
        birthCity: principal.civility.birthCity ? this.removePostalCode(principal.civility.birthCity.label) : null,
        birthPostalCode: principal.civility.birthCity ? principal.civility.birthCity.code : null,
        maritalStatus: principal.name.maritalStatus ? principal.name.maritalStatus.code : null,
        numberOfChildren: principal.family.childrenNbr,
        streetName: principal.adresse.adresseLabel,
        additionalStreetInfo: principal.adresse.complementAdresse,
        postalCode: principal.adresse.adresseCity ? principal.adresse.adresseCity.code : null,
        city: principal.adresse.adresseCity ? this.removePostalCode(principal.adresse.adresseCity.label) : null,
        countryCode: 'FR',
        housingCode: principal.adresse.housingType ? principal.adresse.housingType.code : null,
        movingDate: convertDateMMyyyy(principal.adresse.sinceWhen + ''),
        telephoneFixNumber: this.checkFixNumber(this.formatNumber(principal.contact.homePhone)),
        mobileNumber: this.formatNumber(principal.contact.mobilePhone),
        email: principal.contact.email,
        subscribeDigitalRevStatement: null,
        companyStatus: principal.employment.professionalStatus ? principal.employment.professionalStatus.code : null,
        companySector: principal.employment.activitySector ? principal.employment.activitySector.code : null,
        jobSituation: principal.employment.profession ? principal.employment.profession.code : null,
        contractType: principal.employment.contract ? principal.employment.contract.code : null,
        hiringDate: convertDateMMyyyy(principal.employment.sinceWhen + ''),
        monthlyNetSalary: principal.incomes.netIncome
          ? this.validateAmount(principal.incomes.netIncome.toString())
          : null,
        numberOfSalaryMonths: principal.incomes.salaryMonths,
        otherMonthlyNetIncome: principal.incomes.otherIncomes
          ? this.validateAmount(principal.incomes.otherIncomes.toString())
          : null,
        familyBenefitsAmount: principal.incomes.familyBenefits,
        housingHelpAmount: principal.incomes.housingBenefits,
        housingLoanAmount: principal.charges.refundHouseCredit
          ? this.validateAmount(principal.charges.refundHouseCredit.toString())
          : null,
        rentingMonthlyAmount: principal.charges.rental
          ? this.validateAmount(principal.charges.rental.toString())
          : null,
        creditConsumerMonthlyAmount: principal.charges.revolving
          ? this.validateAmount(principal.charges.revolving.toString())
          : null,
        creditAutoMonthlyAmount: principal.charges.carCredit
          ? this.validateAmount(principal.charges.carCredit.toString())
          : null,
        creditOtherMonthlyAmount: principal.charges.otherCredit
          ? this.validateAmount(principal.charges.otherCredit.toString())
          : null,
        otherChargeAmount: principal.charges.hasAlimony ? principal.charges.alimony : 0,
        optIn: principal.contact.lenderOffer,
        externalOptIn: false,
        stopReminderCustomer: principal.contact.contactOffer,
        hasInsurance: principal.insurance.acceptInsuranceOffer === true,
        externalId: context ? context.customerContext.externalCustomerId : null
      },
      coBorrower: principal.name.hasSpouse
        ? {
            loyaltyCardId: null,
            customerId: null,
            civilityCode: secondary.name.gender === Gender.WOMAN ? 2 : 1,
            lastName: secondary.name.lastName,
            maidenName:
              secondary.name.gender === Gender.WOMAN
                ? secondary.name.birthName
                  ? secondary.name.birthName
                  : null
                : null,
            firstName: secondary.name.firstName,
            birthDate: convertDate(secondary.civility.birthDate + ''),
            citizenshipCode: secondary.civility.nationality ? secondary.civility.nationality.code : null,
            birthCountryCode: secondary.civility.birthCountry ? secondary.civility.birthCountry.code : null,
            birthCity: secondary.civility.birthCity ? this.removePostalCode(secondary.civility.birthCity.label) : null,
            birthPostalCode: secondary.civility.birthCity ? secondary.civility.birthCity.code : null,
            maritalStatus: principal.name.maritalStatus ? principal.name.maritalStatus.code : null,
            numberOfChildren: secondary.family.childrenNbr,
            streetName: principal.adresse.adresseLabel,
            additionalStreetInfo: principal.adresse.complementAdresse,
            postalCode: principal.adresse.adresseCity ? principal.adresse.adresseCity.code : null,
            city: principal.adresse.adresseCity ? this.removePostalCode(principal.adresse.adresseCity.label) : null,
            countryCode: 'FR',
            housingCode: principal.adresse.housingType ? principal.adresse.housingType.code : null,
            movingDate: convertDateMMyyyy(principal.adresse.sinceWhen + ''),
            telephoneFixNumber: this.checkFixNumber(this.formatNumber(secondary.contact.homePhone)),
            mobileNumber: this.formatNumber(secondary.contact.mobilePhone),
            email: secondary.contact.email,
            subscribeDigitalRevStatement: null,
            companyStatus: secondary.employment.professionalStatus
              ? secondary.employment.professionalStatus.code
              : null,
            companySector: secondary.employment.activitySector ? secondary.employment.activitySector.code : null,
            jobSituation: secondary.employment.profession ? secondary.employment.profession.code : null,
            contractType: secondary.employment.contract ? secondary.employment.contract.code : null,
            hiringDate: convertDateMMyyyy(secondary.employment.sinceWhen + ''),
            monthlyNetSalary: secondary.incomes.netIncome
              ? this.validateAmount(secondary.incomes.netIncome.toString())
              : null,
            numberOfSalaryMonths: secondary.incomes.salaryMonths,
            otherMonthlyNetIncome: secondary.incomes.otherIncomes
              ? this.validateAmount(secondary.incomes.otherIncomes.toString())
              : null,
            familyBenefitsAmount: secondary.incomes.familyBenefits,
            housingHelpAmount: secondary.incomes.housingBenefits,
            // The charges are only requested for the principal borrower but are mandatory for the co-borrower
            // To solve that, we put principal borrower charges into the co-borrower charges
            housingLoanAmount: principal.charges.refundHouseCredit
              ? this.validateAmount(principal.charges.refundHouseCredit.toString())
              : null,
            rentingMonthlyAmount: principal.charges.rental
              ? this.validateAmount(principal.charges.rental.toString())
              : null,
            creditConsumerMonthlyAmount: principal.charges.revolving
              ? this.validateAmount(principal.charges.revolving.toString())
              : null,
            creditAutoMonthlyAmount: principal.charges.carCredit
              ? this.validateAmount(principal.charges.carCredit.toString())
              : null,
            creditOtherMonthlyAmount: principal.charges.otherCredit
              ? this.validateAmount(principal.charges.otherCredit.toString())
              : null,
            otherChargeAmount: principal.charges.hasAlimony ? principal.charges.alimony : 0,
            optIn: false,
            externalOptIn: false,
            stopReminderCustomer: true,
            hasInsurance: secondary.insurance.acceptInsuranceOffer === true,
            externalId: context && context.coBorrowerContext ? context.coBorrowerContext.externalCustomerId : null
          }
        : null,
      credit: {
        sourceId: simulationParameter.sourceId,
        partnerId: simulationParameter.partnerId,
        amount: simulationParameter.amount || 0,
        duration: simulationParameter.duration ? simulationParameter.duration : null,
        vehiculeAge: new BusinessRule().getVehicleAge(simulationParameter),
        vehiculeAmount: new BusinessRule().getVehicleAmount(simulationParameter),
        cardType: principal.wantCard ? principal.cardCode : 'S',
        appId: simulationParameter.applicationId,
        scaleCode: simulationParameter.scaleCode,
        orderId: context && context.offerContext ? context.offerContext.orderId : null
        // orderAmount: context && context.offerContext ? context.offerContext.orderAmount : null,
        // TODO: uncomment when enabling subscription data signature
        // personalContributionAmount:
        //   context && context.offerContext ? context.offerContext.personalContributionAmount : null
        // TODO: uncomment when enabling subscription data signature
      },
      partnerData: {
        businessProviderId: simulationParameter.businessProviderId,
        preScoringCode: context && context.offerContext ? context.offerContext.preScoringCode : null,
        equipmentCode: simulationParameter.equipmentCode
        // returnUrl: context && context.providerContext ? context.providerContext.returnUrl : null
        // TODO: uncomment when enabling subscription data signature
      },
      additionalData: {
        attribute1: store.state.subscription.experianData.cfuserPrefs,
        attribute2: store.state.subscription.experianData.cfuserPrefs2
        // attribute3: window.config.data1 // TODO: uncomment when enabling subscription data signature
      }
    };
  }

  public removePostalCode(s: string): string {
    const regExp = new RegExp(' \\(\\d*\\)$');
    return s ? s.replace(regExp, '') : '';
  }

  public validateAmount(s: string | null): string {
    if (!s) {
      return '0';
    }
    const regExp = new RegExp('[^0-9.,]', 'g');
    s = s.replace(regExp, '');
    const n = parseInt(s, 10);
    if (isNaN(n)) {
      return '0';
    } else {
      return n + '';
    }
  }

  public formatNumber(phone: string | null) {
    if (phone) {
      return phone.replace(/\s/g, '').replace(/^\+?(3){2}([0-9]{9})$/, '0$2');
    }
    return phone;
  }

  public checkFixNumber(phone: string | null) {
    if (phone) {
      return /^0[1234589][0-9]{8}$/.test(phone) ? phone : '';
    }
    return phone;
  }

  public sendW2SEmail(contractId: any, token: any, simulationParameter: SimulationParameter) {
    return axios.get(`${getConfig().API_SOUSCRIPTION}${contractId}/sendWebToStoreEmail`, {
      headers: {
        Authorization: `Bearer ${token}`,
        Correlationid: getConfig().CORRELATION_ID,
        'Content-Type': 'application/json',
        'Context-Applicationid': simulationParameter.applicationId,
        'Context-Partnerid': simulationParameter.partnerId,
        'Context-Sourceid': simulationParameter.sourceId
      }
    });
  }

  public getCRBPRule(): Promise<CRBPResponse> {
    let requestConfig = {};
    if (getConfig().API_CRBP.indexOf('.cacd2.io') >= 0) {
      requestConfig = {
        headers: {
          Authorization: 'Basic cGIyMzg6dGFlZzEl'
        }
      };
    }
    return axios
      .get(`${getConfig().API_CRBP}`, requestConfig)
      .then((res) => res.data)
      .catch((ex) => {
        return { crbpRule: 0, crbpVersion: 2 };
      });
  }

  public getSEConfig(
    contractId: string,
    creditType: string,
    impactOfferCode: string,
    productId: string,
    isCreditForTwo: boolean
  ): Promise<SeConfig> {
    const requestParams: SeConfigParams = {
      seType: 'WEB',
      contractId,
      productType: creditType === CreditTypeEnum.REVOLVING ? 'rev' : 'loan',
      borrower: 'T1',
      hasT2: isCreditForTwo,
      productCode: impactOfferCode,
      productId
    };
    let requestConfig: AxiosRequestConfig = {
      params: requestParams
    };
    if (getConfig().API_SE_CHOICE.indexOf('.cacd2.io') >= 0) {
      requestConfig = {
        ...requestConfig,
        headers: {
          Authorization: 'Basic cGIyMzg6dGFlZzEl'
        }
      };
    }
    return axios.get(`${getConfig().API_SE_CHOICE}`, requestConfig).then((res) => {
      if (res.data) {
        return res.data;
      } else {
        return {
          hasAccessUploadSE: null,
          hasForcedSE: null,
          hasUbble: null,
          hasUploadPaper: null,
          hasDQE: null
        };
      }
    });
  }

  public disableContractPostalSending(contractId: string, accessToken: string, idToken: string): Promise<any> {
    const params = {
      hasActivated: false
    };
    const auth = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'h-authorization': `Bearer ${idToken}`,
        Correlationid: getConfig().CORRELATION_ID,
        'context-applicationid': 'SOFINCO',
        'context-channel-code': 'K'
      }
    };
    return axios.patch(`${getConfig().API_SOUSCRIPTION}${contractId}/contractPostalSending`, params, auth);
  }

  public sendSummaryEmail(contractId: string, token: string, hasForcedSE: boolean) {
    const data = {
      mailType: hasForcedSE ? 'NEW_FORCED_SE' : 'NEW'
    };
    return axios.post(`${getConfig().API_SOUSCRIPTION}${contractId}/summaryEmail`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
        Correlationid: getConfig().CORRELATION_ID,
        'Content-Type': 'application/json'
      }
    });
  }

  public isRequestAccepted(response: FolderResponse): boolean {
    if (response === null) {
      return false;
    }
    if (response.acceptedCode === '2' || response.acceptedCode === '0') {
      return true;
    } else {
      return false;
    }
  }
}
