import axios from 'axios';
import { AxiosPromise } from 'axios';
import { ISupportingDocuments } from '@/models/subscription/api/Documents';
import getConfig from '@/config/config';

export class DocumentsApi {
  /**
   * Empty constructor.
   */
  public constructor() {
    // Do nothing
  }

  public getSupportingDocuments(
    accessToken: string,
    contractId: string,
    borrowerCode: string,
    applicationId: string,
    partnerId: string,
    offerCode: string,
    channelCode: string
  ): AxiosPromise<ISupportingDocuments[]> {
    const auth = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        Correlationid: getConfig().CORRELATION_ID,
        'context-applicationid': applicationId,
        'context-partnerId': partnerId,
        'context-channel-code': channelCode
      }
    };
    if (offerCode) {
      auth.headers['context-sourceid'] = offerCode;
    }
    return axios.get(
      `${
        getConfig().API_SUPPORTING_DOCUMENTS
      }/contracts/${contractId}/supportingDocuments?borrowerCode=${borrowerCode}`,
      auth
    );
  }
}
