import { DateTime } from 'luxon';

import { Adresse } from '@/models/subscription/Adresse';
import { InsuranceInfo, InsurancePartnerName } from '@/models/subscription/Insurance';
// Import json files
import partnerSuperSenior from '@/assets/json/insurance/partner/superSenior.json';
import partnerSenior from '@/assets/json/insurance/partner/senior.json';
import partnerYoung from '@/assets/json/insurance/partner/young.json';
import sofincoCrOld from '@/assets/json/insurance/sofinco/crOld.json';
import sofincoCrYoung from '@/assets/json/insurance/sofinco/crYoung.json';
import sofincoMhOld from '@/assets/json/insurance/sofinco/mhOld.json';
import sofincoMhYoung from '@/assets/json/insurance/sofinco/mhYoung.json';
import sofincoPbOld from '@/assets/json/insurance/sofinco/pbOld.json';
import sofincoPbYoung from '@/assets/json/insurance/sofinco/pbYoung.json';
import sofincoAlternativeLoanInsurance from '@/assets/json/insurance/sofinco/alternativeLoanInsurrance.json';
import sofincoRecommendedLoanInsurance from '@/assets/json/insurance/sofinco/recommendedLoanInsurrance.json';

import { CreditTypeEnum } from '@/models/simulator/business/creditType';
import { Gender, MaritalStatusCode, Name } from '@/models/subscription/Name';
import Config from '@/config/configSimulator';
import { AppConfiguration } from '@/models/Configuration';
import { RoutePathId } from '@/models/RoutesPath';
import { FinancialProposals, SimulationParameter } from '@/models/Simulation';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';
import store from '@/store';
import { StepState } from '@/models/StepState';
import { RouteStates } from '@/models/subscription/RouteState';
import { isNumericalValue } from '@/utils/utils';
import { InsuranceInfoV2, InsuranceV2Type } from '@/models/subscription/InsuranceV2';
import { Employment } from '@/models/subscription/Employment';

export class BusinessRule {
  public static MAX_LENGTH_NAME: number = 20;
  public static MAX_LENGTH_BIRTH_POST_CODE: number = 2;
  public static MAX_LENGTH_BIRTH_CITY: number = 20;
  public static MAX_LENGTH_EMAIL: number = 64;
  public static MAX_LENGTH_ADDRESS: number = 32;

  public static DEFAULT_MIN_AGE_INSURANCE = 18;
  public static DEFAULT_MAX_AGE_INSURANCE = 80;
  public static DEFAULT_LIMIT_YOUNG_INSURANCE = 59;
  public static LIMIT_YOUNG_PB = 64;

  /**
   * Socio professional category code for a retired person.
   */
  public static JOB_STATUS_RETIRED_PERSON_CODE = '70_70';

  /**
   * Professional status code for a retired person.
   */
  public static PROFESSIONAL_STATUS_RETIRED_PERSON_CODE = '6_6';

  public static hasCardChoice(creditType: string) {
    return creditType === 'REVOLVING';
  }

  public static shouldDisplayContactModule(appConfiguration: AppConfiguration): boolean {
    // First check if contact module is enabled in conf
    if (this.shouldDisplay('show-contact-module', appConfiguration)) {
      // Check at least one contact module's feature is enabled
      // Then check that we are not in independant simulator
      if (
        // I.e. only 1 step which is subscription
        (appConfiguration.steps.length === 1 && appConfiguration.steps[0].pathId === RoutePathId.SUBSCRIPTION) ||
        // I.e. more than 1 step but contains subscription
        Config.chainWithSubscription

        // Note: this test could be replaced by a test checking that a step subscription exist but I'm not
        // sure it is more performant
      ) {
        return true;
      }
    }
    return false;
  }

  public static shouldDisplayIAdvize(appConfiguration: AppConfiguration): boolean {
    if (!this.shouldDisplayContactModule(appConfiguration)) {
      return false;
    }
    // Check if Iadvize is enabled in conf
    return this.shouldDisplay('show-contact-module-iadvize', appConfiguration);
  }

  public static shouldDisplayWannaSpeak(appConfiguration: AppConfiguration): boolean {
    if (!this.shouldDisplayContactModule(appConfiguration)) {
      return false;
    }
    // Check if Wannaspeak is enabled in conf, a wannaspeak id is mandatory
    return (
      this.getWannaSpeakId(appConfiguration) && this.shouldDisplay('show-contact-module-wannaspeak', appConfiguration)
    );
  }

  public static getWannaSpeakId(appConfiguration: AppConfiguration): string {
    return (
      appConfiguration.screenOptions &&
      appConfiguration.screenOptions['show-contact-module-wannaspeak-id'] &&
      appConfiguration.screenOptions['show-contact-module-wannaspeak-id'].value
    );
  }

  public static getIAdvizeId(appConfiguration: AppConfiguration): string {
    return (
      appConfiguration.screenOptions &&
      appConfiguration.screenOptions['show-contact-module-iadvize-id'] &&
      appConfiguration.screenOptions['show-contact-module-iadvize-id'].value
    );
  }

  public static hasInterruptibility(appConfiguration: AppConfiguration) {
    return (
      appConfiguration &&
      appConfiguration.screenOptions &&
      appConfiguration.screenOptions['has-interruptibility'] &&
      appConfiguration.screenOptions['has-interruptibility'].value
    );
  }

  public static hasPartnerOptin() {
    const stepState = store.getters.getStep(RoutePathId.CONTACT);
    return (
      stepState &&
      stepState.screenOptions['partner-offer-optin'] &&
      !!stepState.screenOptions['partner-offer-optin'].value
    );
  }

  public static showReportInformations(proposal: SimulationFinancialProposal | FinancialProposals) {
    if (proposal && proposal.deferralMonthNumber > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Return insurance age configuration: limitYoung, limitSenior, minAgeInsurance and maxAgeInsurance
   * @param insuranceStep
   */
  public static getInsuranceConfiguration(insuranceStep: StepState) {
    if (insuranceStep && insuranceStep.screenOptions) {
      const limitYoung =
        insuranceStep.screenOptions['limit-young'] && isNumericalValue(insuranceStep.screenOptions['limit-young'].value)
          ? Number(insuranceStep.screenOptions['limit-young'].value)
          : null;
      const limitSenior =
        insuranceStep.screenOptions['limit-senior'] &&
        isNumericalValue(insuranceStep.screenOptions['limit-senior'].value)
          ? Number(insuranceStep.screenOptions['limit-senior'].value)
          : null;
      const minAgeInsurance =
        insuranceStep.screenOptions['min-age'] && isNumericalValue(insuranceStep.screenOptions['min-age'].value)
          ? Number(insuranceStep.screenOptions['min-age'].value)
          : null;
      const maxAgeInsurance =
        insuranceStep.screenOptions['max-age'] && isNumericalValue(insuranceStep.screenOptions['max-age'].value)
          ? Number(insuranceStep.screenOptions['max-age'].value)
          : null;
      return { limitYoung, limitSenior, minAgeInsurance, maxAgeInsurance };
    } else {
      return {
        limitYoung: null,
        limitSenior: null,
        minAgeInsurance: null,
        maxAgeInsurance: null
      };
    }
  }

  /**
   * Show the go to summary step button, if the user is:
   * - on mobile
   * - the current step is not the summary step
   * - the next step is not the summary step (to avoid two CTA with the same action)
   * - the summary step is not disabled
   * @param isMobile true if the user is on mobile, else false
   * @param currentStep the current step
   * @param nextStep the next step
   * @param routeStates the route states
   */
  public static showGoToSummaryStepBtn(
    isMobile,
    currentStep: StepState,
    nextStep: StepState,
    routeStates: RouteStates
  ): boolean {
    return (
      isMobile &&
      currentStep &&
      currentStep.pathId !== RoutePathId.SUMMARY &&
      nextStep &&
      nextStep.pathId !== RoutePathId.SUMMARY &&
      routeStates[RoutePathId.SUMMARY] &&
      !routeStates[RoutePathId.SUMMARY].disabled
    );
  }

  private static shouldDisplay(module: string, appConfiguration: AppConfiguration) {
    return (
      appConfiguration.screenOptions &&
      appConfiguration.screenOptions[module] &&
      appConfiguration.screenOptions[module].value === true
    );
  }

  private readonly LIMIT_YOUNG: number;
  private readonly MIN_AGE_ASSURANCE: number;
  private readonly MAX_AGE_ASSURANCE: number;
  private readonly LIMIT_SENIOR: number;

  // TODO: make this class a singleton to avoid set LIMIT_YOUNG, MIN_AGE_ASSURANCE and MAX_AGE_ASSURANCE everytime
  constructor(
    minAge: number = BusinessRule.DEFAULT_MIN_AGE_INSURANCE,
    maxAge: number = BusinessRule.DEFAULT_MAX_AGE_INSURANCE,
    limitYoung: number = BusinessRule.DEFAULT_LIMIT_YOUNG_INSURANCE,
    limitSenior?: number
  ) {
    this.LIMIT_YOUNG = limitYoung ? limitYoung : BusinessRule.DEFAULT_LIMIT_YOUNG_INSURANCE;
    this.MIN_AGE_ASSURANCE = minAge ? minAge : BusinessRule.DEFAULT_MIN_AGE_INSURANCE;
    this.MAX_AGE_ASSURANCE = maxAge ? maxAge : BusinessRule.DEFAULT_MAX_AGE_INSURANCE;
    this.LIMIT_SENIOR = limitSenior;
  }

  public showRefundHouseCredit(adresse: Adresse): boolean {
    return adresse.housingType && adresse.housingType.code === 'C_C';
  }

  public showRentCharges(adresse: Adresse): boolean {
    return adresse.housingType && adresse.housingType.code === 'L_L';
  }

  /**
   * Test if the borrower has a marital status equals to 'C_C' to know if he is single.
   *
   * @param name store state that contains the marital status
   */
  public isSingle(name: Name) {
    return name && name.maritalStatus && name.maritalStatus.code === MaritalStatusCode.SINGLE;
  }

  /**
   * Gets the gender (Man or Woman) from the gender code that can have the following values:
   * - "1" = MR
   * - "2" = MME
   * - "3" = MLLE
   *
   * @param genderCode the gender code
   */
  public getGender(genderCode: string | number): Gender {
    // Add an empty string after the genderCode to be sure that will be a string
    return genderCode + '' === '1' ? Gender.MAN : Gender.WOMAN;
  }

  public getInsuranceInfo(partnerName: string, creditType: string, product: string, birthday: string): InsuranceInfo {
    const age = this.computeAge(birthday);
    if (partnerName === InsurancePartnerName.SOFINCO) {
      if (creditType === CreditTypeEnum.REVOLVING) {
        return this.selectAccordingToAge(age, sofincoCrYoung, sofincoCrOld);
      } else {
        if (product === 'PBCAMPIN') {
          return this.selectAccordingToAge(age, sofincoMhYoung, sofincoMhOld, null, BusinessRule.LIMIT_YOUNG_PB);
        } else {
          return this.selectAccordingToAge(age, sofincoPbYoung, sofincoPbOld, null, BusinessRule.LIMIT_YOUNG_PB);
        }
      }
    } else {
      return this.selectAccordingToAge(
        age,
        partnerYoung,
        partnerSenior,
        partnerSuperSenior,
        this.LIMIT_YOUNG,
        this.LIMIT_SENIOR
      );
    }
  }

  public canSeeInsurance(birthday: string): boolean {
    const age = this.computeAge(birthday);
    // MIN_AGE and MAX_AGE are inclusive (i.e. user that is 80 years old will see insurance if MAX_AGE = 80)
    return age >= this.MIN_AGE_ASSURANCE && age <= this.MAX_AGE_ASSURANCE;
  }

  /**
   * Check if a borrower can take an insurance. The simulation API returns a null value when the borrower can not take an insurance.
   *
   * In case of REVOLVING credit, we check that the 'firstMonthlyInsuranceAmount' value returned by the API is not null and greater than 0.
   * In case of LOAN credit, we check that the 'totalInsuranceAmount' value returned by the API is not null and greater than 0.
   *
   * @param creditType the type of credit (REVOLVING or LOAN)
   * @param firstMonthlyInsuranceAmount the first monthly insurance amount
   * @param totalInsuranceAmount the total insurance amount
   */
  public canTakeInsurance(
    creditType: string,
    firstMonthlyInsuranceAmount?: number,
    totalInsuranceAmount?: number
  ): boolean {
    return (
      (creditType === CreditTypeEnum.REVOLVING && firstMonthlyInsuranceAmount > 0) ||
      (creditType === CreditTypeEnum.LOAN && totalInsuranceAmount > 0)
    );
  }

  public isSofincoPartnerId(partnerId: string): boolean {
    return partnerId === 'web_sofinco';
  }

  public isEligibleForInsuranceV2(partnerId: string, creditType: CreditTypeEnum | string): boolean {
    // return this.isSofincoPartnerId(partnerId) && creditType === CreditTypeEnum.LOAN;
    // The V2 insurance is temporarily disabled for all credit type
    return false;
  }

  public getInsuranceInfoV2ByInsuranceType(insuranceType: InsuranceV2Type): InsuranceInfoV2 {
    switch (insuranceType) {
      case InsuranceV2Type.ALTERNATIVE:
        return sofincoAlternativeLoanInsurance;
      case InsuranceV2Type.RECOMMENDED:
        return sofincoRecommendedLoanInsurance;
      default:
        // The insurance file does not exist for the requested insurance type
        return null;
    }
  }

  public getVehicleAmount(simulationParameters: SimulationParameter): number {
    return this.isPbVehicle(simulationParameters.productId) ? simulationParameters.amount : null;
  }

  public getVehicleAge(simulationParameters: SimulationParameter): number {
    if (this.isPbVehicle(simulationParameters.productId)) {
      try {
        return parseInt(simulationParameters.vehicleAge ? simulationParameters.vehicleAge : '0', 10);
      } catch (ex) {
        return 0;
      }
    } else {
      return null;
    }
  }

  public getJobSituationCode(employment: Employment) {
    let jobSituationCode = null;
    if (
      employment &&
      employment.professionalStatus &&
      employment.professionalStatus.code === BusinessRule.PROFESSIONAL_STATUS_RETIRED_PERSON_CODE
    ) {
      jobSituationCode = BusinessRule.JOB_STATUS_RETIRED_PERSON_CODE;
    } else {
      if (employment && employment.profession && employment.profession.code) {
        jobSituationCode = employment.profession.code;
      }
    }
    return jobSituationCode;
  }

  private isPbVehicle(productId: string): boolean {
    return (
      productId &&
      (productId === 'MOTOPERS' || productId === 'AUTOPERS' || productId === 'CARPERS' || productId === 'PBCAMPIN')
    );
  }

  private selectAccordingToAge(
    age: number,
    obj1: InsuranceInfo,
    obj2: InsuranceInfo,
    obj3: InsuranceInfo = null,
    limitAge1: number = this.LIMIT_YOUNG,
    limitAge2?: number
  ): InsuranceInfo {
    // limitAge1 and limitAge2 are inclusive
    if (age <= limitAge1) {
      return obj1;
    } else if (!limitAge2 || age <= limitAge2) {
      return obj2;
    } else {
      return obj3;
    }
  }

  private computeAge(birthday: string) {
    if (!birthday) {
      return 21;
    }
    return Math.floor(
      DateTime.fromJSDate(new Date()).diff(DateTime.fromFormat(birthday, 'd/M/yyyy'), 'years').as('years')
    );
  }
}
