import axios from 'axios';
import { ICustomerIdentificationForm, SendBy } from '@/models/subscription/authentication/ICustomerIdentification';
import getConfig, { ID_TOKEN } from '@/config/config';
import DateUtils from '@/utils/dateUtils';

export class IdentificationService {
  public retrieveCustomerIdentification(
    customerIdentification: ICustomerIdentificationForm,
    recaptchaToken: string
  ): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(getConfig().API_AUTHEN_CLIENT + `/identify`, {
          headers: {
            Authorization: `Bearer ${ID_TOKEN}`,
            Correlationid: getConfig().CORRELATION_ID,
            'context-applicationid': 'uploadSE'
          },
          params: this.getFormattedCustomerIdentification(customerIdentification, recaptchaToken)
        })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  public sendCustomerIdentificationBy(sendBy: SendBy, id: string, token: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .post(
          getConfig().API_AUTHEN_CLIENT + `/identify/${id}/identifier`,
          {
            otpType: sendBy
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
              Correlationid: getConfig().CORRELATION_ID,
              'context-applicationid': 'uploadSE'
            }
          }
        )
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  private getFormattedCustomerIdentification(
    customerIdentification: ICustomerIdentificationForm,
    recaptchaToken: string
  ) {
    const { lastName, firstName, birthDate, birthPostCode } = customerIdentification;
    return {
      lastName,
      firstName,
      birthDate: DateUtils.formatDateTo(
        birthDate,
        DateUtils.FORMAT_YYYY_MM_DD_WITH_DASH,
        DateUtils.FORMAT_DD_MM_YYYY_WITH_SLASH
      ),
      birthPostCode,
      captcha: recaptchaToken
    };
  }
}
