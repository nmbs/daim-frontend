/**
 *
 * Copyright (c) 2018 - 2019 - Credit Agricole Consumer Finance - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * This file can not be copied and/or distributed without the express permission of Credit Agricole Consumer Finance
 *
 */

import axios from 'axios';
import getConfig from '@/config/config';

export function GetCountries(search: string) {
  return axios.get(getConfig().API_COUNTRY, {
    params: {
      q: search
    }
  });
}

export function GetNationalities(search: string) {
  return axios.get(getConfig().API_NATIONALITY, {
    params: {
      q: search
    }
  });
}

export function GetCities(search: string) {
  return axios
    .get(getConfig().API_CITY, {
      params: {
        q: search
      }
    })
    .then((res) => {
      return res;
    });
}
