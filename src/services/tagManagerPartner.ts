import { Route } from 'vue-router';
import store from '@/store';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';
import { Web2storeMail } from '@/models/Web2Store';
import { RoutePathId } from '@/models/RoutesPath';
import { TagManagerService } from '@/services/tagManagerFactory';
import { testIsMobile } from '@/utils/browsers';

export class TagManagerPartnerService implements TagManagerService {
  public postClick(element: HTMLElement): void {}

  public pageSubscriptionProcessChange(route: Route): void {
    const stepNumber = store.getters.getCurrentStepIndex(route.name) + 1;

    if (window.EA_collector) {
      const previousStep = store.getters.getPreviousStep(route.name, true);
      const previousStepName = previousStep ? previousStep.pathId : null;
      const data = [
        ...this.getInfoBasePageSubscriptionProcess(route),
        ...this.getScart(route.name),
        ...this.getStep(stepNumber),
        // Here we use previousStepName to determine which tag should be sent with
        // page's tag because data that need to be sent is only available after user
        // go to next page (tags are triggered when page is displayed, not when user leave page).
        ...this.getInfoSubscriptionSteps(previousStepName)
      ];
      window.EA_collector(data);
    }
    if (window.dataLayer) {
      const offerCode = store.state.apiConfig.offerCode;
      const pagename = this.getPageName(route.name);
      if (route.name === RoutePathId.RESULT) {
        window.dataLayer.push(this.getGTMDecisionPageData(route));
      } else {
        window.dataLayer.push({
          event: 'VirtualPageview',
          virtualPageURL: route.fullPath.replace(/\//g, '|'),
          virtualPageTitle: stepNumber + '–' + pagename,
          nav_category: offerCode
        });
      }
    }
  }

  public pageChange(to: Route, from: Route): void {
    const offerCode = store.state.apiConfig.offerCode;
    const parcours = store.state.client.isMobile ? 'mobile' : 'desktop';
    const pagename = this.getPageName(to.name);
    const productName = store.state.apiConfig.partnerCode + '|' + store.state.apiConfig.offerCode;
    let amount;
    let taeg;
    let monthlyPayments;
    if (store.state.subscription.response && store.state.subscription.response.proposal) {
      amount = store.state.subscription.response.proposal.amount;
      taeg = store.state.subscription.response.proposal.taeg;
      monthlyPayments = store.state.subscription.response.proposal.monthlyAmountNonInsurance;
    } else if (store.state.simulator.selectedProposal) {
      amount = store.state.simulator.selectedProposal.amount;
      taeg = store.state.simulator.selectedProposal.taeg;
      monthlyPayments = store.state.simulator.selectedProposal.monthlyAmountWithoutInsurance;
    } else if (store.state.simulator.requiredAmount) {
      amount = store.state.simulator.requiredAmount;
    }

    if (window.dataLayer) {
      window.dataLayer.push({
        parcours,
        pagename,
        partner: store.state.apiConfig.partnerCode,
        nav_category: offerCode,
        product_amount: amount,
        product_name: productName,
        product_taeg: taeg,
        product_payment: monthlyPayments
      });
    }
  }

  public offerOptInClick(optIn: boolean | null): void {
    if (window.dataLayer) {
      window.dataLayer.push({
        checkout_optin_partenaire: optIn
      });
    }
  }

  public insuranceNextClick(hasInsurance: boolean | null): void {
    if (window.dataLayer) {
      window.dataLayer.push({
        checkout_insurance: hasInsurance
      });
    }
  }

  public stopPubClick(optIn: boolean): void {
    if (window.dataLayer) {
      window.dataLayer.push({
        stoppub_option: optIn
      });
    }
  }

  public gaClick(ecat: string, eaction: string, elabel: string, callback?: () => void) {
    if ((window as any).dataLayer) {
      let called = false;
      if (callback) {
        (window as any).dataLayer.push({
          event: 'gaClick',
          ecat,
          eaction,
          elabel,
          eventCallback: () => {
            called = true;
            callback();
          }
        });
        // In case GA is not working (blocked), wait 2s and call the callback anyway
        setTimeout(() => {
          if (!called) {
            callback();
          }
        }, 2000);
      } else {
        (window as any).dataLayer.push({
          event: 'gaClick',
          ecat,
          eaction,
          elabel
        });
      }
    } else {
      callback();
    }
  }

  public tagError(errorStatus: any) {
    if (window.EA_collector) {
      const ref = store.state.subscription.response ? store.state.subscription.response.contractId : null;
      const site = store.state.apiConfig.appConfiguration
        ? store.state.apiConfig.appConfiguration.screenOptions.site.value
        : null;

      const uid = store.state.subscription.context
        ? store.state.subscription.context.customerContext.externalCustomerId
        : null;

      window.EA_collector([
        'error',
        '1',
        'rtgsite',
        site,
        'rtgpg',
        'error',
        'uid',
        uid,
        'email',
        store.state.subscription.principal.contact.email,
        'ref',
        ref,
        'rtgerrorcode',
        errorStatus,
        'rtgmobilephone',
        store.state.subscription.principal.contact.mobilePhone,
        'rtgphonenumber',
        store.state.subscription.principal.contact.homePhone
      ]);
    }
  }

  public tagIndependantSimulatorAmountPage(route: Route) {
    const rtgpg = 'form';
    const rtgpagename = 'simulation_independante_montant';

    if (window.EA_collector) {
      window.EA_collector([...this.getMinimumInfo(route), 'rtgpg', rtgpg, 'rtgpagename', rtgpagename]);
    }
  }

  public tagIndependantSimulatorOffersPage(route: Route) {
    const rtgpg = 'form';
    let amount;
    if (store.state.simulator.requiredAmount) {
      amount = store.state.simulator.requiredAmount;
    }
    const rtgpagename = 'simulation_independante_mensualite';

    if (window.EA_collector) {
      window.EA_collector([
        ...this.getMinimumInfo(route),
        'rtgpg',
        rtgpg,
        'amount',
        amount,
        'rtgpagename',
        rtgpagename
      ]);
    }
  }

  public tagIndependantSimulatorAllPageGTM(route: Route) {
    window.dataLayer.push({
      event: 'VirtualPageview',
      virtualPageURL: route.fullPath.replace(/\//g, '|'),
      virtualPageTitle: '1' + '–' + 'choix_mensualite',
      nav_category: store.state.apiConfig.offerCode
    });
  }

  /**
   * Tag that will be triggered at each offer selected by user for independant simulator
   */
  public tagIndependantSimulatorOffersSelection(route: Route, selectedOffer: SimulationFinancialProposal) {
    let amount;
    if (store.state.simulator.requiredAmount) {
      amount = store.state.simulator.requiredAmount;
    }

    const rtgpagename = 'simulation_independante_mensualite_offre_selectionnee';
    const rtgpg = 'form';

    let duration;
    let monthlyPayments;
    let taeg;
    if (selectedOffer) {
      duration = selectedOffer.creditDurationWithoutInsurance;
      monthlyPayments = selectedOffer.monthlyAmountWithoutInsurance;
      taeg = selectedOffer.taeg;
    }

    if (window.EA_collector) {
      window.EA_collector([
        ...this.getMinimumInfo(route),
        ...this.getInfoHitSimulator(),
        'amount',
        amount,
        'rtgpg',
        rtgpg,
        'rtgpagename',
        rtgpagename,
        'rtgduration',
        duration,
        'rtgpayments',
        monthlyPayments,
        'rtgtaeg',
        taeg
      ]);
    }
    if (window.dataLayer) {
      window.dataLayer.push({
        event: 'select_offer',
        list: store.state.apiConfig.offerCode,
        product_name: store.state.apiConfig.partnerCode + '|' + store.state.apiConfig.offerCode,
        product_amount: amount,
        simulator_hit: store.state.simulator.timesUserSelectedOffer,
        taeg,
        duration
      });
    }
  }

  /**
   * Seems not used anymore
   */
  public chooseMonthlyPage(route: Route, amount: number | null): void {
    if (window.EA_collector) {
      window.EA_collector(this.getInfoBasePageSubscriptionProcess(route, amount));
    }
  }

  public preContractClick(): void {
    let amount;
    if (
      store.state.simulator.selectedProposal !== null &&
      store.state.simulator.selectedProposal.totalAmountWithoutInsurance
    ) {
      amount = store.state.simulator.selectedProposal.totalAmountWithoutInsurance;
    }
    if (window.dataLayer) {
      window.dataLayer.push({
        event: 'info_precontract',
        list: store.state.apiConfig.offerCode,
        product_name: store.state.apiConfig.partnerCode + '|' + store.state.apiConfig.offerCode,
        product_amount: amount
      });
    }
  }

  public selectProduct(): void {
    let amount;
    if (
      store.state.simulator.selectedProposal !== null &&
      store.state.simulator.selectedProposal.totalAmountWithoutInsurance
    ) {
      amount = store.state.simulator.selectedProposal.totalAmountWithoutInsurance;
    }
    if (window.dataLayer) {
      window.dataLayer.push({
        event: 'product_detail',
        list: store.state.apiConfig.offerCode,
        product_name: store.state.apiConfig.partnerCode + '|' + store.state.apiConfig.offerCode,
        product_amount: amount,
        simulator_hit: store.state.simulator.timesUserSelectedOffer
      });
    }
  }

  public clickSummaryMailW2S(): void {
    if (window.EA_event) {
      window.EA_event('envoi_email_recapitulatif_w2s');
    }
    if (window.EA_collector) {
      const site = store.state.apiConfig.appConfiguration
        ? store.state.apiConfig.appConfiguration.screenOptions.site.value
        : null;
      const partner = store.state.apiConfig.partnerCode;
      const type = store.state.apiConfig.offerCode;
      const rtgparcours = testIsMobile() ? 'mobile' : 'desktop';
      const scoring = store.state.subscription.response ? store.state.subscription.response.acceptedCode : null;

      let amount;
      if (store.state.subscription.response && store.state.subscription.response.proposal) {
        amount = store.state.subscription.response.proposal.amount;
      }
      window.EA_collector([
        'enotrack',
        1,
        'rtgclickid',
        'envoi_email_recapitulatif_w2s',
        'rtgclickcat',
        'envoi_email',
        'rtgsite',
        site,
        'from',
        site,
        'rtgpartenaire',
        partner,
        'rtgparcours',
        rtgparcours,
        'type',
        type,
        'scoring',
        scoring,
        'amount',
        amount
      ]);
    }
  }

  private getGTMDecisionPageData(route: Route) {
    if (window.dataLayer) {
      let amount;
      let monthlyAmount;
      let taeg;
      if (store.state.subscription.response && store.state.subscription.response.proposal) {
        amount = store.state.subscription.response.proposal.amount;
        monthlyAmount = store.state.subscription.response.proposal.monthlyAmountNonInsurance;
        taeg = store.state.subscription.response.proposal.taeg;
      }

      const stepNumber = store.getters.getCurrentStepIndex(route.name) + 1;
      const ref = store.state.subscription.response ? store.state.subscription.response.contractId : null;
      const scoring = store.state.subscription.response ? store.state.subscription.response.acceptedCode : null;
      const offerCode = store.state.apiConfig.offerCode;
      const pagename = this.getPageName(route.name);

      return {
        event: 'VirtualPageview',
        virtualPageURL: route.fullPath.replace(/\//g, '|'),
        virtualPageTitle: stepNumber + '–' + pagename + '–' + scoring + '–' + ref,
        list: store.state.apiConfig.offerCode,
        product_name: store.state.apiConfig.partnerCode + '|' + store.state.apiConfig.offerCode,
        product_amount: amount,
        product_payments: monthlyAmount,
        product_taeg: taeg,
        nav_category: offerCode
      };
    }
  }

  private getStep(stepNumber: number) {
    if (stepNumber > 0) {
      return ['rtgstep', stepNumber];
    }
    return [];
  }

  private getScart(routeName: string) {
    if (routeName !== RoutePathId.RESULT) {
      return ['scart', '1'];
    }
    return [];
  }

  /**
   * Minimum information that will be sent in all Eulerian tags
   */
  private getMinimumInfo(route: Route) {
    const site = store.state.apiConfig.appConfiguration
      ? store.state.apiConfig.appConfiguration.screenOptions.site.value
      : null;
    const uid = store.state.subscription.context
      ? store.state.subscription.context.customerContext.externalCustomerId
      : null;
    const type = store.state.apiConfig.offerCode;
    const rtgparcours = store.state.client.isMobile ? 'mobile' : 'desktop';

    return [
      'rtgsite',
      site,
      'rtgpartenaire',
      store.state.apiConfig.partnerCode,
      'rtgparcours',
      rtgparcours,
      'rtgidform',
      store.state.apiConfig.offerCode,
      'from',
      site,
      'uid',
      uid,
      'path',
      route.fullPath.replace(/\//g, '|'),
      'type',
      type
    ];
  }

  /**
   * Basic info that will be sent for each page of subscription process
   */
  private getInfoBasePageSubscriptionProcess(route: Route, amount: any = null) {
    const routeName = route.name;
    const rtgpg = routeName === RoutePathId.RESULT ? 'completedform' : 'form';

    if (
      amount === null &&
      store.state.subscription.response &&
      store.state.subscription.response.proposal &&
      store.state.subscription.response.proposal.amount
    ) {
      amount = store.state.subscription.response.proposal.amount;
    } else if (amount === null && store.state.simulator.requiredAmount) {
      amount = store.state.simulator.requiredAmount;
    }

    return [
      ...this.getMinimumInfo(route),
      'rtgpg',
      rtgpg,
      'amount',
      amount,
      'rtgpagename',
      this.getPageName(routeName)
    ];
  }

  private getPageName(routeName: string) {
    switch (routeName) {
      case RoutePathId.NAME:
        return 'votre_nom';
      case RoutePathId.CONTACT:
        return 'vos_coordonnees';
      case RoutePathId.CIVILITY:
        return 'votre_civilite';
      case RoutePathId.FAMILY_SITUATION:
        return 'votre_situation_familiale';
      case RoutePathId.HOUSING:
        return 'votre_adresse';
      case RoutePathId.EMPLOYMENT:
        return 'votre_travail';
      case RoutePathId.INCOMES:
        return 'vos_revenus';
      case RoutePathId.CHARGES:
        return 'vos_charges';
      case RoutePathId.CARD:
        return 'choix_carte';
      case RoutePathId.INSURANCE:
        return 'choix_assurance';
      case RoutePathId.SUMMARY:
        return 'fiche_dialogue';
      case RoutePathId.LOADING:
        return 'chargement_reponse';
      case RoutePathId.RESULT:
        const subscriptionResponse = store.state.subscription.response;
        if (subscriptionResponse.acceptedCode === '0' || subscriptionResponse.acceptedCode === '2') {
          const screenOptions = store.state.apiConfig.appConfiguration.screenOptions;
          if (screenOptions['web2store-enabled'] && !!screenOptions['web2store-enabled'].value) {
            if (screenOptions['web2store-mail'] && screenOptions['web2store-mail'].value === Web2storeMail.AUTO) {
              return 'reponse_de_principe_ok_plv';
            } else {
              return 'reponse_de_principe_ok_w2S';
            }
          } else {
            return 'reponse_de_principe_ok';
          }
        } else {
          return 'reponse_de_principe_ko';
        }
      case RoutePathId.SIMULATION:
        return 'choix_mensualite';
      case RoutePathId.CREDIT_FROM_SCRATCH:
      case RoutePathId.PLV:
        return 'page_pedagogique';
      case RoutePathId.ERROR_PAGE:
        return 'erreur';
      default:
        return null;
    }
  }

  private getInfoSubscriptionSteps(previousStepName: string): string[] | number[] | boolean[] {
    let tags = [];
    // Note: order of switch cases is very important here as it determine
    // which tags will be send regarding current viewed page
    switch (previousStepName) {
      case RoutePathId.LOADING:
        tags = [...tags, ...this.getInfoStepResult()];
      case RoutePathId.SUMMARY:
      case RoutePathId.INSURANCE:
        tags = [...tags, ...this.getInfoStepInsurance()];
      case RoutePathId.CARD:
        tags = [...tags, ...this.getInfoStepCard()];
      case RoutePathId.CHARGES:
        tags = [...tags, ...this.getInfoStepCharges()];
      case RoutePathId.INCOMES:
        tags = [...tags, ...this.getInfoStepIncomes()];
      case RoutePathId.EMPLOYMENT:
        tags = [...tags, ...this.getInfoStepEmployment()];
      case RoutePathId.HOUSING:
        tags = [...tags, ...this.getInfoStepHousing()];
      case RoutePathId.FAMILY_SITUATION:
        tags = [...tags, ...this.getInfoStepFamily()];
      case RoutePathId.CIVILITY:
        tags = [...tags, ...this.getInfoStepCivility()];
      case RoutePathId.CONTACT:
        tags = [...tags, ...this.getInfoStepContact()];
      case RoutePathId.NAME:
        tags = [...tags, ...this.getInfoStepName()];
      case RoutePathId.SIMULATION:
        tags = [...tags, ...this.getInfoFirstSubscriptionStep(), ...this.getInfoHitSimulator()];
        break;
      case RoutePathId.CREDIT_FROM_SCRATCH:
        tags = [...tags, ...this.getInfoFirstSubscriptionStep()];
        break;
      case RoutePathId.PLV:
        tags = [...tags, ...this.getInfoFirstSubscriptionStep()];
        break;
      default:
        break;
    }
    return tags;
  }

  private getInfoFirstSubscriptionStep(): Array<string | number | null> {
    let duration;
    let monthlyAmount;
    let taeg;
    if (store.state.subscription && store.state.subscription.response && store.state.subscription.response.proposal) {
      duration = store.state.subscription.response.proposal.creditDuration;
      monthlyAmount = store.state.subscription.response.proposal.monthlyAmountNonInsurance;
      taeg = store.state.subscription.response.proposal.taeg;
    } else if (store.state.simulator.selectedProposal !== null) {
      duration = store.state.simulator.selectedProposal.creditDurationWithoutInsurance;
      monthlyAmount = store.state.simulator.selectedProposal.monthlyAmountWithoutInsurance;
      taeg = store.state.simulator.selectedProposal.taeg;
    }

    return ['rtgduration', duration, 'rtgpayments', monthlyAmount, 'rtgtaeg', taeg];
  }

  private getInfoStepName(): Array<string | number | null> {
    const maritalStatus = store.state.subscription.principal.name.maritalStatus
      ? store.state.subscription.principal.name.maritalStatus.code
      : null;
    const hasSpouse = store.state.subscription.principal.name.hasSpouse;

    return [
      'rtgcivilite',
      store.state.subscription.principal.name.gender,
      'rtglastname',
      store.state.subscription.principal.name.lastName,
      'rtgname',
      store.state.subscription.principal.name.firstName,
      'rtgmaritalstatus',
      maritalStatus,
      'rtgcoemprunt',
      hasSpouse,
      'rtgcocivilite',
      store.state.subscription.secondary.name.gender,
      'rtgcolastname',
      store.state.subscription.secondary.name.lastName,
      'rtgconame',
      store.state.subscription.secondary.name.firstName
    ];
  }

  private getInfoStepContact(): Array<string | number | boolean | null | undefined> {
    return [
      'email',
      store.state.subscription.principal.contact.email,
      'rtgmobilephone',
      store.state.subscription.principal.contact.mobilePhone,
      'rtgphonenumber',
      store.state.subscription.principal.contact.homePhone,
      'rtgoptin_bonplan',
      store.state.subscription.principal.contact.lenderOffer,
      'rtgstoppub',
      store.state.subscription.principal.contact.contactOffer
    ];
  }

  private getInfoStepCivility(): Array<string | number | boolean | Date | null | undefined> {
    const pBirthCountry = store.state.subscription.principal.civility.birthCountry
      ? store.state.subscription.principal.civility.birthCountry.code
      : null;
    const pBirthCity = store.state.subscription.principal.civility.birthCity
      ? store.state.subscription.principal.civility.birthCity.code
      : null;
    const pNationality = store.state.subscription.principal.civility.nationality
      ? store.state.subscription.principal.civility.nationality.code
      : null;

    return [
      'rtgdateofbirth',
      store.state.subscription.principal.civility.birthDate,
      'rtgcountryofbirth',
      pBirthCountry,
      'rtgzipcodeofbirth',
      pBirthCity,
      'rtgnationality',
      pNationality
    ];
  }

  private getInfoStepFamily(): Array<string | number | null> {
    return ['rtgnbchildren', store.state.subscription.principal.family.childrenNbr];
  }

  private getInfoStepHousing(): Array<string | number> {
    const livingCity = store.state.subscription.principal.adresse.adresseCity
      ? store.state.subscription.principal.adresse.adresseCity.code
      : null;
    const housingType = store.state.subscription.principal.adresse.housingType
      ? store.state.subscription.principal.adresse.housingType.code
      : null;
    const sinceYear = store.state.subscription.principal.adresse.sinceWhen
      ? store.state.subscription.principal.adresse.sinceWhen.toString().split('/')[1]
      : null;

    return [
      'rtgadress',
      store.state.subscription.principal.adresse.adresseLabel,
      'rtgzipcode',
      livingCity,
      'rtghomeownershipstatus',
      housingType,
      'rtghomeownershipstatusyear',
      sinceYear
    ];
  }

  private getInfoStepEmployment(): Array<string | number> {
    const pProfession = store.state.subscription.principal.employment.profession
      ? store.state.subscription.principal.employment.profession.code
      : null;
    const pSinceMonth = store.state.subscription.principal.employment.sinceWhen
      ? store.state.subscription.principal.employment.sinceWhen.toString().split('/')[0]
      : null;
    const pSinceYear = store.state.subscription.principal.employment.sinceWhen
      ? store.state.subscription.principal.employment.sinceWhen.toString().split('/')[1]
      : null;
    const pContract = store.state.subscription.principal.employment.contract
      ? store.state.subscription.principal.employment.contract.code
      : null;
    const pArea = store.state.subscription.principal.employment.activitySector
      ? store.state.subscription.principal.employment.activitySector.code
      : null;

    return [
      'rtgprofession',
      pProfession,

      'rtgstartprofessionmonth',
      pSinceMonth,

      'rtgstartprofessionyear',
      pSinceYear,

      'rtgemploymentcontrat',
      pContract,

      'rtgemploymentsector',
      pArea
    ];
  }

  private getInfoStepIncomes(): Array<string | number | null> {
    const pHousing = store.state.subscription.principal.incomes.housingBenefits
      ? parseInt(store.state.subscription.principal.incomes.housingBenefits.toString(), 10)
      : 0;
    const pFamilyBenefits = store.state.subscription.principal.incomes.familyBenefits
      ? parseInt(store.state.subscription.principal.incomes.familyBenefits.toString(), 10)
      : 0;
    const pOtherIncomes = store.state.subscription.principal.incomes.otherIncomes
      ? parseInt(store.state.subscription.principal.incomes.otherIncomes.toString(), 10)
      : 0;
    const pIncomes = pHousing + pFamilyBenefits + pOtherIncomes;

    return [
      'rtgmonthlyincome',
      store.state.subscription.principal.incomes.netIncome,

      'rtgothermonthlyincome',
      pIncomes,

      'rtgnbmonthsalary',
      store.state.subscription.principal.incomes.salaryMonths
    ];
  }

  private getInfoStepCharges(): Array<string | number | null> {
    const other = store.state.subscription.principal.charges.otherCredit
      ? parseInt(store.state.subscription.principal.charges.otherCredit.toString(), 10)
      : 0;
    const house = store.state.subscription.principal.charges.refundHouseCredit
      ? parseInt(store.state.subscription.principal.charges.refundHouseCredit.toString(), 10)
      : 0;
    const car = store.state.subscription.principal.charges.carCredit
      ? parseInt(store.state.subscription.principal.charges.carCredit.toString(), 10)
      : 0;
    const renewable = store.state.subscription.principal.charges.revolving
      ? parseInt(store.state.subscription.principal.charges.revolving.toString(), 10)
      : 0;
    const charges = other + house + car + renewable;

    return [
      'rtgmonthlyexpense',
      store.state.subscription.principal.charges.rental,
      'rtgothermonthlyloanrefund',
      charges
    ];
  }

  private getInfoStepCard(): Array<string | boolean | null> {
    // Card step is not present in VAC process
    if (store.state.subscription && store.state.subscription.principal && store.state.subscription.principal.wantCard) {
      return ['rtgoptin_mastercard', store.state.subscription.principal.wantCard];
    } else {
      return [];
    }
  }

  private getInfoStepInsurance(): Array<string | boolean | null> {
    return ['rtgoptin_assurance', store.state.subscription.principal.insurance.acceptInsuranceOffer];
  }

  private getInfoStepResult(): Array<string | boolean | number | null> {
    const ref = store.state.subscription.response ? store.state.subscription.response.contractId : null;
    const scoring = store.state.subscription.response ? store.state.subscription.response.acceptedCode : null;
    const seeligibility = store.state.subscription.response ? store.state.subscription.response.eligibilitySE : null;

    return ['ref', ref, 'scoring', scoring, 'estimate', '1', 'seeligibility', seeligibility];
  }

  private getInfoHitSimulator(): Array<string | number | null> {
    return ['rtgsimulator_hit', store.state.simulator.timesUserSelectedOffer];
  }
}

declare global {
  interface Window {
    EA_collector: any;
    EA_event: any;
    dataLayer: any;
  }
}
