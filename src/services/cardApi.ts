import { Basket, CardBasket } from '@/models/basket';
import axios, { AxiosPromise } from 'axios';
import Config, { ID_TOKEN } from '@/config/config';
import getConfig from '@/config/config';

export function getCard(cardId: string): Promise<CardBasket> {
  return axios
    .get(`${getConfig().API_CARD}/${cardId}`, {
      headers: {
        Authorization: `Bearer ${ID_TOKEN}`,
        //   Correlationid: Config.CORRELATION_ID,
        'Content-Type': 'application/json'
      }
    })
    .then((res) => res.data);
}

export function getKeyring(keyring: string): Promise<CardBasket> {
  return axios
    .get(`${getConfig().API_KEYRING}/${keyring}`, {
      headers: {
        Authorization: `Bearer ${ID_TOKEN}`,
        //   Correlationid: Config.CORRELATION_ID,
        'Content-Type': 'application/json'
      }
    })
    .then((res) => res.data);
}

export function putCard(card: CardBasket) {
  axios.put(`${getConfig().API_CARD}/${card.identifier}`, card, {
    headers: {
      Authorization: `Bearer ${ID_TOKEN}`,
      Correlationid: getConfig().CORRELATION_ID
    }
  });
}

export function postCard(card: CardBasket): Promise<string> {
  return axios
    .post(`${getConfig().API_CARD}`, card, {
      headers: {
        Authorization: `Bearer ${ID_TOKEN}`,
        // Correlationid: Config.CORRELATION_ID,
        'Content-Type': 'application/json'
      }
    })
    .then((res) => {
      return res.data ? res.data.identifier : '';
    });
}
