import store from '@/store';
import { TagManagerPartnerService } from '@/services/tagManagerPartner';
import { TagManagerSofincoService } from '@/services/tagManagerSofinco';
import { Route } from 'vue-router';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';

export function getTagManagerInstance(): TagManagerService {
  return store.getters.isSofincoPartnerCode ? new TagManagerSofincoService() : new TagManagerPartnerService();
}

export interface TagManagerService {
  pageSubscriptionProcessChange(route: Route): void;
  pageChange(to: Route, from: Route): void;
  offerOptInClick(optIn: boolean | null): void;
  insuranceNextClick(hasInsurance: boolean | null);
  stopPubClick(optIn: boolean): void;
  gaClick(ecat: string, eaction: string, elabel: string, callback?: () => void);
  selectProduct(): void;
  preContractClick(): void;
  clickSummaryMailW2S(): void;
  tagError(errorStatus: any): void;
  tagIndependantSimulatorOffersPage(route: Route);
  tagIndependantSimulatorAmountPage(route: Route);
  tagIndependantSimulatorAllPageGTM(route: Route);
  tagIndependantSimulatorOffersSelection(route: Route, selectedOffer: SimulationFinancialProposal);
  postClick(element: HTMLElement): void;
}
