import axios from 'axios';
import { IKeypadsParametersV2, IKeypadsResponse } from '@/models/subscription/authentication/Keypad';
import { AxiosPromise } from 'axios';
import getConfig, { ID_TOKEN } from '@/config/config';

export class KeypadApi {
  public constructor() {
    // Do nothing
  }

  public getKeypads(parameters: IKeypadsParametersV2): AxiosPromise<IKeypadsResponse> {
    return axios.post(getConfig().API_INIT_KEYPAD, parameters, {
      headers: {
        Authorization: `Bearer ${ID_TOKEN}`,
        Correlationid: getConfig().CORRELATION_ID,
        'Content-Type': 'application/json'
      }
    });
  }

  public getKeypad(keypadId: string): string {
    return getConfig().API_INIT_KEYPAD + `/${keypadId}`;
  }
}
