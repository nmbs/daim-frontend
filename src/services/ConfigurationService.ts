import { RouteConfig } from 'vue-router';
import store from '@/store';

import { AppConfiguration } from '@/models/Configuration';
import { Step } from '@/models/Step';
import { getComponentOfStep } from '@/services/getComponent';
import { StepState } from '@/models/StepState';
import { RoutePathId } from '@/models/RoutesPath';

/**
 * Initialize StepStates structure.
 */
export function initStepsStates(appConfiguration: AppConfiguration): StepState {
  const configurationRoutes: StepState[] = [];

  const rootSteps = new StepState('', [], null, null, true);
  appConfiguration.steps.forEach((step: Step) => {
    // todo remove when pedagoic will be removed from API Conf
    if (step.pathId !== RoutePathId.PEDAGOGIC) {
      configurationRoutes.push(computeStepState(step, rootSteps));
    }
  });
  rootSteps.steps = configurationRoutes;
  return rootSteps;
}

/**
 * Builds a StepState from a Step.
 * @param step The current Step.
 * @param parent The StepState of the parent step.
 */
function computeStepState(step: Step, parent: StepState): StepState {
  const childrenStates: StepState[] = [];
  const stepState = new StepState(step.pathId, [], step.screenOptions, parent, step.routed, step.noReturn);

  if (step.steps) {
    step.steps.forEach((child: Step) => {
      childrenStates.push(computeStepState(child, stepState));
    });
    stepState.steps = childrenStates;
  }
  return stepState;
}

/**
 * Computes RouteConfig (that will be used to initialize the router) from an AppConfiguration.
 * @param appConfiguration Configuration coming from the API.
 */
export function getRoutesConfig(appConfiguration: AppConfiguration): RouteConfig[] {
  // for each main steps, compute its associated routes and children routes
  const routesConfig: RouteConfig[] = [];
  appConfiguration.steps.forEach((step: any) => {
    const items = computeStepRoutes(step, '', null);
    if (items) {
      routesConfig.push(items);
    }
  });

  // Add redirection for '/' route: redirect to the first routed step
  routesConfig.unshift({
    path: '/',
    redirect: routesConfig[0].path
  });

  return routesConfig;
}

/**
 * Build a RouteConfig object from a given current step
 * @param step The current step
 * @param parentPath The path of the parent step route
 * @param parentStep The parent step of the current step
 */
function computeStepRoutes(step: Step, parentPath: string, parentStep: Step | null): RouteConfig | null {
  const componentOfStep = getComponentOfStep(step, parentStep);
  if (!componentOfStep) {
    return null;
  }
  const routeConfig: RouteConfig = {
    name: step.pathId,
    path: `${parentPath}/${step.pathId}`,
    component: componentOfStep,
    props: {
      stepState: store.getters.getCurrentStepStates(step.pathId)
    },
    children: []
  };

  const steps = step.steps;
  if (steps) {
    steps.forEach((childStep: Step) => {
      if (childStep.routed) {
        // @ts-ignore children cannot be null at this point as it was initialized earlier
        const computeStepRoutes1 = computeStepRoutes(childStep, routeConfig.path, step);
        if (computeStepRoutes1 && routeConfig && routeConfig.children) {
          routeConfig.children.push(computeStepRoutes1);
        }
      }
    });
  }

  // @ts-ignore children cannot be null at this point as it was initialized earlier
  if (routeConfig.children.length > 0) {
    // @ts-ignore
    routeConfig.redirect = { name: routeConfig.children[0].name };
  }

  return routeConfig;
}

/**
 * Computes and returns a flat array containing all steps ordered from the first to the last.
 * @param appConfiguration
 */
export function getRoutesList(appConfiguration: AppConfiguration): string[] {
  const subscriptionStep = appConfiguration.steps.find((step: StepState) => step.pathId === RoutePathId.SUBSCRIPTION);
  const subscriptionRoutes = subscriptionStep ? subscriptionStep.steps : [];
  const routesList: string[] = [];

  subscriptionRoutes.forEach((step: StepState) => {
    routesList.push(step.pathId);

    if (step.steps && step.steps.length > 0) {
      step.steps.forEach((subStep: StepState) => {
        if (subStep.routed) {
          routesList.push(subStep.pathId);
        }
      });
    }
  });

  return routesList;
}

/**
 * Computes and returns a map that associate a bloc to its sub routes for the steps of the subscription tunnel.
 * @param subscriptionRouteConfig
 */
export function getSubscriptionRoutesMap(subscriptionRouteConfig: any) {
  const routesConfig: Map<any, any> = new Map();
  if (subscriptionRouteConfig && subscriptionRouteConfig.children) {
    subscriptionRouteConfig.children.forEach((child: any) => {
      const arrayChildPath: any[] = [];
      child.children.forEach((littleChild: any) => {
        arrayChildPath.push(littleChild.name);
      });
      if (arrayChildPath.length === 0) {
        arrayChildPath.push(child.name);
      }
      routesConfig.set(child.name, arrayChildPath);
    });
  }
  return routesConfig;
}

/**
 * Merge offer and partner configuration.
 * Options are merged. If an option is present in offer and partner configuration, offer value is used.
 * Steps are not merged. If steps are defined in offer, offer steps are used. Otherwise partner steps are used.
 * @param offerConf
 * @param partnerConf
 */
export function mergeConfigurations(offerConf: AppConfiguration, partnerConf: AppConfiguration): AppConfiguration {
  const commonConfig: any = {};
  if (offerConf) {
    if (offerConf.steps && offerConf.steps.length) {
      commonConfig.steps = offerConf.steps;
    } else {
      commonConfig.steps = partnerConf.steps;
    }
    if (partnerConf.screenOptions) {
      commonConfig.screenOptions = {
        ...commonConfig.screenOptions,
        ...partnerConf.screenOptions
      };
    }
    if (offerConf.screenOptions) {
      commonConfig.screenOptions = {
        ...commonConfig.screenOptions,
        ...offerConf.screenOptions
      };
    }
  } else {
    commonConfig.steps = partnerConf.steps;
    commonConfig.screenOptions = partnerConf.screenOptions;
  }
  return commonConfig;
}
