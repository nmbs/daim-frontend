import CryptoJS from 'crypto-js';

import { generatePrivateKey } from '@/utils/encryption';
import { Basket, CardBasket, SubscriptionVersion } from '@/models/basket';
import { BasketRecoveryStorage, Keys, Type, TypeConversion } from '@/models/basketRecoveryStorage';
import { LOCAL_STORAGE_CARD_API_IDENTIFIER } from '@/models/basket';
import * as CardAPI from '@/services/cardApi';
import { getKeyring, postCard, putCard } from '@/services/cardApi';
import { BusinessRule } from '@/services/BusinessRule';
import store from '@/store';

import * as Mutations from '@/store/Mutations';
import { Gender, Name } from '@/models/subscription/Name';
import { SubscriptionData } from '@/store/types';
import { Family } from '@/models/subscription/Family';
import { Contact } from '@/models/subscription/Contact';
import { Civility } from '@/models/subscription/Civility';
import { Incomes } from '@/models/subscription/Incomes';
import { Charges } from '@/models/subscription/Charges';
import { Employment } from '@/models/subscription/Employment';
import { Adresse } from '@/models/subscription/Adresse';
import { Insurance } from '@/models/subscription/Insurance';
import { RouteState, RouteStates } from '@/models/subscription/RouteState';
import { InsuranceFormState } from '@/models/subscription/FormState';

/**
 * This service manage the interruptibility concerning the local storage and use the cardApi service :
 *  Store Cart API ID and Private Keys
 */
export class InterruptibilityService {
  private storage: any = null;
  private basketRecoveryStorage: BasketRecoveryStorage | null = null;

  constructor() {
    this.storage = window.localStorage;
    this.basketRecoveryStorage = this.getStore();
    if (!this.basketRecoveryStorage && BusinessRule.hasInterruptibility(store.state.apiConfig.appConfiguration)) {
      this.createStore();
    }
  }

  /**
   * Manage the interruptiblity :
   *  - activate only if it's active in conf and phone number is filled
   *  - save the state in the card api
   *  - udpate the card type is the result is ok
   *  - delete the card if the result is ko
   * @param label
   */
  public manageInterruptibility(label: string) {
    if (
      BusinessRule.hasInterruptibility(store.state.apiConfig.appConfiguration) &&
      store.getters.interruptibilityPossible
    ) {
      if (
        store.state.simulator.simulationContext.simulationOrigin &&
        store.state.subscription.cardId !== store.state.simulator.simulationContext.simulationOrigin
      ) {
        store.commit('updateCardId', store.state.simulator.simulationContext.simulationOrigin);
      }
      let type = null;
      if (store.getters.isResultOK) {
        type = Type.FAVORABLE;
        this.saveState(type, label);
      } else if (store.getters.isResultKO) {
        this.removeCard(store.state.subscription.cardId);
      } else {
        type = Type.SUBSCRIPTION;
        this.saveState(type);
      }
    }
  }

  /**
   * Restore the vuex state and return the route name to be redirected.
   *
   * @param savedState the saved state that has been decrypted from the basket
   * @param subscriptionVersion the version of the saved state
   */
  public restoreState(savedState: any, subscriptionVersion): string {
    let routeStates: RouteStates;
    if (subscriptionVersion === SubscriptionVersion.V1) {
      const isCreditForTwo: boolean = savedState.forms.creditForTwo;
      store.commit(
        Mutations.SAVE_PRINCIPAL,
        this.mapV1SubscriptionDataToV2(savedState.forms.principal, isCreditForTwo)
      );
      if (isCreditForTwo) {
        store.commit(
          Mutations.SAVE_SECONDARY,
          this.mapV1SubscriptionDataToV2(savedState.forms.secondary, isCreditForTwo)
        );
      }
      store.commit(Mutations.SET_CREDIT_FOR_TWO, savedState.forms.creditForTwo);
      routeStates = this.mapV1RouteStatesToV2(savedState.routes);
    } else if (subscriptionVersion === SubscriptionVersion.V2) {
      store.commit(Mutations.SAVE_PRINCIPAL, savedState.subscription.principal);
      if (savedState.subscription.secondary) {
        store.commit(Mutations.SAVE_SECONDARY, savedState.subscription.secondary);
      }
      store.commit(Mutations.SET_CREDIT_FOR_TWO, savedState.subscription.creditForTwo);

      // Manage the case where the insurance step to display is the new version (v2)
      if (savedState.subscription.insuranceFormState) {
        const {
          version,
          t1IsEligibleRecommended,
          t1IsEligibleAlternative,
          t2IsEligibleRecommended,
          t2IsEligibleAlternative
        }: InsuranceFormState = savedState.subscription.insuranceFormState;
        if (version === 'v2') {
          store.commit(Mutations.SET_INSURANCE_FORM_VERSION, savedState.subscription.insuranceFormState.version);
          store.dispatch('setInsuranceEligibility', {
            t1IsEligibleToRecommendedInsurance: t1IsEligibleRecommended,
            t2IsEligibleToRecommendedInsurance: t2IsEligibleRecommended,
            t1IsEligibleToAlternativeInsurance: t1IsEligibleAlternative,
            t2IsEligibleToAlternativeInsurance: t2IsEligibleAlternative
          });
        }
      }

      routeStates = savedState.subscription.routeStates;
    } else {
      throw new Error(`Subscription restore has not yet been implemented for ${subscriptionVersion}`);
    }
    // Manage route states
    if (routeStates) {
      store.commit(Mutations.SET_ROUTE_STATES, routeStates);
      for (const routeName in routeStates) {
        if (routeStates[routeName].active) {
          store.commit(Mutations.ACTIVATE_ROUTE, routeName, { root: true });
          return routeName;
        }
      }
    }
  }

  /**
   *  Load the basket from the basket and save it in the store
   * @param key basket id
   * @param secret secret store in local storage to decrypt the basket
   */
  public restoreSubscriptionFromCardId(key: Keys, secret: string): Promise<Basket> {
    return CardAPI.getCard(key.id).then((card) => {
      const basket: Basket = this.decryptContent(card.value, secret);
      if (card.type !== TypeConversion[Type.SUBSCRIPTION]) {
        throw Error('Incompatible basket type');
      }
      const publicState = JSON.parse(basket.publicState);
      store.commit('updateBasket', basket);
      store.commit('updateCardId', card.identifier);
      store.commit('updateSimulationId', publicState.simulationId);
      let funnelId = publicState.funnelId;
      if (!funnelId) {
        funnelId = generatePrivateKey();
      }
      store.commit('updateFunnelId', funnelId);
      return basket;
    });
  }

  public getKeyById(cardId: string): Keys {
    const card = this.basketRecoveryStorage.keys.find((key: Keys) => {
      return key.id === cardId;
    });

    return card;
  }

  public getSecret() {
    this.basketRecoveryStorage = this.getStore();
    return this.basketRecoveryStorage.secret;
  }

  public getStoredSimulationKeys() {
    this.basketRecoveryStorage = this.getStore();
    return this.basketRecoveryStorage;
  }

  public removeCard(cardIdentifier: string) {
    if (cardIdentifier) {
      this.basketRecoveryStorage.keys = this.basketRecoveryStorage.keys.filter((e) => e.id !== cardIdentifier);
      this.updateStore();
      this.updateMasterKey();
    }
  }

  public decryptContent(serializedJwt: string, secret: string): any {
    let decryptedState = null;
    try {
      const ciphertext = CryptoJS.AES.decrypt(serializedJwt, secret);
      decryptedState = JSON.parse(ciphertext.toString(CryptoJS.enc.Utf8));
    } catch (err) {
      decryptedState = null;
    }
    return decryptedState;
  }

  public updateCard(cardIdentifier: string, type: Type) {
    this.basketRecoveryStorage.keys.forEach((key) => {
      if (key.id === cardIdentifier) {
        key.type = type;
        this.updateStore();
        return;
      }
    });
  }

  public addCard(cardIdentifier: string, type: Type) {
    if (cardIdentifier) {
      this.basketRecoveryStorage.keys.push({
        id: cardIdentifier,
        type
      });
      this.updateStore();
      this.updateMasterKey();
    }
  }

  public encryptContent(content: any, secret: string) {
    return CryptoJS.AES.encrypt(JSON.stringify(content), secret);
  }

  public getBasketRecoveryStorage() {
    return this.basketRecoveryStorage;
  }

  public restoreLocalStorage(keyringID): Promise<any> {
    if (keyringID) {
      return getKeyring(keyringID)
        .then((card) => {
          const serializedStore = card.value;
          this.loadSerializedStore(serializedStore);
        })
        .catch((error) => {});
    }
  }

  public loadSerializedStore(serializedKeyring: string) {
    try {
      this.storage.setItem(`${LOCAL_STORAGE_CARD_API_IDENTIFIER}`, serializedKeyring);
      this.basketRecoveryStorage = this.getStore();
    } catch (err) {}
  }

  public saveState(type: Type, label2Str: string = null) {
    const secretSalt = store.state.subscription.principal.contact.mobilePhone
      .replace(/\s/g, '')
      .replace(/^\+?(3){2}([0-9]{9})$/, '0$2');
    const subscriptionLight = {
      principal: store.state.subscription.principal,
      secondary: store.state.subscription.secondary,
      creditForTwo: store.state.subscription.creditForTwo,
      // The insurance form state need to be stored in order to know which version of the insurance step need to be displayed to the user
      // If the insurance version is v2, the eligibility to the recommended and alternative insurance need to be stored
      insuranceFormState: store.state.subscription.formStates.insurance,
      routeStates: store.state.subscription.routeStates
    };
    const encryptedStore = this.encryptContent(
      { subscription: subscriptionLight },
      this.getSecret().concat(secretSalt)
    );

    let offerEndDateLong: number = null;
    if (type === Type.FAVORABLE) {
      let offerEndDate: Date = null;
      offerEndDate = new Date();
      offerEndDate.setDate(offerEndDate.getDate() + 15);
      offerEndDateLong = offerEndDate.getTime();
    }

    const basket: Basket = {
      publicState: JSON.stringify({
        loggedUser:
          store.state.subscription.authentication && store.state.subscription.authentication.response
            ? !!store.state.subscription.authentication.response.customerId
            : false, // todo get this value,
        coborrower: store.state.subscription.creditForTwo,
        cardType: store.state.simulator.simulationContext.cardType,
        label: store.state.simulator.selectedProposal.label,
        amount: store.state.simulator.selectedProposal.amount,
        sourceId: store.state.simulator.simulationContext.sourceId,
        projectId: store.state.simulator.simulationContext.projectId,
        vehicleAge: store.state.simulator.simulationContext.vehicleAge,
        partnerId: store.state.simulator.simulationContext.partnerId,
        simulationId: store.state.simulator.simulationContext.simulationId,
        funnelId: '', // to generate
        commercialCode: null, // TODO: fill this param
        taeg: store.state.simulator.selectedProposal.taeg,
        duration: store.state.simulator.selectedProposal.nbMonthlyPaymentWithInsurance,
        monthlyAmountNonInsurance: store.state.simulator.selectedProposal.monthlyAmountWithoutInsurance,
        label2: label2Str,
        offerEndDate: offerEndDateLong,
        subscriptionVersion: SubscriptionVersion.V2
      }),
      encryptedState: encryptedStore.toString()
    };

    const encryptedBasket = this.encryptContent(basket, this.getSecret());

    const currentIdentifier = store.state.subscription.cardId;
    if (currentIdentifier) {
      this.updateCard(currentIdentifier, type);
      putCard({ value: encryptedBasket.toString(), type: TypeConversion[type], identifier: currentIdentifier });
    } else {
      postCard({ value: encryptedBasket.toString(), type: TypeConversion[type] }).then((identifier) => {
        this.addCard(identifier, type);
        store.commit('updateCardId', identifier);
      });
    }
  }

  private createStore() {
    if (!this.basketRecoveryStorage) {
      try {
        this.basketRecoveryStorage = {
          keys: [],
          secret: generatePrivateKey(),
          masterKey: ''
        };
      } catch (err) {
        this.basketRecoveryStorage = null;
      }
      this.updateStore();
      this.createMasterKey();
    }
  }

  private updateStore() {
    try {
      this.storage.setItem(`${LOCAL_STORAGE_CARD_API_IDENTIFIER}`, JSON.stringify(this.basketRecoveryStorage));
    } catch (err) {}
  }

  private getStore() {
    try {
      return JSON.parse(this.storage.getItem(`${LOCAL_STORAGE_CARD_API_IDENTIFIER}`));
    } catch (err) {}
    return null;
  }

  private createMasterKey() {
    const ciphertext = CryptoJS.AES.encrypt('[]', this.getBasketRecoveryStorage().secret);
    return postCard({
      type: 'keys-v1',
      value: ciphertext.toString()
    }).then((identifier) => {
      this.getBasketRecoveryStorage().masterKey = identifier;
      this.updateStore();
      return identifier;
    });
  }

  private getMasterKey() {
    try {
      return this.getStore().masterKey;
    } catch (err) {}
    return null;
  }

  private getOrCreateMasterKey(): Promise<string> {
    if (this.getMasterKey()) {
      return Promise.resolve(this.getMasterKey());
    } else {
      return this.createMasterKey();
    }
  }

  private updateMasterKey() {
    const ciphertext = CryptoJS.AES.encrypt(
      JSON.stringify(this.basketRecoveryStorage.keys),
      this.getBasketRecoveryStorage().secret
    );
    this.getOrCreateMasterKey().then((key) => {
      putCard({
        identifier: key,
        type: 'keys-v1',
        value: ciphertext.toString()
      });
    });
  }

  private mapV1RouteStatesToV2(routes): RouteStates {
    const informationBloc: RouteState = {
      valid: routes.infosFormBlock.valid,
      disabled: routes.infosFormBlock.disabled,
      active: routes.infosFormBlock.active,
      visible: routes.infosFormBlock.visible,
      isBlock: routes.infosFormBlock.isBlock,
      hasChildren: true
    };
    const loan: RouteState = {
      valid: routes.infosLoansForm.valid,
      disabled: routes.infosLoansForm.disabled,
      active: routes.infosLoansForm.active,
      visible: routes.infosLoansForm.visible,
      hasChildren: false
    };
    const name: RouteState = {
      valid: routes.infosNameForm.valid,
      disabled: routes.infosNameForm.disabled,
      active: routes.infosNameForm.active,
      visible: routes.infosNameForm.visible,
      hasChildren: false
    };
    const contact: RouteState = {
      valid: routes.infosContactForm.valid,
      disabled: routes.infosContactForm.disabled,
      active: routes.infosContactForm.active,
      visible: routes.infosContactForm.visible,
      hasChildren: false
    };
    const situationBloc: RouteState = {
      valid: routes.situationFormBlock.valid,
      disabled: routes.situationFormBlock.disabled,
      active: routes.situationFormBlock.active,
      visible: routes.situationFormBlock.visible,
      isBlock: routes.situationFormBlock.isBlock,
      hasChildren: true
    };
    const civility: RouteState = {
      valid: routes.situationIdentityForm.valid,
      disabled: routes.situationIdentityForm.disabled,
      active: routes.situationIdentityForm.active,
      visible: routes.situationIdentityForm.visible,
      hasChildren: false
    };
    const family: RouteState = {
      valid: routes.situationFamilyForm.valid,
      disabled: routes.situationFamilyForm.disabled,
      active: routes.situationFamilyForm.active,
      visible: routes.situationFamilyForm.visible,
      hasChildren: false
    };
    const housing: RouteState = {
      valid: routes.situationAddressForm.valid,
      disabled: routes.situationAddressForm.disabled,
      active: routes.situationAddressForm.active,
      visible: routes.situationAddressForm.visible,
      hasChildren: false
    };
    const incomesBloc: RouteState = {
      valid: routes.incomesFormBlock.valid,
      disabled: routes.incomesFormBlock.disabled,
      active: routes.incomesFormBlock.active,
      visible: routes.incomesFormBlock.visible,
      isBlock: routes.incomesFormBlock.isBlock,
      hasChildren: true
    };
    const employment: RouteState = {
      valid: routes.incomesEmploymentForm.valid,
      disabled: routes.incomesEmploymentForm.disabled,
      active: routes.incomesEmploymentForm.active,
      visible: routes.incomesEmploymentForm.visible,
      hasChildren: false
    };
    const incomes: RouteState = {
      valid: routes.incomesIncomesForm.valid,
      disabled: routes.incomesIncomesForm.disabled,
      active: routes.incomesIncomesForm.active,
      visible: routes.incomesIncomesForm.visible,
      hasChildren: false
    };
    const charges: RouteState = {
      valid: routes.incomesChargesForm.valid,
      disabled: routes.incomesChargesForm.disabled,
      active: routes.incomesChargesForm.active,
      visible: routes.incomesChargesForm.visible,
      hasChildren: false
    };
    const insurance: RouteState = {
      valid: routes.insuranceForm.valid,
      disabled: routes.insuranceForm.disabled,
      active: routes.insuranceForm.active,
      visible: routes.insuranceForm.visible,
      isBlock: false,
      hasChildren: false
    };
    // Init with default value (card step is not present in the v1 subscription)
    const card: RouteState = {
      disabled: true,
      active: false,
      visible: true,
      isBlock: true,
      hasChildren: false
    };
    const summary: RouteState = {
      disabled: routes.summaryForm.disabled,
      active: routes.summaryForm.active,
      visible: routes.summaryForm.visible,
      valid: routes.summaryForm.valid,
      isBlock: true,
      hasChildren: false
    };
    // Init with default value (the loading step does not contain the interruptibility)
    const loading: RouteState = {
      disabled: true,
      active: false,
      visible: false,
      isBlock: true,
      hasChildren: false
    };
    // Init with default value (the result step does not contain the interruptibility)
    const result: RouteState = {
      disabled: true,
      active: false,
      visible: true,
      isBlock: true,
      hasChildren: false
    };
    // Init with default value (the result sofinco step does not contain the interruptibility)
    const resultSofinco: RouteState = {
      disabled: true,
      active: false,
      visible: true,
      isBlock: true,
      hasChildren: false
    };
    return {
      informationBloc,
      loan,
      name,
      contact,
      situationBloc,
      civility,
      family,
      housing,
      incomesBloc,
      employment,
      incomes,
      charges,
      insurance,
      card,
      summary,
      loading,
      result,
      resultSofinco
    };
  }

  private mapV1SubscriptionDataToV2(borrowerData, isCreditForTwo): SubscriptionData {
    const name: Name = {
      gender: borrowerData.nameForm.gender === Gender.MAN ? Gender.MAN : Gender.WOMAN,
      firstName: borrowerData.nameForm.firstName,
      lastName: borrowerData.nameForm.lastName,
      birthName: borrowerData.nameForm.maidenName,
      maritalStatus: borrowerData.familyForm.maritalStatus,
      hasSpouse: isCreditForTwo
    };
    const family: Family = {
      childrenNbr: borrowerData.familyForm.children
    };
    const contact: Contact = {
      email: borrowerData.contactForm.email,
      mobilePhone: borrowerData.contactForm.mobilePhone,
      homePhone: borrowerData.contactForm.homePhone,
      lenderOffer: borrowerData.contactForm.lenderOffer,
      partnerOffer: borrowerData.contactForm.partnerOffer,
      contactOffer: borrowerData.housingForm.contactOffer
    };
    const civility: Civility = {
      birthDate: borrowerData.identityForm.birthDate,
      birthCity: borrowerData.identityForm.birthCity,
      birthCountry: borrowerData.identityForm.birthCountry,
      nationality: borrowerData.identityForm.nationality
    };
    const incomes: Incomes = {
      netIncome: borrowerData.incomesForm.netIncome,
      salaryMonths: borrowerData.incomesForm.salaryMonths,
      moreIncomes: borrowerData.incomesForm.moreIncomes,
      housingBenefits: borrowerData.incomesForm.housingBenefits,
      familyBenefits: borrowerData.incomesForm.familyBenefits,
      otherIncomes: borrowerData.incomesForm.otherIncomes
    };
    const charges: Charges = {
      rental: borrowerData.chargesForm.monthlyRentCharges,
      refundHouseCredit: borrowerData.chargesForm.refundHouseCredit,
      hasAlimony: borrowerData.chargesForm.hasAlimonyCharges,
      alimony: borrowerData.chargesForm.alimonyCharges,
      moreCredits: borrowerData.chargesForm.moreCredit,
      carCredit: borrowerData.chargesForm.refundCarCredit,
      revolving: borrowerData.chargesForm.refundRenewableCredit,
      otherCredit: borrowerData.chargesForm.refundOtherCredit
    };
    const employment: Employment = {
      activitySector: borrowerData.professionalForm.area,
      contract: borrowerData.professionalForm.contractType,
      profession: borrowerData.professionalForm.profession,
      professionalStatus: borrowerData.professionalForm.status,
      sinceWhen: borrowerData.professionalForm.since
    };
    const adresse: Adresse = {
      adresseLabel: borrowerData.housingForm.address,
      complementAdresse: borrowerData.housingForm.additionalAddress,
      adresseCity: borrowerData.housingForm.livingCity,
      housingType: borrowerData.housingForm.housingType,
      sinceWhen: borrowerData.housingForm.since
    };
    const insurance: Insurance = {
      acceptInsuranceOffer: borrowerData.insuranceForm.acceptInsuranceOffer,
      confirmRefusal: !borrowerData.insuranceForm.acceptInsuranceOffer
    };
    return {
      name,
      family,
      contact,
      civility,
      incomes,
      charges,
      employment,
      adresse,
      insurance,
      // Init with default value, the following properties are not present in the v1 subscription data
      insurancev2: {
        selectedInsurance: null,
        refuseInsurance: false
      },
      wantCard: null,
      cardCode: null
    };
  }
}
