import { EFilterCriterionAttribute } from '@/models/simulator/tools/filterCriterion';

export const sortSimulationProposal = (proposals, proposalFilter) => {
  const sortEqualOffer = (a, b) => {
    if (a.taeg === 0 && b.taeg !== 0) {
      return -1;
    } else if (a.taeg !== 0 && b.taeg === 0) {
      return 1;
    } else {
      if (a.deferralMonthNumber < b.deferralMonthNumber) {
        return -1;
      } else if (a.deferralMonthNumber > b.deferralMonthNumber) {
        return 1;
      }
    }
  };

  return [...proposals].sort((a, b) => {
    if (proposalFilter.type === EFilterCriterionAttribute.MONTHLY_PAYMENT) {
      if (a.monthlyAmountWithoutInsurance < b.monthlyAmountWithoutInsurance) {
        return -1;
      } else if (a.monthlyAmountWithoutInsurance > b.monthlyAmountWithoutInsurance) {
        return 1;
        // if (a.monthlyAmountWithoutInsurance == b.monthlyAmountWithoutInsurance)
      } else {
        return sortEqualOffer(a, b);
      }
    } else if (proposalFilter.type === EFilterCriterionAttribute.TIMELINE) {
      if (a.nbMonthlyPaymentWithoutInsurance < b.nbMonthlyPaymentWithoutInsurance) {
        return -1;
      } else if (a.nbMonthlyPaymentWithoutInsurance > b.nbMonthlyPaymentWithoutInsurance) {
        return 1;
        // if (a.nbMonthlyPaymentWithoutInsurance == b.nbMonthlyPaymentWithoutInsurance)
      } else {
        return sortEqualOffer(a, b);
      }
    }
  });
};

export const proposalSelected = (filter, proposals, scaleCode = null) => {
  let min = -1;
  let result = -1;
  proposals.forEach((proposal, index) => {
    let distance = null;
    if (filter.type === EFilterCriterionAttribute.MONTHLY_PAYMENT) {
      distance = Math.abs(filter.value - proposal.monthlyAmountWithoutInsurance);
    } else if (filter.type === EFilterCriterionAttribute.TIMELINE) {
      if (proposal.lastMonthlyAmountWithoutInsurance) {
        // Need to add 1 if there is a last adjusted monthly payment
        distance = Math.abs(filter.value - (proposal.nbMonthlyPaymentWithoutInsurance + 1));
      } else {
        distance = Math.abs(filter.value - proposal.nbMonthlyPaymentWithoutInsurance);
      }
    }
    if (min === -1 || min > distance) {
      min = distance;
      result = index;
    } else if (min === distance) {
      if (!scaleCode) {
        if (proposal.taeg === 0) {
          result = index;
        }
      } else {
        if (proposal.scaleCode === scaleCode) {
          result = index;
        }
      }
    }
  });

  return result;
};
