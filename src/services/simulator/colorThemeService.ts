import * as lightTheme from '@/assets/style/color_presets/light_theme.json';
import * as darkTheme from '@/assets/style/color_presets/dark_theme.json';
import * as sofincoTheme from '@/assets/style/color_presets/sofinco_theme.json';
import { ColorThemeEnum } from '@/models/simulator/colorTheme';

export interface IColorEntry {
  name: string;
  value: string;
}

export function loadTheme(theme: string): any {
  if (theme === ColorThemeEnum.LIGHT) {
    return lightTheme.default as IColorEntry[];
  } else if (theme === ColorThemeEnum.DARK) {
    return darkTheme.default as IColorEntry[];
  } else {
    return sofincoTheme.default as IColorEntry[];
  }
}
