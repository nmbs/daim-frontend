import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';

/**
 * Find and return pre-selected offer from list of offers
 * @param offers List of offers
 * @param suggestedDueNumber dueNumber of the offer we want to pre-select
 * @param suggestedScaleCode scaleCode of the offer we want to pre-select
 */
export const findPreSelectedOffer = (
  offers: SimulationFinancialProposal[],
  suggestedDueNumber: number,
  suggestedScaleCode?: string
): SimulationFinancialProposal | null => {
  const index = findIndexPreSelectedOffer(offers, suggestedDueNumber, suggestedScaleCode);
  if (index) {
    return offers[index];
  } else {
    return null;
  }
};

/**
 * Find and return index of pre-selected offer from list of offers
 * @param offers List of offers
 * @param suggestedDueNumber dueNumber of the offer we want to pre-select
 * @param suggestedScaleCode scaleCode of the offer we want to pre-select
 */
export const findIndexPreSelectedOffer = (
  offers: SimulationFinancialProposal[],
  suggestedDueNumber: number,
  suggestedScaleCode?: string
): number | null => {
  if (offers && offers.length) {
    const preSelectedOfferIndex = offers.findIndex((offer) => {
      // Add 1 to nbMonthlyPayment if there is a last monthly payment
      let totalNbMonthlyPayments = offer.nbMonthlyPaymentWithInsurance;
      if (offer.lastMonthlyAmountWithInsurance) {
        totalNbMonthlyPayments++;
      }

      if (totalNbMonthlyPayments === suggestedDueNumber) {
        if (suggestedScaleCode) {
          return offer.scaleCode === suggestedScaleCode;
        } else {
          return true;
        }
      } else {
        return false;
      }
    });
    return preSelectedOfferIndex >= 0 ? preSelectedOfferIndex : null;
  } else {
    return null;
  }
};
