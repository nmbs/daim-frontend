import store from '@/store';
import { CORRELATION_ID } from '@/config/config';

export function getBaseHeader() {
  return {
    Correlationid: CORRELATION_ID,
    'Content-Type': 'application/json',
    'Context-Sourceid': store.state.apiConfig.offerCode,
    'Context-Applicationid': store.state.apiConfig.applicationId,
    'Context-Partnerid': store.state.apiConfig.partnerCode
  };
}
