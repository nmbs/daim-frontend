import { Route } from 'vue-router';
import store from '@/store';

import { convertToNumber } from '@/utils/strings';
import { TagManagerService } from '@/services/tagManagerFactory';
import { CreditTypeEnum } from '@/models/simulator/business/creditType';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';
import { hash } from '@/utils/encryption';

interface SimpleRoute {
  name: string;
  fullPath: string;
}
export class TagManagerSofincoService implements TagManagerService {
  public insuranceNextClick(hasInsurance: boolean | null) {}

  public offerOptInClick(optIn: boolean | null): void {}

  public pageSubscriptionProcessChange(route: Route): void {}

  public stopPubClick(optIn: boolean): void {}

  public clickSummaryMailW2S(): void {}

  public preContractClick(): void {}

  public selectProduct(): void {}

  public tagIndependantSimulatorOffersPage(route: Route) {}

  public tagIndependantSimulatorAmountPage(route: Route) {}

  public tagIndependantSimulatorAllPageGTM(route: Route) {}

  public tagIndependantSimulatorOffersSelection(route: Route, selectedOffer: SimulationFinancialProposal) {}

  public postClick(element: HTMLElement): void {
    if (element.attributes['data-ecat'] && element.attributes['data-eaction'] && element.attributes['data-elabel']) {
      this.gaClick(
        element.attributes['data-ecat'].value,
        element.attributes['data-eaction'].value,
        element.attributes['data-elabel'].value
      );
    }

    if (
      window.EA_event &&
      window.EA_collector &&
      element.attributes['data-event'] &&
      element.attributes['data-click'] &&
      element.attributes['data-type']
    ) {
      const eventName = element.attributes['data-event'].value;
      const clickName = element.attributes['data-click'].value;
      const clickType = element.attributes['data-type'].value;

      const partner = store.state.simulator.simulationContext.partnerId;

      window.EA_event(eventName);
      window.EA_collector(
        'enotrack',
        '1',
        'rtgclickid',
        clickType,
        'rtgclickcat',
        clickName,
        'rtgsite',
        this.getRtgSite(),
        'from',
        'SITE_EULERIAN',
        'rtgpartenaire',
        partner,
        'rtgparcours',
        'DESKTOP_OU_HYBRIDE_OU_MOBILE'
      );
    }
  }

  public pageChange(route: Route, from: Route): void {
    const simpleRoute = this.convertRoute(route);
    const stepNumber = simpleRoute.page;
    this.sendGoogleTag(from, simpleRoute, stepNumber);

    // Eulerian
    if (window.EA_collector) {
      if (stepNumber > 0) {
        window.EA_collector([
          ...this.getInfoBase(route),
          ...this.getScart(),
          ...this.getStep(route),
          ...this.getInfoStep1(stepNumber),
          ...this.getInfoStep2(stepNumber),
          ...this.getInfoStep3(stepNumber),
          ...this.getInfoStep4(stepNumber),
          ...this.getInfoStep5(stepNumber),
          ...this.getInfoStep6(stepNumber),
          ...this.getInfoStep7(stepNumber),
          ...this.getInfoStep8(stepNumber),
          ...this.getInfoStep9(stepNumber),
          ...this.getInfoStep10(stepNumber),
          ...this.getInfoStep11(stepNumber),
          ...this.getInfoStep12(stepNumber)
        ]);
      } else if (stepNumber === 0) {
        window.EA_collector([...this.getInfoBase(route), ...this.getScart()]);
      }
    }
  }

  public tagError(errorStatus: any) {
    if (window.EA_collector) {
      const userId = this.getCustomerId();

      window.EA_collector([
        'error',
        '1',
        'rtgsite',
        this.getRtgSite(),
        'rtgpg',
        'error',
        'uid',
        userId ? userId : '',
        'email',
        store.state.subscription.principal.contact.email,
        'ref',
        this.getContractId(),
        'rtglogged',
        this.isMemberNew() ? 1 : 0,
        'rtgerrorcode',
        errorStatus,
        'rtgmobilephone',
        store.state.subscription.principal.contact.mobilePhone,
        'rtgphonenumber',
        store.state.subscription.principal.contact.homePhone
      ]);
    }
  }

  public gaClick(ecat: string, eaction: string, elabel: string, callback?: () => void) {
    if ((window as any).dataLayer) {
      let called = false;
      if (callback) {
        (window as any).dataLayer.push({
          event: 'gaClick',
          ecat,
          eaction,
          elabel,
          eventCallback: () => {
            called = true;
            callback();
          }
        });
        // In case GA is not working (blocked), wait 2s and call the callback anyway
        setTimeout(() => {
          if (!called) {
            callback();
          }
        }, 2000);
      } else {
        (window as any).dataLayer.push({
          event: 'gaClick',
          ecat,
          eaction,
          elabel
        });
      }
    } else {
      callback();
    }
  }

  private getContractId() {
    return store.state.subscription.response ? store.state.subscription.response.contractId : null;
  }

  private getCustomerId() {
    if (store.state.subscription.response && store.state.subscription.response.customerId) {
      return store.state.subscription.response.customerId;
    }
    return store.state.subscription.authentication.response
      ? store.state.subscription.authentication.response.customerId
      : null;
  }

  private isMemberNew() {
    return store.state.subscription.authentication.response
      ? !!store.state.subscription.authentication.response.customerId
      : false;
  }

  private sendGoogleTag(from: Route, to: SimpleRoute, stepNumber) {
    // Google Tag Manager
    setTimeout(() => {
      (window as any).dataLayer.push({
        user_logged: this.getCustomerId() ? 1 : 0
      });
      // SetTimeout is necessary to wait for store to be initialized
      if (from && from.name === 'infosLoansForm') {
        (window as any).dataLayer.push({
          checkout_option: store.state.simulator.requiredAmount
        });
      } else if (from && from.name === 'insurance') {
        (window as any).dataLayer.push({
          checkout_option: store.state.subscription.principal.insurance.acceptInsuranceOffer ? 1 : 0
        });
      }

      if (store.getters.isResultOK || store.getters.isResultKO) {
        (window as any).dataLayer.push({
          event: 'VirtualPageview',
          virtualPageURL: to.fullPath,
          virtualPageTitle: `${stepNumber} – ${to.name} – ${store.state.simulator.simulationContext.productId} - ${
            store.state.subscription.response.acceptedCode
          } - ${hash(this.getContractId())}`,
          iddoss: hash(this.getContractId()),
          product_name: store.state.simulator.simulationContext.productId,
          product_category: store.state.subscription.response.creditType === CreditTypeEnum.LOAN ? 'PB' : 'CR',
          product_taeg: store.state.subscription.response.proposal.taeg,
          product_amount: store.state.subscription.response.proposal.amount,
          score: store.state.subscription.response.acceptedCode
        });
      } else {
        (window as any).dataLayer.push({
          event: 'VirtualPageview',
          virtualPageURL: to.fullPath,
          virtualPageTitle: `${stepNumber} – ${to.name} – ${store.state.simulator.simulationContext.productId}`,
          product_name: store.state.simulator.simulationContext.productId,
          product_category:
            store.state.simulator.selectedProposal.creditType === CreditTypeEnum.REVOLVING ? 'CR' : 'PB',
          product_taeg: store.state.simulator.selectedProposal ? store.state.simulator.selectedProposal.taeg : 0,
          product_amount: store.state.simulator.requiredAmount
        });
      }
    }, 200);
  }

  private getStep(route: Route) {
    const simpleRoute = this.convertRoute(route);
    if (simpleRoute.page > 0) {
      return ['rtgstep', simpleRoute.page];
    }
    return [];
  }

  private getScart() {
    if (store.state.subscription.response && store.state.subscription.response.contractId) {
      return [];
    } else {
      return ['scart', '1'];
    }
  }
  private convertRoute(route: Route) {
    switch (route.name) {
      case 'loan':
        return { name: 'infosLoansForm', fullPath: '/formulaire/informations/emprunt', page: 1 };
      case 'name':
        return { name: 'infosNameForm', fullPath: '/formulaire/informations/nom', page: 2 };
      case 'contact':
        return { name: 'infosContactForm', fullPath: '/formulaire/informations/coordonnees', page: 3 };
      case 'civility':
        return { name: 'situationIdentityForm', fullPath: '/formulaire/situation/identite', page: 4 };
      case 'family':
        return { name: 'situationFamilyForm', fullPath: '/formulaire/situation/famille', page: 5 };
      case 'housing':
        return { name: 'situationAddressForm', fullPath: '/formulaire/situation/adresse', page: 6 };
      case 'employment':
        return { name: 'incomesEmploymentForm', fullPath: '/formulaire/revenus/votre-travail', page: 7 };
      case 'incomes':
        return { name: 'incomesIncomesForm', fullPath: '/formulaire/revenus/vos-revenus', page: 8 };
      case 'charges':
        return { name: 'incomesChargesForm', fullPath: '/formulaire/revenus/vos-revenus', page: 9 };
      case 'insurance':
        return { name: 'insuranceForm', fullPath: '/formulaire/votre-assurance', page: 10 };
      case 'summary':
        return { name: 'summaryForm', fullPath: '/formulaire/votre-recapitulatif', page: 11 };
      case 'loading':
        return { name: 'loadingPage', fullPath: '/formulaire/traitement-en-cours', page: undefined };
      case 'resultSofinco':
        return store.getters.isResultOK
          ? { name: 'resultForm', fullPath: '/formulaire/reponse-de-principe', page: 12 }
          : { name: 'resultKoForm', fullPath: '/formulaire/reponse-de-principe-ko', page: 12 };
    }
  }
  private getRtgSite() {
    return this.isMemberNew() ? 'memberNew' : 'prospectNew';
  }
  private getInfoBase(route: Route) {
    const simpleRoute = this.convertRoute(route);
    const userId = this.getCustomerId();
    const rtgpg = simpleRoute.page === 12 ? 'completedform' : 'form';
    const offer = store.state.simulator.simulationContext.offer;

    const cardId = store.state.subscription.cardId;
    const funnelId = store.state.subscription.funnelId;
    const simulationId = store.state.simulator.simulationContext.simulationId;

    return [
      'rtgsite',
      this.getRtgSite(),
      'rtgpg',
      rtgpg,
      'uid',
      userId ? userId : '',
      'rtgpagename',
      simpleRoute.name,
      'path',
      simpleRoute.fullPath.replace(/\//g, '|'),
      'rtglogged',
      this.isMemberNew() ? 1 : 0,
      'rtgcustomer',
      store.state.subscription.authentication.response.authenSource === 'EC' ? 1 : 0,
      'rtgoffre',
      offer,
      'cartid',
      cardId,
      'simulationid',
      simulationId,
      'funnelid',
      funnelId,
      'rtgcarte',
      store.state.simulator.simulationContext.cardType,
      'mfactoryid',
      store.state.simulator.simulationContext.mfactoryid
    ];
  }

  private getInfoStep1(page: number): Array<string | number> {
    if (page >= 1) {
      const creditAmount = store.state.simulator.requiredAmount;
      const duration = store.state.simulator.selectedProposal.creditDurationWithoutInsurance;
      const monthlyAmount = store.state.simulator.selectedProposal.monthlyAmountWithoutInsurance;
      const course = store.state.simulator.selectedProposal.creditType;
      const prdref = store.state.simulator.simulationContext.productId;
      const taeg = store.state.simulator.selectedProposal.taeg;

      return [
        'rtgidform',
        'devis_5min',
        'prdref',
        prdref,
        'prdamount',
        creditAmount,
        'prdquantity',
        '1',
        'rtgduration',
        duration,
        'rtgpayments',
        monthlyAmount,
        'rtgtaeg',
        taeg
      ];
    }

    return [];
  }

  private getInfoStep2(page: number): Array<string | number> {
    if (page >= 2) {
      const creditForTwo = store.state.subscription.creditForTwo ? 2 : 1;
      const maritalStatus = store.state.subscription.principal.name.maritalStatus.code;

      return ['rtgmaritalstatus', maritalStatus, 'rtgnbemprunt', creditForTwo];
    }

    return [];
  }

  private getInfoStep3(page: number): Array<string | number> {
    if (page >= 3) {
      return [
        'rtglastname',
        store.state.subscription.principal.name.lastName,

        'rtgname',
        store.state.subscription.principal.name.firstName,

        'rtgcivilite',
        //
        store.state.subscription.principal.name.gender ? 1 : 2,

        'rtgcocivilite',
        store.state.subscription.creditForTwo ? (store.state.subscription.secondary.name.gender ? 1 : 2) : null
      ];
    }

    return [];
  }

  private getInfoStep4(page: number): Array<string | number | boolean> {
    if (page >= 4) {
      const stopPub = store.state.subscription.principal.contact.contactOffer ? 1 : 0;
      return [
        'email',
        store.state.subscription.principal.contact.email,

        'rtgmobilephone',
        store.state.subscription.principal.contact.mobilePhone,

        'rtgphonenumber',
        store.state.subscription.principal.contact.homePhone,

        'rtgoptin',
        store.state.subscription.principal.contact.lenderOffer,

        'rtgoptinpartenaire',
        store.state.subscription.principal.contact.partnerOffer,

        'rtgstoppub',
        stopPub
      ];
    }

    return [];
  }

  private getInfoStep5(page: number): Array<string | number> {
    if (page >= 5) {
      const pBirthCountry = store.state.subscription.principal.civility.birthCountry
        ? store.state.subscription.principal.civility.birthCountry.code
        : null;
      const pBirthCity = store.state.subscription.principal.civility.birthCity
        ? store.state.subscription.principal.civility.birthCity.code
        : null;
      const pNationality = store.state.subscription.principal.civility.nationality
        ? store.state.subscription.principal.civility.nationality.code
        : null;

      const sBirthCountry = store.state.subscription.secondary.civility.birthCountry
        ? store.state.subscription.secondary.civility.birthCountry.code
        : null;
      const sBirthCity = store.state.subscription.secondary.civility.birthCity
        ? store.state.subscription.secondary.civility.birthCity.code
        : null;
      const sNationality = store.state.subscription.secondary.civility.nationality
        ? store.state.subscription.secondary.civility.nationality.code
        : null;

      return [
        'rtgdateofbirth',
        store.state.subscription.principal.civility.birthDate,

        'rtgcountryofbirth',
        pBirthCountry,

        'rtgzipcodeofbirth',
        pBirthCity,

        'rtgnationality',
        pNationality,

        'rtgcodateofbirth',
        store.state.subscription.creditForTwo ? store.state.subscription.secondary.civility.birthDate : null,

        'rtgcocountryofbirth',
        store.state.subscription.creditForTwo ? sBirthCountry : null,

        'rtgcozipcodeofbirth',
        store.state.subscription.creditForTwo ? sBirthCity : null,

        'rtgconationality',
        store.state.subscription.creditForTwo ? sNationality : null
      ];
    }

    return [];
  }

  private getInfoStep6(page: number): Array<string | number> {
    if (page >= 6) {
      return ['rtgnbchildren', store.state.subscription.principal.family.childrenNbr];
    }

    return [];
  }

  private getInfoStep7(page: number): Array<string | number> {
    if (page >= 7) {
      const livingCity = store.state.subscription.principal.adresse.adresseCity
        ? store.state.subscription.principal.adresse.adresseCity.code
        : null;
      const housingType = store.state.subscription.principal.adresse.housingType
        ? store.state.subscription.principal.adresse.housingType.code
        : null;

      const sinceMonth = store.state.subscription.principal.adresse.sinceWhen
        ? store.state.subscription.principal.adresse.sinceWhen.split('/')[0]
        : null;
      const sinceYear = store.state.subscription.principal.adresse.sinceWhen
        ? store.state.subscription.principal.adresse.sinceWhen.split('/')[1]
        : null;

      return [
        'rtgadress',
        store.state.subscription.principal.adresse.adresseLabel,

        'rtgzipcode',
        livingCity,

        'rtghomeownershipstatus',
        housingType,

        'rtghomeownershipstatusmonth',
        sinceMonth,

        'rtghomeownershipstatusyear',
        sinceYear
      ];
    }

    return [];
  }

  private getInfoStep8(page: number): Array<string | number> {
    if (page >= 8) {
      const pProfession = store.state.subscription.principal.employment.profession
        ? store.state.subscription.principal.employment.profession.code
        : null;
      const pSinceMonth = store.state.subscription.principal.employment.sinceWhen
        ? store.state.subscription.principal.employment.sinceWhen.split('/')[0]
        : null;
      const pSinceYear = store.state.subscription.principal.employment.sinceWhen
        ? store.state.subscription.principal.employment.sinceWhen.split('/')[1]
        : null;
      const pContract = store.state.subscription.principal.employment.contract
        ? store.state.subscription.principal.employment.contract.code
        : null;
      const pArea = store.state.subscription.principal.employment.activitySector
        ? store.state.subscription.principal.employment.activitySector.code
        : null;

      const sProfession = store.state.subscription.secondary.employment.profession
        ? store.state.subscription.secondary.employment.profession.code
        : null;
      const sSinceMonth = store.state.subscription.secondary.employment.sinceWhen
        ? store.state.subscription.secondary.employment.sinceWhen.split('/')[0]
        : null;
      const sSinceYear = store.state.subscription.secondary.employment.sinceWhen
        ? store.state.subscription.secondary.employment.sinceWhen.split('/')[1]
        : null;
      const sContract = store.state.subscription.secondary.employment.contract
        ? store.state.subscription.secondary.employment.contract.code
        : null;
      const sArea = store.state.subscription.secondary.employment.activitySector
        ? store.state.subscription.secondary.employment.activitySector.code
        : null;

      return [
        'rtgprofession',
        pProfession,

        'rtgstartprofessionmonth',
        pSinceMonth,

        'rtgstartprofessionyear',
        pSinceYear,

        'rtgemploymentcontrat',
        pContract,

        'rtgemploymentsector',
        pArea,

        'rtgcoprofession',
        store.state.subscription.creditForTwo ? sProfession : null,

        'rtgccostartprofessionmonth',
        sSinceMonth,

        'rtgcostartprofessionyear',
        store.state.subscription.creditForTwo ? sSinceYear : null,

        'rtgcoemploymentcontrat',
        store.state.subscription.creditForTwo ? sContract : null,

        'rtgcoemploymentsector',
        store.state.subscription.creditForTwo ? sArea : null
      ];
    }

    return [];
  }

  private getInfoStep9(page: number): Array<string | number> {
    if (page >= 9) {
      const pHousing = store.state.subscription.principal.incomes.housingBenefits
        ? store.state.subscription.principal.incomes.housingBenefits
        : 0;
      const pFamilyBenefits = store.state.subscription.principal.incomes.familyBenefits
        ? store.state.subscription.principal.incomes.familyBenefits
        : 0;
      const pOtherIncomes = store.state.subscription.principal.incomes.otherIncomes
        ? store.state.subscription.principal.incomes.otherIncomes
        : 0;
      const pIncomes = convertToNumber(pHousing) + convertToNumber(pFamilyBenefits) + convertToNumber(pOtherIncomes);

      const sHousing = store.state.subscription.secondary.incomes.housingBenefits
        ? store.state.subscription.secondary.incomes.housingBenefits
        : 0;
      const sFamilyBenefits = store.state.subscription.secondary.incomes.familyBenefits
        ? store.state.subscription.secondary.incomes.familyBenefits
        : 0;
      const sOtherIncomes = store.state.subscription.secondary.incomes.otherIncomes
        ? store.state.subscription.secondary.incomes.otherIncomes
        : 0;
      const sIncomes = convertToNumber(sHousing) + convertToNumber(sFamilyBenefits) + convertToNumber(sOtherIncomes);

      return [
        'rtgmonthlyincome',
        store.state.subscription.principal.incomes.netIncome,

        'rtgothermonthlyincome',
        pIncomes,

        'rtgnbmonthsalary',
        store.state.subscription.principal.incomes.salaryMonths,

        'rtgcomonthlyincome',
        store.state.subscription.creditForTwo ? store.state.subscription.secondary.incomes.netIncome : null,

        'rtgcoothermonthlyincome',
        store.state.subscription.creditForTwo ? sIncomes : null,

        'rtgconbmonthsalary',
        store.state.subscription.creditForTwo ? store.state.subscription.secondary.incomes.salaryMonths : null
      ];
    }

    return [];
  }

  private getInfoStep10(page: number): Array<string | number> {
    if (page >= 10) {
      const other = store.state.subscription.principal.charges.otherCredit
        ? store.state.subscription.principal.charges.otherCredit
        : 0;
      const house = store.state.subscription.principal.charges.refundHouseCredit
        ? store.state.subscription.principal.charges.refundHouseCredit
        : 0;
      const car = store.state.subscription.principal.charges.carCredit
        ? store.state.subscription.principal.charges.carCredit
        : 0;
      const renewable = store.state.subscription.principal.charges.revolving
        ? store.state.subscription.principal.charges.revolving
        : 0;
      const charges =
        convertToNumber(other) + convertToNumber(house) + convertToNumber(car) + convertToNumber(renewable);
      return [
        'rtgmonthlyexpense',
        store.state.subscription.principal.charges.rental,
        'rtgothermonthlyloanrefund',
        charges
      ];
    }

    return [];
  }

  private getInfoStep11(page: number): Array<string | number> {
    if (page >= 11) {
      const pInsurance = store.state.subscription.principal.insurance.acceptInsuranceOffer ? 1 : 0;
      const sInsurance = store.state.subscription.secondary.insurance.acceptInsuranceOffer ? 1 : 0;

      return ['rtgassurance', pInsurance, 'rtgcoassurance', store.state.subscription.creditForTwo ? sInsurance : null];
    }

    return [];
  }

  private getInfoStep12(page: number): Array<string | number> {
    if (page >= 12) {
      const productType = store.state.simulator.simulationContext.productId
        ? `PROSPECTNEW_${store.state.simulator.simulationContext.productId.trim()}`
        : null;

      const productId = store.state.simulator.simulationContext.productId
        ? store.state.simulator.simulationContext.productId.trim()
        : null;

      return [
        'amount',
        store.state.subscription.response.proposal.amount,

        'type',
        productType,

        'rtgcmdidproduit',
        productId,

        'rtgcmdpayments',
        store.state.subscription.response.proposal.monthlyAmountWithInsurance,

        'rtgcmdduration',
        store.state.subscription.response.proposal.creditDuration,

        'rtgcmdtaeg',
        store.state.subscription.response.proposal.taeg,

        'scoring',
        store.state.subscription.response.acceptedCode,

        'scoreinterne',
        store.state.subscription.response.acceptedCode,

        'ref',
        this.getContractId(),

        'estimate',
        '1',

        'rtgseeligibility',
        store.state.subscription.response.eligibilitySE ? 1 : 0,

        'rtgrefsimilaire',
        '0',
        'rtgrbp',
        store.state.subscription.response.rgbpCode,
        'rtgseeligibility',
        store.state.subscription.response.eligibilitySE
      ];
    }

    return [];
  }
}

declare global {
  interface Window {
    EA_collector: any;
    EA_event: any;
  }
}
