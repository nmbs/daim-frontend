import store from '@/store';
import SimulationApi from '@/services/SimulationApi';
import { convertDate } from '@/mixins/dates';
import { simulator } from '@/store/modules/simulator';

export function initializeSofinco(urlParams: any): Promise<void> {
  if (!urlParams) {
    return;
  }
  store.commit('updateCardId', urlParams.CARDID);
  store.commit('updateKeyring', urlParams.KEYRING);
  store.commit('updateRequiredAmount', parseInt(urlParams.AMOUNT, 10));
  store.commit('updateDueNumber', urlParams.DUENUMBER);
  store.commit('updateProductId', urlParams.PRODUCTID);
  store.commit('updatePartnerId', urlParams.PARTNERID);
  store.commit('updateProjectId', urlParams.PROJECTID);
  store.commit('updateVehicleAmount', urlParams.VEHICLEAMOUNT);
  store.commit('updateVehicleAge', urlParams.VEHICLEAGE);
  store.commit('updateCardType', urlParams.CARTE);
  store.commit('updateSourceId', urlParams.PROVENANCEID);
  store.commit('updateSimulationId', urlParams.SIMULATIONID);
  store.commit('updateSimulationOrigin', urlParams.SIMULATIONORIGIN);
  store.commit('updateOffer', urlParams.OFFER);
  store.commit('updateMfactoryid', urlParams.MFACTORYID);
  return SimulationApi.getSimulationV1({
    amount: store.state.simulator.requiredAmount,
    duration: store.state.simulator.dueNumber,
    birthdateT1: null,
    birthdateT2: null,
    hasInsuranceT1: true,
    hasInsuranceT2: false,
    sourceId: store.state.simulator.simulationContext.sourceId,
    projectId: store.state.simulator.simulationContext.projectId
  }).then((res) => {
    store.commit('updateSelectedProposal', res[0]);
  });
}
