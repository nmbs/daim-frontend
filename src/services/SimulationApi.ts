import axios, { AxiosPromise } from 'axios';

import store from '@/store';

import { formatDate } from '@/utils/utils';

import getConfig, { ACCESS_TOKEN, ID_TOKEN } from '@/config/config';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';
import { LoanFinancialProposal, LoanSimulationParameters } from '@/models/simulator/dto/loanFinancialProposal';
import {
  RevolvingFinancialProposal,
  RevolvingSimulationParameters
} from '@/models/simulator/dto/revolvingFinancialProposal';
import {
  Proposal,
  SimulationParameter,
  SimulationV1MailParameter,
  SimulationV1Parameter,
  SimulationV1Result
} from '@/models/Simulation';

import * as CommonsService from '@/services/simulator/commons';

/**
 * Call LOAN simulation API and format response to format common to all simulation APIs
 * @param amount Credit amount
 * @param duration Credit duration
 * @param hasInsurance Whether simulation should be performed with insurance
 */
export default class SimulationApi {
  public static getLoanSimulation(simulationParameter: SimulationParameter): Promise<SimulationFinancialProposal[]> {
    const params: LoanSimulationParameters = {
      amount: simulationParameter.amount
    };
    if (simulationParameter.duration) {
      params.dueNumbers = [simulationParameter.duration];
    }
    if (simulationParameter.hasInsurance) {
      params.hasBorrowerInsurance = simulationParameter.hasInsurance;
    }
    if (simulationParameter.businessProviderId) {
      params.businessProviderId = simulationParameter.businessProviderId;
    }
    if (simulationParameter.equipmentCode) {
      params.equipmentCode = simulationParameter.equipmentCode;
    }
    if (simulationParameter.scaleCode) {
      params.scaleCode = simulationParameter.scaleCode;
    }

    return axios
      .post(getConfig().API_LOAN_SIMULATION, params, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`,
          ...CommonsService.getBaseHeader()
        }
      })
      .then((res: any) => {
        if (res.status === 204) {
          return [];
        }

        const data = res.data as LoanFinancialProposal[];
        return data.map((item) => ({
          amount: simulationParameter.amount,
          creditCost: item.totalCostWithoutInsurance,
          creditDurationWithoutInsurance: item.loanDuration,
          creditDurationWithInsurance: item.loanDuration,
          nbMonthlyPaymentWithoutInsurance: item.dueNumber,
          nbMonthlyPaymentWithInsurance: item.dueNumber,
          monthlyAmountWithoutInsurance: item.monthlyAmountWithoutInsurance,
          monthlyAmountWithInsurance: item.monthlyAmountWithInsurance,
          lastMonthlyAmountWithoutInsurance: null,
          lastMonthlyAmountWithInsurance: null,
          totalAmountWithoutInsurance: item.totalAmountWithoutInsurance,
          totalAmountWithInsurance: item.totalAmountWithInsurance,
          firstPaymentDate: null,
          taeg: item.annualPercentageRateOfCharge,
          taea: item.annualPercentageRateOfInsurance,
          debitRate: item.annualPercentageRate,
          fixedRate: item.fixedRate,
          monthlyInsuranceAmount: item.monthlyInsuranceAmount,
          utilizationDate: null,
          dueDay: null,
          deferralMonthNumber: item.dueDeferralNumber,
          firstMonthlyInsuranceAmount: item.monthlyInsuranceAmount,
          scaleCode: item.scaleCode,
          totalInsuranceCost: item.borrowerTotalInsuranceAmount
        }));
      });
  }

  /**
   * Call REVOLVING simulation API and format response to format common to all simulation APIs
   * @param amount Credit amount
   * @param duration Credit duration
   * @param hasInsurance Whether simulation should be performed with insurance
   */
  public static getRevolvingSimulation(
    simulationParameter: SimulationParameter,
    isPrincipal: boolean = false
  ): Promise<SimulationFinancialProposal[]> {
    const params: RevolvingSimulationParameters = {
      amount: simulationParameter.amount,
      isPrincipal
    };
    if (simulationParameter.duration) {
      params.durations = [simulationParameter.duration];
    }
    if (simulationParameter.hasInsurance) {
      params.hasBorrowerInsurance = simulationParameter.hasInsurance;
    }
    if (simulationParameter.businessProviderId) {
      params.businessProviderId = simulationParameter.businessProviderId;
    }
    if (simulationParameter.equipmentCode) {
      params.equipmentCode = simulationParameter.equipmentCode;
    }
    if (simulationParameter.scaleCode) {
      params.scaleCode = simulationParameter.scaleCode;
    }

    return axios
      .post(getConfig().API_REVOLVING_SIMULATION, params, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`,
          ...CommonsService.getBaseHeader()
        }
      })
      .then((res: any) => {
        if (res.status === 204) {
          return [];
        }

        const data = res.data as RevolvingFinancialProposal[];
        return data.map((item) => ({
          amount: simulationParameter.amount,
          creditCost: item.totalCostWithoutInsurance,
          creditDurationWithoutInsurance: item.creditDuration,
          creditDurationWithInsurance: item.creditDuration,
          nbMonthlyPaymentWithoutInsurance: item.dueNumberWithoutInsurance,
          nbMonthlyPaymentWithInsurance: item.dueNumberWithInsurance,
          monthlyAmountWithoutInsurance: item.monthlyAmountWithoutInsurance,
          monthlyAmountWithInsurance: item.monthlyAmountWithInsurance,
          lastMonthlyAmountWithoutInsurance: item.lastMonthlyAmountWithoutInsurance,
          lastMonthlyAmountWithInsurance: item.lastMonthlyAmountWithInsurance,
          totalAmountWithoutInsurance: item.totalAmountWithoutInsurance,
          totalAmountWithInsurance: item.totalAmountWithInsurance,
          firstPaymentDate: item.firstPaymentDate,
          taeg: item.annualGlobalEffectiveRate,
          taea: item.annualInsuranceEffectiveRate,
          debitRate: item.annualDebitRate,
          fixedRate: item.fixedRate,
          monthlyInsuranceAmount: item.borrowerMonthlyInsuranceAmount,
          utilizationDate: formatDate(new Date()),
          dueDay: item.dueDay,
          deferralMonthNumber: item.deferralMonthNumber,
          firstMonthlyInsuranceAmount: item.borrowerFirstMonthlyInsuranceAmount,
          scaleCode: item.scaleCode,
          totalInsuranceCost: item.borrowerTotalInsuranceAmount
        }));
      });
  }

  /**
   * Call the simulation V1 API and format response to format common to all simulation APIs
   * @param parameters the V1 simulation parameters
   */
  public static getSimulationV1(parameters: SimulationV1Parameter): Promise<SimulationFinancialProposal[]> {
    return axios
      .get(getConfig().API_SIMULATION_V1, {
        headers: {
          Authorization: `Bearer ${ID_TOKEN}`,
          Correlationid: getConfig().CORRELATION_ID,
          'Content-Type': 'application/json'
        },
        params: parameters
      })
      .then((res) => {
        const data: SimulationV1Result = res.data as SimulationV1Result;

        return data.proposals.map((item) => this.mapSimulationData(data, item));
      });
  }

  public static sendEmail(parameter: SimulationParameter, email: string, creditType: string): AxiosPromise<any> {
    let mailRoute = `${getConfig().API_DOCUMENT}/emails?email=${email}&creditType=${creditType}`;
    mailRoute += parameter.amount ? '&amount=' + parameter.amount : '';
    mailRoute += parameter.duration ? '&duration=' + parameter.duration : '';
    mailRoute += parameter.scaleCode ? '&scaleCode=' + parameter.scaleCode : '';
    mailRoute += parameter.businessProviderId ? '&businessProviderId=' + parameter.businessProviderId : '';
    mailRoute += parameter.equipmentCode ? '&equipementCode=' + parameter.equipmentCode : '';
    return axios.post(
      mailRoute,
      {},
      {
        headers: {
          Authorization: `Bearer ${ID_TOKEN}`,
          Correlationid: getConfig().CORRELATION_ID,
          'Content-Type': 'application/json',
          'Context-Sourceid': parameter.sourceId,
          'Context-Applicationid': parameter.applicationId,
          'Context-Partnerid': parameter.partnerId
        }
      }
    );
  }

  public static getPreContractDocuments(parameter: SimulationParameter, creditType: string): AxiosPromise<any> {
    let preDocumentsUrl = `${getConfig().API_DOCUMENT}/downloads/all?creditType=${creditType}`;
    preDocumentsUrl += parameter.amount ? '&amount=' + parameter.amount : '';
    preDocumentsUrl += parameter.duration ? '&duration=' + parameter.duration : '';
    preDocumentsUrl += parameter.scaleCode ? '&scaleCode=' + parameter.scaleCode : '';
    preDocumentsUrl += parameter.businessProviderId ? '&businessProviderId=' + parameter.businessProviderId : '';
    preDocumentsUrl += parameter.equipmentCode ? '&equipementCode=' + parameter.equipmentCode : '';
    return axios.get(preDocumentsUrl, {
      headers: {
        Correlationid: getConfig().CORRELATION_ID,
        'Content-Type': 'application/json',
        'Context-Applicationid': parameter.applicationId,
        'Context-Partnerid': parameter.partnerId,
        'Context-Sourceid': parameter.sourceId
      },
      responseType: 'arraybuffer'
    });
  }

  public static sendEmailV1(parameter: SimulationV1MailParameter): AxiosPromise<any> {
    return axios.post(
      `${getConfig().API_SIMULATION_V1}/${parameter.paramId}/emails`,
      {},
      {
        headers: {
          Authorization: `Bearer ${ID_TOKEN}`,
          Correlationid: getConfig().CORRELATION_ID,
          'Content-Type': 'application/json'
        },
        params: parameter
      }
    );
  }

  public static sendEmailDocV1(parameter: SimulationV1MailParameter): AxiosPromise<any> {
    return axios.post(
      `${getConfig().API_DOC_V1}/${parameter.paramId}/emails`,
      {},
      {
        headers: {
          Authorization: `Bearer ${ID_TOKEN}`,
          Correlationid: getConfig().CORRELATION_ID,
          'Content-Type': 'application/json'
        },
        params: parameter
      }
    );
  }

  private static mapSimulationData(data: SimulationV1Result, item: Proposal): SimulationFinancialProposal {
    return {
      amount: data.amount,
      creditCost: item.totalCostNonInsurance,
      creditDurationWithoutInsurance: item.creditDuration,
      creditDurationWithInsurance: item.creditDuration,
      nbMonthlyPaymentWithoutInsurance: item.dueNumber,
      nbMonthlyPaymentWithInsurance: item.dueNumber,
      monthlyAmountWithoutInsurance: item.monthlyAmountNonInsurance,
      monthlyAmountWithInsurance: item.monthlyAmountWithInsurance,
      lastMonthlyAmountWithoutInsurance: item.lastMonthlyAmountNonInsurance,
      lastMonthlyAmountWithInsurance: item.lastMonthlyAmountWithInsurance,
      totalAmountWithoutInsurance: item.totalAmountNonInsurance,
      totalAmountWithInsurance: item.totalAmount,
      firstPaymentDate: item.firstPaymentDate,
      taeg: item.taeg,
      taea: item.annualInsuranceEffectiveRate,
      debitRate: item.annualDebitRate,
      fixedRate: item.fixedRate,
      monthlyInsuranceAmount: item.monthlyInsuranceAmountT1,
      monthlyInsuranceAmountT2: item.monthlyInsuranceAmountT2,
      utilizationDate: formatDate(new Date()),
      dueDay: item.dueDay,
      deferralMonthNumber: null, // TODO: Get deferralMonthNumber from response
      firstMonthlyInsuranceAmount: item.firstMonthlyInsuranceAmountT1,
      firstMonthlyInsuranceAmountT2: item.firstMonthlyInsuranceAmountT2,
      scaleCode: null, // TODO: Get scaleCode from response
      totalInsuranceCost: item.totalCostWithInsurance,
      totalInsuranceAmount: item.totalInsuranceAmountT1,
      totalInsuranceAmountT2: item.totalInsuranceAmountT2,
      insuranceRate: item.annualInsuranceEffectiveRate,
      label: data.creditLabel,
      creditType: data.creditType,
      maxProposal: item.maxProposal ? this.mapSimulationData(data, item.maxProposal) : null,
      paramId: data.paramId,
      insuranceTypeT1: item.insuranceTypeT1 ? item.insuranceTypeT1 : null,
      insuranceTypeT2: item.insuranceTypeT2 ? item.insuranceTypeT2 : null,
      hasSaleInsuranceT1: item.hasSaleInsuranceT1,
      hasSaleInsuranceT2: item.hasSaleInsuranceT2
    };
  }
}
