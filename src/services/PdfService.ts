import axios from 'axios';

export function fetchPdf(url: string) {
  return axios({
    url,
    method: 'GET',
    responseType: 'blob'
  });
}
