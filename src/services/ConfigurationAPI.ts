import axios, { AxiosError } from 'axios';
import getConfig from '@/config/config';
import { isNotFound } from '@/utils/http';

// Mocked offers
import offerSofinco from '@/assets/json/configurationMocks/sofinco/offerSofinco.json';
import partnerSofinco from '@/assets/json/configurationMocks/sofinco/partnerSofinco.json';
import ikea from '@/assets/json/configurationMocks/ikea/ikea.json';
import ikeaCrs from '@/assets/json/configurationMocks/ikea/offers/crs.json';
import ikeaCrsPlv from '@/assets/json/configurationMocks/ikea/offers/crs_plv.json';
import ikeaSimuCr from '@/assets/json/configurationMocks/ikea/offers/simu_cr.json';
import ikeaSimuVac from '@/assets/json/configurationMocks/ikea/offers/simu_vac.json';
import ikeaCraVac from '@/assets/json/configurationMocks/ikea/offers/cra_vac.json';
import apple from '@/assets/json/configurationMocks/apple/apple.json';
import appleVac from '@/assets/json/configurationMocks/apple/offers/vac.json';
import appleSimuVac from '@/assets/json/configurationMocks/apple/offers/simu_vac.json';
import redoute from '@/assets/json/configurationMocks/redoute/redoute.json';
import redouteCrs from '@/assets/json/configurationMocks/redoute/offers/crs.json';
import redouteSimuCr from '@/assets/json/configurationMocks/redoute/offers/simu_cr.json';
import fnac from '@/assets/json/configurationMocks/fnac/fnac.json';
import fnacCrs from '@/assets/json/configurationMocks/fnac/offers/crs.json';
import fnacSimuCr from '@/assets/json/configurationMocks/fnac/offers/simu_cr.json';
import decathlon from '@/assets/json/configurationMocks/decathlon/decathlon.json';
import decathlonVac from '@/assets/json/configurationMocks/decathlon/offers/vac.json';
import decathlonSimuVac from '@/assets/json/configurationMocks/decathlon/offers/simu_vac.json';
import voltalia from '@/assets/json/configurationMocks/voltalia/voltalia.json';
import voltaliaVac from '@/assets/json/configurationMocks/voltalia/offers/vac.json';
import voltaliaSimuVac from '@/assets/json/configurationMocks/voltalia/offers/simu_vac.json';
import em from '@/assets/json/configurationMocks/em/em.json';
import emVac from '@/assets/json/configurationMocks/em/offers/vac.json';
import emSimuVac from '@/assets/json/configurationMocks/em/offers/simu_vac.json';

export default class ConfigurationAPI {
  /**
   * Fetch the partner configuration from API
   * @param token Bearer token for authentication
   * @param Q6 PartnerCode
   * @param X1 OfferCode
   */
  public static getPartnerConfiguration(token: string, Q6: string, X1: string): any {
    if (getConfig().MOCK_CONFIGURATION) {
      return getMockPartnerConfiguration(Q6);
    } else {
      return axios
        .get(getConfig().API_CONFIGURATION + Q6, {
          headers: {
            Authorization: `Bearer ${token}`,
            Correlationid: getConfig().CORRELATION_ID,
            'Content-Type': 'application/json',
            'Context-Sourceid': X1,
            'Context-Partnerid': Q6,
            'Context-Applicationid': 'creditPartner'
          }
        })
        .catch(manageConfigurationError);
    }
  }

  /**
   * Fetch the configuration offer from API
   * @param token Bearer token for authentication
   * @param Q6
   * @param X1
   */
  public static getOfferConfiguration(token: string, Q6: string, X1: string): any {
    if (getConfig().MOCK_CONFIGURATION) {
      return getMockOfferConfiguration(Q6, X1);
    } else {
      return axios
        .get(getConfig().API_CONFIGURATION + Q6 + '/routes/' + X1, {
          headers: {
            Authorization: `Bearer ${token}`,
            Correlationid: getConfig().CORRELATION_ID,
            'Content-Type': 'application/json',
            'Context-Sourceid': X1,
            'Context-Partnerid': Q6,
            'Context-Applicationid': 'creditPartner'
          }
        })
        .catch(manageConfigurationError);
    }
  }

  public static async switchConfigurationIfNeeded(
    offerConfig: any,
    amount: string,
    partnerCode: string,
    accessToken: string
  ) {
    if (offerConfig && offerConfig.screenOptions && offerConfig.screenOptions.switchThreshold) {
      let newOfferCode;
      if (amount) {
        const amountNumber = +amount;
        if (isNaN(amountNumber)) {
          throw Error();
        }
        if (amountNumber <= offerConfig.screenOptions.switchThreshold.value) {
          newOfferCode = offerConfig.screenOptions.lowerX1.value;
        } else {
          newOfferCode = offerConfig.screenOptions.upperX1.value;
        }
      } else {
        newOfferCode = offerConfig.screenOptions.noAmountX1.value;
      }
      const newConfig = await ConfigurationAPI.getOfferConfiguration(accessToken, partnerCode, newOfferCode);
      return { newX1: newOfferCode, newOfferConfig: newConfig.data };
    } else {
      return { newX1: null, newConfig: null };
    }
  }
}

function manageConfigurationError(error: AxiosError) {
  const status = error.response && error.response.status;
  if (isNotFound(status)) {
    throw new Error('NOT_FOUND');
  } else {
    return null;
  }
}

function getMockPartnerConfiguration(Q6: string): Promise<any> {
  return new Promise((resolve, reject) => {
    switch (Q6) {
      case 'web_sofinco':
        return resolve({ data: partnerSofinco });
      case 'web_ikea':
        return resolve({ data: ikea });
      case 'web_redoute':
        return resolve({ data: redoute });
      case 'web_apple':
        return resolve({ data: apple });
      case 'web_fnac':
        return resolve({ data: fnac });
      case 'web_decathlon':
        return resolve({ data: decathlon });
      case 'web_voltalia':
        return resolve({ data: voltalia });
      case 'web_em':
        return resolve({ data: em });
      default:
        return reject(`Canno't find a configuration for partner ${Q6}`);
    }
  });
}

function getMockOfferConfiguration(Q6: string, X1: string): Promise<any> {
  return new Promise((resolve, reject) => {
    const getErrorString = (partnerCode, offerCode) =>
      `Canno't find configuration for partner ${partnerCode} and offer ${offerCode}`;
    switch (Q6) {
      case 'web_sofinco':
        resolve({ data: offerSofinco });
      case 'web_ikea':
        switch (X1) {
          case 'crs':
            return resolve({ data: ikeaCrs });
          case 'crs_plv':
            return resolve({ data: ikeaCrsPlv });
          case 'simu_cr':
            return resolve({ data: ikeaSimuCr });
          case 'simu_vac':
            return resolve({ data: ikeaSimuVac });
          case 'cra_vac':
            return resolve({ data: ikeaCraVac });
          default:
            return reject(getErrorString(Q6, X1));
        }
      case 'web_redoute':
        switch (X1) {
          case 'crs':
            return resolve({ data: redouteCrs });
          case 'simu_cr':
            return resolve({ data: redouteSimuCr });
          default:
            return reject(getErrorString(Q6, X1));
        }
      case 'web_apple':
        switch (X1) {
          case 'vac':
            return resolve({ data: appleVac });
          case 'simu_vac':
            return resolve({ data: appleSimuVac });
          default:
            return reject(getErrorString(Q6, X1));
        }
      case 'web_em':
        switch (X1) {
          case 'vac':
            return resolve({ data: emVac });
          case 'simu_vac':
            return resolve({ data: emSimuVac });
          default:
            return reject(getErrorString(Q6, X1));
        }
      case 'web_decathlon':
        switch (X1) {
          case 'vac':
            return resolve({ data: decathlonVac });
          case 'simu_vac':
            return resolve({ data: decathlonSimuVac });
          default:
            return reject(getErrorString(Q6, X1));
        }
      case 'web_voltalia':
        switch (X1) {
          case 'vac':
            return resolve({ data: voltaliaVac });
          case 'simu_vac':
            return resolve({ data: voltaliaSimuVac });
          default:
            return reject(getErrorString(Q6, X1));
        }
      case 'web_fnac':
        switch (X1) {
          case 'crs':
            return resolve({ data: fnacCrs });
          case 'simu_cr':
            return resolve({ data: fnacSimuCr });
          default:
            return reject(getErrorString(Q6, X1));
        }
      default:
        return reject(`Unknown partner ${Q6}`);
    }
  });
}
