import 'url-polyfill';
import getConfig from '@/config/config';
import { SimulationFinancialProposal } from '@/models/simulator/dto/simulationResults';

export default class PdfInfoService {
  public getFicadeUrl(simulationResult: SimulationFinancialProposal): string {
    return this.getUrl(simulationResult, 'ficade');
  }

  public getFipenUrl(simulationResult: SimulationFinancialProposal): string {
    return this.getUrl(simulationResult, 'fipen');
  }

  public getAFipenUrl(simulationResult: SimulationFinancialProposal): string {
    return this.getUrl(simulationResult, 'afipen');
  }

  public getIpidUrl(simulationResult: SimulationFinancialProposal): string {
    return this.getUrl(simulationResult, 'ipid');
  }

  public getFicadeUrlDl(simulationResult: SimulationFinancialProposal): string {
    return this.getUrlDl(simulationResult, 'ficade');
  }

  public getFipenUrlDl(simulationResult: SimulationFinancialProposal): string {
    return this.getUrlDl(simulationResult, 'fipen');
  }

  public getAFipenUrlDL(simulationResult: SimulationFinancialProposal): string {
    return this.getUrlDl(simulationResult, 'afipen');
  }

  public getIpidUrlDl(simulationResult: SimulationFinancialProposal): string {
    return this.getUrlDl(simulationResult, 'ipid');
  }
  private getUrl(simulationResult: SimulationFinancialProposal, documentType: string): string {
    if (!simulationResult.paramId) {
      return '';
    }
    const searchParam = new URLSearchParams();
    searchParam.append('amount', '' + simulationResult.amount);
    searchParam.append('duration', '' + simulationResult.creditDurationWithoutInsurance);
    return (
      getConfig().API_DOCUMENT +
      '/' +
      simulationResult.paramId +
      '/viewers/' +
      documentType +
      '?' +
      searchParam.toString()
    );
  }

  private getUrlDl(simulationResult: SimulationFinancialProposal, documentType: string): string {
    const searchParam = new URLSearchParams();
    searchParam.append('amount', '' + simulationResult.amount);
    searchParam.append('duration', '' + simulationResult.creditDurationWithoutInsurance);
    return (
      getConfig().API_DOCUMENT +
      '/' +
      simulationResult.paramId +
      '/downloads/' +
      documentType +
      '?' +
      searchParam.toString()
    );
  }
}
