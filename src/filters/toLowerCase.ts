/**
 * Return a string in lower case.
 * @param text Input string
 */
export const toLowerCase = (text: string) => (text ? text.toString().toLowerCase() : '');
