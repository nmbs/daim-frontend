/**
 * Return string with first letter in upperCase
 * @param text Input string
 */
export const firstLetterUpperCase = (text: string) => `${text[0].toUpperCase()}${text.slice(1)}`;
