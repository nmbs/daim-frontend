/**
 * Strip zero digits from a string
 * Ex: 555.00 --> 555
 * @param value
 */
export const stripZeros = (value: string) => {
  if (!value) {
    return '';
  } else {
    try {
      // Split strings in words to avoid managing symbols and other stuff through the process of stripping zeros
      const regex = /^0*(\d+(?:(\.|\,)(?:(?!0+$)\d)+)?)/g;
      const words = value.split(' ').map((word) => {
        // Search for substring in word that match regex
        const valueWithoutZeros = word.match(regex);
        if (valueWithoutZeros && valueWithoutZeros.length) {
          // If we're here, some zeros have been stripped
          return valueWithoutZeros[0];
        } else {
          return word;
        }
      });
      return words.join(' ');
    } catch (err) {
      throw new Error(`Error while stripping zeros from ${value}`);
    }
  }
};
