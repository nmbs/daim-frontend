import Vue from 'vue';
import VueI18n from 'vue-i18n';
import '@babel/polyfill';
import Router from 'vue-router';
import VueCookies from 'vue-cookies';
import VueScrollTo from 'vue-scrollto';
import VueCurrencyFilter from 'vue-currency-filter';
import BackToTop from 'vue-backtotop';
import axios from 'axios';
import 'url-polyfill';
import { polyfill } from 'smoothscroll-polyfill';
import jwtDecode from 'jwt-decode';
import store from '@/store';
import { initI18N } from '@/i18n';
import App from './App.vue';
import ConfigurationAPI from '@/services/ConfigurationAPI';
import NomenclatureAPI from '@/services/NomenclatureAPI';
import '@sofinco/portail-shared-component';
import { getBaseUrl } from '@/mixins/getBaseUrl';
import { addDynamicStyles } from '@/mixins/addDynamicStyles';
import {
  getRoutesConfig,
  getRoutesList,
  initStepsStates,
  getSubscriptionRoutesMap,
  mergeConfigurations
} from '@/services/ConfigurationService';
import { AppConfiguration } from '@/models/Configuration';
import { getRouter } from './router';
import { Colors } from '@/models/Colors';
import { hexToRgb } from '@/mixins/HexToRgb';
import { convertDate } from '@/mixins/dates';
import { getBorrower } from '@/mixins/getBorrower';
import * as Mutations from '@/store/Mutations';
import { getCurrentQueryStringParams } from '@/utils/utils';
import ErrorPage from '@/views/ErrorPage.vue';
import {
  API_BASE_URL,
  API_REF_BASE_URL,
  APIM_BASE_URL,
  API_SE_CHOICE,
  APPLICATION_ID,
  UPLOAD_URL,
  REFRESH_TOKEN_URL,
  VALIDATE_TOKEN_URL,
  ACCESS_TOKEN,
  ID_TOKEN,
  REFRESH_TOKEN,
  RECAPTCHA_SITE_KEY,
  API_SOUSCRIPTION_VERSION,
  API_CRBP,
  API_CONF_BASE_URL,
  setIdToken,
  setAccessToken,
  setUploadUrl,
  setApiBaseUrl,
  setApiRefBaseUrl,
  setApimBaseUrl,
  setApplicationId,
  setRefreshTokenUrl,
  setValidateTokenUrl,
  setRecaptchaSiteKey,
  setRefreshToken,
  setApiSouscriptionVersion,
  setApiSeChoice,
  setApiCRBP,
  setApiConfBaseUrl
} from '@/config/config';
import Config from '@/config/configSimulator';
import { firstLetterUpperCase } from '@/filters/firstLetterUpperCase';
import { stripZeros } from '@/filters/stripZeros';
import { toLowerCase } from '@/filters/toLowerCase';
import { CreditTypeEnum } from '@/models/simulator/business/creditType';
import SimulationApi from '@/services/SimulationApi';
import messagesSofinco from '@/assets/i18n/messages_web_sofinco.json';
import { BusinessRule } from '@/services/BusinessRule';
import { initializeSofinco } from '@/services/SofincoService';
import { DocumentsApi } from '@/services/DocumentsApi';
import { enableAxiosRetry } from '@/utils/axiosUtils';
import { KeypadApi } from '@/services/KeypadApi';
import { AuthService } from '@/services/AuthService';
import { IdentificationService } from '@/services/IdentificationService';
import { RoutePathId } from '@/models/RoutesPath';
import { SimulationParameter } from '@/models/Simulation';
import { AuthJwt } from '@/models/subscription/authentication/Authentication';
import tag from '@/directives/tag';

// Define global config object
declare global {
  interface Window {
    config: any;
    dataExchange: any;
    partnerConfig?: any;
    offerConfig?: any;
  }
}

if (!window.config) {
  window.config = {};
}

const WConfig = window.config;
const accessToken = WConfig.ACCESS_TOKEN || ACCESS_TOKEN;
const idToken = WConfig.ID_TOKEN || ID_TOKEN;
const refreshToken = WConfig.REFRESH_TOKEN || REFRESH_TOKEN;

if (process.env.NODE_ENV === 'development') {
  setUploadUrl(WConfig.UPLOAD_Url || UPLOAD_URL);
} else {
  setUploadUrl(WConfig.UPLOAD_Url);
}

setRefreshTokenUrl(WConfig.REFRESH_TOKEN_URL || REFRESH_TOKEN_URL);
setValidateTokenUrl(WConfig.VALIDATE_TOKEN_URL || VALIDATE_TOKEN_URL);
setApimBaseUrl(WConfig.APIM_FRONT_Url || APIM_BASE_URL);
setApiBaseUrl(WConfig.API_Url || API_BASE_URL);
setApiConfBaseUrl(WConfig.API_CONF_BASE_URL || API_CONF_BASE_URL);
setApiRefBaseUrl(WConfig.API_REF_URL || API_REF_BASE_URL);
setApiSeChoice(WConfig.API_SE_CHOICE || API_SE_CHOICE);
setApiCRBP(WConfig.API_CRBP || API_CRBP);
setApplicationId(WConfig.APPLICATION_ID || APPLICATION_ID);
setRecaptchaSiteKey(WConfig.RECAPTCHA_SITE_KEY || RECAPTCHA_SITE_KEY);
setApiSouscriptionVersion(WConfig.API_SOUSCRIPTION_VERSION || API_SOUSCRIPTION_VERSION);
setAccessToken(accessToken);
setIdToken(idToken);
setRefreshToken(refreshToken);

// ScrollIntoViewSmooth polyfill
polyfill();
enableAxiosRetry();

Vue.config.productionTip = false;

Vue.use(VueScrollTo);
Vue.use(Router);
Vue.use(VueCurrencyFilter, {
  symbol: '€',
  thousandsSeparator: ' ', // Note: filter stripZeros need to be updated if thousand separator is changed
  fractionCount: 2,
  fractionSeparator: ',',
  symbolPosition: 'back',
  symbolSpacing: true
});
Vue.use(VueCookies);
Vue.use(BackToTop);
Vue.directive('tag', tag);
Vue.mixin(getBorrower);
Vue.mixin(getBaseUrl);
Vue.mixin(addDynamicStyles);
Vue.mixin(hexToRgb);
Vue.mixin(convertDate);

Vue.filter('firstLetterUpperCase', firstLetterUpperCase);
Vue.filter('stripZeros', stripZeros);
Vue.filter('toLowerCase', toLowerCase);

let i18nVar: any;
let router: Router;
let appConfiguration: AppConfiguration;

let Q6 = ''; // Partner code
let S3 = ''; // Amount
let D2 = ''; // Due number
let SD2 = ''; // Suggested due number
let X1 = ''; // Offer code
let A9 = ''; // Business provider id
let N2 = ''; // Equipment code
let C5 = ''; // Scale code
let SC5 = ''; // Suggested scale code
let LD = ''; // Simulateur version light
let NUMFID = ''; // Fidelity number customer

let stepId = '';
let businessDataTransferToken = ''; // BusinessDataTransfert token

const isLegalMentions = window.location.href.includes(`/${RoutePathId.LEGAL_MENTION}`);

// Example of url: http://localhost:8080/?Q6=web_ikea&X1=rev-simple
const urlParams = getCurrentQueryStringParams();

if (urlParams !== null) {
  Q6 = urlParams.Q6 || 'web_sofinco';
  X1 = (window.offerConfig && window.offerConfig.sysType) || urlParams.X1;
  S3 = urlParams.S3;
  D2 = urlParams.D2;
  SD2 = urlParams.SD2;
  A9 = urlParams.A9;
  N2 = urlParams.N2;
  C5 = urlParams.C5;
  SC5 = urlParams.SC5;
  LD = urlParams.LD;
  NUMFID = urlParams.NUMFID;

  stepId = urlParams.STEPID;
  businessDataTransferToken = urlParams.TOKEN;

  sessionStorage.setItem('Q6', Q6 || '');
  sessionStorage.setItem('X1', X1 || '');
  sessionStorage.setItem('S3', S3 || '');
  sessionStorage.setItem('D2', D2 || '');
  sessionStorage.setItem('SD2', SD2 || '');
  sessionStorage.setItem('A9', A9 || '');
  sessionStorage.setItem('N2', N2 || '');
  sessionStorage.setItem('C5', C5 || '');
  sessionStorage.setItem('SC5', SC5 || '');
  sessionStorage.setItem('businessDataTransferToken', businessDataTransferToken || '');
  sessionStorage.setItem('LD', LD || '');
  sessionStorage.setItem('NUMFID', NUMFID || '');
}

Q6 = Q6 || sessionStorage.getItem('Q6');
X1 = X1 || sessionStorage.getItem('X1');
S3 = S3 || sessionStorage.getItem('S3');
D2 = D2 || sessionStorage.getItem('D2');
SD2 = SD2 || sessionStorage.getItem('SD2');
A9 = A9 || sessionStorage.getItem('A9');
N2 = N2 || sessionStorage.getItem('N2');
C5 = C5 || sessionStorage.getItem('C5');
SC5 = SC5 || sessionStorage.getItem('SC5');
LD = LD || sessionStorage.getItem('LD');
NUMFID = NUMFID || sessionStorage.getItem('NUMFID');
businessDataTransferToken = businessDataTransferToken || sessionStorage.getItem('businessDataTransferToken');

store.commit('savePartnerCode', Q6);
store.commit('saveOfferCode', X1);
store.commit('saveBusinessProviderId', A9);
store.commit('saveEquipmentCode', N2);
store.commit('saveScaleCode', C5);
store.commit('updatePreSelectionSuggestedDueNumber', SD2);
store.commit('updatePreSelectionSuggestedScaleCode', SC5);
store.commit('saveSimulateurLight', LD);
store.commit('saveFidelityNumber', NUMFID);

if (Number(S3) > 0) {
  store.commit('updateRequiredAmount', S3);
}
if (Number(D2) > 0) {
  store.commit('updateDueNumber', D2);
}

// Force router to go to 1st step, unless we target legal mention page
if (!isLegalMentions) {
  location.hash = '/';
}

const promises = [
  NomenclatureAPI.getNomenclatureData(accessToken),
  window.partnerConfig
    ? Promise.resolve({ data: window.partnerConfig })
    : ConfigurationAPI.getPartnerConfiguration(accessToken, Q6, X1),
  window.offerConfig
    ? Promise.resolve({ data: window.offerConfig })
    : ConfigurationAPI.getOfferConfiguration(accessToken, Q6, X1)
];

if (new BusinessRule().isSofincoPartnerId(store.state.apiConfig.partnerCode)) {
  promises.push(initializeSofinco(urlParams));
}

cleanUrl();

axios
  .all(promises)
  .then(
    axios.spread(async (nomenclatureData: any, partnerInformation: any, offerInformation: any) => {
      if (nomenclatureData === null || partnerInformation === null || offerInformation === null) {
        throw new Error();
      }

      const partnerConfiguration: AppConfiguration = partnerInformation.data;
      let offerConfiguration: AppConfiguration = offerInformation.data;

      if (process.env.NODE_ENV === 'development') {
        // Only needed for dev as configuration switch will be performed in server for real-life app
        const { newX1, newOfferConfig } = await ConfigurationAPI.switchConfigurationIfNeeded(
          offerConfiguration,
          S3,
          Q6,
          accessToken
        );
        if (newX1 && newX1 !== X1) {
          X1 = newX1;
          store.commit('saveOfferCode', X1);
          sessionStorage.setItem('X1', X1);
          offerConfiguration = newOfferConfig;
        }
      }

      const screenOption = store.getters.screenOption;
      appConfiguration = mergeConfigurations(offerConfiguration, partnerConfiguration);
      store.commit(Mutations.SAVE_NOMENCLATURE, nomenclatureData.data);
      store.commit(Mutations.SAVE_APP_CONFIGURATION, appConfiguration);

      if (new BusinessRule().isSofincoPartnerId(store.state.apiConfig.partnerCode)) {
        store.commit('updateCreditType', store.state.simulator.selectedProposal.creditType);
      }
      // TODO: check that steps contain subscription to improve code robustness
      if (appConfiguration.steps && appConfiguration.steps.length > 1) {
        Config.chainWithSubscription = true;
      } else {
        Config.chainWithSubscription = false;
      }

      Config.style.resourcesPath = screenOption('resources-path');

      if (!window.dataExchange) {
        window.dataExchange = JSON.parse(sessionStorage.getItem('dataExchange'));
      }
      // Retrieve context if any
      if (window.dataExchange) {
        if (window.dataExchange.providerContext) {
          store.commit('updateHomeReturnUrl', window.dataExchange.providerContext.homeReturnUrl);
        }
        sessionStorage.setItem('dataExchange', JSON.stringify(window.dataExchange));
        store.dispatch('loadBusinessDataContext', window.dataExchange);
        if (store.state.subscription.context) {
          if (
            store.state.subscription.context.customerContext &&
            store.state.subscription.context.customerContext.loyaltyCardId
          ) {
            const loyaltyCardId = store.state.subscription.context.customerContext.loyaltyCardId;
            store.commit('saveFidelityNumber', loyaltyCardId);
          }
          // Override URL parameters if there equivalent is defined in business data transfert
          if (store.state.subscription.context.offerContext) {
            if (store.state.subscription.context.offerContext.amount) {
              // Override S3
              store.commit('updateRequiredAmount', store.state.subscription.context.offerContext.amount / 100); // amount is in centimes
            }
            if (store.state.subscription.context.offerContext.equipmentCode) {
              // Override N2
              store.commit('saveEquipmentCode', store.state.subscription.context.offerContext.equipmentCode);
            }
            if (store.state.subscription.context.offerContext.scaleId) {
              // Override C5
              store.commit('saveScaleCode', store.state.subscription.context.offerContext.scaleId);
            }
            if (store.state.subscription.context.offerContext.duration) {
              // Override D2
              store.commit('updateDueNumber', store.state.subscription.context.offerContext.duration);
            }
            if (store.state.subscription.context.offerContext.suggestedDueNumber) {
              // Overrides SD2
              store.commit(
                'updatePreSelectionSuggestedDueNumber',
                store.state.subscription.context.offerContext.suggestedDueNumber
              );
            }
            if (store.state.subscription.context.offerContext.suggestedScaleId) {
              // Overrides SC5
              store.commit(
                'updatePreSelectionSuggestedScaleCode',
                store.state.subscription.context.offerContext.suggestedScaleId
              );
            }
          }
          if (
            store.state.subscription.context.providerContext &&
            store.state.subscription.context.providerContext.businessProviderId
          ) {
            // Override A9
            store.commit('saveBusinessProviderId', store.state.subscription.context.providerContext.businessProviderId);
          }
        }
      }
      if (process.env.NODE_ENV === 'development') {
        computeSignature();
      }

      const rootSteps = initStepsStates(appConfiguration);
      store.commit(Mutations.SAVE_ROOT_STEP_STATES, rootSteps);

      const routesConfig = getRoutesConfig(appConfiguration);

      router = getRouter(routesConfig);
      store.commit(Mutations.SAVE_CONFIGURATION_ROUTES, routesConfig);

      const routesMap = getSubscriptionRoutesMap(routesConfig.find((route) => route.name === RoutePathId.SUBSCRIPTION));
      store.commit(Mutations.SET_BLOC_ROUTE_TO_FORM_ROUTES, routesMap);

      const routesList = getRoutesList(appConfiguration);
      store.commit(Mutations.SAVE_ROUTE_LIST, routesList);

      // TODO: This condition needs to be removed and it is only for test purpose

      return initI18N(
        screenOption('messages-default'),
        screenOption('messages-partner'),
        screenOption('messages-route'),
        screenOption('legal-notices')
      );
    })
  )
  .then(async (i18n: any) => {
    i18nVar = i18n;
    configurePartnerColors(appConfiguration, i18n);
    const screenOption = store.getters.screenOption;
    const colors: Colors = {
      primary: i18n.t(screenOption('primary-color') || 'personnalisation.couleurs.primaire'),
      secondary: i18n.t(screenOption('secondary-color') || 'personnalisation.couleurs.secondaire'),
      tertiary: i18n.t(screenOption('tertiary-color') || 'personnalisation.couleurs.tertiaire'),
      quaternary: i18n.t(screenOption('quaternary-color') || 'personnalisation.couleurs.quaternaire'),
      quintinary: i18n.t(screenOption('quintinary-color') || 'personnalisation.couleurs.quinquenaire')
    };
    store.commit('saveColors', colors);

    if (appConfiguration && appConfiguration.steps && appConfiguration.steps[0].pathId === RoutePathId.SIMULATION) {
      if (store.state.simulator.requiredAmount) {
        const simulationParameter: SimulationParameter = {
          amount: store.state.simulator.requiredAmount,
          businessProviderId: store.state.apiConfig.businessProviderId,
          equipmentCode: store.state.apiConfig.equipmentCode,
          scaleCode: store.state.apiConfig.scaleCode,
          hasInsurance: true
        };
        try {
          let res;
          if (screenOption('credit-type') === CreditTypeEnum.LOAN) {
            res = await SimulationApi.getLoanSimulation(simulationParameter);
          } else {
            res = await SimulationApi.getRevolvingSimulation(simulationParameter);
          }
          store.commit('updateProposals', res);
        } catch (err) {
          throw new Error(err);
        }
      }

      if (screenOption('credit-type') === CreditTypeEnum.REVOLVING) {
        try {
          // Get example standard conditions for example text of simulator
          const amount = screenOption('amount-simulation') || null;
          const duration = screenOption('duration-simulation') || null;
          const simulationParameter: SimulationParameter = {
            amount,
            duration,
            businessProviderId: store.state.apiConfig.businessProviderId,
            equipmentCode: store.state.apiConfig.equipmentCode,
            scaleCode: store.state.apiConfig.scaleCode,
            hasInsurance: true
          };
          const res = await SimulationApi.getRevolvingSimulation(simulationParameter, true);
          store.commit('updateSimulationStandardConditions', res[0]);
        } catch (err) {
          throw new Error(err);
        }
      }
    }

    if (!isLegalMentions) {
      await loggedClient();
    }

    if (router && store && i18nVar) {
      renderApp();
    } else {
      throw new Error();
    }
  })
  .catch((err: Error) => {
    const message = err.message;
    const invalidParameters: boolean = message === 'NOT_FOUND';
    const serverError: boolean = message !== 'NOT_FOUND';
    renderErrorPage(invalidParameters, serverError);
  });

function renderApp() {
  if (isLegalMentions) {
    new Vue({
      router,
      store,
      i18n: i18nVar,
      render: (h) => h(App)
    }).$mount('#app');
  } else {
    new Vue({
      router,
      store,
      i18n: i18nVar,
      provide: initProviders(),
      render: (h) => h(App)
    }).$mount('#app');
  }
}

function renderErrorPage(invalidParameters: boolean, serverError: boolean) {
  new Vue({
    render: (h) =>
      h(ErrorPage, {
        props: {
          invalidParameters,
          serverError
        }
      })
  }).$mount('#app');
}

/**
 * Fill Config data structure with data fetched from configuration API (colors, resourcesPath, ...)
 */
function configurePartnerColors(appConf: AppConfiguration, i18n: any) {
  Config.style.colors.primary = i18n.t(appConf.screenOptions['primary-color'].value);
  Config.style.colors.secondary = i18n.t(appConf.screenOptions['secondary-color'].value);
  Config.style.colors.tertiary = i18n.t(appConf.screenOptions['tertiary-color'].value);
  Config.style.colors.quaternary = i18n.t(appConf.screenOptions['quaternary-color'].value);
  Config.style.colors.quintinary = i18n.t(appConf.screenOptions['quintinary-color'].value);
  Config.style.theme = appConf.screenOptions['theme'] ? appConf.screenOptions['theme'].value : Config.style.theme;
}

function initProviders() {
  const documentsApi = new DocumentsApi();
  const keypadApi = new KeypadApi();
  const authService = new AuthService();
  const identificationService = new IdentificationService();
  return {
    documentsApi,
    keypadApi,
    authService,
    identificationService
  };
}

/**
 * Function that will remove from the URL the following parameters:
 * - t0 (idToken)
 */
function cleanUrl() {
  if (window.history && window.history.replaceState) {
    let url = window.location.href;
    url = url.replace(/(.*)t0=[^&#]*(.*)/g, '$1$2');
    history.replaceState(null, '', url);
  }
}

/**
 * Verify that the user has a authenticated token in order to know if he is already logged.
 *
 * If the idToken contains a sub (client id), it means that the client is already logged.
 * To retrieve client data, the authentication customer API is called with idToken and sub.
 *
 * @param providers the list of providers (services) that have already been instanced
 */
async function loggedClient() {
  // Initialize logged client
  if (idToken) {
    const tokenJwt: AuthJwt = jwtDecode(idToken);
    if (tokenJwt && tokenJwt.sub) {
      store.commit('setAuthenticationClientIsLogging', true);
      // Call customer API to retrieve client data and fill fields
      const authService = new AuthService();
      try {
        const response = await authService.getClientData(tokenJwt.sub, idToken);
        await authService.fillLoggedClient(
          response.data,
          store.state.subscription.principal,
          store.getters.getHousingStateType,
          store.getters.getMaritalStatus(false)
        );
        store.commit('setAuthenResponse', { customerId: tokenJwt.sub });
      } catch (error) {
        // Do nothing if an error occurred while retrieving client data (this is not a blocking error)
      }
      store.commit('setAuthenticationClientIsLogging', false);
    }
  }
}

// Only call if development in order to avoir having signature code in frontend
function computeSignature() {
  const hash = require('hash.js');

  const partnerCode = store.state.apiConfig.partnerCode;
  const offerCode = store.state.apiConfig.offerCode;
  const equipmentCode = store.state.apiConfig.equipmentCode;
  const businessProviderCode = store.state.apiConfig.businessProviderId;
  const scaleCode = store.state.apiConfig.scaleCode;
  const amount = store.state.simulator.requiredAmount;
  const duration = store.state.simulator.dueNumber;
  let orderAmount = '';
  let personalContributionAmount = '';
  let returnUrl = '';
  let homeReturnUrl = '';
  let loyaltyCardId = '';
  let orderId = '';

  const dataExchange = store.state.subscription.context;
  if (dataExchange) {
    if (dataExchange.offerContext) {
      if (dataExchange.offerContext.orderAmount) {
        orderAmount = `${dataExchange.offerContext.orderAmount}`;
      }
      if (dataExchange.offerContext.personalContributionAmount) {
        personalContributionAmount = `${dataExchange.offerContext.personalContributionAmount}`;
      }
      if (dataExchange.offerContext.orderId) {
        orderId = dataExchange.offerContext.orderId;
      }
    }
    if (dataExchange.providerContext) {
      if (dataExchange.providerContext.returnUrl) {
        returnUrl = dataExchange.providerContext.returnUrl;
      }
      if (dataExchange.providerContext.homeReturnUrl) {
        homeReturnUrl = dataExchange.providerContext.homeReturnUrl;
      }
    }
    if (dataExchange.customerContext) {
      if (dataExchange.customerContext.loyaltyCardId) {
        loyaltyCardId = dataExchange.customerContext.loyaltyCardId;
      }
    }
  }

  const key =
    [
      partnerCode,
      offerCode,
      equipmentCode,
      businessProviderCode,
      scaleCode,
      amount,
      orderAmount,
      personalContributionAmount,
      duration,
      returnUrl,
      loyaltyCardId,
      orderId,
      homeReturnUrl
    ].join('+') + `=${process.env.VUE_APP_SALT}`;
  window.config.data1 = hash.sha256().update(key).digest('hex');
}
