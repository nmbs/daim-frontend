import Vue from 'vue';
import VueI18n from 'vue-i18n';

import axios from 'axios';

import messages from '@/assets/i18n/messages.json';

import merge from 'deepmerge';

Vue.use(VueI18n);

export async function initI18N(
  urlMessagesDefault: string,
  urlMessagesPartner: string,
  urlMessagesOffer: string,
  urlLegalMention: string
): Promise<any> {
  try {
    const response = await Promise.all(
      [urlMessagesDefault, urlMessagesPartner, urlMessagesOffer, urlLegalMention]
        .filter((e) => e)
        .map((e) => axios.get(e, getAuthentication(e)))
    );
    const mergedMessages: any = merge.all(response.map((res) => res.data));
    return new VueI18n({
      locale: 'fr',
      fallbackLocale: 'fr',
      // messages: messages
      messages: mergedMessages ? mergedMessages : messages
    });
  } catch (err) {
    return new VueI18n({
      locale: 'fr',
      fallbackLocale: 'fr',
      messages
    });
  }
}

/**
 * Add Authorization header if target URL is CACD2 Jahia
 * @param url
 */
const getAuthentication = (url: string) => {
  if (url.indexOf('cacd2.io') >= 0) {
    return {
      headers: {
        Authorization: `Basic cGIyMzg6dGFlZzEl`
      }
    };
  } else {
    return {};
  }
};
