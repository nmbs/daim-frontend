import uuidv4 from 'uuid/v4';

export let APIM_BASE_URL = process.env.VUE_APP_APIM_BASE_URL;
export let UPLOAD_URL = process.env.VUE_APP_UPLOAD_URL;
export let API_BASE_URL = process.env.VUE_APP_API_BASE_URL;
export let API_REF_BASE_URL = process.env.VUE_APP_API_BASE_REF_URL;
export let API_SE_CHOICE = process.env.VUE_APP_API_SE_CHOICE;
export let API_CRBP = process.env.VUE_APP_API_CRBP;
export let ACCESS_TOKEN = process.env.VUE_APP_ACCESS_TOKEN;
export let ID_TOKEN = process.env.VUE_APP_ID_TOKEN;
export let REFRESH_TOKEN = process.env.VUE_APP_REFRESH_TOKEN;
export let REFRESH_TOKEN_URL = process.env.VUE_APP_REFRESH_TOKEN_URL;
export let VALIDATE_TOKEN_URL = process.env.VUE_APP_VALIDATE_TOKEN_URL;
export let APPLICATION_ID = 'creditPartner';
export let CORRELATION_ID = window.config ? window.config.CORRELATION_ID : uuidv4();
export let RECAPTCHA_SITE_KEY = process.env.VUE_APP_RECAPTCHA_SITE_KEY;
export let API_SOUSCRIPTION_VERSION = process.env.VUE_APP_API_SOUSCRIPTION_VERSION;
export let API_CONF_BASE_URL = process.env.VUE_APP_API_CONF_BASE_URL;

export default function getConfig() {
  return {
    API_CONFIGURATION: `${API_CONF_BASE_URL}/applicationsPartnersConfiguration/v1/applications/${APPLICATION_ID}/partners/`,
    API_SIMULATION: `${APIM_BASE_URL}/revolvingSimulation/v1/simulations`, // TODO: remove when new simulations APIs integrated
    API_CARD: `${API_REF_BASE_URL}/cart/v1.2/carts`, // TODO: remove when new simulations APIs integrated
    API_KEYRING: `${API_REF_BASE_URL}/cart/v1.2/keyrings`, // TODO: remove when new simulations APIs integrated
    API_REVOLVING_SIMULATION: `${APIM_BASE_URL}/revolvingSimulation/v1/simulations`,
    API_LOAN_SIMULATION: `${APIM_BASE_URL}/loanSimulation/v1/simulations`,
    API_SIMULATION_V1: `${API_REF_BASE_URL}/simulation/v1.1/simulations`,
    API_DOC_V1: `${API_REF_BASE_URL}/simulation/v1.1/documents`,
    API_DOCUMENT: `${API_REF_BASE_URL}/simulation/v1.1/documents`,
    API_SOUSCRIPTION: `${API_REF_BASE_URL}/subscription/${API_SOUSCRIPTION_VERSION}/subscriptions/`,
    API_NOMENCLATURE: `${API_REF_BASE_URL}/nomenclatures/v1/subscription-nomenclatures`,
    API_CITY: `${API_REF_BASE_URL}/nomenclatures/v1/cities`,
    API_COUNTRY: `${API_REF_BASE_URL}/nomenclatures/v1/countries`,
    API_FIRSTNAME: `${API_REF_BASE_URL}/nomenclatures/v1/firstnames`,
    API_NATIONALITY: `${API_REF_BASE_URL}/nomenclatures/v1/nationalities`,
    API_SE_CHOICE,
    API_CRBP,
    API_SUPPORTING_DOCUMENTS: `${API_BASE_URL}/supportingDocuments/v1`,
    API_INIT_KEYPAD: `${API_REF_BASE_URL}/authentication/v2/keypads`,
    API_AUTHEN_CLIENT: `${API_REF_BASE_URL}/authentication/v2/customers`,
    API_SUPPORTING_DOCUMENTS_URL: `${API_REF_BASE_URL}/supportingDocuments/v1`,
    CORRELATION_ID,
    NEW_UPLOAD: true,
    API_UPLOAD_SE: `${API_REF_BASE_URL}/subscription/${API_SOUSCRIPTION_VERSION}/subscriptions`,
    UPLOAD_URL,
    REFRESH_TOKEN_URL,
    VALIDATE_TOKEN_URL,
    MOCK_CONFIGURATION: false,
    OLD_URL_RETROCOMPATIBILITY: true,
    DISABLE_TOS_POPIN: true
  };
}

export function setApimBaseUrl(url: string) {
  APIM_BASE_URL = url;
}

export function setApiConfBaseUrl(url: string) {
  API_CONF_BASE_URL = url;
}

export function setUploadUrl(url: string) {
  UPLOAD_URL = url;
}

export function setRefreshTokenUrl(url: string) {
  REFRESH_TOKEN_URL = url;
}

export function setValidateTokenUrl(url: string) {
  VALIDATE_TOKEN_URL = url;
}

export function setApiBaseUrl(url: string) {
  API_BASE_URL = url;
}

export function setApiRefBaseUrl(url: string) {
  API_REF_BASE_URL = url;
}

export function setApiSeChoice(apiSeChoice: string) {
  API_SE_CHOICE = apiSeChoice;
}

export function setApiCRBP(apiCRBP: string) {
  API_CRBP = apiCRBP;
}

export function setAccessToken(token: string) {
  ACCESS_TOKEN = token;
}

export function setIdToken(token: string) {
  ID_TOKEN = token;
}

export function setApplicationId(appId: string) {
  APPLICATION_ID = appId;
}

export function setRecaptchaSiteKey(siteKey: string) {
  RECAPTCHA_SITE_KEY = siteKey;
}

export function setRefreshToken(refreshToken: string) {
  REFRESH_TOKEN = refreshToken;
}

export function setApiSouscriptionVersion(version: string) {
  API_SOUSCRIPTION_VERSION = version;
}
