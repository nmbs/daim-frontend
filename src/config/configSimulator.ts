export default {
  enableHeaderAndFooter: true,
  enableFilterParameters: true,
  editableAmount: true,
  chainWithSubscription: true,
  currency: '€',
  screen: {
    maxWidthMobile: 768,
    maxHeightMobile: 500
  },
  style: {
    colors: {
      primary: null,
      secondary: null,
      tertiary: null,
      quaternary: null,
      quintinary: null
    },
    theme: 'DARK',
    resourcesPath: ''
  }
};
