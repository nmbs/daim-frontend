export interface PedagogicItemModel {
  img?: string;
  alt?: string;
  text?: string;
  title?: string;
  tooltip?: PedagogicItemTooltipModel;
}

export interface PedagogicItemTooltipModel {
  link: string;
  text: string;
}
