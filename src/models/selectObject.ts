export interface SelectObject {
  label: string;
  code: string | number;
}
