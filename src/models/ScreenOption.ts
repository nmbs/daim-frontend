export interface ScreenOption {
  [key: string]: {
    value: any;
    description: string;
  };
}
