export interface Basket {
  encryptedState: string;
  publicState: string;
}
export interface CardBasket {
  type: string;
  value: string;
  identifier?: string;
  expiration?: number;
}

export enum SubscriptionVersion {
  V1 = 'v1',
  V2 = 'v2'
}

export const LOCAL_STORAGE_CARD_API_KEY = 'sof-card-api-pk-key';
export const LOCAL_STORAGE_CARD_API_IDENTIFIER = 'sof-card-api-identifier';
