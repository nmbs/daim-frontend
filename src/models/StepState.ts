import { ScreenOption } from '@/models/ScreenOption';

/**
 * Represent the state of a step.
 * This is a high level structure made to make operation on steps easier.
 * StepState are built from Step, which are raw API data.
 */
export class StepState {
  public pathId: string;
  public steps: StepState[];
  public screenOptions: ScreenOption | null;
  public parent: StepState | null;
  public routed: boolean;
  public noReturn: boolean;

  constructor(
    pathId: string,
    steps: StepState[],
    screenOptions: ScreenOption | null,
    parent: StepState | null,
    routed: boolean,
    noReturn: boolean = false
  ) {
    this.pathId = pathId;
    this.screenOptions = screenOptions;
    this.parent = parent;
    this.steps = steps;
    this.routed = routed;
    this.noReturn = noReturn;
  }

  public getPrevious(getLastChild?: boolean): StepState | null {
    if (this.parent) {
      const stepIndex = this.parent.steps.findIndex((e) => e.pathId === this.pathId);
      if (stepIndex > 0) {
        const previousBrother = this.parent.steps[stepIndex - 1];
        const lastChild = previousBrother.steps[previousBrother.steps.length - 1];
        return lastChild && lastChild.routed && getLastChild ? lastChild : previousBrother;
      }
    }

    if (this.parent && this.parent.pathId !== '') {
      return this.parent.getPrevious(true);
    }

    return null;
  }

  public getNext(getFirstChild?: boolean): StepState | null {
    if (this.parent) {
      const stepIndex = this.parent.steps.findIndex((e) => e.pathId === this.pathId);
      if (stepIndex > -1 && stepIndex < this.parent.steps.length - 1) {
        const nextBrother = this.parent.steps[stepIndex + 1];
        const firstChild = nextBrother.steps[0];
        return firstChild && firstChild.routed && getFirstChild ? firstChild : nextBrother;
      }
    }

    if (this.parent && this.parent.pathId !== '') {
      return this.parent.getNext(true);
    }

    return null;
  }

  public find(pathId: string): StepState | null {
    if (this.pathId === pathId) {
      return this;
    }

    if (this.steps) {
      for (const child of this.steps) {
        const foundStep = child.find(pathId);
        if (foundStep) {
          return foundStep;
        }
      }
    }

    return null;
  }
}
