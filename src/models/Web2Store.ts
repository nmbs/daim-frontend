export interface W2SOptions {
  web2storeEnabled?: boolean;
  web2storeMail?: Web2storeMail;
}

export enum Web2storeMail {
  AUTO = 'auto',
  MANUAL = 'manual'
}
