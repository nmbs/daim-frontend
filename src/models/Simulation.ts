export interface SimulationParameter {
  amount?: number;
  duration?: number;
  birthdate?: string;
  scaleCode?: string;
  sourceId?: string;
  partnerId?: string;
  applicationId?: string;
  businessProviderId?: string;
  equipmentCode?: string;
  hasInsurance?: boolean;
  productId?: string;
  vehicleAge?: string;
  vehicleAmount?: string;
}

export interface SimulationV1MailParameter {
  email: string;
  paramId: string;
  amount: number;
  duration: number;
  hasInsuranceT1: boolean;
  hasInsuranceT2: boolean;
  birthdateT1: string;
  birthdateT2: string;
}

export interface SimulationV1Parameter {
  amount: number;
  duration: number;
  birthdateT1?: string;
  birthdateT2?: string;
  hasInsuranceT1: boolean;
  hasInsuranceT2: boolean;
  sourceId: string;
  projectId: string;
  jobSituationT1?: string;
  jobSituationT2?: string;
  insuranceTypeT1?: string;
  insuranceTypeT2?: string;
}

export interface SimulationV1Result {
  creditLabel: string;
  amount: number;
  creditType: string;
  productCode: string;
  subProductCode: string;
  impactOfferCode: string;
  paramId: string;
  productId: string;
  proposals: Proposal[];
}

export interface FinancialProposals {
  dueNumberWithInsurance: number;
  dueNumberWithoutInsurance: number;
  monthlyAmountWithoutInsurance: number;
  monthlyAmountWithInsurance: number;
  annualGlobalEffectiveRate: number;
  lastMonthlyAmountWithoutInsurance: number;
  lastMonthlyAmountWithInsurance: number;
  firstMonthlyAmountWithInsurance: number;
  totalCostWithoutInsurance: number;
  totalCostWithInsurance: number;
  totalAmountWithoutInsurance: number;
  totalAmount: number;
  contractFees: number;
  annualDebitRate: number;
  borrowerMonthlyInsuranceAmount: number;
  coBorrowerMonthlyInsuranceAmount: number;
  firstPaymentDate: string;
  dueDay: number;
  fixedRate: boolean;
  borrowerFirstMonthlyInsuranceAmount: number;
  coBorrowerFirstMonthlyInsuranceAmount: number;
  cumulativeInsuranceRate: number;
  annualInsuranceEffectiveRate: number;
  borrowerTotalInsuranceAmount: number;
  coBorrowerTotalInsuranceAmount: number;
  creditDuration: number;
  borrowerHasSaleInsurance: boolean;
  coBorrowerHasSaleInsurance: boolean;
  deferralMonthNumber?: number;
  creditType?: string;
}

export interface Proposal {
  annualDebitRate: number;
  annualInsuranceEffectiveRate: number;
  contractFees: number;
  creditDuration: number;
  cumulativeInsuranceRate: number;
  dueDay: number;
  dueNumber: number;
  firstMonthlyAmountWithInsurance: number;
  firstMonthlyInsuranceAmountT1: number;
  firstMonthlyInsuranceAmountT2: number;
  firstPaymentDate: string;
  fixedRate: boolean;
  hasSaleInsuranceT1: boolean;
  hasSaleInsuranceT2: boolean;
  lastMonthlyAmountNonInsurance: number;
  lastMonthlyAmountWithInsurance: number;
  maxProposal: Proposal | null;
  monthlyAmountNonInsurance: number;
  monthlyAmountWithInsurance: number;
  monthlyInsuranceAmountT1: number;
  monthlyInsuranceAmountT2: number;
  taeg: number;
  totalAmount: number;
  totalAmountNonInsurance: number;
  totalCostNonInsurance: number;
  totalCostWithInsurance: number;
  totalInsuranceAmountT1: number;
  totalInsuranceAmountT2: number;
  insuranceTypeT1?: string;
  insuranceTypeT2?: string;
}
