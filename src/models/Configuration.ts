export interface AppConfiguration {
  _links: Links;
  screenOptions?: any;
  steps: any;
}

export interface Links {
  routes?: string;
  self: string;
  parameters: string;
}

export interface MessagesInfo {
  description: string;
  value: string;
}

export interface ColorInfo {
  description: string;
  value: string;
}

export interface ResourceInfo {
  description: string;
  value: string;
}
