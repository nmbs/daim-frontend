import { SelectObject } from '@/models/selectObject.ts';

export interface Nomenclature {
  statusPro: SelectObject[] | null;
  areaList1: SelectObject[] | null;
  areaList2: SelectObject[] | null;
  areaList4: SelectObject[] | null;
  areaList6: SelectObject[] | null;
  areaList7: SelectObject[] | null;

  professionList1: SelectObject[] | null;
  professionList2: SelectObject[] | null;
  professionList4: SelectObject[] | null;
  professionList6: SelectObject[] | null;
  professionList7: SelectObject[] | null;

  contractType: SelectObject[] | null;

  maritalStatus1: SelectObject[] | null;
  maritalStatus2: SelectObject[] | null;

  housingStateType: SelectObject[] | null;
}
