export interface ToolTip {
  imgSrc: string | null;
  text: string;
  alt: string;
  level?: TooltipLevel; // optional parameter, 'undefined' is considered to be TooltipLevel.INFO
}

export enum TooltipLevel {
  INFO,
  WARNING,
  ERROR // INFO & ERROR not yet implemented
}
