/**
 * List of routes that can be defined in configuration's steps or used in app
 */
export enum RoutePathId {
  SIMULATION = 'simulation',
  PEDAGOGIC = 'pedagogic',
  PEDAGOGIC_STEP = 'pedagogicSteps',
  SUBSCRIPTION = 'subscription',
  CREDIT_FROM_SCRATCH = 'creditFromScratch',
  PLV = 'homePlv',

  INFORMATION_BLOC = 'informationBloc',
  LOAN = 'loan',
  NAME = 'name',
  FAMILY_SITUATION = 'family',
  CONTACT = 'contact',

  SITUATION_BLOC = 'situationBloc',
  CIVILITY = 'civility',
  HOUSING = 'housing',

  INCOMES_BLOC = 'incomesBloc',
  EMPLOYMENT = 'employment',
  INCOMES = 'incomes',
  CHARGES = 'charges',

  INSURANCE = 'insurance',
  CARD = 'card',
  SUMMARY = 'summary',
  LOADING = 'loading',
  RESULT = 'result',
  RESULT_SOFINCO = 'resultSofinco',

  AUTH = 'auth',
  IDENTIFICATION = 'identification',

  LEGAL_MENTION = 'legal-mentions',
  ERROR_PAGE = 'error'
}
