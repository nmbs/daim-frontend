export interface BasketRecoveryStorage {
  keys: Keys[];
  secret: string;
  masterKey: string;
}

export interface Keys {
  id: string;
  type: Type;
}

export enum Type {
  SIMULATOR = 1, // Page Resultat
  SUBSCRIPTION = 2, // Page < Validation
  FAVORABLE = 3 // Avis Favorable
}

export let TypeConversion = {
  [Type.SIMULATOR]: 'simulator-v1',
  [Type.SUBSCRIPTION]: 'subscription-v1',
  [Type.FAVORABLE]: 'result-v1'
};
