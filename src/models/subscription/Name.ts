export enum Gender {
  WOMAN = 0,
  MAN = 1
}

export enum MaritalStatusCode {
  MARRIED = 'M_M', // Marié
  WIDOW = 'V_V', // Veuf
  DIVORCED = 'D_D', // Divorcé
  SINGLE = 'C_C', // Célibataire
  CIVIL_PARTNERSHIP = 'P_M', // Pacs
  SEPARATED = 'S_S', // Séparé
  IN_RELATION = 'U_U' // En relation
}

export interface Name {
  gender: Gender | null;
  firstName: string | null;
  lastName: string | null;
  birthName: string | null;
  maritalStatus: any | null;
  hasSpouse: boolean | null;
}
