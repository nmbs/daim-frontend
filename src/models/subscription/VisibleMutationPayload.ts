export interface VisibleMutationPayload {
  route: string;
  visible: boolean;
}
