export interface Insurance {
  acceptInsuranceOffer: boolean | null;
  confirmRefusal: boolean;
}

export interface Warranty {
  title: string;
  detail: string;
  image: string;
}

export interface InsuranceInfo {
  title: string;
  info: string;
  confirm: string;
  maxAge: number;
  pdf: string;
  pdf2: string;
  warranties: string;
  insuranceNote: string;
  infoDetail: string;
}

export enum InsurancePartnerName {
  PARTNER = 'partner',
  SOFINCO = 'sofinco'
}
