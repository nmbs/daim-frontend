export interface Charges {
  rental: number | null;
  refundHouseCredit: number | null;
  hasAlimony: boolean;
  alimony?: number | null;
  moreCredits: boolean;
  carCredit?: number | null;
  revolving?: number | null;
  otherCredit?: number | null;
}
