export interface InsuranceV2 {
  selectedInsurance: InsuranceV2Type;
  refuseInsurance: boolean;
}

/**
 * This interface represents the new insurance (v2) file model for the recommended/alternative/... insurance.
 */
export interface InsuranceInfoV2 {
  title: string; // The title of the insurance button (for ex: 'Assurance recommandée')
  image: string; // The image associated to the insurance and used in the insurance button
  importance: string; // The importance of the insurance used for the default warranties displayed when no insurance selected
  shortWarranties: string; // A short description of the warranties used in the insurance button
  warranties: string; // The warranties displayed on the right panel
  warrantiesTitle: string; // The warranties title displayed on the right panel
  commonWarranties: string; // The commons warranties used for the mobile display
  specificWarrantiesTitle: string; // The specifics warranties title used for the mobile display
  specificWarranties: string; // The specifics warranties used for the mobile display
  notIncludedWarranties: string;
  refuseInfoTitle: string;
  refuseInfoDetail: string; // The detail when the user refuse the insurance displayed on the right warning bloc
  refuseCTA: string;
  pdf: string;
  insuranceNote: string;
}

/**
 * The enumeration of available insurance.
 */
export enum InsuranceV2Type {
  RECOMMENDED = 'RECOMMENDED',
  ALTERNATIVE = 'ALTERNATIVE'
}

/**
 * A map to get the code returned by the simulation API depending on the insurance type.
 */
export const InsuranceCodeByType: Map<InsuranceV2Type, string> = new Map([
  [InsuranceV2Type.ALTERNATIVE, '01'],
  [InsuranceV2Type.RECOMMENDED, '02']
]);
