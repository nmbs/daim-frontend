export interface Adresse {
  adresseLabel: string | null;
  complementAdresse: string | null;
  adresseCity: any | null;
  housingType: any | null;
  sinceWhen: string | null;
}
