import { Adresse } from '@/models/subscription/Adresse';
import { Charges } from '@/models/subscription/Charges';
import { Civility } from '@/models/subscription/Civility';
import { Contact } from '@/models/subscription/Contact';
import { Employment } from '@/models/subscription/Employment';
import { Family } from '@/models/subscription/Family';
import { Incomes } from '@/models/subscription/Incomes';
import { Insurance } from '@/models/subscription/Insurance';
import { Name } from '@/models/subscription/Name';
import { InsuranceV2 } from '@/models/subscription/InsuranceV2';

export interface SubscriptionPayload {
  isPrincipal: boolean;
  subStateName: string;
  subStateValue:
    | Adresse
    | Charges
    | Civility
    | Contact
    | Employment
    | Family
    | Incomes
    | Insurance
    | InsuranceV2
    | Name
    | string
    | boolean
    | number
    | null;
}

export interface T1Payload {
  pathId: string;
  t1IsActive: boolean;
}

export interface EligibilityPayload {
  t1IsEligible?: boolean | null;
  t2IsEligible?: boolean | null;
  creditType?: string;
  firstMonthlyInsuranceAmount?: number;
  totalInsuranceAmount?: number;
  firstMonthlyInsuranceAmountT2?: number;
  totalInsuranceAmountT2?: number;
  t1IsEligibleToRecommendedInsurance?: boolean;
  t2IsEligibleToRecommendedInsurance?: boolean;
  t1IsEligibleToAlternativeInsurance?: boolean;
  t2IsEligibleToAlternativeInsurance?: boolean;
}
