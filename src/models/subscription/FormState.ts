export interface FormState {
  t1IsActive: boolean;
}

export interface InsuranceFormState extends FormState {
  version: 'v1' | 'v2';
  t1IsEligible: boolean; // If it's the insurance v2, the value is 'true' if the t1 is at least eligible for one insurance
  t1IsEligibleRecommended: boolean; // If the t1 is eligible to the recommended insurance
  t1IsEligibleAlternative: boolean; // If the t2 is eligible to the alternative insurance
  t2IsEligible: boolean; // If it's the insurance v2, the value is 'true' if the t2 is at least eligible for one insurance
  t2IsEligibleRecommended: boolean; // If the t2 is eligible to the recommended insurance
  t2IsEligibleAlternative: boolean; // If the t2 is eligible to the alternative insurance
}

export interface FormStates {
  loan: FormState;
  name: FormState;
  family: FormState;
  contact: FormState;
  civility: FormState;
  incomes: FormState;
  charges: FormState;
  employment: FormState;
  housing: FormState;
  insurance: InsuranceFormState;
}
