export interface Incomes {
  netIncome: number | null;
  salaryMonths: number;
  moreIncomes: boolean;
  familyBenefits?: number | null;
  housingBenefits?: number | null;
  otherIncomes?: number | null;
}
