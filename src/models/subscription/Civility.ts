export interface Civility {
  birthDate: string | null;
  birthCountry: any | null;
  birthCity: any | null;
  nationality: any | null;
}
