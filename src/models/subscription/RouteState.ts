export interface RouteState {
  valid?: boolean;
  disabled: boolean;
  active: boolean;
  visible: boolean;
  isBlock?: boolean;
  hasChildren: boolean;
  opened?: boolean;
}

export interface RouteStates {
  informationBloc: RouteState;
  loan: RouteState;
  name: RouteState;
  contact: RouteState;
  situationBloc: RouteState;
  civility: RouteState;
  family: RouteState;
  housing: RouteState;
  incomesBloc: RouteState;
  employment: RouteState;
  incomes: RouteState;
  charges: RouteState;
  insurance: RouteState;
  card: RouteState;
  summary: RouteState;
  loading: RouteState;
  result: RouteState;
  resultSofinco: RouteState;
}
