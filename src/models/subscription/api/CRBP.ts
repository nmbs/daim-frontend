export interface CRBPResponse {
  crbpRule: number;
  crbpVersion: number;
}
