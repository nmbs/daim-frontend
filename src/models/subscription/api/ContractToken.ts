export interface ContractToken {
  accessToken: string;
  idToken: string;
  refreshToken: string;
  expiresIn: number;
}
