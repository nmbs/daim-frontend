import { Client } from '@/models/subscription/api/Client';
import { Credit } from '@/models/subscription/api/Credit';
import { PartnerData } from '@/models/subscription/api/PartnerData';

export interface Contract {
  credit: Credit;
  borrower: Client;
  coBorrower: Client | null;
  hasSendingSummaryMail?: boolean;
  partnerData: PartnerData;
  additionalData?: AdditionalSubscriptionData;
  crbpRule?: number;
  crbpVersion?: number;
}

export interface AdditionalSubscriptionData {
  attribute1: string | null;
  attribute2: string | null;
  // attribute3: string; // TODO: uncomment when enabling subscription data signature
}
