export interface Credit {
  sourceId: string | null;
  partnerId: string | null;
  amount: number | null;
  duration: number | null;
  vehiculeAge: number | null;
  vehiculeAmount: number | null;
  cardType: string | null;
  appId: string | null;
  scaleCode?: string;
  orderId?: string;
  orderAmount?: number;
  personalContributionAmount?: number;
}
