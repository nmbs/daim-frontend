export interface ISupportingDocuments {
  borrowerCode: string;
  category: string;
  id: string;
  isAddedByConsultant: boolean;
  maxPages: number;
  minPages: number;
  ocrResult: IOCRResult;
  pages: [any];
  referentialLabels: [ISupportingDocuments];
  shortLabel: string;
  longLabel: string;
  status: string;
}

export interface IOCRResult {
  status: string;
  globalResult: string;
  imageQualityResult: string;
  validityResult: string;
  accountIdentification: string;
  customerInformation: string;
  documentType: string;
}
