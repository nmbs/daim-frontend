export interface PartnerData {
  businessProviderId?: string;
  preScoringCode?: string;
  equipmentCode?: string;
  returnUrl?: string;
}
