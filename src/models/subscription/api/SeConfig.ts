export interface SeConfig {
  hasAccessUploadSE: boolean;
  hasForcedSE: boolean;
  hasUbble: boolean;
  hasUploadPaper: boolean;
  hasDQE: boolean;
}

export interface SeConfigParams {
  seType: string;
  contractId: string;
  productType: string;
  borrower: string;
  hasT2: boolean;
  productCode: string;
  productId: string;
}
