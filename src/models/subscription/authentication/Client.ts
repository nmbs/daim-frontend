export interface ClientData {
  lastName: string;
  firstName: string;
  birthDate: string;
  citizenshipCode: string;
  personalContact: Contact;
  id: string;
  titleCode: string;
  birthName: string;
  birthCountryCode: string;
  birthCityCode: string;
  birthPostalCode: string;
  maritalStatusCode: string;
  housingOccupantStatusCode: string;
}

export interface Contact {
  streetName: string;
  additionalStreetInfo: string;
  postCode: string;
  townName: string;
  countryCode: string;
  mobileNumber: string;
  phoneNumber: string;
  emailAddress: string;
}
