export type SendBy = 'sms' | 'email';

export interface ICustomerIdentification {
  form: ICustomerIdentificationForm;
  choice: ICustomerIdentificationChoice;
}

export interface ICustomerIdentificationForm {
  lastName: string;
  firstName: string;
  birthDate: Date | string;
  birthPostCode: string;
}

export interface ICustomerIdentificationChoice {
  id: string;
  token: string;
  encodedMobileNumber: string;
  encodedEmailAddress: string;
  postCode: string;
  sendBy: SendBy;
}
