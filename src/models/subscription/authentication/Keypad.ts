export interface IKeypadsParameters {
  theme: string;
  fontSize: number;
  rowMargin?: number;
  columnMargin?: number;
  minDigit?: number;
  maxDigit?: number;
  row: number;
  column: number;
  digitWidth: number;
  digitHeight: number;
}

export interface IKeypadsParametersV2 {
  minDigit?: number;
  maxDigit?: number;
  numberOfRows: number;
  numberOfColumns: number;
  cellWidthCss?: number;
  cellHeightCss?: number;
  textFontSizeCss?: number;
  rowMarginCss?: number;
  columnMarginCss?: number;
  bgColorCodeCss?: string;
  textColorCodeCss?: string;
  cellBorderCodeCss?: string;
  cellBorderWidthCss?: number;
  textFontFamilyCss?: string;
}

export interface IKeypadsResponse {
  id: string;
  digitMasks: number[];
}

export interface IPosition {
  x: number;
  y: number;
}

export interface KeypadState {
  id: string;
}
