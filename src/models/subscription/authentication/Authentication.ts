import { ICustomerIdentification } from '@/models/subscription/authentication/ICustomerIdentification';

export interface AuthenticationState {
  request: AuthenRequest;
  response: AuthenResponse;
  identification: ICustomerIdentification;
  clientIsLogging: boolean;
}

export interface AuthenRequest {
  id: string;
  passwordMask: string;
  birthDate: string;
  keypadId: string;
}

export interface AuthenResponse {
  token: string;
  customerId: string;
  role: string;
  authenSource?: string;
}

export interface AuthJwt {
  sub?: string;
}

export enum LoginFormType {
  SigninForm = 1,
  CustomerIdentificationForm = 2,
  CustomerIdentificationChoice = 3,
  CustomerIdentificationChoiceResult = 4
}
