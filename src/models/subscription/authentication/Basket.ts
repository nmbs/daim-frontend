export interface Basket {
  encryptedState: string;
  publicState: string;
}
