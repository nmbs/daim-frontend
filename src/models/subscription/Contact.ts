export interface Contact {
  email: string | null;
  mobilePhone: string | null;
  homePhone: string | null;
  lenderOffer: boolean | null;
  partnerOffer: boolean | null;
  contactOffer?: boolean | null;
}
