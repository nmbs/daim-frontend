import { ContractToken } from '@/models/subscription/api/ContractToken';

export interface FolderResponse {
  acceptedCode: string | null;
  changedProposal: boolean | null;
  contractId: string | null;
  creditType: string | null;
  customerId: string | null;
  eligibilitySE: boolean | null;
  proposal: any | null;
  proposalExpiredDate: string | null;
  rgbpCode: string | null;
  token: string | null;
  contractToken: ContractToken | null;
  orderId?: string;
  partnerId?: string;
  bankingRib?: string;
  cardT1?: string;
  creditPartnerCode?: string;
  creditRouteCode?: string;
  channelCode?: string;
  createdDate?: string;
}
