export interface Employment {
  professionalStatus: any | null;
  profession: any | null;
  activitySector: any | null;
  contract: any | null;
  sinceWhen: string | null;
}
