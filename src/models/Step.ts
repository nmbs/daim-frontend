/**
 * Represents a Step object from the offer configuration.
 * The step structure is recursive as each Step has an array of child Steps.
 * This interface is meant to match the data structure returned by the API.
 */
import { ScreenOption } from '@/models/ScreenOption';

export interface Step {
  /**
   * Relative path of the step. Concatenate parent paths to have the full path.
   */
  pathId: string;

  /**
   * Step options, mostly to provide meta data about the step itself or its content
   */
  screenOptions: ScreenOption;

  /**
   * Children steps
   */
  steps: Step[] | null;

  /**
   * Whether this step can be routed to or is just a sub-part inside another step
   */
  routed: boolean;

  /**
   * Step option to have the possibility to go back.
   */
  noReturn: boolean;
}
