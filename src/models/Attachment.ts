export interface Attachment {
  titre: string;
  icon: string;
  description?: string;
}
