export enum ColorThemeEnum {
  LIGHT = 'LIGHT',
  DARK = 'DARK',
  SOFINCO = 'SOFINCO'
}
