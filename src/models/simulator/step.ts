export class Step {
  public pathID: string;
  public options: any;
  public steps: Step[];

  constructor() {
    this.options = {};
    this.steps = [];
    this.pathID = '';
  }

  public appendChild(node: Step) {
    if (this.steps === undefined) {
      this.steps = [];
    }
    this.steps.push(node);
  }
}
