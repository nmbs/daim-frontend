/**
 * FilterCriterion
 *
 * When the user interacts with the first panel, LoanParameter, then
 * a FilterCriterion is inserted in the app store. This is then used
 * by second panel, OfferSelector, to filter and display LoanProposals.
 *
 * If the user choose to return to previous proposals, the FilterCriterion
 * is then removed from the app store (=> set to null).
 */
export interface FilterCriterion {
  type: EFilterCriterionAttribute;
  value: number;
}

/**
 * EFilterCriterionAttribute
 *
 * Specific value to filter, either time duration
 * or monthly payment value of the loan.
 */
export enum EFilterCriterionAttribute {
  MONTHLY_PAYMENT,
  TIMELINE
}
