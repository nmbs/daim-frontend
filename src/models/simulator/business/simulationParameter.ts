export interface SimulationParameter {
  amount?: number | null;
  duration?: number | null;
}
