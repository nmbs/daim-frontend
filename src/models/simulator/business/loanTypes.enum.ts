/**
 * Le parcours CRA sera présent sur la page panier ikea (au moment de choisir le moyen de payer)
 * Le parcours VAC (simu indé) sera présent sur les pages produits ikea (le client simule, pas d'engagement)
 * Le parcours CRS (crédit renouvelable) et les nouveaux parcours avec le nouveau simulateur,
 * où tu peux renseigner un montant, sera présent sur la page financement ikea
 */
export enum LoanTypes {
  CRS = 'crs', // Montant fixe
  VAC = 'vac', // Montant paramétrable par l'utilisateur
  CRA = 'cra' // Montant fixe
}
