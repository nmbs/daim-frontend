export enum CreditTypeEnum {
  LOAN = 'LOAN',
  REVOLVING = 'REVOLVING'
}
