export interface SimulationFinancialProposal {
  amount: number;
  creditCost: number;
  creditDurationWithoutInsurance: number;
  creditDurationWithInsurance: number;
  nbMonthlyPaymentWithoutInsurance: number; // eq nbRegularMonthlyPaymentWithoutInsurance in confluence
  nbMonthlyPaymentWithInsurance: number; // eq nbRegularMonthlyPaymentWithInsurance in confluence
  monthlyAmountWithoutInsurance: number;
  monthlyAmountWithInsurance: number;
  lastMonthlyAmountWithoutInsurance?: number;
  lastMonthlyAmountWithInsurance?: number;
  totalAmountWithoutInsurance: number;
  totalAmountWithInsurance: number;
  firstPaymentDate?: string;
  taeg: number;
  taea: number;
  debitRate: number;
  fixedRate: boolean;
  insuranceRate?: number;
  monthlyInsuranceAmount: number;
  monthlyInsuranceAmountT2?: number;
  utilizationDate?: string;
  dueDay?: number;
  deferralMonthNumber?: number;
  firstMonthlyInsuranceAmount: number;
  firstMonthlyInsuranceAmountT2?: number;
  scaleCode?: string;
  totalInsuranceCost: number;
  totalInsuranceAmount?: number;
  totalInsuranceAmountT2?: number;
  maxProposal?: SimulationFinancialProposal;
  paramId?: string;
  label?: string;
  contractFees?: number;
  creditType?: string;
  insuranceTypeT1?: string;
  insuranceTypeT2?: string;
  hasSaleInsuranceT1?: boolean;
  hasSaleInsuranceT2?: boolean;
}
