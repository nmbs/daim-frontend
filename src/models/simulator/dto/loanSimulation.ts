import { LoanFinancialProposal } from '@/models/simulator/dto/loanFinancialProposal';
/**
 * LoanSimulation
 *
 * This DTO is returned by the simulation API upon success.
 * It contains all the proposals related to the customer required amount.
 */
export interface LoanSimulation {
  scaleLabel: string;
  amount: number;
  personalContributionAmount: number;
  financialProposals: LoanFinancialProposal[];
}
