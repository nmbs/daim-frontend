export interface SimulationContext {
  offerId: string;
  partnerId: string;
  sourceId: string;
  projectId: string;
  productId: string;
  businessDataTransferToken: string;
  tokenOwner: string;
  simulationId: string;
  simulationOrigin: string;
  vehicleAge: string;
  vehicleAmount: string;
  offer: string;
  cardType: string;
  mfactoryid: string[];
}
