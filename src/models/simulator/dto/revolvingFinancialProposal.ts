export interface RevolvingFinancialProposal {
  dueNumberWithInsurance: number;
  dueNumberWithoutInsurance: number;
  monthlyAmountWithoutInsurance: number;
  monthlyAmountWithInsurance: number;
  annualGlobalEffectiveRate: number;
  lastMonthlyAmountWithoutInsurance: number;
  lastMonthlyAmountWithInsurance: number;
  firstMonthlyAmountWithInsurance: number;
  totalCostWithoutInsurance: number;
  totalCostWithInsurance: number;
  totalAmountWithoutInsurance: number;
  totalAmountWithInsurance: number;
  contractFees: number;
  annualDebitRate: number;
  borrowerMonthlyInsuranceAmount: number;
  coBorrowerMonthlyInsuranceAmount: number;
  firstPaymentDate: string;
  dueDay: number;
  fixedRate: boolean;
  borrowerFirstMonthlyInsuranceAmount: number;
  coBorrowerFirstMonthlyInsuranceAmount: number;
  cumulativeInsuranceRate: number;
  annualInsuranceEffectiveRate: number;
  borrowerTotalInsuranceAmount: number;
  coBorrowerTotalInsuranceAmount: number;
  creditDuration: number;
  hasBorrowerSaleInsurance: boolean;
  hasCoBorrowerSaleInsurance: boolean;
  deferralMonthNumber: number;
  scaleCode?: string;
}

// Note: only needed fields have been defined in this interface, more optional fields
// are defined in swagger
export interface RevolvingSimulationParameters {
  amount: number;
  durations?: number[];
  hasBorrowerInsurance?: boolean;
  businessProviderId?: string;
  isPrincipal?: boolean; // Whether simulation should be performed on principal plan
  equipmentCode?: string;
  scaleCode?: string;
}
