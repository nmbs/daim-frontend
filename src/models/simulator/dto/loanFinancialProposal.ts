/**
 * LoanFinancialProposal
 *
 * This DTO is returned by the simulation API upon success inside LoanSimulation.
 * All the loan details are contained withing this class.
 */
export interface LoanFinancialProposal {
  amount: number;
  dueStandardNumber: number;
  scaleCode: string;
  scaleLabel: string;
  schedule: number;
  periodicity: number;
  personalContributionAmount: number;
  givenLoanAmount: number;
  monthlyAmountWithoutInsurance: number;
  monthlyAmountWithInsuranceAndServices: number;
  monthlyAmountRounded: number;
  monthlyInsuranceAmount: number;
  firstPaymentAmount: number;
  lastPaymentAmount: number;
  loanDuration: number;
  annualPercentageRate: number;
  monthlyPercentageRate: number;
  annualPercentageRateOfCharge: number;
  annualPercentageRateOfInsurance: number;
  totalCostWithoutInsurance: number;
  totalCostWithInsuranceAndServices: number;
  monthlyEquipmentInsuranceAmount: number;
  equipmentInsuranceTotalAmount: number;
  borrowerMonthlyInsuranceAmount: number;
  coBorrowerMonthlyInsuranceAmount: number;
  borrowerTotalInsuranceAmount: number;
  coBorrowerTotalInsuranceAmount: number;
  hasBorrowerSaleInsurance: boolean;
  hasCoBorrowerSaleInsurance: boolean;
  utilizationRate: number;
  totalAmountWithoutInsurance: number;
  totalAmountWithInsurance: number;
  overdraftChargesType: string;
  overdraftChargesAmount: number;
  insuranceBonusAmount: number;
  personInsuranceTotalAmount: number;
  dueNumber: number;
  monthlyAmountWithInsurance: number;
  contractFees: number;
  totalCostWithInsurance: number;
  fixedRate: boolean;
  dueDeferralNumber: number;
}

// Note: only needed fields have been defined in this interface, more optional fields
// are defined in swagger
export interface LoanSimulationParameters {
  amount: number;
  dueNumbers?: number[];
  hasBorrowerInsurance?: boolean;
  businessProviderId?: string;
  equipmentCode?: string;
  scaleCode?: string;
}
