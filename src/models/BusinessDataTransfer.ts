export interface BusinessDataTransfer {
  providerContext: IProviderContext;
  customerContext: ICustomerContext;
  coBorrowerContext: ICustomerContext;
  offerContext: IOfferContext;
}

export interface IProviderContext {
  businessProviderId?: string;
  returnUrl: string | null;
  exchangeUrl?: string | null;
  homeReturnUrl?: string | null;
}

export interface ICustomerContext {
  externalCustomerId?: string;
  civilityCode?: number;
  firstName?: string;
  lastName?: string;
  birthName?: string;
  birthDate?: string;
  citizenshipCode?: string;
  birthCountryCode?: string;
  street?: string;
  additionalStreet?: string;
  city?: string;
  zipCode?: number;
  distributerOffice?: string;
  countryCode?: string;
  phoneNumber?: string;
  mobileNumber?: string;
  emailAddress?: string;
  loyaltyCardId?: string;
}

export interface IOfferContext {
  orderId?: string;
  scaleId?: string;
  equipmentCode?: string;
  amount?: number;
  orderAmount?: number;
  personalContributionAmount?: number;
  duration?: number;
  preScoringCode?: string;
  suggestedDueNumber?: string;
  suggestedScaleId?: string;
}
