/**
 * The enum list of available status for v2 insurance buttons:
 * - INITIAL: when the button is not selected
 * - SELECTED: when the button is selected
 * - IN_ERROR: when the button is in error
 */
export enum InsuranceButtonStatus {
  // Status when the button is not selected
  INITIAL = 'INITIAL',
  // Status when the button is selected
  SELECTED = 'SELECTED',
  // Status when the button is in error
  IN_ERROR = 'IN_ERROR'
}
