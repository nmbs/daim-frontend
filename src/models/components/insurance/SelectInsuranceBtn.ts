import { InsuranceButtonStatus } from '@/models/components/insurance/InsuranceButtonStatus';
import { InsuranceV2Type } from '@/models/subscription/InsuranceV2';

/**
 * Model of the select insuranrance button used in the SelectInsuranceButton.vue.
 */
export interface SelectInsuranceBtn {
  image: string;
  title: string;
  amount: number;
  shortWarranties: string[];
  status: InsuranceButtonStatus;
  isHighlighted: boolean;
  isLoading: boolean;
  highlightedText?: string;
  type: InsuranceV2Type;
}
