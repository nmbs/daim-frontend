import { InsuranceButtonStatus } from '@/models/components/insurance/InsuranceButtonStatus';

/**
 * Model of the refuse insurance button used in RefuseInsuranceButton.vue component.
 */
export interface RefuseInsuranceBtn {
  uniqueId: string;
  title: string;
  confirmationText: string;
  errorText: string;
  isChecked: boolean;
  isInError: boolean;
  status: InsuranceButtonStatus;
}
