export interface CarouselPopinContent {
  title: string;
  action: string;
  slides: string;
}

export interface CarouselSlide {
  title: string;
  content: string;
  imageUrl: string;
}
