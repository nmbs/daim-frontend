export interface Colors {
  primary: string;
  secondary: string;
  tertiary: string;
  quaternary: string;
  quintinary: string;
}

export const defaultColors = {
  primary: '#1AAEB7',
  secondary: '#219AFB',
  tertiary: '#EB3761',
  quaternary: '#4d1867',
  quintinary: '#e38e00'
};
