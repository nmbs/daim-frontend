export interface RadioButtonGroup {
  label: string;
  imgPath: string;
  imgPathSelected: string;
}
