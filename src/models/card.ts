export enum CardType {
  P = 'P',
  I = 'I',
  S = 'S'
}
export interface Card {
  expiration: number;
  identifier: string;
  type: string;
  value: string;
}
