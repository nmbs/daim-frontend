export interface PedagogicStepModel {
  logo: string;
  stepNumber: number;
  hasLink: boolean;
}
