export default interface AccordionData {
  imgUrl: string;
  title: string;
  descriptionText?: string;
  descriptionHtml?: string;
}

export interface AccordionDataList {
  title?: string;
  accordionData: AccordionData[];
}
