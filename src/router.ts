import Router, { Route, RouteConfig } from 'vue-router';

import store from '@/store';
import * as Mutations from '@/store/Mutations';

import { StepState } from '@/models/StepState';
import { RouteState } from '@/models/subscription/RouteState';

import { RoutePathId } from '@/models/RoutesPath';

import LegalMentions from '@/views/LegalMentions.vue';
import ErrorPage from '@/views/ErrorPage.vue';

/**
 * Static routes are defined here. Any static route must be declared here.
 */
const staticRoutes: RouteConfig[] = [
  {
    path: `/${RoutePathId.LEGAL_MENTION}`,
    name: RoutePathId.LEGAL_MENTION,
    component: LegalMentions
  },
  {
    path: `/${RoutePathId.ERROR_PAGE}`,
    name: RoutePathId.ERROR_PAGE,
    component: ErrorPage,
    props: true
  }
];

/**
 * Router factory that builds a vue router from a RouteConfig object.
 */
export function getRouter(routesConfig: RouteConfig[]): Router {
  const router = new Router({
    routes: routesConfig.concat(staticRoutes),
    scrollBehavior() {
      // Scroll to top after routing
      return { x: 0, y: 0 };
    }
  });

  /**
   * Navigation hook called each time a new route is being routed to
   * @param to: The target Route Object being navigated to
   * @param from: The current route being navigated away from
   * @param next: Callback that must be called to resolve the hook
   * @see https://router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards
   */
  router.beforeEach((to: Route, from: Route, next) => {
    const isAccessingStaticRoute = staticRoutes.some((staticRoute) => to.path.includes(staticRoute.path));
    const isLandingOnFirstPage = from.name === null && isAccessingFirstPage(to.path, routesConfig);

    if (isAccessingStaticRoute) {
      // Accessing static route: resume routing without additional actions.
      next();
    } else if (isLandingOnFirstPage) {
      // Accessing first page: resume routing.
      next();
    } else if (isToForm(to)) {
      // Prevent user from going when in result ok or result ko page
      if (from.name === RoutePathId.RESULT || from.name === RoutePathId.RESULT_SOFINCO) {
        next(false);
        return;
      }

      // Controls when navigating on the subscription path. Update route states to update left panel.
      const subscriptionState = (store.state as any).subscription;
      const firstStepPathId: string = store.getters.getFirstSubscriptionStep;
      const routeName: string = to.name || '';

      // Disable all route after routed one to force the user to redo full subscription path
      const routeList = subscriptionState.routesList;
      const destinationIndex = routeList.findIndex((e: string) => e === routeName);

      if (routeName === firstStepPathId) {
        activateRoute(routeName);
        next();
      } else {
        // Get previous step for forms validation checks
        const currentState: RouteState = subscriptionState.routeStates[routeList[destinationIndex - 1]];
        if (!currentState) {
          next({ name: RoutePathId.ERROR_PAGE });
          return;
        }

        let previous: string =
          currentState.isBlock && currentState.hasChildren
            ? routeList[destinationIndex - 2]
            : routeList[destinationIndex - 1];

        // TODO: improve method by making it more generic (support 2 consecutive hidden steps)
        if (previous) {
          const previousState = subscriptionState.routeStates[previous];
          // If previous step is hidden, get previous item
          // Exemple: if insurance is hidden, previous item of summary step is card step
          if (!previousState.visible) {
            previous = routeList[destinationIndex - 2];
          }
        }

        if (previous === null) {
          // Safety check
          activateRoute(routeName);
          next();
        } else {
          const previousFormsValid = checkPreviousFormsValid(to.name || '');
          if (previousFormsValid) {
            // Update root states before resuming routing to synchronize left side panel
            store.commit(Mutations.MARK_FORM_AS_VALID, previous);
            activateRoute(routeName);
            next();
          } else {
            next(false);
          }
        }
      }
    } else {
      // Other cases. Add before route treatment here if you need any.
      next();
    }
  });

  return router;
}

function isAccessingFirstPage(targetPath: string, routesConfig: RouteConfig[]) {
  const initialRouteConfig = routesConfig[1]; // routeConfig[0] is always '/' path hence we take the next one
  let lastChildConfig = initialRouteConfig;
  while (lastChildConfig.children && lastChildConfig.children.length > 0) {
    lastChildConfig = lastChildConfig.children[0];
  }
  const firstPath = initialRouteConfig.path;
  const firstRedirectPath = lastChildConfig.path;
  return targetPath === firstPath || firstPath === firstRedirectPath;
}

/**
 * Function to check if previous forms are valid
 */
function checkPreviousFormsValid(currentStepPathId: string): boolean {
  const hasBorrower = store.getters.hasBorrower;
  const previousSteps = store.getters.getPreviousSteps(currentStepPathId);
  return previousSteps.every((principalStep: StepState) => {
    // Warning: exceptional case for familySituation
    const secondaryStep: any =
      principalStep.pathId === RoutePathId.FAMILY_SITUATION ? principalStep.getPrevious(true) : principalStep;

    const principalBlocIsValid: boolean = store.getters.getValidationStatus(principalStep.pathId, true);
    const secondaryBlocIsValid: boolean = store.getters.getValidationStatus(secondaryStep.pathId, false);
    return principalBlocIsValid && (!hasBorrower || secondaryBlocIsValid);
  });
}

/**
 * Checks whether user is being routed to a subscription form.
 * @param to
 */
function isToForm(to: Route): boolean {
  return to && to.path.startsWith('/subscription/');
}

/**
 * Update route states to make the mark the given route as active.
 * @param routeName
 */
function activateRoute(routeName: string) {
  store.commit(Mutations.ACTIVATE_ROUTE, routeName, { root: true });
}
