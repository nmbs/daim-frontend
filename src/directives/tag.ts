import { DirectiveOptions } from 'vue';
import { getTagManagerInstance } from '@/services/tagManagerFactory';

function handleClick(this: any) {
  getTagManagerInstance().postClick(this.element);
}

const directive: DirectiveOptions = {
  bind(element, binding, vnode) {
    element.addEventListener('click', handleClick.bind({ element }));
  },
  unbind(element, binding, vnode) {
    element.removeEventListener('click', null);
  }
};

export default directive;
