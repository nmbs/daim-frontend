const config = require('config');

const rootFolder = process.env.LOG4JS_PATH ? process.env.LOG4JS_PATH : config.LOG4JS_PATH;
const appFolder = 'webpartner-frontend';

const appenders = {
  access: {
    type: 'dateFile',
    filename: `${rootFolder}/${appFolder}/access.log`,
    pattern: '-yyyy-MM-dd',
    category: 'http',
    numBackups: 10
  },
  app: {
    type: 'file',
    filename: `${rootFolder}/${appFolder}/app.log`,
    maxLogSize: 10485760,
    numBackups: 5
  },
  console: {
    type: 'stdout',
    layout: {
      type: 'colored'
    }
  },
  errorFile: {
    type: 'file',
    maxLogSize: 10485760,
    filename: `${rootFolder}/${appFolder}/errors.log`,
    numBackups: 5
  },
  errors: {
    type: 'logLevelFilter',
    level: 'ERROR',
    appender: 'errorFile'
  }
};
const defaultAppenders = process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'integration-cacd2-daim-ci'? ['app','console', 'errors'] : ['app','errors'];
const logLevel = process.env.LOG_LEVEL || config.log.appLevel || 'info';
const categories = {
  default: { appenders: defaultAppenders, level: logLevel },
  http: { appenders: ['access'], level: config.log.httpLevel }
};

module.exports = {
  appenders,
  categories
};
