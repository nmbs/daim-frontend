const hash = require('hash.js');
const log = require('log4js').getLogger('dataSignature');

/**
 * Compute mandatory data signature
 * @param {*} mandatoryData
 * @param {*} salt
 */
function signData(mandatoryData, salt, correlationID) {
  log.debug(`Sign mandatory subscription data - CorrelationID ${correlationID}`);
  const {
    partnerCode,
    offerCode,
    equipmentCode,
    businessProviderCode,
    scaleCode,
    amount,
    orderAmount,
    personalContributionAmount,
    duration,
    returnUrl,
    loyaltyCardId,
    orderId
  } = mandatoryData;
  const key =
    [
      partnerCode,
      offerCode,
      equipmentCode,
      businessProviderCode,
      scaleCode,
      amount,
      orderAmount,
      personalContributionAmount,
      duration,
      returnUrl,
      loyaltyCardId,
      orderId
    ].join('+') + `=${salt}`;
  const signature = hash.sha256().update(key).digest('hex');
  log.trace(`Generated signature ${signature} - CorrelationID ${correlationID}`);
  return signature;
}

module.exports = {
  signData
};
