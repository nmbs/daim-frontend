const request = require('request');
const config = require('config');
const log = require('log4js').getLogger('configurationService');

/**
 * Get configuration of a partner or an offer
 * @param {*} url Url of configuration
 * @param {*} token
 * @param {*} correlationID
 * @param {*} message Message to add in log - should be used to format partner and offer parameters
 */
function getConfiguration(url, token, correlationID, message) {
  return new Promise(function (resolve, reject) {
    log.debug(`Fetch configuration for ${message} - CorrelationID ${correlationID}`);
    request.get(
      {
        rejectUnauthorized: false,
        headers: { Authorization: `Bearer ${token}` },
        url,
        body: 'grant_type=client_credentials'
      },
      function (err, response, body) {
        if (err) {
          reject({
            message: `Error while fetching configuration for ${message}`,
            err
          });
        } else if (response.statusCode >= 400) {
          reject({
            message: `Error ${response.statusCode} when fetching configuration for ${message}`,
            statusCode: response.statusCode,
            additionalContext: [
              {
                type: 'body',
                data: body
              }
            ]
          });
        } else {
          try {
            const data = JSON.parse(body);
            if (data) {
              resolve(data);
            } else {
              reject({
                message: `No partner configuration for ${message}`
              });
            }
          } catch (err) {
            reject({
              message: `Error while parsing configuration for ${message}`,
              err,
              additionalContext: [
                {
                  type: 'body',
                  data: body
                }
              ]
            });
          }
        }
      }
    );
  });
}
/**
 * Fetch partner configuration
 * @param {string} partner Partner code (Q6)
 * @param {string} token Access token
 * @param {string} correlationID Request correlationID
 */
function getPartnerConfiguration(partner, token, correlationID) {
  return getConfiguration(
    `${config.get('APIM_BACK_Url')}/applicationsPartnersConfiguration/v1/applications/${config.get(
      'APPLICATION_ID'
    )}/partners/${partner}`,
    token,
    correlationID,
    `partner ${partner}`
  );
}

/**
 * Fetch offer configuration of a partner
 * @param {string} partner Partner code (Q6)
 * @param {string} offer Offer code (X1)
 * @param {string} token Access token
 * @param {string} correlationID Request correlationID
 */
function getOfferConfiguration(partner, offer, token, correlationID) {
  return getConfiguration(
    `${config.get('APIM_BACK_Url')}/applicationsPartnersConfiguration/v1/applications/${config.get(
      'APPLICATION_ID'
    )}/partners/${partner}/routes/${offer}`,
    token,
    correlationID,
    `partner ${partner} and offer ${offer}`
  );
}

/**
 * Fetch new offer configuration if current configuration specify a switch. new X1 and new offer configuration.
 * @param {*} offerConfig Current offerConfig
 * @param {*} amount
 * @param {*} partnerCode
 * @param {*} accessToken
 * @param {*} correlationID
 */
const switchConfigurationIfNeeded = async (offerConfig, amount, partnerCode, accessToken, correlationID) => {
  if (
    offerConfig &&
    offerConfig.screenOptions &&
    offerConfig.screenOptions.switchThreshold &&
    offerConfig.screenOptions.lowerX1 &&
    offerConfig.screenOptions.upperX1
  ) {
    log.trace(`Switch of configuration needed - CorrelationID ${correlationID}`);
    let newOfferCode;
    if (amount) {
      const amountNumber = +amount;
      if (isNaN(amountNumber)) {
        throw new Error(`Amount ${amount} is not a number - CorrelationID ${correlationID}`);
      }
      if (amountNumber <= offerConfig.screenOptions.switchThreshold.value) {
        newOfferCode = offerConfig.screenOptions.lowerX1.value;
      } else {
        newOfferCode = offerConfig.screenOptions.upperX1.value;
      }
    } else {
      newOfferCode = offerConfig.screenOptions.noAmountX1.value;
    }
    log.debug(`Switch of configuration detected, new X1 = ${newOfferCode} - CorrelationID ${correlationID}`);
    const newConfig = await getOfferConfiguration(partnerCode, newOfferCode, accessToken, correlationID);
    return { newX1: newOfferCode, newOfferConfig: newConfig };
  } else {
    return { newX1: null, newConfig: null };
  }
};

/**
 * Merge offer and partner configuration.
 * Options are merged. If an option is present in offer and partner configuration, offer value is used.
 * Steps are not merged. If steps are defined in offer, offer steps are used. Otherwise partner steps are used.
 * @param offerConf
 * @param partnerConf
 */
const mergeConfigurations = (offerConf, partnerConf, correlationID) => {
  log.trace(`Merge offer and partner configurations - CorrelationID ${correlationID}`);
  const commonConfig = {};
  if (offerConf) {
    if (offerConf.steps && offerConf.steps.length) {
      commonConfig.steps = offerConf.steps;
    } else {
      commonConfig.steps = partnerConf.steps;
    }
    if (partnerConf.screenOptions) {
      commonConfig.screenOptions = {
        ...commonConfig.screenOptions,
        ...partnerConf.screenOptions
      };
    }
    if (offerConf.screenOptions) {
      commonConfig.screenOptions = {
        ...commonConfig.screenOptions,
        ...offerConf.screenOptions
      };
    }
  } else {
    commonConfig.steps = partnerConf.steps;
    commonConfig.screenOptions = partnerConf.screenOptions;
  }
  return commonConfig;
};

module.exports = {
  getPartnerConfiguration,
  getOfferConfiguration,
  switchConfigurationIfNeeded,
  mergeConfigurations
};
