const request = require('request');
const config = require('config');
const apimHandler = require('../lib/@cacf-modules/apim-token-handler');
const log = require('log4js').getLogger('apim');

const expiredIdTokenDuration = config.get('EXPIRED_TOKEN_DURATION');

/**
 * Fetch access_token
 * @param {string} apimUrl
 * @param {string} token Basic authentication (encoded client id and secret)
 * @param {string} correlationID
 */
function auth(apimUrl, token, correlationID) {
  return new Promise(function (resolve, reject) {
    log.debug(`Perform authentication using token ${token} - Correlation ID ${correlationID}`);
    request.post(
      {
        rejectUnauthorized: false,
        headers: { Authorization: `Basic ${token}` },
        url: `${apimUrl}/token`,
        body: 'grant_type=client_credentials'
      },
      function (err, response, body) {
        if (err) {
          reject({
            message: `Error while fetching access_token`,
            err
          });
        } else if (response.statusCode >= 400) {
          reject({
            message: `Error ${response.statusCode} when getting access_token`,
            additionalContext: [
              {
                type: 'body',
                data: body
              }
            ]
          });
        } else {
          try {
            const data = JSON.parse(body);
            if (data.access_token) {
              resolve(data.access_token);
            } else {
              reject({
                message: `Error while getting access_token: response is empty`
              });
            }
          } catch (err) {
            reject({
              message: `Error while getting access_token: can't parse body`,
              err,
              additionalContext: [
                {
                  type: 'body',
                  data: body
                }
              ]
            });
          }
        }
      }
    );
  });
}

function getIdGeneratorToken(apimUrl, token, subject, tokenDuration, correlationID) {
  return new Promise(function (resolve, reject) {
    log.debug(
      `Generate ID Token using token ${token} - subject ${subject} - duration ${tokenDuration} - CorrelationID ${correlationID}`
    );
    request.post(
      {
        rejectUnauthorized: false,
        headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${token}` },
        url: `${apimUrl}/IDTokenGenerator/V1/idTokens`,
        body: JSON.stringify({
          subject,
          tokenDuration
        })
      },
      function (err, response, body) {
        if (err) {
          reject({
            message: 'Error while fetching ID Token generation',
            err
          });
        } else if (response.statusCode >= 400) {
          reject({
            message: `Error ${response.statusCode} when generating ID Token`,
            additionalContext: [
              {
                type: 'body',
                data: body
              }
            ]
          });
        } else {
          try {
            const data = JSON.parse(body);
            if (data.token) {
              resolve(data.token);
            } else {
              reject({
                message: `Error while generating ID Token - empty response`
              });
            }
          } catch (err) {
            reject({
              message: `Error while parsing genreated ID Token`,
              err,
              additionalContext: [
                {
                  type: 'body',
                  data: body
                }
              ]
            });
          }
        }
      }
    );
  });
}

function getContext(apimUrl, token, data, correlationID) {
  return new Promise(function (resolve, reject) {
    log.debug(
      `Get context associated to token ${data.token} and tokenOwner ${data.tokenOwner} - CorrelationID ${correlationID}`
    );
    request.post(
      {
        rejectUnauthorized: false,
        headers: { Authorization: `Bearer ${token}`, 'Content-Type': 'application/json' },
        url: `${apimUrl}/BusinessDataTransfer/V1/businessDataTransferTokens/validate`,
        body: JSON.stringify({
          token: data.token,
          tokenOwner: data.tokenOwner
        })
      },
      function (err, response, body) {
        if (err) {
          reject({
            message: `Error while fetching business data context`,
            err
          });
        } else if (response.statusCode >= 400) {
          reject({
            message: `Error ${response.statusCode} while fetching business data context`,
            additionalContext: [
              {
                type: 'body',
                data: body
              }
            ]
          });
        } else {
          try {
            if (body) {
              resolve(JSON.parse(body));
            } else {
              reject({
                message: `No context associated to token ${data.token} for tokenOwner ${data.tokenOwner} - Empty response`
              });
            }
          } catch (err) {
            reject({
              message: "Can't parse context fetched",
              additionalContext: [
                {
                  type: 'body',
                  data: body
                }
              ]
            });
          }
        }
      }
    );
  });
}

/**
 * Gets the client credentials grant (<ACCESS_TOKEN>) in order to have access to the defined resources.
 *
 * @param apimUrl the APIM service URL
 * @param apimToken the APIM token to use to call URL
 *
 * @returns {Promise<unknown>} the <ACCESS_TOKEN> if there is no error, else reject an error
 */
function getClientCredentialsToken(apimUrl, apimToken, correlationID) {
  return new Promise((resolve, reject) => {
    try {
      log.debug(`Get client credentials token with apim token ${apimToken} - CorrelationID ${correlationID}`);
      apimHandler(apimUrl, apimToken).getClientCredentialsToken(
        true,
        (err, data) => {
          if (err) {
            reject({
              message: 'Unable to get client credentials token',
              err
            });
          } else {
            if (data.access_token) {
              resolve(data.access_token);
            } else {
              reject({
                message: `Unable to get client credentials token - No access_token in response`
              });
            }
          }
        },
        correlationID
      );
    } catch (err) {
      reject({
        message: 'Unable to get client credentials token',
        err
      });
    }
  });
}

/**
 * Generate a token for an anonymous user by calling APIM services.
 *
 * 1. Gets a back and front access_token
 * 2. From the back access_token, generate an idGeneratorToken
 *
 * @param apimUrl the APIM service URL
 * @param apimToken the APIM token to use to call URL
 *
 * @returns if there is no error returns an <ACCESS_TOKEN> and <ID_TOKEN>
 */
async function generateAnonymousToken(apimUrl, apimBackToken, apimFrontToken, correlationID) {
  try {
    log.debug(`Generate anonymous token - CorrelationID ${correlationID}`);
    // Step 1: Generate a back and front <ACCESS_TOKEN
    const [backAccessToken, frontAccessToken] = await Promise.all([
      getClientCredentialsToken(apimUrl, apimBackToken, correlationID),
      getClientCredentialsToken(apimUrl, apimFrontToken, correlationID)
    ]);

    // Step 2: From the back <ACCESS_TOKEN>, retrieve the <ID_GENERATOR_TOKEN>
    // Generate a random subject id for the anonymous user
    // The subject id is mandatory to call the validate token API (if empty, an error is returned)
    const subjectId = '';
    const idGeneratorToken = await getIdGeneratorToken(
      apimUrl,
      backAccessToken,
      subjectId,
      expiredIdTokenDuration,
      correlationID
    );

    return {
      accessToken: frontAccessToken,
      idToken: idGeneratorToken,
      refreshToken: null
    };
  } catch (err) {
    throw new Error({
      message: `Failed to generate anonymous token`,
      err
    });
  }
}

/**
 * Validate a token by calling apim service method.
 *
 * @param apimUrl the contacted APIM url (front or back)
 * @param apimToken the APIM token to use depending of the APIM url contacted
 * @param token the token that will be validated
 *
 * @returns {Promise<unknown>} if there is no error returns the <ACCESS_TOKEN>, <REFRESH_TOKEN>, <ID_TOKEN>
 * else reject the error
 */
function validateToken(apimUrl, apimToken, token, correlationID) {
  return new Promise((resolve, reject) => {
    log.debug(`Validate token ${token} - CorrelationID ${correlationID}`);
    try {
      apimHandler(apimUrl, apimToken).getJwtBearerToken(
        token,
        true,
        (err, responseData) => {
          if (err) {
            reject({
              message: 'Error while validating token'
            });
          } else {
            resolve({
              accessToken: responseData.access_token,
              idToken: responseData.id_token,
              refreshToken: responseData.refresh_token,
              expiresInSecond: responseData.expires_in
            });
          }
        },
        correlationID
      );
    } catch (err) {
      reject({
        message: 'Unable to validate token',
        err,
        additionalContext: [
          {
            type: 'token',
            data: token
          }
        ]
      });
    }
  });
}

/**
 * Refresh a token by calling apim service method.
 *
 * @param apimUrl the contacted APIM url (front or back)
 * @param apimToken the APIM token to use depending of the APIM url contacted
 * @param refreshToken the refresh token
 *
 * @returns {Promise<unknown>} if there is no error returns the <ACCESS_TOKEN>, <REFRESH_TOKEN>, <ID_TOKEN>
 * else reject the error
 */
function refreshToken(apimUrl, apimToken, refreshToken, correlationID) {
  return new Promise((resolve, reject) => {
    log.debug(`Refresh token with refreshToken ${refreshToken} - CorrelationID ${correlationID}`);
    try {
      return apimHandler(apimUrl, apimToken).refreshJwtBearerToken(
        refreshToken,
        (err, responseData) => {
          if (err) {
            reject({
              message: 'Error while refreshing token'
            });
          } else {
            resolve({
              accessToken: responseData.access_token,
              idToken: responseData.id_token,
              refreshToken: responseData.refresh_token,
              expiresInSecond: responseData.expires_in
            });
          }
        },
        correlationID
      );
    } catch (err) {
      reject({
        message: 'Unable to refresh token',
        err,
        additionalContext: [
          {
            type: 'refreshToken',
            data: refreshToken
          }
        ]
      });
      log.error(`Unable to refresh token - CorrelationID ${correlationID}`);
      reject(err);
    }
  });
}

module.exports = {
  auth,
  getContext,
  validateToken,
  refreshToken,
  generateAnonymousToken
};
