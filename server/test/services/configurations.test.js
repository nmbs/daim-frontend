const {
  mergeConfigurations,
  switchConfigurationIfNeeded,
  getOfferConfiguration,
  getPartnerConfiguration
} = require('../../services/configurations');
const { expect } = require('chai');
const sinon = require('sinon');
const request = require('request');
const config = require('config');

describe('Configuration service', () => {
  describe('switch configurations', () => {
    afterEach(() => {
      try {
        request.get.restore();
      } catch (err) {}
    });

    it('should return new X1 and config for amount lower than switch treshold', async () => {
      const currentOfferConfig = {
        screenOptions: {
          switchThreshold: {
            value: 50
          },
          lowerX1: {
            value: 'x1Lower'
          },
          upperX1: {
            value: 'x1Upper'
          }
        }
      };
      const fakeNewConfig = {
        sysType: 'newFakeConfig'
      };

      // I've failed to mock ConfigurationService.getOfferConfiguration therefore request.get has been stubbed
      sinon.stub(request, 'get').callsFake((params, callback) => {
        expect(params).to.have.property(
          'url',
          `${config.get('APIM_BACK_Url')}/applicationsPartnersConfiguration/v1/applications/${config.get(
            'APPLICATION_ID'
          )}/partners/partnerCode/routes/x1Lower`
        );
        callback(
          undefined,
          {
            statusCode: 200
          },
          JSON.stringify(fakeNewConfig)
        );
      });

      const result = await switchConfigurationIfNeeded(currentOfferConfig, '49', 'partnerCode', 'someAccessToken');
      expect(result).to.deep.equal({
        newX1: 'x1Lower',
        newOfferConfig: fakeNewConfig
      });
    });

    it('should return new X1 and config for amount equal to switch treshold', async () => {
      const currentOfferConfig = {
        screenOptions: {
          switchThreshold: {
            value: 50
          },
          lowerX1: {
            value: 'x1Lower'
          },
          upperX1: {
            value: 'x1Upper'
          }
        }
      };
      const fakeNewConfig = {
        sysType: 'newFakeConfig'
      };

      // I've failed to mock ConfigurationService.getOfferConfiguration therefore request.get has been stubbed
      sinon.stub(request, 'get').callsFake((params, callback) => {
        expect(params).to.have.property(
          'url',
          `${config.get('APIM_BACK_Url')}/applicationsPartnersConfiguration/v1/applications/${config.get(
            'APPLICATION_ID'
          )}/partners/partnerCode/routes/x1Lower`
        );
        callback(
          undefined,
          {
            statusCode: 200
          },
          JSON.stringify(fakeNewConfig)
        );
      });

      const result = await switchConfigurationIfNeeded(currentOfferConfig, '50', 'partnerCode', 'someAccessToken');
      expect(result).to.deep.equal({
        newX1: 'x1Lower',
        newOfferConfig: fakeNewConfig
      });
    });

    it('should return new X1 and config for amount higher than switch treshold', async () => {
      const currentOfferConfig = {
        screenOptions: {
          switchThreshold: {
            value: 50
          },
          lowerX1: {
            value: 'x1Lower'
          },
          upperX1: {
            value: 'x1Upper'
          }
        }
      };
      const fakeNewConfig = {
        sysType: 'newFakeConfig'
      };

      // I've failed to mock ConfigurationService.getOfferConfiguration therefore request.get has been stubbed
      sinon.stub(request, 'get').callsFake((params, callback) => {
        expect(params).to.have.property(
          'url',
          `${config.get('APIM_BACK_Url')}/applicationsPartnersConfiguration/v1/applications/${config.get(
            'APPLICATION_ID'
          )}/partners/partnerCode/routes/x1Upper`
        );
        callback(
          undefined,
          {
            statusCode: 200
          },
          JSON.stringify(fakeNewConfig)
        );
      });

      const result = await switchConfigurationIfNeeded(currentOfferConfig, '51', 'partnerCode', 'someAccessToken');
      expect(result).to.deep.equal({
        newX1: 'x1Upper',
        newOfferConfig: fakeNewConfig
      });
    });

    it('should return new X1 and config when no amount', async () => {
      const currentOfferConfig = {
        screenOptions: {
          switchThreshold: {
            value: 50
          },
          lowerX1: {
            value: 'x1Lower'
          },
          upperX1: {
            value: 'x1Upper'
          },
          noAmountX1: {
            value: 'x1NoAmount'
          }
        }
      };
      const fakeNewConfig = {
        sysType: 'newFakeConfig'
      };

      // I've failed to mock ConfigurationService.getOfferConfiguration therefore request.get has been stubbed
      sinon.stub(request, 'get').callsFake((params, callback) => {
        expect(params).to.have.property(
          'url',
          `${config.get('APIM_BACK_Url')}/applicationsPartnersConfiguration/v1/applications/${config.get(
            'APPLICATION_ID'
          )}/partners/partnerCode/routes/x1NoAmount`
        );
        callback(
          undefined,
          {
            statusCode: 200
          },
          JSON.stringify(fakeNewConfig)
        );
      });

      const result = await switchConfigurationIfNeeded(currentOfferConfig, '', 'partnerCode', 'someAccessToken');
      expect(result).to.deep.equal({
        newX1: 'x1NoAmount',
        newOfferConfig: fakeNewConfig
      });
    });

    it('should return null values if no switchThreshold is defined in config', async () => {
      const currentOfferConfig = {
        screenOptions: {}
      };
      const result = await switchConfigurationIfNeeded(currentOfferConfig, '51', 'partnerCode', 'someAccessToken');
      expect(result).to.deep.equal({
        newX1: null,
        newConfig: null
      });
    });
  });

  describe('merge configuration', () => {
    it('should return offer steps and merged partner/offer options', () => {
      const partnerConfiguration = {
        steps: ['stepPartner'],
        screenOptions: {
          option1: 'value1Partner',
          option2: 'value2Partner'
        }
      };
      const offerConfiguration = {
        steps: ['stepOffer'],
        screenOptions: {
          option1: 'value1Offer',
          option3: 'value3Offer'
        }
      };
      expect(mergeConfigurations(offerConfiguration, partnerConfiguration)).to.deep.equal({
        steps: ['stepOffer'],
        screenOptions: {
          option1: 'value1Offer',
          option2: 'value2Partner',
          option3: 'value3Offer'
        }
      });
    });

    it('should return partner steps', () => {
      const partnerConfiguration = {
        steps: ['stepPartner'],
        screenOptions: {
          option1: 'value1Partner'
        }
      };
      const offerConfiguration = {
        screenOptions: {
          option2: 'value2Offer'
        }
      };
      expect(mergeConfigurations(offerConfiguration, partnerConfiguration)).to.deep.equal({
        steps: ['stepPartner'],
        screenOptions: {
          option1: 'value1Partner',
          option2: 'value2Offer'
        }
      });
    });
  });

  describe('get configurations', () => {
    afterEach(() => {
      try {
        request.get.restore();
      } catch (err) {}
    });

    it('should return offer configuration', async () => {
      const fakeConfig = {
        sysType: 'someConfig'
      };
      sinon.stub(request, 'get').callsFake((params, callback) => {
        expect(params).to.have.property(
          'url',
          `${config.get('APIM_BACK_Url')}/applicationsPartnersConfiguration/v1/applications/${config.get(
            'APPLICATION_ID'
          )}/partners/partnerCode/routes/offerCode`
        );
        expect(params).to.have.deep.property('headers', { Authorization: 'Bearer someToken' });
        callback(
          undefined,
          {
            statusCode: 200
          },
          JSON.stringify(fakeConfig)
        );
      });
      expect(await getOfferConfiguration('partnerCode', 'offerCode', 'someToken', 'correlationID')).to.deep.equal(
        fakeConfig
      );
    });

    it('should return partner configuration', async () => {
      const fakeConfig = {
        sysType: 'someConfig'
      };
      sinon.stub(request, 'get').callsFake((params, callback) => {
        expect(params).to.have.property(
          'url',
          `${config.get('APIM_BACK_Url')}/applicationsPartnersConfiguration/v1/applications/${config.get(
            'APPLICATION_ID'
          )}/partners/partnerCode`
        );
        expect(params).to.have.deep.property('headers', { Authorization: 'Bearer someToken' });
        callback(
          undefined,
          {
            statusCode: 200
          },
          JSON.stringify(fakeConfig)
        );
      });
      expect(await getPartnerConfiguration('partnerCode', 'someToken', 'correlationID')).to.deep.equal(fakeConfig);
    });
  });
});
