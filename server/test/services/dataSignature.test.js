const { signData } = require('../../services/dataSignature');
const { expect } = require('chai');

describe('data signature', () => {
  it('should sign mandatory data', () => {
    const mandatoryData = {
      partnerCode: 'partner_1',
      offerCode: 'offer2',
      equipmentCode: '250',
      businessProviderCode: '987655456',
      scaleCode: 'N2443',
      amount: '998.34',
      orderAmount: '998.35',
      personalContributionAmount: '233.3',
      duration: '34',
      returnUrl: 'https://toto.com',
      loyaltyCardId: '98762T',
      orderId: '5678IY22'
    };
    const signature = signData(mandatoryData, 'thisIsMySalt');
    expect(signature).to.equal('4ec718da5bb9859a621e580bd71bba1d84f424207a6071000abc65baeb87e9b5');
  });

  it('should mandatory data when data is undefined', () => {
    const mandatoryData = {
      partnerCode: '',
      offerCode: '',
      equipmentCode: '',
      businessProviderCode: '',
      scaleCode: '',
      amount: '',
      orderAmount: '',
      personalContributionAmount: '',
      duration: '',
      returnUrl: '',
      loyaltyCardId: '',
      orderId: ''
    };
    const signature = signData(mandatoryData, 'thisIsMySalt');
    expect(signature).to.equal('5603890f81086a71e159068ea7d3548e657e258137740674b751850e278967da');
  });
});
