const routes = require('express').Router();
const tokenController = require('../controllers/tokenController');
const rootController = require('../controllers/rootController');
const contextController = require('../controllers/contextController');
const { v4: uuidv4 } = require('uuid');
const log4js = require('log4js');
const log = log4js.getLogger('app');

const apiLogger = (req, res, next) => {
  req.correlationID = uuidv4();
  log.debug(`${req.method} request to ${req.url} - CorrelationID ${req.correlationID}`);
  res.on('finish', () => {
    log.info(
      `${req.method} request to ${req.url} responded with ${res.statusCode} - CorrelationID ${req.correlationID}`
    );
  });
  next();
};

// Root controller
routes.get('/', apiLogger, rootController.loadIndex);

// Token controller
routes.post('/token/refresh', apiLogger, tokenController.refreshToken);
routes.post('/token/validate', apiLogger, tokenController.validateToken);

// Context controller
routes.post('/get-context', apiLogger, contextController.context);

module.exports = routes;
