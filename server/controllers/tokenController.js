const config = require('config');
const jwt = require('jwt-decode');
const constants = require('../constants/constants');
const apimService = require('../services/apim');
const cookieHelper = require('../helpers/cookieHelper');
const tokenHelper = require('../helpers/tokenHelper');
const responseHelper = require('../helpers/responseHelper');
const log = require('log4js').getLogger('tokenController');

const apimUrl = config.get('APIM_BACK_Url');
const apimFrontToken = config.get('APIM_FRONT_Token');
const apimBackToken = config.get('APIM_BACK_Token');
const expiredRefreshTokenDuration = config.get('EXPIRED_REFRESH_TOKEN_DURATION');

/**
 * Validate a token by calling APIM services.
 *
 * If the token is validated, a refreshToken is returned and stored into a server cookie.
 *
 * @param req the received request
 * @param res the returned response
 * @param next call next middleware
 */
async function validateToken(req, res, next) {
  responseHelper.setDefaultHeaders(res, req.correlationID);

  const { token } = req.body;

  if (!tokenHelper.isValidLength(token, req.correlationID)) {
    log.error(`JWT bearer token is undefined or has not the required length: ${token}`);
    return res.sendStatus(401);
  } else {
    try {
      const { accessToken, idToken, refreshToken } = await apimService.validateToken(
        apimUrl,
        apimFrontToken,
        token,
        req.correlationID
      );
      // Store the refreshToken in a cookie in order to retrieve it
      cookieHelper.setServerCookies(
        res,
        constants.COOKIES.REFRESH_TOKEN,
        refreshToken,
        expiredRefreshTokenDuration * 1000,
        req.correlationID
      );
      // Send the response with the accessToken, idToken and refreshToken
      res.json({ accessToken, idToken, refreshToken });
    } catch (err) {
      next(err);
    }
  }
}

/**
 * Refresh a token by calling APIM services.
 *
 * The token can be refresh for an authenticated user or an anonymous user.
 *
 * @param req the received request
 * @param res the returned response
 * @param next call next middleware
 */
async function refreshToken(req, res, next) {
  responseHelper.setDefaultHeaders(res, req.correlationID);

  // Retrieve refresh token cookie if it exists
  const { idToken, refreshToken } = req.body;
  try {
    const decodedJwt = jwt(idToken);
    let tokenInformations;
    if (decodedJwt && decodedJwt.sub && refreshToken) {
      tokenInformations = await apimService.refreshToken(apimUrl, apimFrontToken, refreshToken, req.correlationID);
      cookieHelper.setServerCookies(
        res,
        constants.COOKIES.REFRESH_TOKEN,
        tokenInformations.refreshToken,
        expiredRefreshTokenDuration * 1000,
        req.correlationID
      );
    } else {
      tokenInformations = await apimService.generateAnonymousToken(
        apimUrl,
        apimBackToken,
        apimFrontToken,
        req.correlationID
      );
    }
    return res.json({
      accessToken: tokenInformations.accessToken,
      idToken: tokenInformations.idToken,
      refreshToken: tokenInformations.refreshToken
    });
  } catch (err) {
    return next(err);
  }
}

module.exports = { validateToken, refreshToken };
