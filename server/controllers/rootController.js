const fs = require('fs');
const path = require('path');
const config = require('config');
const constants = require('../constants/constants');
const cookieHelper = require('../helpers/cookieHelper');
const log = require('log4js').getLogger('rootController');
const PROD = config.util.getEnv('NODE_ENV') !== 'development';
const ROOT_FOLDER_PATH = PROD ? '../dist' : '../../dist';
const apimService = require('../services/apim');
const { signData } = require('../services/dataSignature');
const {
  getPartnerConfiguration,
  getOfferConfiguration,
  switchConfigurationIfNeeded,
  mergeConfigurations
} = require('../services/configurations');

const defaultAppConf = Object.freeze({
  APIM_FRONT_Url: config.get('APIM_FRONT_Url'),
  API_Url: config.get('API_Url'),
  API_REF_URL: config.get('API_REF_URL'),
  API_SE_CHOICE: config.get('API_SE_CHOICE'),
  API_CRBP: config.get('API_CRBP'),
  APPLICATION_ID: config.get('APPLICATION_ID'),
  EULERIAN_Url: config.get('EULERIAN_Url'),
  UPLOAD_Url: config.get('UPLOAD_Url'),
  REFRESH_TOKEN_URL: config.get('REFRESH_TOKEN_URL'),
  VALIDATE_TOKEN_URL: config.get('VALIDATE_TOKEN_URL'),
  API_SOUSCRIPTION_VERSION: config.get('API_SOUSCRIPTION_VERSION'),
  RECAPTCHA_SITE_KEY: config.get('RECAPTCHA_SITE_KEY'),
  API_CONF_BASE_URL: config.get('API_CONF_BASE_URL')
});

async function loadIndex(req, res, next) {
  const apimUrl = config.get('APIM_BACK_Url');
  const apimFrontToken = config.get('APIM_FRONT_Token');
  const apimBackToken = config.get('APIM_BACK_Token');
  const expiredRefreshTokenDuration = config.get('EXPIRED_REFRESH_TOKEN_DURATION');

  try {
    const cookies = cookieHelper.getCookies(req);
    let q6;
    let x1;
    let s3;
    try {
      ({ q6, x1, s3 } = getAndStoreQueryParams(req, res, cookies));
    } catch (errParams) {
      log.error(errParams);
      return res.status(404).sendFile(path.join(__dirname, ROOT_FOLDER_PATH, '/404.html'));
    }

    let tokenInformations = null;
    // Enable JWT token for creditPartner app
    if (config.get('APPLICATION_ID') !== 'autoPartner') {
      const refreshToken = cookieHelper.getCookieByKey(cookies, constants.COOKIES.REFRESH_TOKEN, req.correlationID);
      if (req.query.t0) {
        try {
          tokenInformations = await apimService.validateToken(apimUrl, apimFrontToken, req.query.t0, req.correlationID);
        } catch (err) {
          // Do nothing -> an anonymous token will be generated
        }
      } else if (refreshToken) {
        try {
          tokenInformations = await apimService.refreshToken(apimUrl, apimFrontToken, refreshToken, req.correlationID);
        } catch (error) {
          cookieHelper.deleteServerCookie(res, constants.COOKIES.REFRESH_TOKEN, req.correlationID);
        }
      }
      if (tokenInformations === null) {
        tokenInformations = await apimService.generateAnonymousToken(
          apimUrl,
          apimBackToken,
          apimFrontToken,
          req.correlationID
        );
      }
    } else {
      const frontAccessToken = await apimService.auth(apimUrl, apimFrontToken, req.correlationID);
      tokenInformations = {
        accessToken: frontAccessToken,
        idToken: null,
        refreshToken: null
      };
    }

    if (tokenInformations && tokenInformations.refreshToken) {
      cookieHelper.setServerCookies(
        res,
        constants.COOKIES.REFRESH_TOKEN,
        tokenInformations.refreshToken,
        expiredRefreshTokenDuration * 1000,
        req.correlationID
      );
    }
    let commonConfiguration;
    let partnerConfig;
    let offerConfig;
    let gtm;
    let thirdPartyScript;
    let loaderMessage;
    try {
      try {
        partnerConfig = q6 ? await getPartnerConfiguration(q6, tokenInformations.accessToken, req.correlationID) : {};
        offerConfig = x1 ? await getOfferConfiguration(q6, x1, tokenInformations.accessToken, req.correlationID) : {};
      } catch (errConfig) {
        if (errConfig && errConfig.statusCode && errConfig.statusCode === 404) {
          log.error(errConfig);
          return res.status(404).sendFile(path.join(__dirname, ROOT_FOLDER_PATH, '/404.html'));
        } else {
          return next(errConfig);
        }
      }

      commonConfiguration = mergeConfigurations(partnerConfig, offerConfig);

      gtm =
        commonConfiguration &&
        commonConfiguration.screenOptions &&
        commonConfiguration.screenOptions.gtm &&
        commonConfiguration.screenOptions.gtm.value
          ? commonConfiguration.screenOptions.gtm.value
          : config.get('DEFAULT_GTM');

      loaderMessage =
        commonConfiguration &&
        commonConfiguration.screenOptions &&
        commonConfiguration.screenOptions.loaderMessage &&
        commonConfiguration.screenOptions.loaderMessage.value
          ? commonConfiguration.screenOptions.loaderMessage.value
          : config.get('LOADER_MESSAGE');

      thirdPartyScript =
        commonConfiguration &&
        commonConfiguration.screenOptions &&
        commonConfiguration.screenOptions.thirdPartyScript &&
        commonConfiguration.screenOptions.thirdPartyScript.value
          ? commonConfiguration.screenOptions.thirdPartyScript.value
          : null;
    } catch (err) {
      return next(err);
    }

    let dataExchange;
    if (req.query.token) {
      try {
        // Decode businessDataTransfertToken
        const backAccessToken = await apimService.auth(apimUrl, apimBackToken);
        const tokenOwner =
          commonConfiguration &&
          commonConfiguration.screenOptions &&
          commonConfiguration.screenOptions.tokenOwner &&
          commonConfiguration.screenOptions.tokenOwner.value;
        if (!tokenOwner) {
          throw new Error(`No token Owner found for partner ${q6}`);
        }
        const response = await apimService.getContext(apimUrl, backAccessToken, {
          token: req.query.token,
          tokenOwner: tokenOwner
        });
        dataExchange = JSON.parse(response.businessContext);
        if (dataExchange && dataExchange.offerContext && dataExchange.offerContext.amount && req) {
          cookieHelper.setServerCookies(res, 's3', dataExchange.offerContext.amount, req.correlationID);
          s3 = dataExchange.offerContext.amount / 100;
        }
      } catch (err) {
        log.error(err);
      }
    }

    // Perform configuration switch if needed
    var { newX1, newOfferConfig } = await switchConfigurationIfNeeded(
      offerConfig,
      s3,
      q6,
      tokenInformations.accessToken,
      req.correlationID
    );
    if (newX1 && newX1 !== x1) {
      x1 = newX1;
      offerConfig = newOfferConfig;
      cookieHelper.setServerCookies(res, 'x1', x1, req.correlationID);
    }

    const mandatoryData = {
      partnerCode: q6 || '',
      offerCode: x1 || req.query.x1 || req.query.X1 || '',
      equipmentCode: req.query.n2 || req.query.N2 || '',
      businessProviderCode: req.query.a9 || req.query.A9 || '',
      scaleCode: req.query.c5 || req.query.C5 || '',
      amount: s3 || '',
      duration: req.query.d2 || req.query.D2 || '',
      orderAmount: '',
      personalContributionAmount: '',
      returnUrl: '',
      loyaltyCardId: '',
      orderId: ''
    };
    const dataSignature = signMandatoryData(
      mandatoryData,
      dataExchange,
      config.get('DATA_SIGNATURE_SALT'),
      req.correlationID
    );

    const APP_CONFIG = {
      ACCESS_TOKEN: tokenInformations.accessToken,
      ID_TOKEN: tokenInformations.idToken,
      REFRESH_TOKEN: tokenInformations.refreshToken,
      data1: dataSignature,
      CORRELATION_ID: req.correlationID,
      ...defaultAppConf
    };

    return renderSubscription(
      res,
      APP_CONFIG,
      thirdPartyScript,
      gtm,
      dataExchange,
      partnerConfig,
      offerConfig,
      loaderMessage,
      q6
    );
  } catch (err) {
    return next(err);
  }
}

let baseHTML = null;
function getBaseHTML() {
  if (!baseHTML) {
    baseHTML = fs.readFileSync(path.join(__dirname, ROOT_FOLDER_PATH, '/index.html'), 'utf8');
  }
  return baseHTML;
}

function renderSubscription(
  res,
  subscriptionConfig,
  thirdPartyScript,
  gtm,
  dataTransfert,
  partnerConfig,
  offerConfig,
  loaderMessage,
  q6
) {
  let html = getBaseHTML();
  html = html.replace(new RegExp('/\\*GTM\\*/', 'g'), gtm);
  html = html.replace(new RegExp('/\\*LOADER_MESSAGE\\*/', 'g'), loaderMessage);
  if (thirdPartyScript) {
    html = html.replace('<head>', '<head>\n' + thirdPartyScript);
  }
  if (dataTransfert) {
    html = html.replace('<head>', `<head>\n<script>window.dataExchange = ${JSON.stringify(dataTransfert)}</script>`);
  }
  if (q6) {
    html = html.replace('<head>', `<head>\n<script>window.q6 = ${JSON.stringify({ partner: q6 })}</script>`);
  }
  // if (partnerConfig) {
  //   html = html.replace('<head>', `<head>\n<script>window.partnerConfig = ${JSON.stringify(partnerConfig)}</script>`);
  // }
  if (offerConfig) {
    html = html.replace('<head>', `<head>\n<script>window.offerConfig = ${JSON.stringify(offerConfig)}</script>`);
  }
  html = html.replace('<head>', `<head>\n<script>window.config = ${JSON.stringify(subscriptionConfig)};</script>`);
  // We force to disable cache for all queries that require auth
  res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.header('Pragma', 'no-cache');
  res.header('Expires', '0');
  return res.send(html);
}

// function addHeaders(res) {
//   res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
//   res.header('Pragma', 'no-cache');
//   res.header('Expires', 0);
// }

function signMandatoryData(mandatoryData, dataExchange, salt, correlationID) {
  if (dataExchange) {
    if (dataExchange.offerContext) {
      if (dataExchange.offerContext.amount) {
        // Override S3
        mandatoryData.amount = dataExchange.offerContext.amount / 100;
      }
      if (dataExchange.offerContext.equipmentCode) {
        // Override N2
        mandatoryData.equipmentCode = dataExchange.offerContext.equipmentCode;
      }
      if (dataExchange.offerContext.scaleId) {
        // Override C5
        mandatoryData.scaleCode = dataExchange.offerContext.scaleId;
      }
      if (dataExchange.offerContext.orderAmount) {
        mandatoryData.orderAmount = dataExchange.offerContext.orderAmount;
      }
      if (dataExchange.offerContext.personalContributionAmount) {
        mandatoryData.personalContributionAmount = dataExchange.offerContext.personalContributionAmount;
      }
      if (dataExchange.offerContext.duration) {
        // Override D2
        mandatoryData.duration = dataExchange.offerContext.duration;
      }
      if (dataExchange.offerContext.orderId) {
        mandatoryData.orderId = dataExchange.offerContext.orderId;
      }
    }
    if (dataExchange.providerContext) {
      if (dataExchange.providerContext.businessProviderId) {
        // Override A9
        mandatoryData.businessProviderCode = dataExchange.providerContext.businessProviderId;
      }
      if (dataExchange.providerContext.returnUrl) {
        mandatoryData.returnUrl = dataExchange.providerContext.returnUrl;
      }
    }
    if (dataExchange.customerContext) {
      if (dataExchange.customerContext.loyaltyCardId) {
        mandatoryData.loyaltyCardId = dataExchange.customerContext.loyaltyCardId;
      }
    }
  }
  return signData(mandatoryData, salt, correlationID);
}

/**
 * Get query params from request or cookies
 * If not set, set parameters in server cookie
 * @param {*} req
 * @param {*} cookies
 */
function getAndStoreQueryParams(req, res, cookies) {
  let q6 = req.query.q6 || req.query.Q6;
  let x1 = req.query.x1 || req.query.X1;
  let s3 = req.query.s3 || req.query.S3;

  if (q6) {
    cookieHelper.setServerCookies(res, 'q6', q6, req.correlationID);
  } else if (cookies['q6']) {
    q6 = cookies['q6'];
  } else {
    throw Error('No partner code provided');
  }

  if (x1) {
    cookieHelper.setServerCookies(res, 'x1', x1, req.correlationID);
  } else if (cookies['x1']) {
    x1 = cookies['x1'];
  } else {
    throw Error('No offer code provided');
  }

  if (s3) {
    cookieHelper.setServerCookies(res, 's3', s3, req.correlationID);
  } else if (cookies['s3']) {
    s3 = cookies['s3'];
  }

  return {
    q6,
    x1,
    s3
  };
}

module.exports = { loadIndex };
