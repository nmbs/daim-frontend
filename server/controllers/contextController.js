const config = require('config');
const log = require('log4js').getLogger('contextController');
const { auth, getContext } = require('../services/apim');

async function context(req, res, next) {
  const backToken = config.get('APIM_BACK_Token');
  const apimUrl = config.get('APIM_BACK_Url');
  const data = req.body;
  try {
    const accessToken = await auth(apimUrl, backToken, req.correlationID);
    const context = await getContext(apimUrl, accessToken, data, req.correlationID);
    return res.json(context);
  } catch (err) {
    return next(err);
  }
}

module.exports = { context };
