process.env['NODE_CONFIG_DIR'] = __dirname + '/config/';
const path = require('path');
const routes = require('./routes/');
const config = require('config');
const express = require('express');
const cors = require('cors');
const log4js = require('log4js');

const configLog4js = require('./config/log/log4js');
const errorHelper = require('./helpers/errorHelper');

log4js.configure(configLog4js);
const log = log4js.getLogger('app');

const app = express();

app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded({ extended: true })); // to support URL-encoded bodies
log.info('process.env.NODE_ENV',process.env.NODE_ENV);
if (process.env.NODE_ENV === 'development') {
  log.info('CORS enabled for http://localhost:8080');
  app.use(
    cors({
      origin: 'http://localhost:8080',
      optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
    })
  );
}

/* TODO : Variable */
const PORT = config.SERVER_PORT || 3000;

const PROD = config.util.getEnv('NODE_ENV') !== 'development';
const rootFolder = PROD ? './dist' : '/../dist';

app.use('/', routes);
app.use(express.static(path.join(__dirname, rootFolder)));

// 404 handling middleware
app.use(function (req, res) {
  log.error('Unknown route (404):', req.originalUrl);
  res.status(404).sendFile(path.join(__dirname, rootFolder, '/404.html'));
});

// Error middleware
app.use((err, req, res, next) => {
  if (err) {
    errorHelper.handleError(err, req, res, req.correlationID);
  } else {
    log.error('Error without information', err);
    res.status(500).send('Internal server error');
  }
});

app.listen(PORT, () => {
  log.info('Config loaded for ', process.env.NODE_ENV);
  log.info('express server listening at port:', PORT);
});
