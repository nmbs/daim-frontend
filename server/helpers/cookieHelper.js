const log = require('log4js').getLogger('cookieHelper');

function getCookies(req) {
  log.trace(`Get cookies - CorrelationID ${req.correlationID}`);
  const ret = {};
  if (typeof req.headers.cookie !== 'undefined') {
    try {
      let cookies = req.headers.cookie.split('; ');
      for (let x = 0; x < cookies.length; x++) {
        let cookie = cookies[x].split('=');
        ret[cookie[0]] = cookie[1];
      }
    } catch (err) {
      log.error(err);
    }
  }
  return ret;
}

function setServerCookies(res, cookieName, cookieValue, maxAge, correlationID) {
  log.trace(
    `Set server cookie ${cookieName} with value ${cookieValue} and maxAge ${maxAge} - CorrelationID ${correlationID}`
  );
  const options = {
    httpOnly: true,
    secure: true
  };
  if (Number.isInteger(maxAge)) {
    options.maxAge = maxAge;
  }
  res.cookie(cookieName, cookieValue, options);
}

function deleteServerCookie(res, cookieName, correlationID) {
  log.trace(`Delete server cookie ${cookieName} - CorrelationID ${correlationID}`);
  res.clearCookie(cookieName);
}

/**
 * Retrieve a cookie if it exists by its key.
 *
 * @param cookies the list of cookies contained in a request
 * @param key the cookie key
 * @returns {*} the cookie if it exists
 */
function getCookieByKey(cookies, key, correlationID) {
  log.trace(`Get cookie by key ${key} - CorrelationID ${correlationID}`);
  return cookies && cookies[key];
}

module.exports = { getCookies, setServerCookies, deleteServerCookie, getCookieByKey };
