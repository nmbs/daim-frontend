const log = require('log4js').getLogger('tokenHelper');

function isValidLength(token, correlationID) {
  log.trace(`Check if token ${token} is valid - CorrelationID ${correlationID}`);
  return token && token.length >= 10;
}

module.exports = { isValidLength };
