const log = require('log4js').getLogger('responseHelper');

function setDefaultHeaders(response, correlationID) {
  log.trace(`Set default headers in response - CorrelationID ${correlationID}`);
  response.setHeader('Content-Type', 'application/json');
}

module.exports = { setDefaultHeaders };
