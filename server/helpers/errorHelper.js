const log = require('log4js').getLogger('errorHelper');

/**
 * Manage and log error
 * @param {*} err object describing error
 * @param {*} req Request
 * @param {*} res Response
 * @param {string} correlationID request correlationID
 */
function handleError(err, req, res, correlationID) {
  if (err.message) {
    log.error(err.message, ' - correlationID', correlationID);
  }
  if (err.err) {
    log.error('Original error', err.err, ' - correlationID', correlationID);
  }
  if (err.additionalContext && err.additionalContext.length) {
    err.additionalContext.forEach((item) => {
      log.error('Additional context of type ', item.type, item.data, ' - correlationID', correlationID);
    });
  }
  if (req.method === 'POST' || req.method === 'PUT') {
    log.error('Body of original request', req.body);
  }
  if (!(err.message || err.err || err.additionalContext || err.userMessage)) {
    log.error('Error without information', err);
  }
  if (err.userMessage) {
    // TODO: see with hindsight if we should handle specific error code in this middleware too
    res.status(500).send(err.userMessage);
  } else {
    res.status(500).send('Internal server error');
  }
}

module.exports = {
  handleError
};
