module.exports = function (apimUrl, apimKey) {
  var apimService = require('./src/service/apimService')(apimUrl, apimKey);

  /**
   * getClientCredentialsToken
   * @param {*} isOpenIdScope
   * @param {*} callback
   */
  function getClientCredentialsToken(isOpenIdScope, callback, correlationID) {
    apimService.getClientCredentialsToken(isOpenIdScope, callback, correlationID);
  }

  /**
   * getJwtBearerToken
   * @param {*} idToken
   * @param {*} isOpenIdScope
   * @param {*} callback
   */
  function getJwtBearerToken(idToken, isOpenIdScope, callback, correlationID) {
    apimService.getJwtBearerToken(idToken, isOpenIdScope, callback, correlationID);
  }

  /**
   * refreshJwtBearerToken
   * @param {*} refreshToken
   * @param {*} callback
   */
  function refreshJwtBearerToken(refreshToken, callback, correlationID) {
    apimService.refreshJwtBearerToken(refreshToken, callback, correlationID);
  }

  /**
   * revokeToken
   * @param {*} token
   * @param {*} callback
   */
  function revokeToken(token, callback, correlationID) {
    apimService.revokeToken(token, callback, correlationID);
  }

  return {
    getClientCredentialsToken: getClientCredentialsToken,
    getJwtBearerToken: getJwtBearerToken,
    refreshJwtBearerToken: refreshJwtBearerToken,
    revokeToken: revokeToken
  };
};
