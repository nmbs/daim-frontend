module.exports = Object.freeze({
  TOKEN_PATH: '/token',
  REVOKE_PATH: '/revoke',
  BASE_KEY_PREFIX: 'Basic '
});
