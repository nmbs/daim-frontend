var request = require('request');
var config = require('../config/config.js');
var log = require('log4js').getLogger('apimClientService');

module.exports = function (apimUrl, apimKey) {
  /**
   * getClientCredentialsToken
   * @param {*} isOpenIdScope
   * @param {*} callback
   */
  function getClientCredentialsToken(isOpenIdScope, callback, correlationID) {
    let formData = { grant_type: 'client_credentials' };
    if (isOpenIdScope) {
      formData.scope = 'openid';
    }
    doPost(config.TOKEN_PATH, formData, callback, correlationID);
  }

  /**
   * getJwtBearerToken
   * @param {*} idToken
   * @param {*} isOpenIdScope
   * @param {*} callback
   */
  function getJwtBearerToken(idToken, isOpenIdScope, callback, correlationID) {
    let formData = { grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer', assertion: idToken };
    if (isOpenIdScope) {
      formData.scope = 'openid';
    }
    doPost(config.TOKEN_PATH, formData, callback, correlationID);
  }

  /**
   * refreshJwtBearerToken
   * @param {*} refreshToken
   * @param {*} callback
   */
  function refreshJwtBearerToken(refreshToken, callback, correlationID) {
    let formData = { grant_type: 'refresh_token', refresh_token: refreshToken };
    doPost(config.TOKEN_PATH, formData, callback, correlationID);
  }

  /**
   * revokeToken
   * @param {*} token
   * @param {*} callback
   */
  function revokeToken(token, callback, correlationID) {
    let formData = { token };
    doPost(config.REVOKE_PATH, formData, callback, correlationID);
  }

  function doPost(pathAction, formData, callback, correlationID) {
    request.post(
      {
        rejectUnauthorized: false,
        url: apimUrl + pathAction,
        form: formData,
        timeout: 3000,
        headers: {
          Authorization: config.BASE_KEY_PREFIX + apimKey
        }
      },
      function (error, response, body) {
        try {
          if (error) {
            log.warn(
              `Error while calling ${pathAction} - FormData ${JSON.stringify(
                formData
              )} - Body ${body} - CorrelationID ${correlationID}`
            );
            return callback(error, null);
          } else if (response && response.statusCode >= 400) {
            log.warn(
              `Error ${response.statusCode}: while calling ${pathAction} - FormData ${JSON.stringify(
                formData
              )} - Body ${body} - CorrelationID ${correlationID}`
            );
            return callback(`Error ${response.statusCode} while calling ${pathAction}`, null);
          } else if (body) {
            return callback(null, JSON.parse(body));
          } else {
            log.error(
              `Error ${response.statusCode}: while calling ${pathAction} - FormData ${JSON.stringify(
                formData
              )} - Body ${body} - CorrelationID ${correlationID}`
            );
            callback(`Error while calling ${pathAction} : empty response`, null);
          }
        } catch (err) {
          callback(
            `Unknown error when processing result of doPost on ${pathAction}- FormData ${JSON.stringify(
              formData
            )} - Body ${body} - CorrelationID ${correlationID}`,
            null
          );
        }
      }
    );
  }

  return {
    getClientCredentialsToken: getClientCredentialsToken,
    getJwtBearerToken: getJwtBearerToken,
    refreshJwtBearerToken: refreshJwtBearerToken,
    revokeToken: revokeToken
  };
};
