var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai

describe('apimService', function () {
  it('getClientCredentialsToken() should return false in value of isSuccess if if failed to get token', function () {
    const apimUrl = 'https://rct-gw-intranet-api.ca-cf.gca/HAPPY_DEMO/V1/persons/Correlationid';
    const apimKey = 'toto';
    var apimService = require('../../src/service/apimService')(apimUrl, apimKey);
    apimService.getClientCredentialsToken(true, (isOk, data) => {
      expect(isOk).to.equal(false);
    });
  });
});
