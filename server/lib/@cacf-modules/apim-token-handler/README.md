# README

## Install dependencies

`npm install`

## Publish to nexus

`npm publish`

## Include the module in your project

`npm i @cacf-modules/apim-token-handler`

## How to use this module

### Init function:

`var apimHandler = require("@cacf-modules/apim-token-handler")(apimUrl,apimKey);`
`// apimUrl: Base URL of APIM`
`// apimKey: APIM Key`

### Get Client Credentials Token

`apimHandler.getClientCredentialsToken(isOpenIdScope, (err, data) => {`
`// isOpenIdScope: Boolean Param for isOpenIdScope: 'true' if you want to get id-token`
`// err: contains error`
`// data: data returned`
`})`

### Get JWT Bearer Token

`apimHandler.getJwtBearerToken(idToken, isOpenIdScope, (err, data) => {`
`// idToken: Param for id-token`
`// isOpenIdScope: Boolean Param for isOpenIdScope`
`// err: contains error`
`// data: data returned`
`})`

### Refresh JWT Bearer Token

`apimHandler.refreshJwtBearerToken(refreshToken, (err, data) => {`
`// refreshToken: String Param for refreshToken`
`// err: contains error`
`// data: data returned`
`})`

### Revoke Token

`apimHandler.revokeToken(token, (err, data) => {`
`// token: String Param for token`
`// err: contains error`
`// data: data returned`
`})`
