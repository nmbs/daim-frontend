# daim-frontend

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

You should create a .env.local file defining all environnment variables needed (list of variables needed in .env)
**Do not define variables in .env file !**

Exemple of .env.local file

```
VUE_APP_APIM_BASE_URL = https://proxy.portail.sofinco.dev.cacd2.io/rct-gw-intranet-api
VUE_APP_UPLOAD_URL = https://daim-upload-ci.daim.sofinco.dev.cacd2.io
VUE_APP_API_BASE_URL = https://rct-api.sofinco.fr
VUE_APP_API_BASE_REF_URL = https://rct-api-ref.sofinco.fr
VUE_APP_ACCESS_TOKEN = 7129994b-ad8c-3a7d-b7af-f0104b149aa5
VUE_APP_ID_TOKEN = eyJhbGciOiJSUzI1NiIsImN0eSI6IkpXVCIsIng1dCI6IlFrTXdRMFF5TjBRelFrRXdSa1U1UlRGQ1JqUXlRakEzUkRFM01FVXpOVGs1UkRGQ1JERXhSQT09In0.eyJzdWIiOiIiLCJhdWQiOlsiaHR0cHM6XC9cL3JjdC1hdXRoZW50LWFwaS5jYS1jZi5nY2FcL29hdXRoMlwvdG9rZW4iLCJDTElFTlRfUEFSVE5FUlNfV0VCX1NFUlZFUiJdLCJpc3MiOiJJTlQtSUQtVE9LRU4tR0VORVJBVE9SLUFQSSIsImV4cCI6MTYwMTYzNDU0NCwianRpIjoiNjAwMDAxMzNhY2Q5YS01ZDQyLTM2OGUtOWVkYy02ZGY0MDU2OTQ5MjhjNWQ2NWEwMS1mZmI4LTQwYmItYWQyNy1lMmU3ZTE0YzZkNTEiLCJhcHBsaWNhdGlvbm5hbWUiOiJDTElFTlRfUEFSVE5FUlNfV0VCX1NFUlZFUiJ9.QmCIVTYR-kxhIwhvk59Wv7dye4godW0dxhdWtfqFeUSy3hzkj_LFzH7x63iUbETIfuOCKN5GGYSLNVZx2yLYdKHvl0dErSGAzwqZEoo_ObhOTgAQdBKuGiyGq3MT3yD0YDtqL2Sf6BjxjRPSFwvCT-Ty4d9aZu6pcLr7gOWXO6HPtdH4N3teYzKUHBEz03MZRN9mEDBs6_gsNuC35MxMvWsJJUGZYUvnNcLEmW_sZbRRz5bdE_MehVTzlycHWBKIGcjxOnUPf5Gy9A1qVYOecChJBqHDihNVehEFwCA8yHIjPmVkfrRmHo9bCC0XyfaBD0D3zkBG6isPz4nlvXK7Sw
VUE_APP_REFRESH_TOKEN_URL = http://localhost:3000/token/refresh
VUE_APP_VALIDATE_TOKEN_URL = http://localhost:3000/token/validate
VUE_APP_RECAPTCHA_SITE_KEY=6LfbgP8UAAAAALIptLhz11l65o6Uq-DyiUZbQORx
VUE_APP_API_SE_CHOICE=https://portail-jahia-ci.portail.sofinco.dev.cacd2.io/sites/sofinco/home.seChoice.do
VUE_APP_SALT=f90764c9-5930-4567-b4d2-d72f71196519
VUE_APP_API_SOUSCRIPTION_VERSION=v1.2
VUE_APP_API_CONF_BASE_URL=https://rct-api.sofinco.fr
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Run your unit tests

```
npm run test:unit
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Config your local project to pull Package from CACD2 Nexus

In order to be able to pull packages from the CACD2 Nexus:

Create a `.npmrc` file in the root folder

```
registry=https://nexus.factory.prod.cacd2.io/repository/js-group/
email=
always-auth=true
_auth=
```

Put your email after `email=`

Execute `echo -n '${NEXUS_USERNAME}:${NEXUS_PASSWORD}' | base64` in a terminal
`${NEXUS_USERNAME}` and `${NEXUS_PASSWORD}` should be your Nexus IDs.

Paste the result after `_auth=`

## Troubleshooting Husky hook not executed

If you use a GUI and/or nvm you can experience issue with husky hooks not executed when commiting.
Please check your git GUI settings to check if an option need to be enabled (case for IntelliJ).

Otherwise try defining nvm path in file ~/.huskyrc with following content:

```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
```

More info available at https://confluence.cacd2.io/display/CACF/Git+hooks+via+Husky
