export const MockSimulationParameter = {
  amount: 900,
  duration: 18,
  businessProviderId: 'businessProviderId',
  birthdate: '20/02/1985',
  sourceId: 'sourceId',
  partnerId: 'partnerId',
  applicationId: 'applicationId',
  equipmentCode: '280',
  scaleCode: 'ABS'
};
