import { RouteConfig } from 'vue-router';

export const offerConfigRoutesOut: RouteConfig[] = [
  { path: '/', redirect: '/subscription' },
  {
    name: 'subscription',
    path: '/subscription',
    props: { stepState: null },
    children: [
      { name: 'informationBloc', path: '/subscription/informationBloc', props: { stepState: null }, children: [] },
      { name: 'situationBloc', path: '/subscription/situationBloc', props: { stepState: null }, children: [] }
    ],
    redirect: { name: 'informationBloc' }
  }
];
