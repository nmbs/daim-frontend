import { AppConfiguration } from '@/models/Configuration';

export const offerConfigIn: AppConfiguration = {
  screenOptions: {},
  steps: [
    {
      pathId: 'subscription',
      screenOptions: {},
      noReturn: false,
      routed: true,
      steps: [
        {
          pathId: 'informationBloc',
          screenOptions: {},
          noReturn: false,
          routed: true,
          steps: []
        },
        {
          pathId: 'situationBloc',
          screenOptions: {},
          noReturn: false,
          routed: true,
          steps: []
        }
      ]
    }
  ],
  _links: {
    self:
      'https://rct-api.sofinco.fr/applicationsPartnersConfiguration/v1/applications/creditPartner/partners/web_ikea/routes/crs',
    parameters:
      'https://rct-api.sofinco.fr/applicationsPartnersConfiguration/v1/applications/creditPartner/partners/web_ikea/routes/crs/parameters/'
  }
};
