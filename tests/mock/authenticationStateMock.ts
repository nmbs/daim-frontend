import { AuthenticationState } from '@/models/subscription/authentication/Authentication';

export const MockAuthentication: AuthenticationState = {
  request: {
    id: null,
    birthDate: null,
    keypadId: null,
    passwordMask: null
  },
  response: {
    customerId: '92807343185',
    role: 'AUTHENTICATED_CUSTOMER',
    token:
      'eyJhbGciOiJSUzI1NiIsImN0eSI6IkpXVCIsIng1dCI6Ik9EVTNSakEzTTBKR05VWTRNREV3UVVRd01rTTNNRGczT0RVNFFVUkVOa016UlRWRU9UVkRNZz09In0.eyJzdWIiOiI5MjgwNzM0MzE4NSIsImF1ZCI6WyJodHRwczpcL1wvcmN0LWF1dGhlbnQtYXBpLmNhLWNmLmdjYVwvb2F1dGgyXC90b2tlbiIsIkNMSUVOVF9QUk9DRVNTX1VQTE9BRF9TRV9TRVJWRVIiXSwicm9sZSI6IkFVVEhFTlRJQ0FURURfQ1VTVE9NRVIiLCJpc3MiOiJSQ1QtSUQtVE9LRU4tR0VORVJBVE9SLUFQSSIsInRva2VuRm9yQXBpbSI6dHJ1ZSwiZXhwIjoxNjAwMjY0ODY1LCJqdGkiOiI2MDAwMGU1MjE0NmRhLWZlOTYtMzk0OS05YTczLWQ5NTBmYzEyNDA1OTAxN2IzMDcxLWFkOTQtNDViYS04NTRkLTQ0NGM3ZjY1N2U4NiIsImFwcGxpY2F0aW9ubmFtZSI6IkNMSUVOVF9QUk9DRVNTX1VQTE9BRF9TRV9TRVJWRVIifQ.msdQZSbDk5gwTQY5hCO-ye8Foe502npwTzCf2seanGxd77CaWyVfE9Knac3m7RSnnv9v1aaKNLuOSyP18u2l7t1y4foQdu2Vsk0pbmecJ4Qt9-OD7fhJD7PhuLtjFI2Eu73UvhmxL0UKB38Pt7JApsC_ih4DlmfhrWSkYCDbaKJZQVi5FJtNutgWr7P2NVb-WT3tNwl56KO8m9L1CSyfcn4WVaByYXbS1kiyERe2uUZcXy3Q4MnHOitiJJR38WkXXhxWBVLWIrQjRc9UN0LIMECfJJwj_CuT_miy9Dq3yMni6V4NA8YpXh-SiT_E7luOPZnW55ZYDQceAX6xt1EHlQ'
  },
  clientIsLogging: false,
  identification: {
    form: {
      lastName: null,
      firstName: null,
      birthDate: null,
      birthPostCode: null
    },
    choice: {
      id: null,
      token: null,
      encodedMobileNumber: null,
      encodedEmailAddress: null,
      postCode: null,
      sendBy: null
    }
  }
};
