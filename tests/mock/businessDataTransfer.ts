export const MockIProviderContext = {
  businessProviderId: 'businessProviderIdOther',
  returnUrl: 'test.com'
};

export const MockICustomerContext1 = {
  externalCustomerId: 'externalCustomerId1',
  civilityCode: 0,
  firstName: 'firstName',
  lastName: 'lastName',
  birthName: 'birthName',
  birthDate: 'birthDate',
  citizenshipCode: 'citizenshipCode',
  birthCountryCode: 'birthCountryCode',
  street: 'street',
  additionalStreet: 'additionalStreet',
  city: 'city',
  zipCode: 0,
  distributerOffice: 'distributerOffice',
  countryCode: 'countryCode',
  phone0: 'phone0',
  mobile0: 'mobile0',
  emailAddress: 'emailAddress',
  loyaltyCardId: 'loyaltyCardId'
};

export const MockICustomerContext2 = {
  externalCustomerId: 'externalCustomerId2',
  civilityCode: 0,
  firstName: 'firstName',
  lastName: 'lastName',
  birthName: 'birthName',
  birthDate: 'birthDate',
  citizenshipCode: 'citizenshipCode',
  birthCountryCode: 'birthCountryCode',
  street: 'street',
  additionalStreet: 'additionalStreet',
  city: 'city',
  zipCode: 0,
  distributerOffice: 'distributerOffice',
  countryCode: 'countryCode',
  phone0: 'phone0',
  mobile0: 'mobile0',
  emailAddress: 'emailAddress',
  loyaltyCardId: 'loyaltyCardId'
};

export const IOfferContext = {
  orderId: 'orderId',
  scaleId: 'scaleId',
  equipmentCode: 'equipmentCode',
  amount: 0,
  orderAmount: 0,
  personalContributionAmount: 0,
  duration: 0,
  preScoringCode: '1'
};

export const MockBusinessDataTransfer = {
  providerContext: MockIProviderContext,
  customerContext: MockICustomerContext1,
  coBorrowerContext: MockICustomerContext2,
  offerContext: IOfferContext
};
