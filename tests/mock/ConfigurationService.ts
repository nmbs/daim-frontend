import { AppConfiguration } from '@/models/Configuration';
import { Step } from '@/models/Step';
import { RouteConfig } from 'vue-router';
import { StepState } from '@/models/StepState';

/**
 * Computes RouteConfig (that will be used to initialize the router) from an AppConfiguration.
 * @param appConfiguration Configuration coming from the API.
 */
export function getRoutesConfig(appConfiguration: AppConfiguration): RouteConfig[] {
  // for each main steps, compute its associated routes and children routes
  const routesConfig: RouteConfig[] = [];
  appConfiguration.steps.forEach((step: any) => {
    routesConfig.push(computeStepRoutes(step, '', null));
  });

  // Add redirection for '/' route: redirect to the first routed step
  routesConfig.unshift({
    path: '/',
    redirect: routesConfig[0].path
  });

  return routesConfig;
}

/**
 * Build a RouteConfig object from a given current step
 * @param step The current step
 * @param parentPath The path of the parent step route
 * @param parentStep The parent step of the current step
 */
function computeStepRoutes(step: Step, parentPath: string, parentStep: Step | null): RouteConfig {
  const routeConfig: RouteConfig = {
    name: step.pathId,
    path: `${parentPath}/${step.pathId}`,
    component: getComponentOfStep(),
    children: []
  };

  const steps = step.steps;
  if (steps) {
    steps.forEach((childStep: Step) => {
      if (childStep.routed) {
        // @ts-ignore children cannot be null at this point as it was initialized earlier
        routeConfig.children.push(computeStepRoutes(childStep, routeConfig.path, step));
      }
    });
  }

  // @ts-ignore children cannot be null at this point as it was initialized earlier
  if (routeConfig.children.length > 0) {
    // @ts-ignore
    routeConfig.redirect = { name: routeConfig.children[0].name };
  }

  return routeConfig;
}

/**
 * Initialize StepStates structure.
 */
export function initStepsStates(appConfiguration: AppConfiguration): StepState {
  const configurationRoutes: StepState[] = [];

  const rootSteps = new StepState('', [], null, null, true);
  appConfiguration.steps.forEach((step: Step) => {
    configurationRoutes.push(computeStepState(step, rootSteps));
  });
  rootSteps.steps = configurationRoutes;
  return rootSteps;
}

/**
 * Builds a StepState from a Step.
 * @param step The current Step.
 * @param parent The StepState of the parent step.
 */
function computeStepState(step: Step, parent: StepState): StepState {
  const childrenStates: StepState[] = [];
  const stepState = new StepState(step.pathId, [], step.screenOptions, parent, step.routed, step.noReturn);

  if (step.steps) {
    step.steps.forEach((child: Step) => {
      childrenStates.push(computeStepState(child, stepState));
    });
    stepState.steps = childrenStates;
  }
  return stepState;
}

function getComponentOfStep(): any {
  return null;
}
