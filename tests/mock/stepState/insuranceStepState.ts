import { StepState } from '@/models/StepState';

const insuranceStepState1: StepState = {
  noReturn: false,
  parent: undefined,
  pathId: '',
  routed: false,
  screenOptions: {
    'limit-senior': { value: '', description: 'limit senior for insurance' },
    'limit-young': { value: '', description: 'limit young for insurance' },
    'max-age': { value: '', description: 'max age for insurance' },
    'min-age': { value: '', description: 'min age for insurance' },
    partnerName: {
      value: 'sofinco',
      description: 'Used to apply specific business rules depending on the partner name'
    },
    documentUrl: { value: '', description: 'insurance informations document' },
    label: { value: 'souscription.menuNavigation.votreAssurance', description: 'label key in Jahia' },
    label2: { value: 'souscription.carte.boutonSuivant', description: 'label key in Jahia' }
  },
  steps: [],
  find(pathId: string): StepState | null {
    return undefined;
  },
  getNext(getFirstChild?: boolean): StepState | null {
    return undefined;
  },
  getPrevious(getLastChild?: boolean): StepState | null {
    return undefined;
  }
};

const insuranceStepState2: StepState = {
  noReturn: false,
  parent: undefined,
  pathId: '',
  routed: false,
  screenOptions: {
    partnerName: {
      value: 'sofinco',
      description: 'Used to apply specific business rules depending on the partner name'
    },
    documentUrl: { value: '', description: 'insurance informations document' },
    label: { value: 'souscription.menuNavigation.votreAssurance', description: 'label key in Jahia' },
    label2: { value: 'souscription.carte.boutonSuivant', description: 'label key in Jahia' }
  },
  steps: [],
  find(pathId: string): StepState | null {
    return undefined;
  },
  getNext(getFirstChild?: boolean): StepState | null {
    return undefined;
  },
  getPrevious(getLastChild?: boolean): StepState | null {
    return undefined;
  }
};

const insuranceStepState3: StepState = {
  noReturn: false,
  parent: undefined,
  pathId: '',
  routed: false,
  screenOptions: null,
  steps: [],
  find(pathId: string): StepState | null {
    return undefined;
  },
  getNext(getFirstChild?: boolean): StepState | null {
    return undefined;
  },
  getPrevious(getLastChild?: boolean): StepState | null {
    return undefined;
  }
};

const insuranceStepState4: StepState = {
  noReturn: false,
  parent: undefined,
  pathId: '',
  routed: false,
  screenOptions: {
    'limit-senior': { value: '74', description: 'limit senior for insurance' },
    'limit-young': { value: '59', description: 'limit young for insurance' },
    'max-age': { value: '80', description: 'max age for insurance' },
    'min-age': { value: '18', description: 'min age for insurance' },
    partnerName: {
      value: 'sofinco',
      description: 'Used to apply specific business rules depending on the partner name'
    },
    documentUrl: { value: '', description: 'insurance informations document' },
    label: { value: 'souscription.menuNavigation.votreAssurance', description: 'label key in Jahia' },
    label2: { value: 'souscription.carte.boutonSuivant', description: 'label key in Jahia' }
  },
  steps: [],
  find(pathId: string): StepState | null {
    return undefined;
  },
  getNext(getFirstChild?: boolean): StepState | null {
    return undefined;
  },
  getPrevious(getLastChild?: boolean): StepState | null {
    return undefined;
  }
};

const insuranceStepState5: StepState = {
  noReturn: false,
  parent: undefined,
  pathId: '',
  routed: false,
  screenOptions: {
    'limit-senior': { value: '0A', description: 'limit senior for insurance' },
    'limit-young': { value: null, description: 'limit young for insurance' },
    'max-age': { value: '80', description: 'max age for insurance' },
    'min-age': { value: '', description: 'min age for insurance' },
    partnerName: {
      value: 'sofinco',
      description: 'Used to apply specific business rules depending on the partner name'
    },
    documentUrl: { value: '', description: 'insurance informations document' },
    label: { value: 'souscription.menuNavigation.votreAssurance', description: 'label key in Jahia' },
    label2: { value: 'souscription.carte.boutonSuivant', description: 'label key in Jahia' }
  },
  steps: [],
  find(pathId: string): StepState | null {
    return undefined;
  },
  getNext(getFirstChild?: boolean): StepState | null {
    return undefined;
  },
  getPrevious(getLastChild?: boolean): StepState | null {
    return undefined;
  }
};

export { insuranceStepState1, insuranceStepState2, insuranceStepState3, insuranceStepState4, insuranceStepState5 };
