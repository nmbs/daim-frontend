export const stateMock = {
  partnerCode: 'web_ikea',
  offerCode: 'crs',
  applicationId: 'creditPartner',
  appConfiguration: {
    screenOptions: {
      'primary-color': {
        value: 'personnalisation.couleurs.primaire',
        description: 'primary color'
      },
      'messages-partner': {
        value: 'https://recette.esigate.sofinco.fr/sites/partners/home.labelsAction.do?path=lb&partner=web-ikea',
        description: 'partner messages link'
      },
      'quaternary-color': {
        value: 'personnalisation.couleurs.quaternaire',
        description: 'quaternary color'
      },
      site: {
        value: 'credit-ikea-preprod',
        description: 'Eulerian tag for rtgsite'
      },
      'secondary-color': {
        value: 'personnalisation.couleurs.secondaire',
        description: 'secondary color'
      },
      'resources-path': {
        value: 'https://recette.esigate.sofinco.fr/files/live/sites/partners/files/web_ikea/resources',
        description: 'resources path to use for icons, logos,...'
      },
      tokenOwner: {
        value: 'CLIENT_PARTNERS_WEB_SERVER',
        description: 'tokenOwner to use with BusinessDataTransfer'
      },
      'tertiary-color': {
        value: 'personnalisation.couleurs.tertiaire',
        description: 'tertiary color'
      },
      'messages-default': {
        value: 'https://recette.esigate.sofinco.fr/sites/partners/home.labelsAction.do?path=lb&partner=default',
        description: 'default messages link'
      },
      'quintinary-color': {
        value: 'personnalisation.couleurs.quinquenaire',
        description: 'quintinary color'
      },
      'duration-simulation': {
        value: '20',
        description: 'example duration simulation'
      },
      'legal-notices': {
        value:
          'https://recette.esigate.sofinco.fr/sites/partners/home.labelsAction.do?path=ln&partner=web-ikea&route=crs',
        description: 'legal notices link'
      },
      'amount-simulation': {
        value: '1000',
        description: 'example amount simulation'
      }
    },
    steps: [
      {
        pathId: 'creditFromScratch',
        screenOptions: {},
        noReturn: false,
        routed: true,
        steps: [
          {
            pathId: 'pedagogicSteps',
            screenOptions: {
              'pedagogic-steps-key': {
                value: 'pedagogicSteps',
                description: 'key of pedagogic steps'
              },
              'pedagogic-steps-length': {
                value: 3,
                description: 'length of pedagogic steps description'
              }
            },
            noReturn: false,
            routed: false,
            steps: []
          },
          {
            pathId: 'attachments',
            screenOptions: {
              'attachments-length': {
                value: 3,
                description: 'length of attachments'
              },
              'attachments-key': {
                value: 'attachments.documents',
                description: 'key of attachments'
              }
            },
            noReturn: false,
            routed: false,
            steps: []
          },
          {
            pathId: 'downloadPrecontract',
            screenOptions: {},
            noReturn: false,
            routed: false,
            steps: []
          }
        ]
      },
      {
        pathId: 'subscription',
        screenOptions: {
          coBorrowerDisplayed: {
            value: false,
            description: 'whether the user can see the co-borrower'
          }
        },
        noReturn: false,
        routed: true,
        steps: [
          {
            pathId: 'informationBloc',
            screenOptions: {
              label: {
                value: 'souscription.menuNavigation.blocInformation',
                description: 'label key in Jahia'
              },
              type: {
                value: 'category',
                description: ''
              }
            },
            noReturn: false,
            routed: true,
            steps: [
              {
                pathId: 'name',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.votreNom',
                    description: 'label key in Jahia'
                  },
                  type: {
                    value: 'form',
                    description: ''
                  }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'contact',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.vosCoordonnes',
                    description: 'label key in Jahia'
                  },
                  type: {
                    value: 'form',
                    description: ''
                  }
                },
                noReturn: false,
                routed: true,
                steps: []
              }
            ]
          },
          {
            pathId: 'situationBloc',
            screenOptions: {
              label: {
                value: 'souscription.menuNavigation.blocSituation',
                description: 'label key in Jahia'
              },
              type: {
                value: 'category',
                description: ''
              }
            },
            noReturn: false,
            routed: true,
            steps: [
              {
                pathId: 'civility',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.votreCivilite',
                    description: 'label key in Jahia'
                  },
                  type: {
                    value: 'form',
                    description: ''
                  }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'family',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.situationFamiliale',
                    description: 'label key in Jahia'
                  },
                  type: {
                    value: 'form',
                    description: ''
                  }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'housing',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.votreAdresse',
                    description: 'label key in Jahia'
                  },
                  type: {
                    value: 'form',
                    description: ''
                  }
                },
                noReturn: false,
                routed: true,
                steps: []
              }
            ]
          },
          {
            pathId: 'incomesBloc',
            screenOptions: {
              label: {
                value: 'souscription.menuNavigation.votreBudget',
                description: 'label key in Jahia'
              },
              type: {
                value: 'category',
                description: ''
              }
            },
            noReturn: false,
            routed: true,
            steps: [
              {
                pathId: 'employment',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.votreTravail',
                    description: 'label key in Jahia'
                  },
                  type: {
                    value: 'form',
                    description: ''
                  }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'incomes',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.vosRevenus',
                    description: 'label key in Jahia'
                  },
                  type: {
                    value: 'form',
                    description: ''
                  }
                },
                noReturn: false,
                routed: true,
                steps: []
              },
              {
                pathId: 'charges',
                screenOptions: {
                  label: {
                    value: 'souscription.menuNavigation.vosCharges',
                    description: 'label key in Jahia'
                  },
                  type: {
                    value: 'form',
                    description: ''
                  }
                },
                noReturn: false,
                routed: true,
                steps: []
              }
            ]
          },
          {
            pathId: 'card',
            screenOptions: {
              preselected: {
                value: true,
                description: 'preselected card'
              },
              'card-code': {
                value: 'E',
                description: 'card code'
              },
              documentUrl: {
                value:
                  'https://recette.esigate.sofinco.fr/sites/partners/home.documentsAction.do?q6=web_ikea&p=/cartes/informations-carte.pdf',
                description: 'card informations document'
              },
              label: {
                value: 'souscription.menuNavigation.votreCarte',
                description: 'label key in Jahia'
              }
            },
            noReturn: false,
            routed: true,
            steps: [
              {
                pathId: 'advantages',
                screenOptions: {
                  'advantages-key': {
                    value: 'souscription.carte.avantages',
                    description: 'key of card advantages'
                  },
                  'advantages-length': {
                    value: 6,
                    description: 'length of card advantages'
                  }
                },
                noReturn: false,
                routed: false,
                steps: []
              }
            ]
          },
          {
            pathId: 'insurance',
            screenOptions: {
              documentUrl: {
                value:
                  'https://recette.esigate.sofinco.fr/sites/partners/home.documentsAction.do?q6=web_ikea&p=/assurances/informations-assurance-rev.pdf',
                description: 'insurance informations document'
              },
              label: {
                value: 'souscription.menuNavigation.votreAssurance',
                description: 'label key in Jahia'
              }
            },
            noReturn: false,
            routed: true,
            steps: []
          },
          {
            pathId: 'summary',
            screenOptions: {
              label: {
                value: 'souscription.menuNavigation.ficheDialogue',
                description: 'label key in Jahia'
              }
            },
            noReturn: false,
            routed: true,
            steps: []
          },
          {
            pathId: 'loading',
            screenOptions: {},
            noReturn: false,
            routed: true,
            steps: []
          },
          {
            pathId: 'result',
            screenOptions: {
              label: {
                value: 'souscription.menuNavigation.reponsePrincipe',
                description: 'label key in Jahia'
              }
            },
            noReturn: false,
            routed: true,
            steps: []
          }
        ]
      }
    ]
  },
  colors: {
    primary: '#1AAEB7',
    secondary: '#219AFB',
    tertiary: '#EB3761',
    quaternary: '#4d1867',
    quintinary: '#e38e00'
  }
};
