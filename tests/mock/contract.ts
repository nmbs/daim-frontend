import { BusinessRule } from '@/services/BusinessRule';
import store from '@/store';

export const MockContract = {
  hasSendingSummaryMail: true,
  borrower: {
    loyaltyCardId: null,
    customerId: '92807343185',
    civilityCode: 1,
    lastName: null,
    maidenName: null,
    firstName: null,
    birthDate: 'null',
    citizenshipCode: null,
    birthCountryCode: null,
    birthCity: null,
    birthPostalCode: null,
    maritalStatus: null,
    numberOfChildren: 0,
    streetName: null,
    additionalStreetInfo: null,
    postalCode: null,
    city: null,
    countryCode: 'FR',
    housingCode: null,
    movingDate: 'null',
    telephoneFixNumber: null,
    mobileNumber: null,
    email: null,
    subscribeDigitalRevStatement: null,
    companyStatus: null,
    companySector: null,
    jobSituation: null,
    contractType: null,
    hiringDate: 'null',
    monthlyNetSalary: null,
    numberOfSalaryMonths: 12,
    otherMonthlyNetIncome: null,
    familyBenefitsAmount: 0,
    housingHelpAmount: 0,
    housingLoanAmount: null,
    rentingMonthlyAmount: null,
    creditConsumerMonthlyAmount: null,
    creditAutoMonthlyAmount: null,
    creditOtherMonthlyAmount: null,
    otherChargeAmount: 0,
    optIn: null,
    externalOptIn: false,
    stopReminderCustomer: false,
    hasInsurance: false,
    externalId: 'externalCustomerId1'
  },
  coBorrower: null,
  credit: {
    sourceId: 'sourceId',
    partnerId: 'partnerId',
    amount: 900,
    duration: 18,
    vehiculeAge: null,
    vehiculeAmount: null,
    cardType: 'S',
    appId: 'applicationId',
    orderId: 'orderId',
    scaleCode: 'ABS'
    // orderAmount: 0, // TODO: uncomment when enabling subscription data signature
    // personalContributionAmount: 0 // TODO: uncomment when enabling subscription data signature
  },
  partnerData: {
    businessProviderId: 'businessProviderId',
    preScoringCode: '1',
    equipmentCode: '280'
    // returnUrl: 'test.com' // TODO: uncomment when enabling subscription data signature
  },
  additionalData: {
    attribute1: null,
    attribute2: null
    // attribute3: 'someData' // TODO: uncomment when enabling subscription data signature
  }
};
