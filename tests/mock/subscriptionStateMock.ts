import { SubscriptionState } from '@/store/types';

export function initStateMock(): SubscriptionState {
  return {
    preContractMail: {
      hasBeenSent: false,
      email: '',
      offer: null
    },
    principal: null,
    secondary: null,
    formStates: {
      loan: {
        t1IsActive: true
      },
      name: {
        t1IsActive: true
      },
      family: {
        t1IsActive: true
      },
      contact: {
        t1IsActive: true
      },
      civility: {
        t1IsActive: true
      },
      incomes: {
        t1IsActive: true
      },
      charges: {
        t1IsActive: true
      },
      employment: {
        t1IsActive: true
      },
      housing: {
        t1IsActive: true
      },
      insurance: {
        version: 'v1',
        t1IsActive: true,
        t1IsEligible: true,
        t1IsEligibleRecommended: false,
        t1IsEligibleAlternative: false,
        t2IsEligible: true,
        t2IsEligibleRecommended: false,
        t2IsEligibleAlternative: false
      }
    },
    routeStates: null,
    routesList: [],
    blocRouteToFormRoutes: new Map(),
    response: null,
    context: null,
    creditForTwo: null,
    seConfig: null,
    experianData: {
      cfuserPrefs: null,
      cfuserPrefs2: null
    },
    authentication: {
      request: {
        id: null,
        birthDate: null,
        keypadId: null,
        passwordMask: null
      },
      response: {
        token: null,
        customerId: null,
        role: null
      },
      clientIsLogging: false,
      identification: {
        form: {
          lastName: null,
          firstName: null,
          birthDate: null,
          birthPostCode: null
        },
        choice: {
          id: null,
          token: null,
          encodedMobileNumber: null,
          encodedEmailAddress: null,
          postCode: null,
          sendBy: null
        }
      }
    }
  };
}
