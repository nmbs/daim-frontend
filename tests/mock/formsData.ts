import { Gender, Name } from '@/models/subscription/Name';
import { Contact } from '@/models/subscription/Contact';
import { Civility } from '@/models/subscription/Civility';
import { Incomes } from '@/models/subscription/Incomes';
import { Adresse } from '@/models/subscription/Adresse';
import { Employment } from '@/models/subscription/Employment';
import { Charges } from '@/models/subscription/Charges';

export const name1: Name = {
  gender: Gender.MAN,
  firstName: 'Potter',
  birthName: null,
  lastName: 'Potter',
  hasSpouse: false,
  maritalStatus: {
    code: 'M_M',
    label: 'Marié'
  }
};

export const name2: Name = {
  gender: null,
  firstName: null,
  birthName: null,
  lastName: null,
  hasSpouse: null,
  maritalStatus: null
};

export const name3: Name = {
  gender: Gender.WOMAN,
  firstName: 'Lola',
  birthName: null,
  lastName: 'XX',
  hasSpouse: null,
  maritalStatus: null
};

export const contact1: Contact = {
  email: null,
  mobilePhone: null,
  homePhone: null,
  lenderOffer: null,
  partnerOffer: null,
  contactOffer: null
};

export const contact2: Contact = {
  email: 'test@test.fr',
  mobilePhone: null,
  homePhone: '0142336989',
  lenderOffer: true,
  partnerOffer: true,
  contactOffer: true
};

export const civility: Civility = {
  nationality: {
    code: 'F',
    label: 'France'
  },
  birthCountry: {
    code: 'F',
    label: 'France'
  },
  birthCity: {
    code: '75001',
    label: 'Paris'
  },
  birthDate: '01/01/1960'
};

export const incomes1: Incomes = {
  netIncome: 3000,
  salaryMonths: 13,
  moreIncomes: false,
  housingBenefits: null,
  familyBenefits: null,
  otherIncomes: null
};

export const incomes2: Incomes = {
  netIncome: 3000,
  salaryMonths: 13,
  moreIncomes: true,
  housingBenefits: null,
  familyBenefits: null,
  otherIncomes: null
};

export const incomes3: Incomes = {
  netIncome: 3000,
  salaryMonths: 13,
  moreIncomes: true,
  housingBenefits: 1200,
  familyBenefits: 100,
  otherIncomes: 50
};

export const employment1: Employment = {
  professionalStatus: null,
  activitySector: null,
  contract: null,
  profession: null,
  sinceWhen: null
};

export const employment2: Employment = {
  professionalStatus: {
    code: '1_1',
    label: '1_1'
  },
  activitySector: {
    code: '1_1',
    label: '1_1'
  },
  contract: {
    code: '1_1',
    label: '1_1'
  },
  profession: {
    code: '1_1',
    label: '1_1'
  },
  sinceWhen: '01/01/2000'
};

export const employment3: Employment = {
  professionalStatus: {
    code: '4_4',
    label: '1_1'
  },
  activitySector: {
    code: '1_1',
    label: '1_1'
  },
  contract: {
    code: '1_1',
    label: '1_1'
  },
  profession: {
    code: '1_1',
    label: '1_1'
  },
  sinceWhen: '01/01/2000'
};

export const employment4: Employment = {
  professionalStatus: {
    code: '7_7',
    label: '1_1'
  },
  activitySector: {
    code: '1_1',
    label: '1_1'
  },
  contract: null,
  profession: {
    code: '1_1',
    label: '1_1'
  },
  sinceWhen: '01/01/2000'
};

export const employment5: Employment = {
  professionalStatus: {
    code: '6_6',
    label: '1_1'
  },
  activitySector: null,
  contract: null,
  profession: null,
  sinceWhen: '01/01/2000'
};

export const charges1: Charges = {
  alimony: null,
  carCredit: null,
  hasAlimony: false,
  moreCredits: false,
  otherCredit: null,
  refundHouseCredit: null,
  rental: null,
  revolving: null
};

export const charges2: Charges = {
  alimony: 123,
  carCredit: 77,
  hasAlimony: true,
  moreCredits: true,
  otherCredit: 6768,
  refundHouseCredit: 576,
  rental: 67687,
  revolving: 43546
};

export const housing1: Adresse = {
  adresseLabel: null,
  adresseCity: null,
  complementAdresse: null,
  housingType: null,
  sinceWhen: null
};

export const housing2: Adresse = {
  adresseLabel: 'Avenue des champs Elysées',
  adresseCity: {
    code: '75001',
    label: 'Paris'
  },
  complementAdresse: null,
  housingType: {
    code: 'C_C',
    label: 'Locataire'
  },
  sinceWhen: '01/01/1960'
};
