export const MockSubscriptionData = {
  name: {
    gender: null,
    firstName: null,
    lastName: null,
    birthName: null,
    maritalStatus: null,
    hasSpouse: false
  },
  family: {
    childrenNbr: 0
  },
  contact: {
    email: null,
    mobilePhone: null,
    homePhone: null,
    lenderOffer: null,
    partnerOffer: null,
    contactOffer: false
  },
  civility: {
    birthDate: null,
    birthCity: null,
    birthCountry: null,
    nationality: null
  },
  incomes: {
    netIncome: null,
    salaryMonths: 12,
    moreIncomes: false,
    housingBenefits: 0,
    familyBenefits: 0,
    otherIncomes: 0
  },
  charges: {
    rental: null,
    refundHouseCredit: null,
    hasAlimony: false,
    alimony: null,
    moreCredits: false,
    carCredit: 0,
    revolving: 0,
    otherCredit: 0
  },
  employment: {
    activitySector: null,
    contract: null,
    profession: null,
    professionalStatus: null,
    sinceWhen: null
  },
  adresse: {
    adresseLabel: null,
    complementAdresse: null,
    adresseCity: null,
    housingType: null,
    sinceWhen: null
  },
  insurance: {
    acceptInsuranceOffer: null,
    confirmRefusal: false
  },
  insurancev2: {
    selectedInsurance: null,
    refuseInsurance: false
  },
  wantCard: null,
  cardCode: null
};
