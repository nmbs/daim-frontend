import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import Header from '@/components/Header.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

const store = new Vuex.Store({
  state: {
    apiConfig: { colors: {}, appConfiguration: { screenOptions: {} }, partnerCode: {} },
    client: { isMobile: {} },
    subscription: {
      routeStates: {}
    }
  },
  getters: {
    getNextStep: (pathId) => {},
    isClientLogged: () => {},
    screenOption: (key) => (key) => null
  }
});

describe('Header.vue', () => {
  it('should pass', () => {
    const wrapper = shallowMount(Header, {
      i18n,
      localVue,
      store
    });
    expect(wrapper).toBeTruthy();
  });
});
