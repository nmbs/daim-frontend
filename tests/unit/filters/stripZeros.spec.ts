import { stripZeros } from '@/filters/stripZeros';

describe('stripZeros', () => {
  it('should return value', () => {
    expect(stripZeros('555')).toBe('555');
    expect(stripZeros('555.154')).toBe('555.154');
    expect(stripZeros('555,154')).toBe('555,154');
    expect(stripZeros('555.12 €')).toBe('555.12 €');
    expect(stripZeros('555.04')).toBe('555.04');
    expect(stripZeros('3 455,34')).toBe('3 455,34');
    expect(stripZeros('1 000,09 €')).toBe('1 000,09 €');
    expect(stripZeros('1 000 000,57 €')).toBe('1 000 000,57 €');
    expect(stripZeros('1 000 000.57 €')).toBe('1 000 000.57 €');
  });

  it('should return value without zero digits', () => {
    expect(stripZeros('3 455,00')).toBe('3 455');
    expect(stripZeros('555.00')).toBe('555');
    expect(stripZeros('555.000')).toBe('555');
    expect(stripZeros('555,000')).toBe('555');
    expect(stripZeros('555.000 €')).toBe('555 €');
    expect(stripZeros('1 000,00 €')).toBe('1 000 €');
    expect(stripZeros('1 000 000,00 €')).toBe('1 000 000 €');
    expect(stripZeros('1 000 000.00 €')).toBe('1 000 000 €');
  });
});
