import { findPreSelectedOffer, findIndexPreSelectedOffer } from '@/services/simulator/offerPreSelection';

describe('preSelectedOffer', () => {
  describe('findPreSelectedOffer', () => {
    it('should return pre-selected offer when only dueNumber suggested', () => {
      const preSelectedOffer = {
        nbMonthlyPaymentWithInsurance: 12,
        id: 3
      };
      const offers: any[] = [
        {
          nbMonthlyPaymentWithInsurance: 10,
          id: 1
        },
        {
          nbMonthlyPaymentWithInsurance: 11,
          id: 2
        },
        preSelectedOffer,
        {
          nbMonthlyPaymentWithInsurance: 13,
          id: 4
        },
        {
          nbMonthlyPaymentWithInsurance: 14,
          id: 5
        },
        {
          id: 6
        }
      ];
      expect(findPreSelectedOffer(offers, 12)).toEqual(preSelectedOffer);
    });

    it('should return pre-selected offer when dueNumber and scaleCode suggested', () => {
      const preSelectedOffer = {
        nbMonthlyPaymentWithInsurance: 12,
        scaleCode: 'CODE1',
        id: 4
      };
      const offers: any[] = [
        {
          nbMonthlyPaymentWithInsurance: 11,
          scaleCode: 'something',
          id: 1
        },
        {
          nbMonthlyPaymentWithInsurance: 11,
          scaleCode: 'CODE1',
          id: 2
        },
        {
          nbMonthlyPaymentWithInsurance: 12,
          scaleCode: 'something',
          id: 3
        },
        preSelectedOffer,
        {
          nbMonthlyPaymentWithInsurance: 13,
          scaleCode: 'something',
          id: 5
        },
        {
          nbMonthlyPaymentWithInsurance: 13,
          scaleCode: 'CODE1',
          id: 6
        },
        {
          scaleCode: 'something',
          id: 7
        },
        {
          nbMonthlyPaymentWithInsurance: 14,
          id: 8
        },
        {
          id: 9
        }
      ];
      expect(findPreSelectedOffer(offers, 12, 'CODE1')).toEqual(preSelectedOffer);

      const preSelectedOfferLastPayment = {
        nbMonthlyPaymentWithInsurance: 11,
        lastMonthlyAmountWithInsurance: 23,
        id: 2
      };
      const offersLastPayment: any[] = [
        {
          nbMonthlyPaymentWithInsurance: 11,
          id: 1
        },
        preSelectedOfferLastPayment,
        {
          nbMonthlyPaymentWithInsurance: 12,
          id: 3
        }
      ];
      expect(findPreSelectedOffer(offersLastPayment, 12)).toEqual(preSelectedOfferLastPayment);
    });

    it('should return null', () => {
      expect(findPreSelectedOffer([], 12)).toBeNull();
      expect(findPreSelectedOffer([{ id: 1, noNbMonthlyPaymentWithInsurancePropety: 'toto' } as any], 12)).toBeNull();
    });
  });

  describe('findIndexPreSelectedOffer', () => {
    it('should return index of pre-selected offer when only dueNumber suggested', () => {
      const offers: any[] = [
        {
          nbMonthlyPaymentWithInsurance: 10,
          id: 1
        },
        {
          nbMonthlyPaymentWithInsurance: 11,
          id: 2
        },
        {
          nbMonthlyPaymentWithInsurance: 12,
          id: 3
        },
        {
          nbMonthlyPaymentWithInsurance: 13,
          id: 4
        },
        {
          nbMonthlyPaymentWithInsurance: 14,
          id: 5
        },
        {
          id: 6
        }
      ];
      expect(findIndexPreSelectedOffer(offers, 12)).toBe(2);
    });

    it('should return index of pre-selected offer when dueNumber and scaleCode suggested', () => {
      const offers: any[] = [
        {
          nbMonthlyPaymentWithInsurance: 11,
          scaleCode: 'something',
          id: 1
        },
        {
          nbMonthlyPaymentWithInsurance: 11,
          scaleCode: 'CODE1',
          id: 2
        },
        {
          nbMonthlyPaymentWithInsurance: 12,
          scaleCode: 'something',
          id: 3
        },
        {
          nbMonthlyPaymentWithInsurance: 12,
          scaleCode: 'CODE1',
          id: 4
        },
        {
          nbMonthlyPaymentWithInsurance: 13,
          scaleCode: 'something',
          id: 5
        },
        {
          nbMonthlyPaymentWithInsurance: 13,
          scaleCode: 'CODE1',
          id: 6
        },
        {
          scaleCode: 'something',
          id: 7
        },
        {
          nbMonthlyPaymentWithInsurance: 14,
          id: 8
        },
        {
          id: 9
        }
      ];
      expect(findIndexPreSelectedOffer(offers, 12, 'CODE1')).toBe(3);

      const offersLastPayment: any[] = [
        {
          nbMonthlyPaymentWithInsurance: 11,
          id: 1
        },
        {
          nbMonthlyPaymentWithInsurance: 11,
          lastMonthlyAmountWithInsurance: 23,
          id: 2
        },
        {
          nbMonthlyPaymentWithInsurance: 12,
          id: 3
        }
      ];
      expect(findIndexPreSelectedOffer(offersLastPayment, 12)).toBe(1);
    });

    it('should return null', () => {
      expect(findPreSelectedOffer([], 12)).toBeNull();
      expect(
        findIndexPreSelectedOffer([{ id: 1, noNbMonthlyPaymentWithInsurancePropety: 'toto' } as any], 12)
      ).toBeNull();
    });
  });
});
