import { proposalSelected, sortSimulationProposal } from '@/services/simulator/sortSimulateur';
import { EFilterCriterionAttribute } from '@/models/simulator/tools/filterCriterion';

const proposals = [
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 10 sans frais
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 4, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 10 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 2, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 10 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 2, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 10
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 11 sans frais
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 11 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 11 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 3, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 11
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 12 sans frais
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 12 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 12 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 12
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 13 sans frais
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 13 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 13 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 13
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 14 sans frais
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 14 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 14 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' } // 14
];
const proposalsResult = [
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 10 sans frais
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 2, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 10
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 2, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 10 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 4, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 10 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 11 sans frais
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 3, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 11
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 11 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 11 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 12 sans frais
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 12
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 12 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 12 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 13 sans frais
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 13
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 13 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 13 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 14 sans frais
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 14
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 14 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' } // 14 remboursement 6 mois
];

const proposalsResult2 = [
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 10 sans frais
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 2, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 10
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 2, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 10 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 10, taeg: 4, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 10 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 11 sans frais
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 3, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 11
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 11 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 11, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 11 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 12
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 12 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 12 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 13 sans frais
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 13
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 13 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 13, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' }, // 13 remboursement 6 mois
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' }, // 14 sans frais
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }, // 14
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }, // 14 remboursement 3 mois
  { nbMonthlyPaymentWithoutInsurance: 14, taeg: 1, deferralMonthNumber: 6, scaleCode: 'VOLT39R6' } // 14 remboursement 6 mois
];

describe('test', () => {
  it('sortSimulationProposal ', () => {
    const proposalFilter = {
      type: EFilterCriterionAttribute.TIMELINE
    };
    expect(sortSimulationProposal(proposals, proposalFilter)).toEqual(proposalsResult);
  });
  it('proposalSelected with sans frais', () => {
    const filter = {
      value: 12,
      type: EFilterCriterionAttribute.TIMELINE
    };
    // { nbMonthlyPaymentWithoutInsurance: 12, taeg: 0, deferralMonthNumber: 0, scaleCode: 'VOLT39' };  12 sans frais
    const result = 8;
    expect(proposalSelected(filter, proposalsResult)).toEqual(result);
  });

  it('proposalSelected', () => {
    const filter = {
      value: 12,
      type: EFilterCriterionAttribute.TIMELINE
    };
    // { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' };  12
    const result = 8;

    expect(proposalSelected(filter, proposalsResult2)).toEqual(result);
  });

  it('proposalSelected', () => {
    const filter = {
      value: 12,
      type: EFilterCriterionAttribute.TIMELINE
    };
    // { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 0, scaleCode: 'VOLT39F' }; // 12
    const result = 8;
    expect(proposalSelected(filter, proposalsResult2)).toEqual(result);
  });

  it('proposalSelected scaleCode = VOLT39R3 ', () => {
    const filter = {
      value: 12,
      type: EFilterCriterionAttribute.TIMELINE
    };
    // { nbMonthlyPaymentWithoutInsurance: 12, taeg: 1, deferralMonthNumber: 3, scaleCode: 'VOLT39R3' }; // 12
    const scaleCode = 'VOLT39R3';
    const result = 9;
    expect(proposalSelected(filter, proposalsResult2, scaleCode)).toEqual(result);
  });
});
