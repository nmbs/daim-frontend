import ConfigurationAPI from '@/services/ConfigurationAPI';

describe('ConfigurationAPI', () => {
  describe('Switch configuration', () => {
    it('should return new X1 and config for amount lower than switch treshold', async () => {
      const currentOfferConfig = {
        screenOptions: {
          switchThreshold: {
            value: 50
          },
          lowerX1: {
            value: 'x1Lower'
          },
          upperX1: {
            value: 'x1Upper'
          }
        }
      };
      const fakeNewConfig = {
        sysType: 'newFakeConfig'
      };

      jest.spyOn(ConfigurationAPI, 'getOfferConfiguration').mockResolvedValue({ data: fakeNewConfig });
      const result = await ConfigurationAPI.switchConfigurationIfNeeded(
        currentOfferConfig,
        '49',
        'partnerCode',
        'someAccessToken'
      );
      expect(ConfigurationAPI.getOfferConfiguration).toHaveBeenCalledWith('someAccessToken', 'partnerCode', 'x1Lower');
      expect(result).toEqual({
        newX1: 'x1Lower',
        newOfferConfig: fakeNewConfig
      });
    });

    it('should return new X1 and config for amount equal to switch treshold', async () => {
      const currentOfferConfig = {
        screenOptions: {
          switchThreshold: {
            value: 50
          },
          lowerX1: {
            value: 'x1Lower'
          },
          upperX1: {
            value: 'x1Upper'
          }
        }
      };
      const fakeNewConfig = {
        sysType: 'newFakeConfig'
      };

      jest.spyOn(ConfigurationAPI, 'getOfferConfiguration').mockResolvedValue({ data: fakeNewConfig });
      const result = await ConfigurationAPI.switchConfigurationIfNeeded(
        currentOfferConfig,
        '50',
        'partnerCode',
        'someAccessToken'
      );
      expect(ConfigurationAPI.getOfferConfiguration).toHaveBeenCalledWith('someAccessToken', 'partnerCode', 'x1Lower');
      expect(result).toEqual({
        newX1: 'x1Lower',
        newOfferConfig: fakeNewConfig
      });
    });

    it('should return new X1 and config for amount higher than switch treshold', async () => {
      const currentOfferConfig = {
        screenOptions: {
          switchThreshold: {
            value: 50
          },
          lowerX1: {
            value: 'x1Lower'
          },
          upperX1: {
            value: 'x1Upper'
          }
        }
      };
      const fakeNewConfig = {
        sysType: 'newFakeConfig'
      };

      jest.spyOn(ConfigurationAPI, 'getOfferConfiguration').mockResolvedValue({ data: fakeNewConfig });
      const result = await ConfigurationAPI.switchConfigurationIfNeeded(
        currentOfferConfig,
        '51',
        'partnerCode',
        'someAccessToken'
      );
      expect(ConfigurationAPI.getOfferConfiguration).toHaveBeenCalledWith('someAccessToken', 'partnerCode', 'x1Upper');
      expect(result).toEqual({
        newX1: 'x1Upper',
        newOfferConfig: fakeNewConfig
      });
    });

    it('should return new X1 and config if no amount defined', async () => {
      const currentOfferConfig = {
        screenOptions: {
          switchThreshold: {
            value: 50
          },
          lowerX1: {
            value: 'x1Lower'
          },
          upperX1: {
            value: 'x1Upper'
          },
          noAmountX1: {
            value: 'x1NoValue'
          }
        }
      };
      const fakeNewConfig = {
        sysType: 'newFakeConfig'
      };

      jest.spyOn(ConfigurationAPI, 'getOfferConfiguration').mockResolvedValue({ data: fakeNewConfig });
      const result = await ConfigurationAPI.switchConfigurationIfNeeded(
        currentOfferConfig,
        '',
        'partnerCode',
        'someAccessToken'
      );
      expect(ConfigurationAPI.getOfferConfiguration).toHaveBeenCalledWith('someAccessToken', 'partnerCode', 'x1Upper');
      expect(result).toEqual({
        newX1: 'x1NoValue',
        newOfferConfig: fakeNewConfig
      });
    });

    it('should return null values if no switchThreshold is defined in config', async () => {
      const currentOfferConfig = {
        screenOptions: {}
      };
      const result = await ConfigurationAPI.switchConfigurationIfNeeded(
        currentOfferConfig,
        '51',
        'partnerCode',
        'someAccessToken'
      );
      expect(result).toEqual({
        newX1: null,
        newConfig: null
      });
    });

    it('should throw an error if amount is not a real number and switchThreshold is defined', async () => {
      await expect(
        ConfigurationAPI.switchConfigurationIfNeeded(
          {
            screenOptions: {
              switchThreshold: {
                value: 50
              }
            }
          },
          '51s',
          'partnerCode',
          'someAccessToken'
        )
      ).rejects.toThrowError();
    });
  });
});
