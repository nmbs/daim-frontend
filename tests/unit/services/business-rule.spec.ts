import { BusinessRule } from '@/services/BusinessRule';
import { DateTime } from 'luxon';

import superSenior from '@/assets/json/insurance/partner/superSenior.json';
import senior from '@/assets/json/insurance/partner/senior.json';
import young from '@/assets/json/insurance/partner/young.json';
import sofincoCrOld from '@/assets/json/insurance/sofinco/crOld.json';
import sofincoCrYoung from '@/assets/json/insurance/sofinco/crYoung.json';
import sofincoMhOld from '@/assets/json/insurance/sofinco/mhOld.json';
import sofincoMhYoung from '@/assets/json/insurance/sofinco/mhYoung.json';
import sofincoPbOld from '@/assets/json/insurance/sofinco/pbOld.json';
import sofincoPbYoung from '@/assets/json/insurance/sofinco/pbYoung.json';
import { CreditTypeEnum } from '@/models/simulator/business/creditType';
import { InsurancePartnerName } from '@/models/subscription/Insurance';
import Config from '@/config/configSimulator';
import { StepState } from '@/models/StepState';
import { RoutePathId } from '@/models/RoutesPath';
import { RouteStates } from '@/models/subscription/RouteState';
import {
  insuranceStepState1,
  insuranceStepState2,
  insuranceStepState3,
  insuranceStepState4,
  insuranceStepState5
} from '../../mock/stepState/insuranceStepState';
import { Employment } from '@/models/subscription/Employment';

describe('business rule', () => {
  beforeEach(() => {
    Config.chainWithSubscription = true;
  });

  describe('getInsuranceInfo', () => {
    it('should returns the insurance info for partner cr young', () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 60 }).plus({ days: 1 });
      const info = businessRule.getInsuranceInfo(null, null, null, date.toFormat('d/M/yyyy'));
      expect(info).toEqual(young);
    });

    it('should returns the insurance info for cr senior', () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 60 });
      const info = businessRule.getInsuranceInfo(null, null, null, date.toFormat('d/M/yyyy'));
      expect(info).toEqual(senior);
    });

    it('should returns the insurance info for cr super senior', () => {
      const businessRule = new BusinessRule(18, 81, 60, 65);
      const date = DateTime.fromJSDate(new Date()).minus({ years: 66 });
      const info = businessRule.getInsuranceInfo(null, null, null, date.toFormat('d/M/yyyy'));
      expect(info).toBe(superSenior);
    });

    it('should returns the insurance info for sofinco cr young', () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 60 }).plus({ days: 1 });
      const info = businessRule.getInsuranceInfo(
        InsurancePartnerName.SOFINCO,
        CreditTypeEnum.REVOLVING,
        null,
        date.toFormat('d/M/yyyy')
      );
      expect(info).toEqual(sofincoCrYoung);
    });

    it('should returns the insurance info for sofinco cr young with a BusinessRule init with null values', () => {
      const businessRule = new BusinessRule(null, null, null);
      const date = DateTime.fromJSDate(new Date()).minus({ years: 60 }).plus({ days: 1 });
      const info = businessRule.getInsuranceInfo(
        InsurancePartnerName.SOFINCO,
        CreditTypeEnum.REVOLVING,
        null,
        date.toFormat('d/M/yyyy')
      );
      expect(info).toEqual(sofincoCrYoung);
    });

    it('should returns the insurance info for sofinco cr old', () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 60 });
      const info = businessRule.getInsuranceInfo(
        InsurancePartnerName.SOFINCO,
        CreditTypeEnum.REVOLVING,
        null,
        date.toFormat('d/M/yyyy')
      );
      expect(info).toEqual(sofincoCrOld);
    });

    it('should returns the insurance info for sofinco pb young', () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 65 }).plus({ days: 1 });
      const info = businessRule.getInsuranceInfo(
        InsurancePartnerName.SOFINCO,
        CreditTypeEnum.LOAN,
        null,
        date.toFormat('d/M/yyyy')
      );
      expect(info).toEqual(sofincoPbYoung);
    });

    it('should returns the insurance info for sofinco pb old', () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 65 });
      const info = businessRule.getInsuranceInfo(
        InsurancePartnerName.SOFINCO,
        CreditTypeEnum.LOAN,
        null,
        date.toFormat('d/M/yyyy')
      );
      expect(info).toEqual(sofincoPbOld);
    });

    it('should returns the insurance info for sofinco mh young', () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 65 }).plus({ days: 1 });
      const info = businessRule.getInsuranceInfo(
        InsurancePartnerName.SOFINCO,
        CreditTypeEnum.LOAN,
        'PBCAMPIN',
        date.toFormat('d/M/yyyy')
      );
      expect(info).toEqual(sofincoMhYoung);
    });

    it('should returns the insurance info for sofinco mh old', () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 65 });
      const info = businessRule.getInsuranceInfo(
        InsurancePartnerName.SOFINCO,
        CreditTypeEnum.LOAN,
        'PBCAMPIN',
        date.toFormat('d/M/yyyy')
      );
      expect(info).toEqual(sofincoMhOld);
    });
  });

  describe('canSeeInsurance', () => {
    it('should returns can see insurance infos', () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 60 });
      const canSee = businessRule.canSeeInsurance(date.toFormat('d/M/yyyy'));
      expect(canSee).toBeTruthy();
    });

    it(`should returns can't see insurance infos`, () => {
      const businessRule = new BusinessRule();
      const date = DateTime.fromJSDate(new Date()).minus({ years: 82 });
      const canSee = businessRule.canSeeInsurance(date.toFormat('d/M/yyyy'));
      expect(canSee).toBeFalsy();
    });
  });

  describe('canTakeInsurance', () => {
    it('should return that client cannot take insurance', () => {
      const businessRule = new BusinessRule();
      const canTakeInsurance = businessRule.canTakeInsurance('LOAN', 100, 0);
      expect(canTakeInsurance).toBe(false);
    });

    it('should return that client cannot take insurance', () => {
      const businessRule = new BusinessRule();
      const canTakeInsurance = businessRule.canTakeInsurance('REVOLVING', 0, 100);
      expect(canTakeInsurance).toBe(false);
    });

    it('should return that client can take insurance', () => {
      const businessRule = new BusinessRule();
      const canTakeInsurance = businessRule.canTakeInsurance('LOAN', 0, 100);
      expect(canTakeInsurance).toBe(true);
    });

    it('should return that client can take insurance', () => {
      const businessRule = new BusinessRule();
      const canTakeInsurance = businessRule.canTakeInsurance('REVOLVING', 100, 0);
      expect(canTakeInsurance).toBe(true);
    });
  });

  describe('get intializer', () => {
    it('should returns Sofinco Initializer', () => {
      const businessRule = new BusinessRule();
      expect(businessRule.isSofincoPartnerId('web_sofinco')).toBeTruthy();
      expect(businessRule.isSofincoPartnerId('web_ikea')).toBeFalsy();
    });
  });

  describe('shouldDisplayContactModule', () => {
    it('should return false is contact module is not enabled in conf', () => {
      expect(BusinessRule.shouldDisplayContactModule({} as any)).toBe(false);
    });

    it('should return false if there is only a simulator step (independant simulator)', () => {
      Config.chainWithSubscription = false;
      expect(
        BusinessRule.shouldDisplayContactModule({
          screenOptions: {
            'show-contact-module': {
              value: true
            },
            'show-contact-call-button': {
              value: true
            }
          },
          steps: [
            {
              pathId: 'simulation'
            }
          ]
        } as any)
      ).toBe(false);
    });

    it('should return true if there is only the step subcription', () => {
      Config.chainWithSubscription = false;
      expect(
        BusinessRule.shouldDisplayContactModule({
          screenOptions: {
            'show-contact-module': {
              value: true
            },
            'show-contact-call-button': {
              value: true
            }
          },
          steps: [
            {
              pathId: 'subscription'
            }
          ]
        } as any)
      ).toBe(true);
    });

    it('should return true if steps are chained with subscription', () => {
      expect(
        BusinessRule.shouldDisplayContactModule({
          screenOptions: {
            'show-contact-module': {
              value: true
            },
            'show-contact-call-button': {
              value: true
            }
          },
          // We do not need to define steps here as the test is performed on Config.chainWithSubscription. However,
          // they have been included in appConfig mock in order to represent what will lead to Config.chainWithSubscription === true
          steps: [
            {
              pathId: 'simulation'
            },
            {
              pathId: 'subscription'
            }
          ]
        } as any)
      ).toBe(true);
    });
    it('should display IAdvize if option enable', () => {
      Config.chainWithSubscription = false;
      expect(
        BusinessRule.shouldDisplayIAdvize({
          screenOptions: {
            'show-contact-module': {
              value: true
            },
            'show-contact-module-iadvize': {
              value: true
            }
          },
          steps: [
            {
              pathId: 'subscription'
            }
          ]
        } as any)
      ).toBeTruthy();
    });

    it('should not display IAdvize if contact module is disabled', () => {
      Config.chainWithSubscription = false;
      expect(
        BusinessRule.shouldDisplayIAdvize({
          screenOptions: {
            'show-contact-module': {
              value: false
            },
            'show-contact-module-iadvize': {
              value: true
            }
          },
          steps: [
            {
              pathId: 'subscription'
            }
          ]
        } as any)
      ).toBeFalsy();
    });
    it('should  display WannaSpeak if option enable and wannaspeak id is filled', () => {
      Config.chainWithSubscription = false;
      expect(
        BusinessRule.shouldDisplayWannaSpeak({
          screenOptions: {
            'show-contact-module': {
              value: true
            },
            'show-contact-module-wannaspeak': {
              value: true
            },
            'show-contact-module-wannaspeak-id': {
              value: 'test'
            }
          },
          steps: [
            {
              pathId: 'subscription'
            }
          ]
        } as any)
      ).toBeTruthy();
    });

    it('should not display WannaSpeak if  wannaspeak id is missing', () => {
      Config.chainWithSubscription = false;
      expect(
        BusinessRule.shouldDisplayWannaSpeak({
          screenOptions: {
            'show-contact-module': {
              value: true
            },
            'show-contact-module-wannaspeak': {
              value: true
            }
          },
          steps: [
            {
              pathId: 'subscription'
            }
          ]
        } as any)
      ).toBeFalsy();
    });
  });

  describe('showReportVac', () => {
    it('should return true if proposal has deferral', () => {
      expect(
        BusinessRule.showReportInformations({
          deferralMonthNumber: 1
        } as any)
      ).toBeTruthy();
    });

    it('should return false if proposal has no deferral', () => {
      expect(
        BusinessRule.showReportInformations({
          deferralMonthNumber: 0
        } as any)
      ).toBeFalsy();
    });
  });

  describe('showGoToSummaryStepBtn', () => {
    const isMobile = true;
    const isNotMobile = false;
    const currentStep: StepState = new StepState(RoutePathId.CHARGES, [], null, null, false, false);
    const nextStep: StepState = new StepState(RoutePathId.CIVILITY, [], null, null, false, false);
    const summaryStep: StepState = new StepState(RoutePathId.SUMMARY, [], null, null, false, false);
    const routesState: RouteStates = {
      informationBloc: null,
      loan: null,
      name: null,
      contact: null,
      situationBloc: null,
      civility: null,
      family: null,
      housing: null,
      incomesBloc: null,
      employment: null,
      incomes: null,
      charges: null,
      insurance: null,
      card: null,
      summary: {
        valid: false,
        disabled: false,
        active: false,
        visible: false,
        isBlock: false,
        hasChildren: false,
        opened: false
      },
      loading: null,
      result: null,
      resultSofinco: null
    };
    const routesStateWithSummaryDisabled: RouteStates = {
      informationBloc: null,
      loan: null,
      name: null,
      contact: null,
      situationBloc: null,
      civility: null,
      family: null,
      housing: null,
      incomesBloc: null,
      employment: null,
      incomes: null,
      charges: null,
      insurance: null,
      card: null,
      summary: {
        valid: false,
        disabled: true,
        active: false,
        visible: false,
        isBlock: false,
        hasChildren: false,
        opened: false
      },
      loading: null,
      result: null,
      resultSofinco: null
    };

    it('should return true if it is on mobile, the current and next step are not the summary step and the summary step is not disabled', () => {
      expect(BusinessRule.showGoToSummaryStepBtn(isMobile, currentStep, nextStep, routesState)).toBeTruthy();
    });
    it('should return false if it is not on mobile', () => {
      expect(BusinessRule.showGoToSummaryStepBtn(isNotMobile, currentStep, nextStep, routesState)).toBeFalsy();
    });
    it('should return false if the next step is the summary step', () => {
      expect(BusinessRule.showGoToSummaryStepBtn(isMobile, currentStep, summaryStep, routesState)).toBeFalsy();
    });
    it('should return false if the current step is the summary step', () => {
      expect(BusinessRule.showGoToSummaryStepBtn(isMobile, summaryStep, nextStep, routesState)).toBeFalsy();
    });
    it('should return false if it the summary step is disabled', () => {
      expect(
        BusinessRule.showGoToSummaryStepBtn(isMobile, currentStep, nextStep, routesStateWithSummaryDisabled)
      ).toBeFalsy();
    });
  });

  describe('getInsuranceConfiguration', () => {
    it('should return limit ages with only null value', () => {
      const limitAgesExpected = { limitYoung: null, limitSenior: null, minAgeInsurance: null, maxAgeInsurance: null };
      // Insurance step state with the following screenOption:
      // - limit-senior = ''
      // - limit-young  = ''
      // - max-age      = ''
      // - min-age      = ''
      const insuranceConfiguration1 = BusinessRule.getInsuranceConfiguration(insuranceStepState1);
      expect(insuranceConfiguration1).toEqual(limitAgesExpected);

      // Insurance step state with the following no limit age in screenOption
      const insuranceConfiguration2 = BusinessRule.getInsuranceConfiguration(insuranceStepState2);
      expect(insuranceConfiguration2).toEqual(limitAgesExpected);

      // Insurance step state with no screenOption
      const insuranceConfiguration3 = BusinessRule.getInsuranceConfiguration(insuranceStepState3);
      expect(insuranceConfiguration3).toEqual(limitAgesExpected);
    });

    it('should return limit ages with values', () => {
      const limitAgesExpected = { limitYoung: 59, limitSenior: 74, minAgeInsurance: 18, maxAgeInsurance: 80 };
      const insuranceConfiguration = BusinessRule.getInsuranceConfiguration(insuranceStepState4);
      expect(insuranceConfiguration).toEqual(limitAgesExpected);
    });

    it('should return limit ages with values and null values', () => {
      const limitAgesExpected = { limitYoung: null, limitSenior: null, minAgeInsurance: null, maxAgeInsurance: 80 };
      const insuranceConfiguration = BusinessRule.getInsuranceConfiguration(insuranceStepState5);
      expect(insuranceConfiguration).toEqual(limitAgesExpected);
    });
  });

  describe('getJobSituationCode', () => {
    it('should return the job situation code of a retired person', () => {
      const employment: Employment = {
        professionalStatus: {
          code: '6_6'
        },
        profession: null,
        activitySector: null,
        contract: null,
        sinceWhen: '01/2017'
      };
      expect(new BusinessRule().getJobSituationCode(employment)).toEqual('70_70');
    });

    it('should return the current job situation code', () => {
      const employment: Employment = {
        activitySector: {
          label: "Activités financières et d'assurance",
          alternativeLabel: null,
          code: '9_9'
        },
        contract: {
          label: 'CDI',
          alternativeLabel: null,
          code: '0'
        },
        profession: {
          label: "Cadres d'entreprise",
          alternativeLabel: null,
          code: '37_36'
        },
        professionalStatus: {
          label: 'Salarié du privé',
          alternativeLabel: null,
          code: '1_1'
        },
        sinceWhen: '01/2017'
      };
      expect(new BusinessRule().getJobSituationCode(employment)).toEqual('37_36');
    });

    it('should return a null code for the job situation', () => {
      const employment: Employment = {
        activitySector: null,
        contract: null,
        profession: null,
        professionalStatus: null,
        sinceWhen: null
      };
      expect(new BusinessRule().getJobSituationCode(employment)).toBeNull();
    });
  });

  describe('isEligibleForInsuranceV2', () => {
    it('should be eligible to the insurance V2 step', () => {
      expect(new BusinessRule().isEligibleForInsuranceV2('web_sofinco', CreditTypeEnum.LOAN));
    });

    it('should not be eligible to the insurance V2 step', () => {
      expect(new BusinessRule().isEligibleForInsuranceV2('web_ikea', CreditTypeEnum.LOAN));
      expect(new BusinessRule().isEligibleForInsuranceV2('web_sofinco', CreditTypeEnum.REVOLVING));
    });
  });
});
