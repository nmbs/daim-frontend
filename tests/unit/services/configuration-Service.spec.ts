import { RouteConfig } from 'vue-router';

import { AppConfiguration } from '@/models/Configuration';
import { StepState } from '@/models/StepState';
import * as ConfigurationService from '@/services/ConfigurationService';
import { offerConfigIn } from '../../mock/configServiceData/offerConfig';
import { offerConfigRoutesOut } from '../../mock/configServiceData/offerConfigToRouteConfig';
import { expectedStepStateTree } from '../../mock/configServiceData/offerConfigToStepState';

describe('Configuration service tests', () => {
  it('should return offer steps and merged partner/offer options', () => {
    const partnerConfiguration: any = {
      steps: ['stepPartner'],
      screenOptions: {
        option1: 'value1Partner',
        option2: 'value2Partner'
      }
    };
    const offerConfiguration: any = {
      steps: ['stepOffer'],
      screenOptions: {
        option1: 'value1Offer',
        option3: 'value3Offer'
      }
    };
    expect(ConfigurationService.mergeConfigurations(offerConfiguration, partnerConfiguration)).toEqual({
      steps: ['stepOffer'],
      screenOptions: {
        option1: 'value1Offer',
        option2: 'value2Partner',
        option3: 'value3Offer'
      }
    });
  });

  it('should return partner steps', () => {
    const partnerConfiguration: any = {
      steps: ['stepPartner'],
      screenOptions: {
        option1: 'value1Partner'
      }
    };
    const offerConfiguration: any = {
      screenOptions: {
        option2: 'value2Offer'
      }
    };
    expect(ConfigurationService.mergeConfigurations(offerConfiguration, partnerConfiguration)).toEqual({
      steps: ['stepPartner'],
      screenOptions: {
        option1: 'value1Partner',
        option2: 'value2Offer'
      }
    });
  });

  it('should initialize StepState from AppConfiguration', () => {
    const stepState: StepState = ConfigurationService.initStepsStates(offerConfigIn as AppConfiguration);

    expect(stepState.steps[0].pathId).toBe(expectedStepStateTree.steps[0].pathId);
    expect(stepState.steps[0].steps[0].pathId).toBe(expectedStepStateTree.steps[0].steps[0].pathId);
    expect(stepState.steps[0].steps[1].pathId).toBe(expectedStepStateTree.steps[0].steps[1].pathId);
  });

  it('should create routes from AppConfiguration', () => {
    const routes: RouteConfig[] = ConfigurationService.getRoutesConfig(offerConfigIn as AppConfiguration);

    expect(JSON.stringify(routes)).toBe(JSON.stringify(offerConfigRoutesOut));
  });

  it('should compute a flat array with routes', () => {
    const expectedResultArray = ['informationBloc', 'situationBloc'];

    const actualArray = ConfigurationService.getRoutesList(offerConfigIn as AppConfiguration);

    expect(actualArray).toEqual(expectedResultArray);
  });

  it('should compute the subscription route map', () => {
    const expectedResultMap: Map<any, any> = new Map();
    expectedResultMap.set('informationBloc', ['informationBloc']);
    expectedResultMap.set('situationBloc', ['situationBloc']);

    const actualMap = ConfigurationService.getSubscriptionRoutesMap(offerConfigRoutesOut[1]);

    expect(actualMap).toEqual(expectedResultMap);
  });
});
