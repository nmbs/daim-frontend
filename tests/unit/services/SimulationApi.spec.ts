import axios from 'axios';
import SimulationApi from '@/services/SimulationApi';
import { CreditTypeEnum } from '@/models/simulator/business/creditType';
import { SimulationParameter } from '@/models/Simulation';
import getConfig from '@/config/config';

jest.mock('axios');

describe('Simulation API', () => {
  const RealDate = Date;
  function mockDate(isoDate) {
    // @ts-ignore
    global.Date = class extends RealDate {
      constructor() {
        super();
        return new RealDate(isoDate);
      }
    };
  }

  beforeEach(() => {
    // @ts-ignore
    axios.get.mockReset();
    // @ts-ignore
    axios.post.mockReset();
    // @ts-ignore
    global.Date = RealDate;
  });
  describe('Loan simulation', () => {
    it('should perform a simulation with amount, durations, hasInsurance, businessProviderId, equipmentCode and scaleCode provided', async () => {
      const parameters: SimulationParameter = {
        amount: 555,
        duration: 12,
        hasInsurance: true,
        businessProviderId: '123456789',
        equipmentCode: '123',
        scaleCode: 'ABC'
      };
      // @ts-ignore
      axios.post.mockResolvedValue({
        data: [
          {
            totalCostWithoutInsurance: 21.75,
            loanDuration: 8,
            dueNumber: 3,
            monthlyAmountWithoutInsurance: 192.25,
            monthlyAmountWithInsurance: 194.08,
            totalAmountWithoutInsurance: 576.75,
            totalAmountWithInsurance: 582.24,
            annualPercentageRateOfCharge: 6.813,
            annualPercentageRateOfInsurance: 1.751,
            annualPercentageRate: 6.691,
            fixedRate: true,
            monthlyInsuranceAmount: 1.83,
            dueDeferralNumber: 6,
            scaleCode: 'ABC',
            borrowerTotalInsuranceAmount: 5.49
          }
        ]
      });
      const res = await SimulationApi.getLoanSimulation(parameters);
      expect(res).toEqual([
        {
          amount: 555,
          creditCost: 21.75,
          creditDurationWithoutInsurance: 8,
          creditDurationWithInsurance: 8,
          nbMonthlyPaymentWithoutInsurance: 3,
          nbMonthlyPaymentWithInsurance: 3,
          monthlyAmountWithoutInsurance: 192.25,
          monthlyAmountWithInsurance: 194.08,
          lastMonthlyAmountWithoutInsurance: null,
          lastMonthlyAmountWithInsurance: null,
          totalAmountWithoutInsurance: 576.75,
          totalAmountWithInsurance: 582.24,
          firstPaymentDate: null,
          taeg: 6.813,
          taea: 1.751,
          debitRate: 6.691,
          fixedRate: true,
          monthlyInsuranceAmount: 1.83,
          utilizationDate: null,
          dueDay: null,
          deferralMonthNumber: 6,
          firstMonthlyInsuranceAmount: 1.83,
          scaleCode: 'ABC',
          totalInsuranceCost: 5.49
        }
      ]);
      // @ts-ignore
      const args = axios.post.mock.calls[0];
      expect(args[1]).toEqual({
        amount: 555,
        dueNumbers: [12],
        hasBorrowerInsurance: true,
        businessProviderId: '123456789',
        equipmentCode: '123',
        scaleCode: 'ABC'
      });
    });

    it('should return an emty list if API respond with 204', async () => {
      // @ts-ignore
      axios.post.mockResolvedValue({ status: 204 });
      const res = await SimulationApi.getLoanSimulation({ amount: 555 });
      expect(res).toEqual([]);
      // @ts-ignore
      const args = axios.post.mock.calls[0];
      expect(args[1]).toEqual({ amount: 555 });
    });
  });

  describe('Revolving simulation', () => {
    it('should perform a simulation with amount, durations, hasInsurance, businessProviderId, equipmentCode and scaleCode provided', async () => {
      const parameters: SimulationParameter = {
        amount: 555,
        duration: 12,
        hasInsurance: true,
        businessProviderId: '123456789',
        equipmentCode: '123',
        scaleCode: 'ABC'
      };
      const isPrincipal = true;
      // @ts-ignore
      axios.post.mockResolvedValue({
        data: [
          {
            dueNumberWithInsurance: 2,
            dueNumberWithoutInsurance: 3,
            monthlyAmountWithoutInsurance: 185.0,
            monthlyAmountWithInsurance: 187.6,
            annualGlobalEffectiveRate: 9.4,
            lastMonthlyAmountWithoutInsurance: 0.0,
            lastMonthlyAmountWithInsurance: 187.57,
            firstMonthlyAmountWithInsurance: 0.0,
            totalCostWithoutInsurance: 0.0,
            totalCostWithInsurance: 7.77,
            totalAmountWithoutInsurance: 555.0,
            totalAmountWithInsurance: 562.77,
            contractFees: 0.0,
            annualDebitRate: 10.0,
            borrowerMonthlyInsuranceAmount: 3.88,
            coBorrowerMonthlyInsuranceAmount: 0.0,
            firstPaymentDate: '2021-02-10',
            dueDay: 10,
            fixedRate: true,
            borrowerFirstMonthlyInsuranceAmount: 3.88,
            coBorrowerFirstMonthlyInsuranceAmount: 0.0,
            cumulativeInsuranceRate: 0.7,
            annualInsuranceEffectiveRate: 4.045,
            borrowerTotalInsuranceAmount: 7.77,
            coBorrowerTotalInsuranceAmount: 0.0,
            creditDuration: 5,
            hasBorrowerSaleInsurance: true,
            hasCoBorrowerSaleInsurance: false,
            deferralMonthNumber: 2,
            scaleCode: 'SCA2'
          }
        ]
      });
      mockDate('2020-11-02T12:32:54.825Z');

      const res = await SimulationApi.getRevolvingSimulation(parameters, isPrincipal);
      expect(res).toEqual([
        {
          amount: 555,
          creditCost: 0.0,
          creditDurationWithoutInsurance: 5,
          creditDurationWithInsurance: 5,
          nbMonthlyPaymentWithoutInsurance: 3,
          nbMonthlyPaymentWithInsurance: 2,
          monthlyAmountWithoutInsurance: 185.0,
          monthlyAmountWithInsurance: 187.6,
          lastMonthlyAmountWithoutInsurance: 0.0,
          lastMonthlyAmountWithInsurance: 187.57,
          totalAmountWithoutInsurance: 555.0,
          totalAmountWithInsurance: 562.77,
          firstPaymentDate: '2021-02-10',
          taeg: 9.4,
          taea: 4.045,
          debitRate: 10,
          fixedRate: true,
          monthlyInsuranceAmount: 3.88,
          utilizationDate: '02/11/2020',
          dueDay: 10,
          deferralMonthNumber: 2,
          firstMonthlyInsuranceAmount: 3.88,
          scaleCode: 'SCA2',
          totalInsuranceCost: 7.77
        }
      ]);
      // @ts-ignore
      const args = axios.post.mock.calls[0];
      expect(args[1]).toEqual({
        amount: 555,
        isPrincipal: true,
        durations: [12],
        hasBorrowerInsurance: true,
        businessProviderId: '123456789',
        equipmentCode: '123',
        scaleCode: 'ABC'
      });

      // // TODO: find way to test headers
      // expect(args[2].headers).toHaveProperty('Context-Sourceid', '');
      // expect(args[2].headers).toHaveProperty('Context-Applicationid', '');
      // expect(args[2].headers).toHaveProperty('Context-Partnerid', '');
    });

    it('should return an empty list if API respond with 204', async () => {
      // @ts-ignore
      axios.post.mockResolvedValue({ status: 204 });
      const res = await SimulationApi.getRevolvingSimulation({ amount: 555 });
      expect(res).toEqual([]);
      // @ts-ignore
      const args = axios.post.mock.calls[0];
      expect(args[1]).toEqual({ amount: 555, isPrincipal: false });
    });
  });

  describe('Partners pre-contract documents email sending', () => {
    it('should call API with amount, duration, scaleCode, businessProviderId and equipmentCode provided', async () => {
      const parameters: SimulationParameter = {
        amount: 444,
        duration: 12,
        scaleCode: 'scaleCodeTest',
        businessProviderId: '123456789',
        equipmentCode: '123',
        applicationId: 'testAppId',
        partnerId: 'partner1',
        sourceId: 'offer1'
      };
      //@ts-ignore
      axios.post.mockImplementationOnce(() => Promise.resolve());
      await SimulationApi.sendEmail(parameters, 'test@test.com', CreditTypeEnum.LOAN);
      //@ts-ignore
      const args = axios.post.mock.calls[0];
      expect(args[0]).toBe(
        `${
          getConfig().API_DOCUMENT
        }/emails?email=test@test.com&creditType=LOAN&amount=444&duration=12&scaleCode=scaleCodeTest&businessProviderId=123456789&equipementCode=123`
      );
      expect(args[1]).toEqual({});
      expect(args[2].headers).toHaveProperty('Context-Applicationid', 'testAppId');
      expect(args[2].headers).toHaveProperty('Context-Partnerid', 'partner1');
      expect(args[2].headers).toHaveProperty('Context-Sourceid', 'offer1');
    });

    it('should call API without amount, duration, scaleCode, businessProviderId and equipmentCode', async () => {
      const parameters: SimulationParameter = {};
      //@ts-ignore
      axios.post.mockImplementationOnce(() => Promise.resolve());
      await SimulationApi.sendEmail(parameters, 'test@test.com', CreditTypeEnum.REVOLVING);
      //@ts-ignore
      const args = axios.post.mock.calls[0];
      expect(args[0]).toBe(`${getConfig().API_DOCUMENT}/emails?email=test@test.com&creditType=REVOLVING`);
      expect(args[1]).toEqual({});
    });
  });

  describe('Partners pre-contract documents download', () => {
    it('should call API with amount, duration, scaleCode, businessProviderId and equipmentCode provided', async () => {
      const parameters: SimulationParameter = {
        amount: 444,
        duration: 12,
        scaleCode: 'scaleCodeTest',
        businessProviderId: '123456789',
        equipmentCode: '123',
        applicationId: 'testAppId',
        partnerId: 'partner1',
        sourceId: 'offer1'
      };
      //@ts-ignore
      axios.get.mockImplementationOnce(() => Promise.resolve());
      await SimulationApi.getPreContractDocuments(parameters, CreditTypeEnum.LOAN);
      //@ts-ignore
      const args = axios.get.mock.calls[0];
      expect(args[0]).toBe(
        `${
          getConfig().API_DOCUMENT
        }/downloads/all?creditType=LOAN&amount=444&duration=12&scaleCode=scaleCodeTest&businessProviderId=123456789&equipementCode=123`
      );
      expect(args[1].headers).toHaveProperty('Context-Applicationid', 'testAppId');
      expect(args[1].headers).toHaveProperty('Context-Partnerid', 'partner1');
      expect(args[1].headers).toHaveProperty('Context-Sourceid', 'offer1');
    });

    it('should call API without amount, duration, scaleCode, businessProviderId and equipmentCode', async () => {
      const parameters: SimulationParameter = {};
      //@ts-ignore
      axios.get.mockImplementationOnce(() => Promise.resolve());
      await SimulationApi.getPreContractDocuments(parameters, CreditTypeEnum.REVOLVING);
      //@ts-ignore
      const args = axios.get.mock.calls[0];
      expect(args[0]).toBe(`${getConfig().API_DOCUMENT}/downloads/all?creditType=REVOLVING`);
    });
  });
});
