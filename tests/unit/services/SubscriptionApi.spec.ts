import SubscriptionApi from '@/services/SubscriptionApi';
import { MockBusinessDataTransfer } from '../../mock/businessDataTransfer';
import { MockSubscriptionData } from '../../mock/subscriptionData';
import { MockSimulationParameter } from '../../mock/simulationParameter';
import { MockContract } from '../../mock/contract';
import { MockAuthentication } from '../../mock/authenticationStateMock';

describe('SubscriptionApi', () => {
  it('should returns formated contract', () => {
    window.config = {
      data1: 'someData'
    };
    const subscriptionApi = new SubscriptionApi();
    expect(
      subscriptionApi.formatData(
        MockBusinessDataTransfer,
        MockSubscriptionData,
        MockSubscriptionData,
        MockAuthentication,
        MockSimulationParameter
      )
    ).toEqual(MockContract);
  });

  it('should returns the city without the postal code', () => {
    const subscriptionApi = new SubscriptionApi();
    const city = 'Paris (75001)';
    const city2 = 'Paris';
    expect(subscriptionApi.removePostalCode(city)).toBe('Paris');
    expect(subscriptionApi.removePostalCode(city2)).toBe('Paris');
  });

  it('should replace +33 with 0', () => {
    const subscriptionApi = new SubscriptionApi();
    const phone = '+33637334414';
    expect(subscriptionApi.formatNumber(phone)).toBe('0637334414');
    const phone2 = '33637334414';
    expect(subscriptionApi.formatNumber(phone2)).toBe('0637334414');
  });

  it('should return empty for a mobile phone', () => {
    const subscriptionApi = new SubscriptionApi();
    const phone = '06 37 33 44 14';
    expect(subscriptionApi.checkFixNumber(subscriptionApi.formatNumber(phone))).toBe('');
    const phone2 = '01 48 84 28 07';
    expect(subscriptionApi.checkFixNumber(subscriptionApi.formatNumber(phone2))).toBe('0148842807');
  });

  it('should remove all characters exempt numbers, . or ,', () => {
    const subscriptionApi = new SubscriptionApi();
    const amount = '1 300';
    const amount2 = '1   250,0 0';
    const amount3 = 'sdfsdkl';
    const amount4 = '1 25 0.50';
    expect(subscriptionApi.validateAmount(amount)).toBe('1300');
    expect(subscriptionApi.validateAmount(amount2)).toBe('1250');
    expect(subscriptionApi.validateAmount(amount3)).toBe('0');
    expect(subscriptionApi.validateAmount(amount4)).toBe('1250');
    expect(subscriptionApi.validateAmount(null)).toBe('0');
  });
});
