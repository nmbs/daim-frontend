import { addOrUpdateUrlParam } from '@/utils/url';

describe('url', () => {
  describe('addOrUpdateUrlParam', () => {
    it('should returns the original url with the new param', () => {
      // https://test.com/ -> https://test.com/?o=w
      const originUrl1 = 'https://test.com/';
      const resultUrl1 = addOrUpdateUrlParam(originUrl1, 'o', 'w');
      expect(originUrl1).toBe(originUrl1);
      expect(resultUrl1).toBe('https://test.com/?o=w');

      // https://test.com -> https://test.com?o=w
      const originUrl2 = 'https://test.com';
      const resultUrl2 = addOrUpdateUrlParam(originUrl2, 'o', 'w');
      expect(originUrl2).toBe(originUrl2);
      expect(resultUrl2).toBe('https://test.com?o=w');

      // https://test.com -> https://test.com?o=12345
      const originUrl3 = 'https://test.com';
      const resultUrl3 = addOrUpdateUrlParam(originUrl2, 'o', 12345);
      expect(originUrl3).toBe(originUrl3);
      expect(resultUrl3).toBe('https://test.com?o=12345');
    });
    it('should returns the original url with the new param and old params', () => {
      // https://test.com/?q6=web_sofinco&x1=crs -> https://test.com/?q6=web_sofinco&x1=crs&o=w
      const originUrl = 'https://test.com/?q6=web_sofinco&x1=crs';
      const resultUrl = addOrUpdateUrlParam(originUrl, 'o', 'w');
      expect(originUrl).toBe(originUrl);
      expect(resultUrl).toBe('https://test.com/?q6=web_sofinco&x1=crs&o=w');
    });
    it('should returns the original url', () => {
      const originUrl = 'https://test.com/?q6=web_sofinco&x1=crs';
      const resultUrl1 = addOrUpdateUrlParam(originUrl, null, 'w');
      expect(resultUrl1).toBe(originUrl);

      const resultUrl2 = addOrUpdateUrlParam(originUrl, undefined, 'w');
      expect(resultUrl2).toBe(originUrl);

      const resultUrl3 = addOrUpdateUrlParam(originUrl, 'o', null);
      expect(resultUrl3).toBe(originUrl);

      const resultUrl4 = addOrUpdateUrlParam(originUrl, 'o', undefined);
      expect(resultUrl4).toBe(originUrl);
    });
  });
});
