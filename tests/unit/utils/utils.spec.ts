import * as config from '@/config/config';
import { getCurrentQueryStringParams, isNullOrEmpty, formatDate, isNumericalValue } from '@/utils/utils';

describe('Utils', () => {
  describe('query paramaters parsing', () => {
    const { location } = window;
    beforeAll(() => {
      delete window.location;
      // @ts-ignore
      window.location = { reload: jest.fn() };
    });

    afterAll(() => {
      window.location = location;
    });

    it("should process query params when URL doesn't contains fragment and old URL retrocompatibility enabled", () => {
      // @ts-ignore
      jest.spyOn(config, 'default').mockImplementation(() => ({
        OLD_URL_RETROCOMPATIBILITY: true
      }));

      window.location.href = 'https://testurl.fr/?param1=toto&param2=titi';

      expect(getCurrentQueryStringParams()).toEqual({
        PARAM1: 'toto',
        PARAM2: 'titi'
      });
    });

    it('should process query params when URL contains fragment and old URL retrocompatibility enabled', () => {
      // @ts-ignore
      jest.spyOn(config, 'default').mockImplementation(() => ({
        OLD_URL_RETROCOMPATIBILITY: true
      }));

      window.location.href = 'https://testurl.fr/?param1=toto&param2=titi#/testPage';

      expect(getCurrentQueryStringParams()).toEqual({
        PARAM1: 'toto',
        PARAM2: 'titi'
      });
    });

    it('should process query params when old URL retrocompatibility disabled', () => {
      // @ts-ignore
      jest.spyOn(config, 'default').mockImplementation(() => ({
        OLD_URL_RETROCOMPATIBILITY: false
      }));

      window.location.search = '?param1=toto&param2=titi';

      expect(getCurrentQueryStringParams()).toEqual({
        PARAM1: 'toto',
        PARAM2: 'titi'
      });
    });
  });

  it('should return true if value is null or empty', () => {
    expect(isNullOrEmpty(null)).toBeTruthy();
    expect(isNullOrEmpty(undefined)).toBeTruthy();
    expect(isNullOrEmpty('')).toBeTruthy();
    expect(isNullOrEmpty(' ')).toBeTruthy();

    expect(isNullOrEmpty('a ')).toBeFalsy();
    expect(isNullOrEmpty(1)).toBeFalsy();
    expect(isNullOrEmpty(true)).toBeFalsy();
  });

  it('should format date in format DD/MM/YYYY', () => {
    const date = new Date('2021-01-12T14:04:42.956Z');
    expect(formatDate(date)).toBe('12/01/2021');
  });

  describe('isNumericalValue', () => {
    const numericalValuesCases = ['0', '-123', '123', 123];
    test.each(numericalValuesCases)('should return is a numerical value for %o', (value) => {
      expect(isNumericalValue(value)).toBeTruthy();
    });

    const notNumericalValuesCases = [null, undefined, '', '0A', 'ABCD'];
    test.each(notNumericalValuesCases)('should return is not a numerical value for %o', (value) => {
      expect(isNumericalValue(value)).toBeFalsy();
    });
  });
});
