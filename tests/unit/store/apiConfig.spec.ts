import { getters } from '@/store/modules/apiConfig';
import { stateMock } from '../../mock/apiConfigStateMock';

describe('apiConfig', () => {
  describe('getStep getter', () => {
    it('should return the step corresponding to the given stepId', () => {
      const pathId = 'subscription';
      // @ts-ignore Don't add unused parameters to getter
      const result = getters.getStep(stateMock)(pathId);
      expect(result.pathId).toBe(pathId);
    });
  });

  describe('coBorrowerDisplayed getter', () => {
    it('shall return false with a configuration which contains coBorrowerDisplayed: value: false', () => {
      // @ts-ignore Don't add unused parameters to getter
      const result = getters.coBorrowerDisplayed(stateMock);
      expect(result).toBe(false);
    });
  });

  describe('isSofincoPartnerCode getter', () => {
    it('should return that the partner code is not Sofinco', () => {
      // @ts-ignore Don't add unused parameters to getter
      const result = getters.isSofincoPartnerCode(stateMock);
      expect(result).toBe(false);
    });
    it('should return that the partner code is Sofinco', () => {
      stateMock.partnerCode = 'web_sofinco';
      // @ts-ignore Don't add unused parameters to getter
      const result = getters.isSofincoPartnerCode(stateMock);
      expect(result).toBe(true);
    });
    it('should return that the parnter code is not Sofinco', () => {
      stateMock.partnerCode = null;
      // @ts-ignore Don't add unused parameters to getter
      const result = getters.isSofincoPartnerCode(stateMock);
      expect(result).toBe(false);
    });
  });
});
