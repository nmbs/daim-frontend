import { isNameFormValid } from '@/store/validators/informationValidators';
import { MaritalStatusCode, Gender } from '@/models/subscription/Name';

describe('Information validators', () => {
  describe('Name form validator', () => {
    it('should return true if man and all field completed', () => {
      expect(
        isNameFormValid(
          {
            gender: Gender.MAN,
            firstName: 'firstname',
            lastName: 'lastName',
            maritalStatus: {
              code: MaritalStatusCode.SINGLE
            },
            hasSpouse: false
          } as any,
          {} as any,
          true
        )
      ).toBe(true);
    });

    it('should return true if women and all field but birtname completed but is not widow or married', () => {
      expect(
        isNameFormValid(
          {
            gender: Gender.WOMAN,
            firstName: 'firstname',
            lastName: 'lastName',
            maritalStatus: {
              code: MaritalStatusCode.SINGLE
            },
            hasSpouse: false
          } as any,
          {} as any,
          true
        )
      ).toBe(true);
    });

    it('should return false if gender or firstname or lastname or hasSpouse is null or empty', () => {
      expect(
        isNameFormValid(
          {
            gender: null,
            firstName: 'firstname',
            lastName: 'lastName',
            maritalStatus: {
              code: MaritalStatusCode.SINGLE
            },
            hasSpouse: false
          } as any,
          {} as any,
          true
        )
      ).toBe(false);
      expect(
        isNameFormValid(
          {
            gender: Gender.MAN,
            firstName: '',
            lastName: 'lastName',
            maritalStatus: {
              code: MaritalStatusCode.SINGLE
            },
            hasSpouse: false
          } as any,
          {} as any,
          true
        )
      ).toBe(false);
      expect(
        isNameFormValid(
          {
            gender: Gender.MAN,
            firstName: 'firstname',
            lastName: undefined,
            maritalStatus: {
              code: MaritalStatusCode.SINGLE
            },
            hasSpouse: false
          } as any,
          {} as any,
          true
        )
      ).toBe(false);
      expect(
        isNameFormValid(
          {
            gender: Gender.MAN,
            firstName: 'firstname',
            lastName: 'lastname',
            maritalStatus: null,
            hasSpouse: false
          } as any,
          {} as any,
          true
        )
      ).toBe(false);
      expect(
        isNameFormValid(
          {
            gender: Gender.MAN,
            firstName: 'firstname',
            lastName: 'lastname',
            maritalStatus: {
              code: MaritalStatusCode.SINGLE
            },
            hasSpouse: null
          } as any,
          {} as any,
          true
        )
      ).toBe(false);
    });

    it('should return false if user is woman, widow or married and has not entered birtname', () => {
      expect(
        isNameFormValid(
          {
            gender: Gender.WOMAN,
            firstName: 'firstname',
            lastName: 'lastname',
            maritalStatus: {
              code: MaritalStatusCode.MARRIED
            },
            hasSpouse: true
          } as any,
          {} as any,
          true
        )
      ).toBe(false);
      expect(
        isNameFormValid(
          {
            gender: Gender.WOMAN,
            firstName: 'firstname',
            lastName: 'lastname',
            maritalStatus: {
              code: MaritalStatusCode.WIDOW
            },
            hasSpouse: true
          } as any,
          {} as any,
          true
        )
      ).toBe(false);
    });
  });
});
