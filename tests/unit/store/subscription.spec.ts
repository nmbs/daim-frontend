import '@/store';
import { gettersSubcription, mutationsSubscription } from '@/store/modules/subscription';
import { initStateMock } from '../../mock/subscriptionStateMock';
import { EligibilityPayload } from '@/models/subscription/SubscriptionPayload';

const { isInsuranceEligibleForT2AndNotT1, isInsuranceEligibleForT1AndNotT2 } = gettersSubcription;
const { setInsuranceEligibility } = mutationsSubscription;

let stateMock = initStateMock();

describe('subscription', () => {
  describe('getters', () => {
    beforeEach(() => {
      stateMock = initStateMock();
    });
    describe('isInsuranceEligibleForT2AndNotT1', () => {
      it('should return that T1 is not eligible and T2 is eligible to insurance', () => {
        // Set T1 and T2 eligibility
        stateMock.formStates.insurance.t1IsEligible = false;
        stateMock.formStates.insurance.t2IsEligible = true;
        // @ts-ignore Don't add unused parameters to getter
        const result = isInsuranceEligibleForT2AndNotT1(stateMock);
        expect(result).toBe(true);
      });
      it('should return that T1 and T2 are eligible to insurance', () => {
        // Set T1 and T2 eligibility
        stateMock.formStates.insurance.t1IsEligible = true;
        stateMock.formStates.insurance.t2IsEligible = true;
        // @ts-ignore Don't add unused parameters to getter
        const result = isInsuranceEligibleForT2AndNotT1(stateMock);
        expect(result).toBe(false);
      });
    });
    describe('isInsuranceEligibleForT1AndNotT2', () => {
      it('should return that T2 is not eligible and T1 is eligible to insurance', () => {
        // Set T1 and T2 eligibility
        stateMock.formStates.insurance.t1IsEligible = true;
        stateMock.formStates.insurance.t2IsEligible = false;
        // @ts-ignore Don't add unused parameters to getter
        const result = isInsuranceEligibleForT1AndNotT2(stateMock);
        expect(result).toBe(true);
      });
      it('should return that T1 and T2 are eligible to insurance', () => {
        // Set T1 and T2 eligibility
        stateMock.formStates.insurance.t1IsEligible = true;
        stateMock.formStates.insurance.t2IsEligible = true;
        // @ts-ignore Don't add unused parameters to getter
        const result = isInsuranceEligibleForT1AndNotT2(stateMock);
        expect(result).toBe(false);
      });
    });
  });

  describe('mutations', () => {
    describe('setInsuranceEligibility', () => {
      beforeEach(() => {
        stateMock = initStateMock();
      });
      it('should mutate t1IsEligible and t2IsEligible values to false', () => {
        const payload = {
          t1IsEligible: false,
          t2IsEligible: false
        };
        setInsuranceEligibility(stateMock, payload);
        expect(stateMock.formStates.insurance.t1IsEligible).toBe(false);
        expect(stateMock.formStates.insurance.t2IsEligible).toBe(false);
      });
      it('should keep the same values for t1IsEligible and t2IsEligible', () => {
        const payload: EligibilityPayload = {
          t1IsEligible: null,
          t2IsEligible: null
        };
        setInsuranceEligibility(stateMock, payload);
        expect(stateMock.formStates.insurance.t1IsEligible).toBe(true);
        expect(stateMock.formStates.insurance.t2IsEligible).toBe(true);
      });
    });
  });
});
