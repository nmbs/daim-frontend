import { getRoutesConfig, initStepsStates } from '../../mock/ConfigurationService';
import { MockOfferConfig } from '../../mock/OfferConfigSeche';
import { getters } from '@/store/modules/routing';

describe('routing', () => {
  const state: any = {};
  state.configurationRoutes = getRoutesConfig(MockOfferConfig);
  state.rootStepsStates = initStepsStates(MockOfferConfig);

  describe('getSubscriptionSteps', () => {
    it('should return all the subscription steps from the config', () => {
      // @ts-ignore
      expect(getters.getSubscriptionSteps(state).length).toBe(8);
    });
  });

  describe('getFirstSubscriptionStep', () => {
    it('should return the first subscription step', () => {
      // @ts-ignore
      expect(getters.getFirstSubscriptionStep(state).pathId).toBe('name');
    });
  });

  describe('initialStep', () => {
    it('should return the inital step', () => {
      // @ts-ignore
      expect(getters.initialStep(state).pathId).toBe('creditFromScratch');
    });
  });

  describe('getNumberSubscriptionSteps', () => {
    it('should return the total number of steps', () => {
      // @ts-ignore
      expect(getters.getNumberSubscriptionSteps(state)).toBe(13);
    });
  });

  describe('getCurrentStepIndex', () => {
    it('should return the index of the given step pathId', () => {
      // @ts-ignore
      expect(getters.getCurrentStepIndex(state)('name')).toBe(1);
    });
  });

  describe('getNextStep', () => {
    it('should return the next step', () => {
      // @ts-ignore
      expect(getters.getNextStep(state)('name').pathId).toBe('contact');
    });
  });

  describe('getPreviousStep', () => {
    it('should return the previous', () => {
      // @ts-ignore
      expect(getters.getPreviousStep(state)('contact', false).pathId).toBe('name');
    });
  });

  describe('getPreviousSteps', () => {
    it('should the previous steps as an array', () => {
      // @ts-ignore
      expect(getters.getPreviousSteps(state, getters)('contact').length).toBe(1);
    });
  });

  describe('getCurrentStepStates', () => {
    it('should the current step state', () => {
      // @ts-ignore
      expect(getters.getCurrentStepStates(state)('contact').pathId).toBe('contact');
    });
  });
});
