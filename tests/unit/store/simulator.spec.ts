import { simulator } from '@/store/modules/simulator';

describe('Simulator state', () => {
  describe('getters', () => {
    describe('emptyProposals', () => {
      it('should return true if proposals is a empty list', () => {
        const stateMock = {
          proposals: []
        } as any;
        // @ts-ignore
        expect(simulator.getters.emptyProposals(stateMock)).toBeTruthy();
      });

      it('should return false if proposals is undefined or not empty', () => {
        const stateMock = {
          proposals: null
        } as any;
        // @ts-ignore
        expect(simulator.getters.emptyProposals(stateMock)).toBeFalsy();
        const stateMock2 = {
          proposals: [{}]
        } as any;
        // @ts-ignore
        expect(simulator.getters.emptyProposals(stateMock2)).toBeFalsy();
      });
    });

    describe('hasOfferPreSelected', () => {
      it('should return true if suggestedDueNumber is defined', () => {
        const state: any = {
          offerPreSelection: {
            suggestedDueNumber: 12
          }
        };
        // @ts-ignore
        expect(simulator.getters.hasOfferPreSelected(state)).toBeTruthy();
        state.offerPreSelection.suggestedDueNumber = 0;
        // @ts-ignore
        expect(simulator.getters.hasOfferPreSelected(state)).toBeTruthy();
      });

      it('should return false if suggestedDueNumber is not defined', () => {
        const state: any = {
          offerPreSelection: {
            suggestedDueNumber: null
          }
        };
        // @ts-ignore
        expect(simulator.getters.hasOfferPreSelected(state)).toBeFalsy();
      });
    });

    describe('hasEnoughProposalsForFiltering', () => {
      it('should return true if there is at least 6 proposals ', () => {
        const stateMock = {
          proposals: [{}, {}, {}, {}, {}, {}]
        } as any;
        // @ts-ignore
        expect(simulator.getters.hasEnoughProposalsForFiltering(stateMock)).toBeTruthy();
      });

      it('should return false if there is not at least 6 proposals ', () => {
        const stateMock = {
          proposals: [{}, {}, {}, {}, {}]
        } as any;
        // @ts-ignore
        expect(simulator.getters.hasEnoughProposalsForFiltering(stateMock)).toBeFalsy();
      });

      it('should return false if proposals is undefined or empty', () => {
        const stateMock = {
          proposals: null
        } as any;
        // @ts-ignore
        expect(simulator.getters.hasEnoughProposalsForFiltering(stateMock)).toBeFalsy();
        const stateMock2 = {
          proposals: []
        } as any;
        // @ts-ignore
        expect(simulator.getters.hasEnoughProposalsForFiltering(stateMock2)).toBeFalsy();
      });
    });
  });

  describe('Actions', () => {
    describe('setSelectedProposal', () => {
      it('should update selected proposal and scaleCode', () => {
        const context: any = {
          commit: jest.fn()
        };
        const fakeProposal: any = {
          scaleCode: 'ABC'
        };
        // @ts-ignore
        simulator.actions.setSelectedProposal(context, fakeProposal);
        expect(context.commit).toHaveBeenCalledWith('updateSelectedProposal', fakeProposal);
        expect(context.commit).toHaveBeenCalledWith('saveScaleCode', fakeProposal.scaleCode);
      });

      it('should update selected proposal but not scaleCode', () => {
        const context: any = {
          commit: jest.fn()
        };
        const fakeProposal: any = {
          otherProp: 'toto'
        };
        // @ts-ignore
        simulator.actions.setSelectedProposal(context, fakeProposal);
        expect(context.commit).toHaveBeenCalledWith('updateSelectedProposal', fakeProposal);
        expect(context.commit).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('mutation', () => {
    describe('updatePreSelectionSuggestedDueNumber', () => {
      let state: any;
      beforeEach(() => {
        state = {
          offerPreSelection: {
            suggestedDueNumber: null
          }
        };
      });

      it('should update suggestedDueNumber', () => {
        simulator.mutations.updatePreSelectionSuggestedDueNumber(state, '12');
        expect(state.offerPreSelection.suggestedDueNumber).toBe(12);
      });

      it('should not update suggestedDueNumber', () => {
        simulator.mutations.updatePreSelectionSuggestedDueNumber(state, '');
        expect(state.offerPreSelection.suggestedDueNumber).toBeNull();
        simulator.mutations.updatePreSelectionSuggestedDueNumber(state, 's');
        expect(state.offerPreSelection.suggestedDueNumber).toBeNull();
      });
    });
  });
});
