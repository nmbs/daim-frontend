import {
  isChargesFormValid,
  isCivilityFormValid,
  isContactFormValid,
  isEmploymentFormValid,
  isFamilyFormValid,
  isHousingFormValid,
  isIncomesFormValid,
  isInsuranceFormValid,
  isNameFormValid
} from '@/store/validators/informationValidators';
import {
  charges1,
  charges2,
  civility,
  contact1,
  contact2,
  employment1,
  employment2,
  employment3,
  employment4,
  employment5,
  housing1,
  housing2,
  incomes1,
  incomes2,
  incomes3,
  name1,
  name2,
  name3
} from '../../mock/formsData';

describe('information validators', () => {
  describe('isNameFormValid', () => {
    it('should return false for empty state', () => {
      expect(isNameFormValid(name2, name2, true)).toBe(false);
    });

    it('should return true for filled state', () => {
      expect(isNameFormValid(name1, name1, true)).toBe(true);
    });

    it('should return false for a married woman without birthName', () => {
      expect(isNameFormValid(name1, name3, false)).toBe(false);
    });
  });

  describe('isFamilyFormValid', () => {
    it('should be false for empty childrenNbr', () => {
      expect(isFamilyFormValid({ childrenNbr: null })).toBe(false);
    });

    it('should be true for non empty childrenNbr', () => {
      expect(isFamilyFormValid({ childrenNbr: 4 })).toBe(true);
    });
  });

  describe('isContactFormValid', () => {
    it('should be false if state is empty', () => {
      expect(isContactFormValid(contact1, true)).toBe(false);
    });

    it('should be true if either mobilePhone or homePhone is filled', () => {
      expect(isContactFormValid(contact2, true)).toBe(true);
    });
  });

  describe('isCivilityFormValid', () => {
    it('should be false if one field is empty', () => {
      const input = { ...civility, nationality: null };
      expect(isCivilityFormValid(input)).toBe(false);
    });

    it('should be true if all fields are filled', () => {
      expect(isCivilityFormValid(civility)).toBe(true);
    });
  });

  describe('isIncomesFormValid', () => {
    it('should be true if netIncomes and salaryMonth are filled and no more incomes', () => {
      expect(isIncomesFormValid(incomes1)).toBe(true);
    });

    it('should be true if netIncomes and salaryMonth are filled and has more incomes', () => {
      expect(isIncomesFormValid(incomes2)).toBe(false);
    });

    it('should be true if everything is filled', () => {
      expect(isIncomesFormValid(incomes3)).toBe(true);
    });
  });

  describe('isChargesFormValid', () => {
    it('should return true for secondary', () => {
      expect(isChargesFormValid(charges1, housing2, false)).toBe(true);
    });

    it('should return false for primary', () => {
      expect(isChargesFormValid(charges1, housing2, true)).toBe(false);
    });

    it('should return true for primary when all fields are filled', () => {
      expect(isChargesFormValid(charges2, housing2, true)).toBe(true);
    });
  });

  describe('isEmploymentFormValid', () => {
    it('should return false if professionalStatus is empty', () => {
      expect(isEmploymentFormValid(employment1)).toBe(false);
    });

    it('should return true if all fields are filled', () => {
      expect(isEmploymentFormValid(employment2)).toBe(true);
    });

    it('should return true if all fields are filled case 4_4', () => {
      expect(isEmploymentFormValid(employment3)).toBe(true);
    });

    it('should return true if all fields are filled case 6_6', () => {
      expect(isEmploymentFormValid(employment5)).toBe(true);
    });

    it('should return true if all fields are filled case7_7', () => {
      expect(isEmploymentFormValid(employment4)).toBe(true);
    });
  });

  describe('isHousingFormValid', () => {
    it('should always be true for secondary', () => {
      expect(isHousingFormValid(housing1, false)).toBe(true);
    });

    it('should always be true for primary if all field are filled', () => {
      expect(isHousingFormValid(housing2, true)).toBe(true);
    });
  });

  describe('isInsuranceFormValid', () => {
    it('should return false no insurance with no confirmation', () => {
      const insurance = { acceptInsuranceOffer: false, confirmRefusal: false };
      expect(isInsuranceFormValid(insurance, true, true, true)).toBe(false);
    });
    it('should return true because T1 is not eligible', () => {
      const insurance = { acceptInsuranceOffer: false, confirmRefusal: false };
      expect(isInsuranceFormValid(insurance, true, false, true)).toBe(true);
    });
    it('should return true because T2 is not eligible', () => {
      const insurance = { acceptInsuranceOffer: false, confirmRefusal: false };
      expect(isInsuranceFormValid(insurance, false, true, false)).toBe(true);
    });
    it('should return true because the client accepted insurance offer', () => {
      const insurance = { acceptInsuranceOffer: true, confirmRefusal: false };
      expect(isInsuranceFormValid(insurance, true, true, true)).toBe(true);
    });
    it('should return true because the client confirmed refusal', () => {
      const insurance = { acceptInsuranceOffer: false, confirmRefusal: true };
      expect(isInsuranceFormValid(insurance, true, true, true)).toBe(true);
    });
  });
});
