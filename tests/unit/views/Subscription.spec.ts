import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import Subscription from '@/views/Subscription.vue';
import BackToTop from 'vue-backtotop';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { hexToRgb } from '@/mixins/HexToRgb';
import { addDynamicStyles } from '@/mixins/addDynamicStyles';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);
localVue.use(Vuex);
localVue.use(VueRouter);
localVue.mixin(hexToRgb);
localVue.mixin(addDynamicStyles);

const router = new VueRouter();

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

describe('Subscription.vue', () => {
  let store: any;
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        apiConfig: { colors: {}, offerCode: {}, partnerCode: {} },
        client: { isMobile: {} },
        subscription: {}
      },
      getters: {
        selectedProposal: () => {},
        getNextStep: (pathId) => {},
        getPreviousStep: (pathId) => {}
      }
    });
  });
  it('should pass', () => {
    const wrapper = shallowMount(Subscription, {
      i18n,
      localVue,
      store,
      router
    });
    expect(wrapper).toBeTruthy();
  });
});
