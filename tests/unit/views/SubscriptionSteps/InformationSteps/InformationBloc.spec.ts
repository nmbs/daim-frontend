import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import InformationBloc from '@/views/SubscriptionSteps/InformationSteps/InformationBloc.vue';
import BackToTop from 'vue-backtotop';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { hexToRgb } from '@/mixins/HexToRgb';
import { convertDate } from '@/mixins/dates';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);
localVue.use(Vuex);
localVue.use(VueRouter);
localVue.mixin(hexToRgb);
localVue.mixin(convertDate);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

const router = new VueRouter();

describe('InformationBloc.vue', () => {
  let store: any;
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        apiConfig: {
          colors: {},
          offerCode: {},
          partnerCode: {},
          appConfiguration: { screenOptions: {} },
          applicationId: {}
        },
        subscription: {
          principal: {
            family: {}
          },
          preContractMail: {}
        }
      },
      actions: {
        setSubscriptionData: (payload) => {}
      },
      getters: {
        getFamilyStateChildren: (childrenNbr) => {}
      }
    });
  });
  it('should pass', () => {
    const wrapper = shallowMount(InformationBloc, {
      i18n,
      localVue,
      store,
      router
    });
    expect(wrapper).toBeTruthy();
  });
});
