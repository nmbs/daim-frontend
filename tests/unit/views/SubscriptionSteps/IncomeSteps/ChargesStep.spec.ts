import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import ChargesStep from '@/views/SubscriptionSteps/IncomeSteps/ChargesStep.vue';
import BackToTop from 'vue-backtotop';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { hexToRgb } from '@/mixins/HexToRgb';
import { convertDate } from '@/mixins/dates';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);
localVue.use(Vuex);
localVue.use(VueRouter);
localVue.mixin(hexToRgb);
localVue.mixin(convertDate);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

const router = new VueRouter();

describe('ChargesStep.vue', () => {
  let store: any;
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        apiConfig: { colors: {}, offerCode: {}, partnerCode: {}, appConfiguration: { screenOptions: {} } },
        subscription: {
          principal: {
            civility: { birthDate: null },
            charges: {},
            adresse: {},
            cardCode: {}
          }
        },
        client: { isMobile: {} },
        simulationResult: {}
      },
      actions: {
        setSubscriptionData: (payload) => {}
      },
      getters: {
        getStep: (state) => {
          return (pathId: string) => null;
        }
      }
    });
  });
  it('should pass', () => {
    const wrapper = shallowMount(ChargesStep, {
      i18n,
      localVue,
      store,
      router
    });
    expect(wrapper).toBeTruthy();
  });
});
