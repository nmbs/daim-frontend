import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import InsuranceStep from '@/views/SubscriptionSteps/InsuranceSteps/InsuranceV1Step.vue';
import BackToTop from 'vue-backtotop';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { hexToRgb } from '@/mixins/HexToRgb';
import { convertDate } from '@/mixins/dates';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);
localVue.use(Vuex);
localVue.use(VueRouter);
localVue.mixin(hexToRgb);
localVue.mixin(convertDate);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

const router = new VueRouter();

describe('InsuranceStep.vue', () => {
  let store: any;
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        apiConfig: { colors: {}, offerCode: {}, partnerCode: {}, appConfiguration: { screenOptions: {} } },
        subscription: {
          principal: {
            civility: { birthDate: null },
            insurance: {}
          },
          secondary: {
            civility: { birthDate: null },
            insurance: {}
          }
        },
        simulator: {
          selectedProposal: {},
          simulationContext: {
            productId: null
          }
        },
        client: { isMobile: {} },
        simulationResult: {}
      },
      getters: {
        hasBorrower: () => {},
        isCreditForTwo: () => {},
        isVersionSimulationApiIn: (state) => (version: number) => {},
        getCreditType: () => {},
        getT1IsActive: (state) => (pathId: string) => {},
        screenOption: (key) => (key) => null
      },
      mutations: {
        updateVisibleRoute: (payload) => {}
      }
    });
  });
  it('should pass', () => {
    const wrapper = shallowMount(InsuranceStep, {
      i18n,
      localVue,
      store,
      router,
      propsData: {
        stepState: {
          pathId: null
        }
      }
    });
    expect(wrapper).toBeTruthy();
  });
});
