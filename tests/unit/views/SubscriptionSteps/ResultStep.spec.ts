import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import ResultStep from '@/views/SubscriptionSteps/ResultStep.vue';
import BackToTop from 'vue-backtotop';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { hexToRgb } from '@/mixins/HexToRgb';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);
localVue.use(Vuex);
localVue.use(VueRouter);
localVue.mixin(hexToRgb);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

const router = new VueRouter();

describe('ResultStep.vue', () => {
  let store: any;
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        apiConfig: { colors: {} },
        subscription: {
          response: {}
        },
        client: { isMobile: {} }
      },

      getters: {
        isResultOK: (state) => true
      }
    });
  });
  it('should pass', () => {
    const wrapper = shallowMount(ResultStep, {
      i18n,
      localVue,
      store,
      router
    });
    expect(wrapper).toBeTruthy();
  });
});
