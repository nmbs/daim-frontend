import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import SituationBloc from '@/views/SubscriptionSteps/SituationSteps/SituationBloc.vue';
import VueRouter from 'vue-router';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(VueRouter);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

const router = new VueRouter();

describe('SituationBloc.vue', () => {
  it('should pass', () => {
    const wrapper = shallowMount(SituationBloc, {
      i18n,
      localVue,
      router
    });
    expect(wrapper).toBeTruthy();
  });
});
