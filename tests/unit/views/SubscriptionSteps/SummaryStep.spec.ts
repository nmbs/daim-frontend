import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import SummaryStep from '@/views/SubscriptionSteps/SummaryStep.vue';
import BackToTop from 'vue-backtotop';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import { hexToRgb } from '@/mixins/HexToRgb';
import Vue from 'vue';
import tag from '@/directives/tag';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);
localVue.use(Vuex);
localVue.use(VueRouter);
localVue.mixin(hexToRgb);
Vue.directive('tag', tag);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

const router = new VueRouter();

describe('SummaryStep.vue', () => {
  let store: any;
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        apiConfig: { colors: {} },
        subscription: {
          principal: {
            name: {},
            contact: {},
            civility: {},
            family: {},
            adresse: {},
            employment: {},
            incomes: {},
            charges: {}
          },
          secondary: {
            name: {},
            contact: {},
            civility: {},
            family: {},
            adresse: {},
            employment: {},
            incomes: {},
            charges: {}
          }
        },
        client: { isMobile: {} },
        simulationResult: {}
      },
      getters: {
        hasBorrower: () => {},
        isCreditForTwo: () => {}
      }
    });
  });
  it('should pass', () => {
    const wrapper = shallowMount(SummaryStep, {
      i18n,
      localVue,
      store,
      router
    });
    expect(wrapper).toBeTruthy();
  });
});
