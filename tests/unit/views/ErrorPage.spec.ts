import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import ErrorPage from '@/views/ErrorPage.vue';
import BackToTop from 'vue-backtotop';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

describe('ErrorPage.vue', () => {
  it('should pass', () => {
    const wrapper = shallowMount(ErrorPage, {
      i18n,
      localVue
    });
    expect(wrapper).toBeTruthy();
  });
});
