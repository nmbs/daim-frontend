import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import CreditFromScratch from '@/views/CreditFromScratch.vue';
import BackToTop from 'vue-backtotop';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);
localVue.use(Vuex);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

describe('CreditFromScratch.vue', () => {
  let store: any;
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        apiConfig: { colors: {}, offerCode: {}, partnerCode: {} },
        client: { isMobile: {} }
      },
      getters: {
        getNextStep: (pathId) => {}
      }
    });
  });
  it('should throw error if no pedagogic step is de', () => {
    // const wrapper = shallowMount(CreditFromScratch, {
    //   i18n,
    //   localVue,
    //   store
    // });
    // expect(wrapper).toBeTruthy();
  });
});
