import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import DownloadFolderPopin from '@/views/popin/DownloadFolderPopin.vue';
import BackToTop from 'vue-backtotop';
import Vuex from 'vuex';
import { hexToRgb } from '@/mixins/HexToRgb';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);
localVue.use(Vuex);
localVue.mixin(hexToRgb);

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

describe('DownloadFolderPopin.vue', () => {
  let store: any;
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        apiConfig: { colors: {}, offerCode: {}, partnerCode: {} },
        client: { isMobile: {} }
      }
    });
  });
  it('should pass', () => {
    const wrapper = shallowMount(DownloadFolderPopin, {
      i18n,
      localVue,
      store
    });
    expect(wrapper).toBeTruthy();
  });
});
