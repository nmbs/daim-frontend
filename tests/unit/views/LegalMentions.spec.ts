import { createLocalVue, mount } from '@vue/test-utils';
import VueI18n from 'vue-i18n';

import messages from '@/assets/i18n/messages.json';
import LegalMentions from '@/views/LegalMentions.vue';
import BackToTop from 'vue-backtotop';
import Vuex from 'vuex';
import VueRouter from 'vue-router';

const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(BackToTop);
localVue.use(Vuex);
localVue.use(VueRouter);

const router = new VueRouter();

const i18n = new VueI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
});

describe('LegalMentions.vue', () => {
  let store: any;
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        apiConfig: { colors: {}, offerCode: {}, partnerCode: {} },
        client: { isMobile: {} }
      },
      getters: {
        getNextStep: (pathId) => {}
      }
    });
  });
  it('should pass', () => {
    // Need to really mount component to enable this.$refs.mobilePopin to be accessible
    const wrapper = mount(LegalMentions, {
      i18n,
      router,
      localVue,
      store
    });
    expect(wrapper).toBeTruthy();
  });
});
