 # Base node image
FROM node:11-alpine

# Metadata
ARG uid=65534
ARG gid=65534
ARG user=nobody
ARG group=nogroup

ARG http_port=3000
ARG logs_dir=logs
ARG project_name=daim-frontend

ENV NODE_PORT=${http_port}

LABEL image.sofinco-daim-daim-frontend.maintainer=developer@cacd2.fr

# Create folder
RUN mkdir -p /usr/src/${project_name}

# Copy application
WORKDIR /usr/src/${project_name}

# Copy server part
COPY server/ .

# Run the installation
RUN npm install --only=prod

# Copy frontend part
COPY dist/ ./dist/

# Add entrypoint to rewrite configuration at launch time
RUN mkdir -p /run/secrets
RUN chown -R ${user}:${group} /run/secrets

# Update rights 
RUN chown -R ${user}:${group} /usr/src/${project_name}

RUN mkdir /usr/src/${project_name}/${logs_dir}
RUN chmod -R 777 /usr/src/${project_name}/${logs_dir}

# Expose API port, can not export 80 as running as non-root
EXPOSE ${HTTP_PORT}

# Run as user
USER ${user}

# Run node as default command
CMD ["node", "app.js"]
